#if defined _map_int_shop_almacen_inc
	#endinput
#endif
#define _map_int_shop_almacen_inc

#include <YSI_Coding\y_hooks>

hook LoadMaps()
{
	Textura = CreateDynamicObject(18045, 2837.558105, -2874.407714, 1103.445922, 0.000000, 0.000000, 0.000000, -1, -1, -1, 100.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	SetDynamicObjectMaterial(Textura, 1, -1, "none", "none", 0xFFFF5555);
	SetDynamicObjectMaterial(Textura, 5, 14526, "sweetsmain", "GB_Pan01", 0x00000000);
	SetDynamicObjectMaterial(Textura, 6, 5168, "lashops6_las2", "sw_wind12", 0x00000000);
	Textura = CreateDynamicObject(1884, 2839.981201, -2872.752685, 1101.443725, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	Textura = CreateDynamicObject(1884, 2839.981201, -2875.753906, 1101.443725, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	Textura = CreateDynamicObject(1884, 2836.387939, -2872.752685, 1101.443725, 0.000009, 0.000000, 89.999977, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	Textura = CreateDynamicObject(18092, 2843.442871, -2871.498291, 1101.953979, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 1, -1, "none", "none", 0xFFFF9999);
	Textura = CreateDynamicObject(1988, 2840.101074, -2869.616455, 1101.450683, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	Textura = CreateDynamicObject(1983, 2839.102050, -2869.616455, 1101.450683, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	Textura = CreateDynamicObject(1983, 2838.102050, -2869.616455, 1101.450683, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	Textura = CreateDynamicObject(1983, 2837.102050, -2869.616455, 1101.450683, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	Textura = CreateDynamicObject(1987, 2836.102050, -2869.616455, 1101.450683, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	Textura = CreateDynamicObject(18070, 2831.157714, -2871.217285, 1101.950439, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFF6666);
	Textura = CreateDynamicObject(2453, 2831.387451, -2874.006103, 1102.836791, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 1, -1, "none", "none", 0xFFFFFFFF);
	Textura = CreateDynamicObject(19786, 2830.923583, -2878.851806, 1104.446533, 6.000000, 0.000000, 135.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 1, 1340, "foodkarts", "chillidog_sign", 0x00000000);
	Textura = CreateDynamicObject(18070, 2830.779785, -2871.097412, 1105.032592, 0.000000, 180.000000, 90.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFF6666);
	Textura = CreateDynamicObject(2642, 2830.222900, -2874.071044, 1103.369384, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	SetDynamicObjectMaterial(Textura, 2, 10431, "hashblock2_sfs", "ws_w's_shopfront_top", 0x00000000);
	Textura = CreateDynamicObject(2665, 2831.693359, -2874.823242, 1104.742065, 10.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, 10431, "hashblock2_sfs", "ws_w's_shopfront_top", 0xFFFFFFFF);
	Textura = CreateDynamicObject(2430, 2833.121582, -2872.778564, 1104.407348, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	SetDynamicObjectMaterial(Textura, 2, 10431, "hashblock2_sfs", "ws_w's_shopfront_top", 0x00000000);
	SetDynamicObjectMaterial(Textura, 4, 10431, "hashblock2_sfs", "ws_w's_shopfront_top", 0x00000000);
	Textura = CreateDynamicObject(19787, 2833.401123, -2874.667480, 1104.961425, 6.000000, 0.000000, 68.279960, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 1, 2772, "airp_prop", "cj_TILL2", 0x00000000);
	Textura = CreateDynamicObject(2420, 2841.313232, -2879.446533, 1101.446655, 0.000000, 0.000000, 180.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 1, 14652, "ab_trukstpa", "CJ_WOOD6", 0x00000000);
	Textura = CreateDynamicObject(2452, 2830.537109, -2872.735595, 1101.351440, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	SetDynamicObjectMaterial(Textura, 2, 10431, "hashblock2_sfs", "ws_w's_shopfront_top", 0x00000000);
	Textura = CreateDynamicObject(3498, 2832.521972, -2873.904052, 1101.448974, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 1, 18800, "mroadhelix1", "road1-3", 0xFFFFFFFF);
	Textura = CreateDynamicObject(14860, 2833.165039, -2878.532226, 1102.677856, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 2, -1, "none", "none", 0x0FFFFFFF);
	Textura = CreateDynamicObject(2464, 2843.352539, -2873.250488, 1102.595947, 0.000000, 0.000000, 122.940032, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, 2215, "chick_tray", "friesbox_cb", 0x00000000);
	SetDynamicObjectMaterial(Textura, 1, 1455, "cj_bar", "CJ_old_pish", 0x00000000);
	SetDynamicObjectMaterial(Textura, 2, 10052, "lomall", "zombiegeddon", 0x00000000);
	SetDynamicObjectMaterial(Textura, 3, 2541, "cj_ss_1", "CJ_CEREAL", 0x00000000);
	SetDynamicObjectMaterial(Textura, 5, 1983, "new_cabinets2", "cereal2", 0x00000000);
	Textura = CreateDynamicObject(2361, 2841.355712, -2869.660644, 1101.436889, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, 1560, "7_11_door", "CJ_CHROME2", 0x00000000);
	SetDynamicObjectMaterial(Textura, 1, 19325, "lsmall_shops", "lsmall_window01", 0x00000000);
	Textura = CreateDynamicObject(2452, 2844.514648, -2876.854492, 1101.418090, 0.000000, 0.000000, -90.000000, -1, -1, -1, 50.0);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	SetDynamicObjectMaterial(Textura, 2, 10431, "hashblock2_sfs", "ws_w's_shopfront_top", 0x00000000);
	CreateDynamicObject(2144, 2830.446044, -2871.136718, 1101.438842, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2451, 2830.816406, -2870.529785, 1101.448974, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(1532, 2844.072509, -2878.930419, 1101.448120, 0.000000, 0.000000, 180.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2125, 2830.934326, -2878.536621, 1101.762329, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2125, 2832.206298, -2878.536621, 1101.762329, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2125, 2833.548583, -2878.536621, 1101.762329, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2125, 2831.067138, -2875.328125, 1101.762329, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(1432, 2836.041503, -2877.724853, 1101.552001, 0.000000, 0.000000, -60.659999, -1, -1, -1, 50.0);
	CreateDynamicObject(2641, 2832.961425, -2869.154541, 1103.613037, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2645, 2834.451171, -2869.148193, 1103.429931, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(3009, 2832.892578, -2874.919433, 1104.633178, -55.380031, -4.699969, -64.920051, -1, -1, -1, 50.0);
	CreateDynamicObject(19326, 2841.841796, -2869.130371, 1103.370239, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(19327, 2838.642089, -2869.126464, 1103.950317, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(19328, 2844.958984, -2875.415527, 1103.289672, 0.000000, 0.000000, -90.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2507, 2844.730224, -2871.957275, 1103.676025, 0.000000, 0.000000, -90.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2507, 2844.793701, -2871.330810, 1103.676025, 0.000000, 0.000000, -90.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2507, 2844.849609, -2872.558105, 1103.676025, 0.000000, 0.000000, -90.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(1514, 2843.161865, -2871.501220, 1102.693847, 0.000000, 0.000000, -90.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2426, 2832.948486, -2869.326416, 1102.452392, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2059, 2835.971191, -2877.640869, 1102.176147, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2422, 2832.695312, -2872.339355, 1102.451538, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2507, 2844.849609, -2873.300048, 1103.676025, 0.000000, 0.000000, -90.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2507, 2844.849609, -2873.936035, 1103.676025, 0.000000, 0.000000, -90.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2507, 2844.793701, -2869.899902, 1103.676025, 0.000000, 0.000000, -90.000000, -1, -1, -1, 50.0);
	CreateDynamicObject(2507, 2844.793701, -2870.641845, 1103.676025, 0.000000, 0.000000, -90.000000, -1, -1, -1, 50.0);
	return 1;
}