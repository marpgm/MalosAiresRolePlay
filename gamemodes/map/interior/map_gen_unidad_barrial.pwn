#if defined _map_gen_unidad_barrial_inc
	#endinput
#endif
#define _map_gen_unidad_barrial_inc

#include <YSI_Coding\y_hooks>


hook LoadMaps()
{
	new tmpobjid;
	tmpobjid = CreateDynamicObject(3828, 2900.000000, 2100.000000, 800.000000, 0.000000, 0.000000, 0.000000, .worldid = -1, .interiorid = -1, .playerid = -1, .streamdistance = -1.0);
	SetDynamicObjectMaterial(tmpobjid, 4, 5986, "chateau_lawn", "backstagefloor1_256", 0x00000000);
	tmpobjid = CreateDynamicObject(3828, 2912.754150, 2100.000000, 800.000000, 0.000000, 0.000000, 180.000000, .worldid = -1, .interiorid = -1, .playerid = -1, .streamdistance = -1.0);
	SetDynamicObjectMaterial(tmpobjid, 4, 5986, "chateau_lawn", "backstagefloor1_256", 0x00000000);
	tmpobjid = CreateDynamicObject(19377, 2905.715576, 2097.549072, 805.680969, 0.000000, 270.000000, -90.000007, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 6102, "gazlaw1", "law_gazwhite1", 0x00000000);
	tmpobjid = CreateDynamicObject(19377, 2915.295654, 2097.549072, 805.680969, 0.000000, 270.000000, -90.000007, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 6102, "gazlaw1", "law_gazwhite1", 0x00000000);
	tmpobjid = CreateDynamicObject(19377, 2900.986816, 2101.957519, 803.012573, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 6102, "gazlaw1", "law_gazwhite1", 0x00000000);
	tmpobjid = CreateDynamicObject(19377, 2912.201416, 2101.957519, 803.012573, 0.000000, -0.000007, 179.999954, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 6102, "gazlaw1", "law_gazwhite1", 0x00000000);
	tmpobjid = CreateDynamicObject(19377, 2905.715576, 2097.789306, 803.012573, 0.000007, 0.000000, 89.999977, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 6102, "gazlaw1", "law_gazwhite1", 0x00000000);
	tmpobjid = CreateDynamicObject(19377, 2915.327148, 2097.789306, 803.012573, 0.000007, 0.000000, 89.999977, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 6102, "gazlaw1", "law_gazwhite1", 0x00000000);
	tmpobjid = CreateDynamicObject(19388, 2906.472656, 2100.565185, 804.442321, 0.000000, 0.000007, 0.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 6102, "gazlaw1", "law_gazwhite1", 0xFFFFFFFF);
	tmpobjid = CreateDynamicObject(19388, 2906.472656, 2097.372558, 804.442321, 0.000000, 0.000007, 0.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 6102, "gazlaw1", "law_gazwhite1", 0xFFFFFFFF);
	tmpobjid = CreateDynamicObject(19388, 2906.472656, 2096.551757, 803.452148, 0.000000, 0.000007, 0.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 6102, "gazlaw1", "law_gazwhite1", 0xFFFFFFFF);
	tmpobjid = CreateDynamicObject(19388, 2910.186279, 2100.565185, 804.442321, 0.000000, 0.000014, 0.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 6102, "gazlaw1", "law_gazwhite1", 0xFFFFFFFF);
	tmpobjid = CreateDynamicObject(19388, 2910.186279, 2097.372558, 804.442321, 0.000000, 0.000014, 0.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 6102, "gazlaw1", "law_gazwhite1", 0xFFFFFFFF);
	tmpobjid = CreateDynamicObject(19388, 2910.186279, 2096.551757, 803.452148, 0.000000, 0.000014, 0.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 6102, "gazlaw1", "law_gazwhite1", 0xFFFFFFFF);
	tmpobjid = CreateDynamicObject(19377, 2905.715576, 2102.237792, 803.012573, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 6102, "gazlaw1", "law_gazwhite1", 0x00000000);
	tmpobjid = CreateDynamicObject(19377, 2915.296142, 2102.237792, 803.012573, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 6102, "gazlaw1", "law_gazwhite1", 0x00000000);
	tmpobjid = CreateDynamicObject(1569, 2903.003906, 2102.174316, 802.742309, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 3741, "cehillhse14", "comptwindo4", 0x00000000);
	tmpobjid = CreateDynamicObject(1569, 2904.494628, 2102.174316, 802.742309, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 3741, "cehillhse14", "comptwindo4", 0x00000000);
	tmpobjid = CreateDynamicObject(1499, 2910.172119, 2101.347656, 802.702270, 0.000000, 0.000000, 270.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0x00000000);
	SetDynamicObjectMaterial(tmpobjid, 1, 6102, "gazlaw1", "law_gazwhite1", 0x00000000);
	tmpobjid = CreateDynamicObject(1499, 2906.448486, 2101.347656, 802.702270, 0.000000, 0.000000, 270.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0x00000000);
	SetDynamicObjectMaterial(tmpobjid, 1, 6102, "gazlaw1", "law_gazwhite1", 0x00000000);
	tmpobjid = CreateDynamicObject(16378, 2902.785400, 2098.627197, 803.492797, 0.000000, 0.000000, 270.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 9, 19426, "all_walls", "mirror01", 0x00000000);
	SetDynamicObjectMaterial(tmpobjid, 10, 19426, "all_walls", "mirror01", 0x00000000);
	SetDynamicObjectMaterial(tmpobjid, 13, 19426, "all_walls", "mirror01", 0x00000000);
	tmpobjid = CreateDynamicObject(2689, 2907.548828, 2101.896728, 804.352966, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 1, -1, "none", "none", 0xFF003300);
	tmpobjid = CreateDynamicObject(2689, 2908.359619, 2101.896728, 804.352966, 0.000000, 0.000000, -166.399795, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 1, -1, "none", "none", 0xFF003300);
	tmpobjid = CreateDynamicObject(2689, 2907.996826, 2101.943115, 804.352966, 0.000000, 0.000000, -166.399795, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 1, 19426, "all_walls", "mirror01", 0xFF003300);
	tmpobjid = CreateDynamicObject(2689, 2909.028320, 2101.934326, 804.352966, 0.000000, 0.000000, 124.700202, -1, -1, -1, 300.00, 300.00); 
	SetDynamicObjectMaterial(tmpobjid, 1, 19426, "all_walls", "mirror01", 0xFF003300);
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	tmpobjid = CreateDynamicObject(18075, 2911.178710, 2100.098876, 805.662597, 0.000000, 0.000014, 0.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(18075, 2913.991210, 2100.098876, 805.662597, 0.000000, 0.000014, 0.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(18075, 2902.144287, 2100.098876, 805.662597, 0.000000, 0.000014, 0.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(18075, 2899.743652, 2100.098876, 805.662597, 0.000000, 0.000014, 0.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(1569, 2905.985107, 2102.174316, 802.742309, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(2514, 2911.265136, 2098.367431, 802.732299, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(2518, 2910.656494, 2101.654541, 802.742309, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(2190, 2903.247070, 2100.321289, 803.392089, 0.000000, 0.000000, 270.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(19808, 2902.787353, 2100.033447, 803.563049, 0.000000, 0.000000, -93.299980, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(1721, 2905.562500, 2098.034912, 802.742309, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(1721, 2904.881835, 2098.034912, 802.742309, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(2002, 2905.903076, 2099.468505, 801.521789, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(2002, 2905.903076, 2098.988037, 801.521789, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(2192, 2902.989501, 2099.860595, 803.512878, 0.000000, 0.000000, 55.299972, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(2167, 2901.564453, 2097.885742, 802.742309, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(2132, 2908.085205, 2098.390869, 802.742309, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(11706, 2909.127441, 2098.256103, 802.722290, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(1744, 2908.037109, 2097.725097, 804.092651, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(2149, 2908.186523, 2098.105224, 804.592834, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(11743, 2907.893066, 2098.093994, 803.792785, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(2149, 2907.656005, 2098.105224, 804.592834, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(19562, 2907.210937, 2098.065673, 804.422546, 0.000000, 0.000000, -102.099967, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(19622, 2909.983642, 2098.233886, 803.454650, 7.999993, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(1238, 2909.768798, 2101.841796, 803.052612, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(1238, 2909.768798, 2101.841796, 803.152709, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(1238, 2909.768798, 2101.841796, 803.272827, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(1435, 2908.608154, 2101.831542, 802.772338, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(1425, 2907.299560, 2101.847412, 803.132690, 0.000000, 0.000000, 2.899998, -1, -1, -1, 300.00, 300.00); 
	tmpobjid = CreateDynamicObject(2372, 2908.984619, 2102.977050, 804.983459, 0.000000, 270.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 

	return 1;
}