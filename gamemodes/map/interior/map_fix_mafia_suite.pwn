#if defined _map_int_fix_mafia_suite_inc
	#endinput
#endif
#define _map_int_fix_mafia_suite_inc

#include <YSI_Coding\y_hooks>

/*	FIX de /ainteriores 114*/

hook RemoveMapsBuildings(playerid) //en caso de no tener Removes, Borrar este hook
{
	RemoveBuildingForPlayer(playerid, 14638, 2187.429, 1627.312, 1042.406, 0.250);
	return 1;
}

hook LoadMaps()
{
	Textura = CreateDynamicObject(18981, 2196.970703, 1619.822875, 1050.533203, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(18981, 2167.938476, 1619.827880, 1050.533203, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(19464, 2187.411376, 1624.309570, 1044.930908, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(19464, 2187.411376, 1631.804321, 1044.930908, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(19464, 2187.411376, 1629.531372, 1047.977905, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(18981, 2174.958984, 1624.414550, 1058.293823, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(19464, 2187.411376, 1618.365234, 1044.930908, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(19426, 2187.231689, 1624.404296, 1044.144409, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(19426, 2187.415039, 1628.796997, 1045.704223, 90.000000, 0.133999, 180.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 1355, "break_s_bins", "CJ_WOOD_DARK", 0xFF999999);
	Textura = CreateDynamicObject(13646, 2182.532226, 1628.827514, 1042.558471, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	SetDynamicObjectMaterial(Textura, 1, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(12921, 2174.511718, 1624.399291, 1045.698852, 0.000000, 0.000015, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 19598, "sfbuilding1", "darkwood1", 0xFF999999);
	Textura = CreateDynamicObject(12921, 2177.584716, 1624.399291, 1045.698852, 0.000000, 0.000015, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 19598, "sfbuilding1", "darkwood1", 0xFF999999);
	Textura = CreateDynamicObject(11714, 2189.545654, 1620.315917, 1043.658691, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 1557, "dave_door_2b", "miragedoor1_256", 0x00000000);
	Textura = CreateDynamicObject(2376, 2188.243408, 1624.720825, 1042.078247, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 14581, "ab_mafiasuitea", "cof_wood2", 0x00000000);
	SetDynamicObjectMaterial(Textura, 1, 1736, "cj_ammo", "CJ_Black_metal", 0x00000000);
	SetDynamicObjectMaterial(Textura, 2, 14581, "ab_mafiasuitea", "cof_wood2", 0x00000000);
	Textura = CreateDynamicObject(1735, 2178.631347, 1621.072265, 1042.408325, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 2562, "cj_hotel_sw", "CJ-COUCHL1", 0xFFDDDD99);
	Textura = CreateDynamicObject(2184, 2181.022460, 1620.779296, 1042.398315, 0.000000, 0.000000, 112.799964, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 19598, "sfbuilding1", "darkwood1", 0x00000000);
	SetDynamicObjectMaterial(Textura, 1, 18835, "mickytextures", "wood051", 0xFF999999);
	Textura = CreateDynamicObject(18766, 2192.309570, 1630.949584, 1047.437011, 0.000000, 0.000000, 180.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 14534, "ab_wooziea", "walp72S", 0xFFDCBBBB);
	Textura = CreateDynamicObject(18766, 2191.999267, 1636.050048, 1041.607299, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 10871, "blacksky_sfse", "ws_slatetiles", 0x00000000);
	Textura = CreateDynamicObject(1523, 2191.708740, 1631.045166, 1042.408325, 0.000000, -0.000007, 179.999954, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFDDAA);
	SetDynamicObjectMaterial(Textura, 1, 2878, "cj_vic", "CJ_VICT_DOOR", 0xDDFFFFFF);
	Textura = CreateDynamicObject(18766, 2192.519042, 1634.028076, 1041.607299, 0.000000, 0.000000, 180.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 10871, "blacksky_sfse", "ws_slatetiles", 0x00000000);
	Textura = CreateDynamicObject(18766, 2187.837158, 1636.010009, 1041.607299, 0.000007, 0.000000, 89.999977, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 10871, "blacksky_sfse", "ws_slatetiles", 0x00000000);
	Textura = CreateDynamicObject(18766, 2189.927734, 1636.020629, 1041.917602, 90.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 15041, "bigsfsave", "AH_flroortile9", 0x00000000);
	Textura = CreateDynamicObject(19445, 2192.298828, 1633.619628, 1044.178222, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 4828, "airport3_las", "brwall_128", 0x00000000);
	Textura = CreateDynamicObject(19445, 2191.598144, 1635.869750, 1044.178222, 0.000000, 0.000000, 180.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 4828, "airport3_las", "brwall_128", 0x00000000);
	Textura = CreateDynamicObject(19445, 2188.245117, 1635.839721, 1044.178222, 0.000000, -0.000007, 179.999954, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 4828, "airport3_las", "brwall_128", 0x00000000);
	Textura = CreateDynamicObject(2694, 2188.815185, 1633.629516, 1045.376708, 5.000000, 90.000000, 270.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 16640, "a51", "vent01_64", 0x00000000);
	Textura = CreateDynamicObject(2528, 2188.947753, 1633.037231, 1042.398315, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 1736, "cj_ammo", "CJ_Black_metal", 0x00000000);
	Textura = CreateDynamicObject(2211, 2189.899902, 1633.302856, 1042.568481, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 14581, "ab_mafiasuitea", "cof_wood2", 0x00000000);
	SetDynamicObjectMaterial(Textura, 1, 19325, "lsmall_shops", "lsmall_window01", 0x00000000);
	Textura = CreateDynamicObject(19455, 2180.104003, 1624.402343, 1044.138061, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 4833, "airprtrunway_las", "policeha02black_128", 0x00000000);
	Textura = CreateDynamicObject(1499, 2186.409667, 1624.445922, 1042.408325, 0.000000, -0.000007, 179.999954, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 14789, "ab_sfgymmain", "ab_wood02", 0xFFFFFFFF);
	SetDynamicObjectMaterial(Textura, 1, 19325, "lsmall_shops", "lsmall_window01", 0xBBFFFFFF);
	Textura = CreateDynamicObject(18764, 2189.806884, 1633.289062, 1048.316406, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 19597, "lsbeachside", "ceilingtiles4-128x128", 0xFFFFFFFF);
	Textura = CreateDynamicObject(19916, 2184.893798, 1633.317382, 1041.257202, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 18081, "cj_barb", "barberschr7b", 0x00000000);
	SetDynamicObjectMaterial(Textura, 1, 1736, "cj_ammo", "CJ_Black_metal", 0x00000000);
	Textura = CreateDynamicObject(2139, 2185.771728, 1633.053955, 1042.408325, 0.000000, 0.000007, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 1, 1736, "cj_ammo", "CJ_Black_metal", 0x00000000);
	Textura = CreateDynamicObject(18763, 2185.988037, 1634.228271, 1040.036743, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 14581, "ab_mafiasuitea", "ab_wood01", 0x00000000);
	Textura = CreateDynamicObject(2264, 2185.820312, 1633.847290, 1043.969604, 270.000000, 0.000000, 180.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 1, 14472, "carls_kit2", "stove_1", 0x00000000);
	Textura = CreateDynamicObject(14804, 2178.139648, 1625.623779, 1043.408813, 0.000000, 0.000000, 124.499923, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 1, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(19893, 2180.299560, 1621.010131, 1043.168579, 0.000000, 0.000000, 270.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 1, 3881, "apsecurity_sfxrf", "WIN_DESKTOP", 0x00000000);
	Textura = CreateDynamicObject(741, 2177.994628, 1627.170166, 1042.718627, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 1, 14803, "bdupsnew", "Bdup2_wallpaperB", 0x00000000);
	Textura = CreateDynamicObject(2894, 2180.254882, 1621.902709, 1043.174072, 0.000000, 0.000000, 291.900024, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	Textura = CreateDynamicObject(14455, 2177.563232, 1623.717773, 1044.048217, 0.000000, 0.000000, 270.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 14581, "ab_mafiasuitea", "cof_wood2", 0x00000000);
	Textura = CreateDynamicObject(18084, 2183.213378, 1625.447021, 1044.218139, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 14581, "ab_mafiasuitea", "cof_wood2", 0xFFFFFFFF);
	Textura = CreateDynamicObject(18084, 2180.051513, 1625.447021, 1044.218139, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 14581, "ab_mafiasuitea", "cof_wood2", 0xFFFFFFFF);
	Textura = CreateDynamicObject(19428, 2185.625732, 1624.392700, 1046.659545, 0.000000, 180.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 4833, "airprtrunway_las", "policeha02black_128", 0x00000000);
	Textura = CreateDynamicObject(18098, 2176.903076, 1624.116210, 1044.898315, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	Textura = CreateDynamicObject(2267, 2187.258300, 1623.101684, 1044.588378, 0.000000, 0.000000, 270.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 1, 14420, "dr_gsbits", "mp_apt1_pic6", 0x00000000);
	Textura = CreateDynamicObject(2286, 2187.273925, 1621.282348, 1045.259521, 0.000000, 0.000000, 270.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 14420, "dr_gsbits", "mp_apt1_pic7", 0x00000000);
	Textura = CreateDynamicObject(2472, 2189.247802, 1631.087768, 1043.778564, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 16640, "a51", "vent01_64", 0x00000000);
	SetDynamicObjectMaterial(Textura, 1, 14548, "ab_cargo_int", "Alumplat64", 0x00000000);
	Textura = CreateDynamicObject(2255, 2190.612304, 1630.088256, 1045.087768, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 14582, "casmafbar", "bottlestacked256", 0x00000000);
	Textura = CreateDynamicObject(19818, 2189.287597, 1630.851562, 1043.518676, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x99999999);
	Textura = CreateDynamicObject(19818, 2189.127441, 1630.671386, 1043.518676, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x99999999);
	Textura = CreateDynamicObject(11744, 2188.869873, 1630.862792, 1043.584350, 0.000000, 72.599975, 270.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 19941, "goldbar1", "chrome", 0x00000000);
	Textura = CreateDynamicObject(19525, 2188.988281, 1630.806274, 1042.528442, 0.000000, 0.000000, 32.399997, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 16640, "a51", "vent01_64", 0x00000000);
	SetDynamicObjectMaterial(Textura, 1, 16640, "a51", "vent01_64", 0x00000000);
	Textura = CreateDynamicObject(19571, 2188.888916, 1630.915039, 1043.139160, 90.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, 18081, "cj_barb", "CJ_TILES_5", 0x00000000);
	Textura = CreateDynamicObject(19818, 2185.007568, 1633.411621, 1043.378540, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x99999999);
	Textura = CreateDynamicObject(19818, 2184.877441, 1633.331542, 1043.378540, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x99999999);
	Textura = CreateDynamicObject(1964, 2188.146972, 1625.456298, 1043.098022, 0.000000, 0.000000, 270.000000, -1, -1, -1, 50.00, 50.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	SetDynamicObjectMaterial(Textura, 6, -1, "none", "none", 0x00FFFFFF);
	CreateDynamicObject(18981, 2179.971435, 1631.870971, 1041.908325, 0.000000, 90.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(18981, 2192.029541, 1632.336791, 1050.533203, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(18981, 2177.113769, 1632.179321, 1050.533203, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(18981, 2179.252929, 1634.070434, 1050.533203, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(18981, 2175.622802, 1618.952636, 1050.533203, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(19426, 2177.703369, 1624.406250, 1044.144409, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(1502, 2187.433105, 1627.296386, 1042.408325, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(1739, 2182.518066, 1632.282104, 1043.287963, 0.000000, 0.000000, 450.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(1739, 2179.066894, 1628.830200, 1043.287963, 0.000000, 0.000000, 180.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(1739, 2180.132324, 1631.342285, 1043.287963, 0.000000, 0.000000, 495.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(1739, 2184.321533, 1627.156616, 1043.287963, 0.000000, 0.000000, 675.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2002, 2181.029052, 1619.992309, 1042.408325, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2707, 2189.537597, 1631.230590, 1044.921264, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2204, 2187.776123, 1630.952270, 1042.398315, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2204, 2187.776123, 1630.952270, 1044.179809, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2204, 2192.680175, 1630.952148, 1046.669311, 0.000000, 180.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2707, 2188.907958, 1631.230590, 1044.921264, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(1846, 2189.265136, 1630.074707, 1043.827636, 0.000007, 90.000000, 89.999977, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2136, 2189.712646, 1631.530029, 1042.388305, 0.000000, 0.000000, 180.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2241, 2191.077636, 1633.091186, 1042.906250, 0.000000, 0.000000, 110.500038, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2855, 2189.960937, 1633.295654, 1042.608276, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(19873, 2189.627197, 1633.348388, 1043.048706, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(11707, 2188.399902, 1632.384643, 1043.508300, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(19829, 2191.508300, 1632.811889, 1043.738281, 0.000000, 0.000000, 270.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(19814, 2188.333007, 1631.386230, 1043.728637, 0.000000, 0.000000, 90.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2140, 2186.756347, 1633.061279, 1042.408325, 0.000000, 0.000007, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2002, 2184.209472, 1632.952026, 1042.408325, 0.000000, 0.000000, 360.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(19814, 2184.521728, 1633.536376, 1043.728637, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(1739, 2180.435791, 1626.695922, 1043.287963, 0.000000, 0.000000, 225.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(948, 2184.063232, 1620.017700, 1042.398315, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(1753, 2186.767822, 1623.299316, 1042.408325, 0.000000, 0.000000, 270.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2001, 2186.871337, 1624.995117, 1042.408325, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2010, 2178.097900, 1633.163818, 1042.388305, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2011, 2187.927734, 1624.383666, 1042.408325, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2245, 2182.558837, 1628.837768, 1043.369018, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(1715, 2182.175537, 1621.085571, 1042.408325, 0.000000, 0.000000, 270.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(1715, 2181.796386, 1622.716430, 1042.408325, 0.000000, 0.000000, -62.400001, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(19807, 2179.718994, 1622.394897, 1043.238647, 0.000000, 0.000000, -46.500000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2707, 2189.417480, 1632.380737, 1045.801391, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(1753, 2190.960937, 1626.548950, 1042.408325, 0.000000, 0.000000, 270.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2707, 2190.428466, 1632.380737, 1045.801391, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(11705, 2189.791992, 1630.710571, 1043.138305, 0.000000, 0.000000, 70.299995, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2710, 2189.248291, 1630.715087, 1043.259033, 0.000000, 0.000000, -7.199998, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(19820, 2185.183593, 1633.416625, 1043.449340, 0.000000, 0.000000, -21.000000, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2828, 2189.808593, 1630.738769, 1043.448608, 0.000000, 0.000000, 159.300003, -1, -1, -1, 50.00, 50.00);
	CreateDynamicObject(2825, 2180.722167, 1620.647338, 1043.169067, 0.000000, 0.000000, 0.000000, -1, -1, -1, 50.00, 50.00);
	return 1;
}