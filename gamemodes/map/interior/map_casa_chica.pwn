/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
   Created by "Teo5584"
*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if defined _map_int_casa_chica_inc
   #endinput
#endif
#define _map_int_casa_chica_inc

#include <YSI_Coding\y_hooks>

hook LoadMaps()
{
   SetDynamicObjectMaterial(CreateDynamicObject(19376, -1143.72632, 1428.28003, 1399.94226,   0.00000, 90.00000, 0.00000, -1, -1, -1, 50.0), 0, 13007, "sw_bankint", "woodfloor1", 0xFFFFFFFF);
   SetDynamicObjectMaterial(CreateDynamicObject(19375, -1148.91724, 1428.24500, 1398.42224,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0), 0, 16150, "ufo_bar", "GEwhite1_64", 0xFFFFFFFF);
   SetDynamicObjectMaterial(CreateDynamicObject(19375, -1143.76929, 1423.40503, 1398.86218,   90.00000, 0.00000, -90.00000, -1, -1, -1, 50.0), 0, 16150, "ufo_bar", "GEwhite1_64", 0xFFFFFFFF);
   SetDynamicObjectMaterial(CreateDynamicObject(19375, -1143.80347, 1433.04028, 1398.86218,   90.00000, 0.00000, -90.00000, -1, -1, -1, 50.0), 0, 16150, "ufo_bar", "GEwhite1_64", 0xFFFFFFFF);
   SetDynamicObjectMaterial(CreateDynamicObject(19375, -1150.21680, 1427.89685, 1398.86218,   90.00000, 0.00000, -90.00000, -1, -1, -1, 50.0), 0, 16150, "ufo_bar", "GEwhite1_64", 0xFFFFFFFF);
   SetDynamicObjectMaterial(CreateDynamicObject(19356, -1145.01331, 1427.86707, 1401.73816,   0.00000, 0.00000, -180.00000, -1, -1, -1, 50.0), 0, 16150, "ufo_bar", "GEwhite1_64", 0xFFFFFFFF);
   SetDynamicObjectMaterial(CreateDynamicObject(19356, -1143.51941, 1427.84900, 1401.74219,   0.00000, 0.00000, -180.00000, -1, -1, -1, 50.0), 0, 16150, "ufo_bar", "GEwhite1_64", 0xFFFFFFFF);
   SetDynamicObjectMaterial(CreateDynamicObject(19377, -1143.75439, 1428.26257, 1403.54626,   0.00000, 90.00000, 0.00000, -1, -1, -1, 50.0), 0, 16150, "ufo_bar", "GEwhite1_64", 0xFFFFFFFF);
   SetDynamicObjectMaterial(CreateDynamicObject(1506, -1148.84143, 1431.46118, 1400.50952,   0.00000, 0.00000, 90.00000, -1, -1, -1, 50.0), 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   SetDynamicObjectMaterial(CreateDynamicObject(19429, -1144.25647, 1426.35388, 1401.74219,   0.00000, 0.00000, -90.00000, -1, -1, -1, 50.0), 0, 16150, "ufo_bar", "GEwhite1_64", 0xFFFFFFFF);
   SetDynamicObjectMaterial(CreateDynamicObject(19375, -1138.48096, 1428.17114, 1398.42224,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0), 0, 16150, "ufo_bar", "GEwhite1_64", 0xFFFFFFFF);
   CreateDynamicObject(2161, -1139.07031, 1425.86584, 1400.07068,   0.00000, 0.00000, -180.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(2152, -1145.97107, 1427.59412, 1400.40967,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(2154, -1147.34485, 1427.59314, 1400.40967,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(2141, -1148.34424, 1427.35803, 1400.45667,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(2149, -1145.59546, 1427.46399, 1401.59766,   0.00000, 0.00000, -28.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(11743, -1146.26575, 1427.51160, 1401.45618,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(2863, -1146.89197, 1427.52075, 1401.27881,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(19786, -1138.44617, 1427.79065, 1401.63599,   0.00000, 0.00000, -90.00000, -1, -1, -1, 50.0);
   SetDynamicObjectMaterialText(19786, 0, "LG", 140, "Arial", 20, 1, 0xFFFFFFFF, 0x00000000, 1);
   CreateDynamicObject(2161, -1140.40161, 1425.86499, 1400.07068,   0.00000, 0.00000, -180.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(2267, -1143.41321, 1427.82764, 1402.12341,   0.00000, 0.00000, 90.00000, -1, -1, -1, 50.0);
   SetDynamicObjectMaterial(CreateDynamicObject(11724, -1138.84314, 1427.79504, 1400.53259,   0.00000, 0.00000, -90.00000, -1, -1, -1, 50.0), 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   SetDynamicObjectMaterial(CreateDynamicObject(2118, -1140.56763, 1431.17065, 1399.88892,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0), 1, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   CreateDynamicObject(2084, -1143.28845, 1427.61560, 1400.01892,   0.00000, 0.00000, 90.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(2123, -1139.39392, 1432.48596, 1400.59387,   0.00000, 0.00000, 74.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(2123, -1140.51196, 1432.42273, 1400.59387,   0.00000, 0.00000, 112.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(2293, -1142.94617, 1426.65942, 1400.02869,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(2293, -1142.93506, 1429.00635, 1400.02869,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0);
   SetDynamicObjectMaterial(CreateDynamicObject(14414, -1142.19519, 1425.41309, 1397.34937,   0.00000, 0.00000, -90.00000, -1, -1, -1, 50.0), 0, 13007, "sw_bankint", "woodfloor1", 0xFFFFFFFF);
   SetDynamicObjectMaterial(CreateDynamicObject(19429, -1144.24207, 1429.34058, 1401.74219,   0.00000, 0.00000, -90.00000, -1, -1, -1, 50.0), 0, 16150, "ufo_bar", "GEwhite1_64", 0xFFFFFFFF);
   SetDynamicObjectMaterial(CreateDynamicObject(1794, -1145.36487, 1429.01257, 1400.02942,   0.00000, 0.00000, 90.00000, -1, -1, -1, 50.0), 2, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   SetDynamicObjectMaterial(CreateDynamicObject(19375, -1136.45190, 1425.76355, 1398.86218,   90.00000, 0.00000, -90.00000, -1, -1, -1, 50.0), 0, 16150, "ufo_bar", "GEwhite1_64", 0xFFFFFFFF);
   CreateDynamicObject(1502, -1141.63672, 1425.02820, 1400.02808,   0.00000, 0.00000, 270.00000, -1, -1, -1, 50.0);
   SetDynamicObjectMaterial(CreateDynamicObject(19386, -1141.61353, 1424.23303, 1401.74219,   0.00000, 0.00000, -180.00000, -1, -1, -1, 50.0), 0, 16150, "ufo_bar", "GEwhite1_64", 0xFFFFFFFF);
   CreateDynamicObject(2528, -1141.12048, 1425.21338, 1400.03015,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(2526, -1140.25378, 1425.21692, 1400.03015,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(2524, -1139.06860, 1424.55298, 1400.03052,   0.00000, 0.00000, -90.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(2286, -1146.46301, 1427.76001, 1402.55396,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(938, -1146.54858, 1427.45215, 1403.31433,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(11707, -1139.32422, 1423.51001, 1400.75220,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(2742, -1138.65173, 1424.65845, 1401.14734,   0.00000, 0.00000, -90.00000, -1, -1, -1, 50.0);
   SetDynamicObjectMaterial(CreateDynamicObject(19087, -1146.70142, 1431.42297, 1401.36108,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0), 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   SetDynamicObjectMaterial(CreateDynamicObject(19429, -1148.44849, 1432.20569, 1400.48010,   0.00000, 90.00000, -180.00000, -1, -1, -1, 50.0), 0, 16150, "ufo_bar", "GEwhite1_64", 0xFFFFFFFF);
   SetDynamicObjectMaterial(CreateDynamicObject(19429, -1148.48938, 1431.43909, 1399.76709,   -90.00000, 90.00000, -180.00000, -1, -1, -1, 50.0), 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   SetDynamicObjectMaterial(CreateDynamicObject(19429, -1146.77637, 1433.09875, 1399.76709,   -90.00000, 90.00000, -270.00000, -1, -1, -1, 50.0), 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   SetDynamicObjectMaterial(CreateDynamicObject(19087, -1147.68445, 1431.42297, 1401.34412,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0), 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   SetDynamicObjectMaterial(CreateDynamicObject(19087, -1148.68042, 1431.42297, 1401.34412,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0), 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   SetDynamicObjectMaterial(CreateDynamicObject(19087, -1146.68835, 1431.42297, 1401.34412,   0.00000, 90.00000, 0.00000, -1, -1, -1, 50.0), 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   SetDynamicObjectMaterial(CreateDynamicObject(19429, -1148.21643, 1432.30640, 1400.31409,   0.00000, 90.00000, -180.00000, -1, -1, -1, 50.0), 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   SetDynamicObjectMaterial(CreateDynamicObject(19429, -1148.05042, 1432.30640, 1400.14807,   0.00000, 90.00000, -180.00000, -1, -1, -1, 50.0), 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   SetDynamicObjectMaterial(CreateDynamicObject(19429, -1147.88440, 1432.30640, 1399.98206,   0.00000, 90.00000, -180.00000, -1, -1, -1, 50.0), 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   SetDynamicObjectMaterial(CreateDynamicObject(19429, -1148.48938, 1432.86707, 1399.76709,   -90.00000, 90.00000, -180.00000, -1, -1, -1, 50.0), 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   CreateDynamicObject(2006, -1148.76123, 1431.62158, 1401.52576,   0.00000, 18.00000, 90.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(19429, -1148.48938, 1432.86707, 1399.76709,   -90.00000, 90.00000, -180.00000, -1, -1, -1, 50.0);
   CreateDynamicObject(18072, -1140.86340, 1442.73242, 1403.45337,   -90.00000, 0.00000, 90.00000, -1, -1, -1, 50.0);
   SetDynamicObjectMaterial(CreateDynamicObject(2081, -1148.87549, 1431.40662, 1400.01025,   0.00000, 0.00000, -90.00000, -1, -1, -1, 50.0), 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   CreateDynamicObject(2240, -1144.22681, 1429.72461, 1400.43030,   0.00000, 0.00000, 0.00000, -1, -1, -1, 50.0);
   SetDynamicObjectMaterial(CreateDynamicObject(911, -1145.42395, 1428.71704, 1400.06458,   0.00000, 0.00000, -90.00000, -1, -1, -1, 50.0), 0, 14650, "ab_trukstpc", "mp_CJ_WOOD5", 0xFF808080);
   CreateDynamicObject(19174, -1148.82764, 1429.53882, 1401.90869,   0.00000, 0.00000, 90.00000, -1, -1, -1, 50.0);

   return 1;
}