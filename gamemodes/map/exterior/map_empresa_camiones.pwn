#if defined _map_empresa_camiones_inc
	#endinput
#endif
#define _map_empresa_camiones_inc

#include <YSI_Coding\y_hooks>

hook LoadMaps()
{
    Textura = CreateDynamicObject(18766, 2247.658203, -2163.214111, 15.067140, 0.000003, 0.000003, 44.399978, -1, -1, -1, 300.00, 300.00); 
    SetDynamicObjectMaterial(Textura, 0, 9514, "711_sfw", "brick", 0x00000000);
    Textura = CreateDynamicObject(18766, 2254.802734, -2156.218261, 15.067140, 0.000003, 0.000003, 44.399978, -1, -1, -1, 300.00, 300.00); 
    SetDynamicObjectMaterial(Textura, 0, 9514, "711_sfw", "brick", 0x00000000);
    Textura = CreateDynamicObject(18766, 2261.949462, -2149.221435, 15.067140, 0.000003, 0.000003, 44.399978, -1, -1, -1, 300.00, 300.00); 
    SetDynamicObjectMaterial(Textura, 0, 9514, "711_sfw", "brick", 0x00000000);
    Textura = CreateDynamicObject(18766, 2269.094482, -2142.238769, 15.067140, 0.000003, 0.000003, 44.299976, -1, -1, -1, 300.00, 300.00); 
    SetDynamicObjectMaterial(Textura, 0, 9514, "711_sfw", "brick", 0x00000000);
    Textura = CreateDynamicObject(18766, 2276.253417, -2135.255859, 15.067140, 0.000003, 0.000003, 44.299976, -1, -1, -1, 300.00, 300.00); 
    SetDynamicObjectMaterial(Textura, 0, 9514, "711_sfw", "brick", 0x00000000);
    Textura = CreateDynamicObject(18766, 2276.318359, -2128.863769, 15.067140, 0.000003, 0.000003, 134.300003, -1, -1, -1, 300.00, 300.00); 
    SetDynamicObjectMaterial(Textura, 0, 9514, "711_sfw", "brick", 0x00000000);
    Textura = CreateDynamicObject(18766, 2269.580810, -2121.956787, 15.067140, 0.000003, 0.000003, 134.300003, -1, -1, -1, 300.00, 300.00); 
    SetDynamicObjectMaterial(Textura, 0, 9514, "711_sfw", "brick", 0x00000000);
    Textura = CreateDynamicObject(18766, 2258.640380, -2126.284667, 15.067140, 0.000003, 0.000003, 44.299976, -1, -1, -1, 300.00, 300.00); 
    SetDynamicObjectMaterial(Textura, 0, 9514, "711_sfw", "brick", 0x00000000);
    Textura = CreateDynamicObject(2789, 2243.261474, -2135.227294, 17.556171, 0.000000, 0.000000, -134.799880, -1, -1, -1, 300.00, 300.00); 
    SetDynamicObjectMaterialText(Textura, 0, "CAMIONES S.A", 120, "Ariel", 80, 1, 0xFF660000, 0x00000000, 1);
    Textura = CreateDynamicObject(19442, 2245.282470, -2133.174072, 18.254455, 90.000000, 0.000000, -44.800018, -1, -1, -1, 300.00, 300.00); 
    SetDynamicObjectMaterial(Textura, 0, 18646, "matcolours", "grey-50-percent", 0x00000000);
    Textura = CreateDynamicObject(19442, 2242.829833, -2135.643798, 18.254455, 90.000000, 0.000000, -44.800018, -1, -1, -1, 300.00, 300.00); 
    SetDynamicObjectMaterial(Textura, 0, 18646, "matcolours", "grey-50-percent", 0x00000000);
    Textura = CreateDynamicObject(19442, 2245.282470, -2133.174072, 17.194473, 90.000000, 0.000000, -44.800018, -1, -1, -1, 300.00, 300.00); 
    SetDynamicObjectMaterial(Textura, 0, 18646, "matcolours", "grey-50-percent", 0x00000000);
    Textura = CreateDynamicObject(19442, 2242.830078, -2135.641357, 17.194473, 90.000000, 0.000000, -44.800018, -1, -1, -1, 300.00, 300.00); 
    SetDynamicObjectMaterial(Textura, 0, 18646, "matcolours", "grey-50-percent", 0x00000000);
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    Textura = CreateDynamicObject(5856, 2242.409912, -2149.106933, 14.069664, 0.000000, 0.000000, 134.900039, -1, -1, -1, 300.00, 300.00); 
    Textura = CreateDynamicObject(19859, 2245.608886, -2132.764160, 13.785297, 0.000000, 0.000000, 44.799995, -1, -1, -1, 300.00, 300.00); 
    Textura = CreateDynamicObject(3577, 2245.407958, -2162.239501, 13.287089, 0.000000, 0.000000, 44.299999, -1, -1, -1, 300.00, 300.00); 
    Textura = CreateDynamicObject(3576, 2238.845703, -2155.848876, 13.999584, 0.000000, 0.000000, 137.000015, -1, -1, -1, 300.00, 300.00); 
    Textura = CreateDynamicObject(19442, 2245.282470, -2133.174072, 17.194473, 90.000000, 0.000000, -44.800018, -1, -1, -1, 300.00, 300.00); 
    return 1;
}

hook OnGameModeInitEnded()
{
	new gateid;

    /*Porton*/
	gateid = Gate_Create(988, 
                2264.461669, -2121.445312, 13.497137, 0.00000, 0.00000, 224.300003,
				2260.646972, -2125.167480, 13.497137, 0.00000, 0.00000, 224.300003,
				.worldid = -1, .speed = 1.8, .type = GATE_TYPE_FACTION, .extraid = 17, .autoCloseTime = 4500);

    Gate_SetVehicleDetection(gateid, 2264.54, -2121.63, 13.74, .size = 8.0); 
    Gate_SetOnFootFrontDetection(gateid, 2261.48, -2122.82,14.0, 0.0, 0.0, 44.3, .labelText = "Reja");
	Gate_SetOnFootBackDetection(gateid, 2267.41, -2120.44, 14.0, 0.0, 0.0, 134.3,.labelText = "Reja");

    Gate_SetTexture(gateid, 0, 969, "electricgate", "KeepOut_64");

	return 1;
}