#if defined _map_aeropuerto_inc
	#endinput
#endif
#define _map_aeropuerto_inc

#include <YSI_Coding\y_hooks>

hook RemoveMapsBuildings(playerid)
{
	RemoveBuildingForPlayer(playerid, 4990, 1646.1953, -2414.0703, 17.9297, 0.25);
	RemoveBuildingForPlayer(playerid, 5010, 1646.1953, -2414.0703, 17.9297, 0.25);
	RemoveBuildingForPlayer(playerid, 5011, 1874.2109, -2286.5313, 17.9297, 0.25);
	RemoveBuildingForPlayer(playerid, 3672, 1921.6406, -2206.3906, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 3672, 2030.0547, -2249.0234, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 3672, 2030.0547, -2315.4297, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 3672, 2030.0547, -2382.1406, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 3672, 2112.9375, -2384.6172, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 3672, 1889.6563, -2666.0078, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 3672, 1822.7344, -2666.0078, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 3672, 1682.7266, -2666.0078, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 3672, 1617.2813, -2666.0078, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 3672, 1754.1719, -2666.0078, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 3769, 1961.4453, -2216.1719, 14.9844, 0.25);
	RemoveBuildingForPlayer(playerid, 3744, 2061.5313, -2209.8125, 14.9766, 0.25);
	RemoveBuildingForPlayer(playerid, 3744, 2082.4063, -2370.0156, 14.7031, 0.25);
	RemoveBuildingForPlayer(playerid, 1290, 1607.0156, -2439.9766, 18.4766, 0.25);
	RemoveBuildingForPlayer(playerid, 3629, 1617.2813, -2666.0078, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 1290, 1649.0625, -2641.4063, 18.4766, 0.25);
	RemoveBuildingForPlayer(playerid, 3663, 1664.4531, -2439.8047, 14.4688, 0.25);
	RemoveBuildingForPlayer(playerid, 3629, 1682.7266, -2666.0078, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 1290, 1686.4453, -2439.9766, 18.4766, 0.25);
	RemoveBuildingForPlayer(playerid, 3629, 1754.1719, -2666.0078, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 1290, 1766.7969, -2439.9766, 18.4766, 0.25);
	RemoveBuildingForPlayer(playerid, 3629, 1822.7344, -2666.0078, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 3663, 1832.4531, -2388.4375, 14.4688, 0.25);
	RemoveBuildingForPlayer(playerid, 1290, 1855.7969, -2641.4063, 18.4766, 0.25);
	RemoveBuildingForPlayer(playerid, 3629, 1889.6563, -2666.0078, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 1290, 1922.2031, -2641.4063, 18.4766, 0.25);
	RemoveBuildingForPlayer(playerid, 3663, 1882.2656, -2395.7813, 14.4688, 0.25);
	RemoveBuildingForPlayer(playerid, 1215, 1980.9219, -2413.8750, 13.0625, 0.25);
	RemoveBuildingForPlayer(playerid, 1215, 1980.9219, -2355.2109, 13.0625, 0.25);
	RemoveBuildingForPlayer(playerid, 3629, 2030.0547, -2382.1406, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 3664, 2042.7734, -2442.1875, 19.2813, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, 2057.7344, -2402.9922, 12.7500, 0.25);
	RemoveBuildingForPlayer(playerid, 1290, 2088.6094, -2422.1719, 18.4766, 0.25);
	RemoveBuildingForPlayer(playerid, 3574, 2082.4063, -2370.0156, 14.7031, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, 2089.3047, -2359.7578, 12.7500, 0.25);
	RemoveBuildingForPlayer(playerid, 1290, 2146.0156, -2409.3516, 18.4766, 0.25);
	RemoveBuildingForPlayer(playerid, 3629, 2112.9375, -2384.6172, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 5006, 1874.2109, -2286.5313, 17.9297, 0.25);
	RemoveBuildingForPlayer(playerid, 1290, 1899.4219, -2328.1406, 18.4766, 0.25);
	RemoveBuildingForPlayer(playerid, 1290, 1899.4219, -2244.5078, 18.4766, 0.25);
	RemoveBuildingForPlayer(playerid, 1215, 1983.8594, -2281.7109, 13.0625, 0.25);
	RemoveBuildingForPlayer(playerid, 3664, 1960.6953, -2236.4297, 19.2813, 0.25);
	RemoveBuildingForPlayer(playerid, 1290, 2003.4531, -2281.3984, 18.3828, 0.25);
	RemoveBuildingForPlayer(playerid, 5031, 2037.0469, -2313.5469, 18.7109, 0.25);
	RemoveBuildingForPlayer(playerid, 3629, 2030.0547, -2315.4297, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 3629, 2030.0547, -2249.0234, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, 2057.0547, -2315.4688, 12.7422, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, 2057.5391, -2270.0703, 12.7500, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, 2089.3047, -2332.5547, 12.7500, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, 2089.7813, -2244.4922, 12.7500, 0.25);
	RemoveBuildingForPlayer(playerid, 3629, 1921.6406, -2206.3906, 18.8828, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 1949.3438, -2227.5156, 13.6563, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 1944.0625, -2227.5156, 13.6563, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 1954.6172, -2227.4844, 13.6875, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 1965.1719, -2227.4141, 13.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 1959.8984, -2227.4453, 13.7266, 0.25);
	RemoveBuildingForPlayer(playerid, 3625, 1961.4453, -2216.1719, 14.9844, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 1975.7266, -2227.4141, 13.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 1970.4453, -2227.4141, 13.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 1981.0000, -2227.4141, 13.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 1996.8281, -2227.3828, 13.7891, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 1991.5547, -2227.4141, 13.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 1986.2813, -2227.4141, 13.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, 1983.8047, -2224.1641, 12.7500, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 2002.1094, -2227.3438, 13.8281, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, 2018.0313, -2224.1641, 12.7500, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 2055.0547, -2224.3828, 13.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 2055.0547, -2219.1094, 13.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, 2056.8281, -2224.1641, 12.7500, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 2054.9844, -2213.7891, 13.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 2054.9219, -2208.4609, 13.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 2054.9219, -2203.1875, 13.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 3574, 2061.5313, -2209.8125, 14.9766, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 2054.9297, -2181.3594, 13.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1412, 2054.9297, -2186.6328, 13.7578, 0.25);
	RemoveBuildingForPlayer(playerid, 1290, 1525.0078, -2439.9766, 18.4766, 0.25);
	RemoveBuildingForPlayer(playerid, 3663, 1580.0938, -2433.8281, 14.5703, 0.25);
	
	return 1;
}

hook LoadMaps()
{
	CreateDynamicObject(10183, 1963.72949, -2226.99072, 12.56770,   0.00000, 0.00000, -134.87996);
	CreateDynamicObject(10183, 1963.75659, -2212.38721, 12.56770,   0.00000, 0.00000, 45.05998);
	CreateDynamicObject(10183, 1963.77600, -2202.95435, 12.56770,   0.00000, 0.00000, -134.87996);
	CreateDynamicObject(19425, 1958.35144, -2176.79468, 12.54240,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(19425, 1961.65161, -2176.79468, 12.54240,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(19425, 1964.95166, -2176.79468, 12.54240,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(19425, 1958.35144, -2189.49463, 12.54240,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(19425, 1961.65161, -2189.49463, 12.54240,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(19425, 1964.95166, -2189.49463, 12.54240,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(8843, 1958.33972, -2180.27002, 12.61176,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(8843, 1964.68115, -2180.26807, 12.55220,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18850, 2031.62390, -2544.38696, 0.34690,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(4100, 1943.53772, -2184.27856, 14.20658,   0.00000, 0.00000, 49.97997);
	CreateDynamicObject(8369, 1963.63965, -2216.48779, 16.78198,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(8041, 2002.34363, -2189.32202, 18.25858,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(8843, 2003.00000, -2184.61792, 12.55220,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(8843, 2003.00000, -2193.82178, 12.55220,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(3629, 1921.52148, -2207.22046, 18.96052,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(3491, 2026.78992, -2294.68286, 20.98215,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(3491, 2026.78992, -2400.22290, 20.98220,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(8240, 1760.23779, -2427.76392, 26.10110,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(8240, 1660.23779, -2427.76392, 26.10110,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(8240, 1560.23779, -2427.76392, 26.10110,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(3491, 1886.21265, -2652.50317, 20.96652,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(3491, 1806.21265, -2652.50317, 20.96650,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(3491, 1726.21265, -2652.50317, 20.96650,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(3491, 1646.21265, -2652.50317, 20.96650,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(3491, 1852.20984, -2429.63745, 20.98220,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(3491, 2112.91870, -2384.62427, 20.99571,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3491, 1454.24060, -2439.75757, 20.96650,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18850, 1941.62390, -2544.38696, 0.34690,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18850, 1985.12390, -2544.38696, 0.34690,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18850, 1821.62390, -2544.38696, 0.34690,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18850, 1691.62390, -2544.38696, 0.34690,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18850, 1755.62390, -2544.38696, 0.34690,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18850, 1611.62390, -2544.38696, 0.34690,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18850, 1511.62390, -2544.38696, 0.34690,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18850, 1561.62390, -2544.38696, 0.34690,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18850, 1461.62390, -2544.38696, 0.34690,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18850, 1411.62390, -2544.38696, 0.34690,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18850, 1561.62390, -2634.38696, 0.34690,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18850, 1511.62390, -2634.38696, 0.34690,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18850, 1461.62390, -2634.38696, 0.34690,   0.00000, 0.00000, 0.00000);

	return 1;
}