#if defined _map_ext_traffic_inc
	#endinput
#endif
#define _map_ext_traffic_inc

#include <YSI_Coding\y_hooks>

hook RemoveMapsBuildings(playerid)
{
    RemoveBuildingForPlayer(playerid, 1315, 2216.828, -1983.375, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2302.585, -1975.351, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2402.507, -1975.484, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2410.765, -1998.804, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2416.664, -1983.867, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2416.664, -2020.968, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2210.929, -1961.210, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2084.531, -1905.492, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2092.914, -1891.375, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2078.632, -1883.328, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2225.210, -1969.250, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2329.546, -1969.968, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2311.226, -1954.453, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2410.765, -1961.703, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2416.664, -1943.328, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2425.046, -2006.843, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2425.046, -1929.203, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2505.437, -1935.148, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2513.695, -1921.359, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2527.976, -1929.398, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2702.906, -2007.296, 15.820, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2717.070, -2015.671, 15.820, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2711.164, -1983.382, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2725.445, -1991.429, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2812.148, -2052.015, 13.375, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2820.335, -2038.593, 13.375, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2535.640, -1738.601, 15.757, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2520.031, -1729.187, 15.757, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2633.281, -1739.757, 13.156, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2645.679, -1723.523, 13.156, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2716.726, -1632.492, 15.484, 0.250);
    RemoveBuildingForPlayer(playerid, 1297, 2843.820, -1661.945, 13.078, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2848.046, -1664.007, 13.125, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2815.757, -1651.851, 13.125, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2716.859, -1514.976, 32.531, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2743.257, -1481.820, 32.531, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2218.789, -1760.007, 15.570, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2221.953, -1725.015, 15.718, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2335.320, -1726.492, 15.781, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2221.835, -1659.117, 17.601, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2235.171, -1659.281, 17.609, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2236.039, -1650.421, 17.601, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2335.382, -1653.289, 15.578, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2350.164, -1664.687, 15.750, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2349.960, -1570.210, 26.132, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2334.960, -1557.796, 26.085, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2347.929, -1516.796, 25.890, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2336.820, -1491.351, 26.289, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2420.578, -1526.968, 26.289, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2436.742, -1496.992, 26.210, 0.250);
    RemoveBuildingForPlayer(playerid, 1297, 2293.195, -1379.273, 26.226, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2295.984, -1378.281, 26.273, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2310.054, -1376.945, 26.242, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2349.804, -1390.046, 26.257, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2364.085, -1378.812, 26.273, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2308.953, -1293.328, 26.210, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2360.648, -1303.593, 26.257, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2373.695, -1268.031, 26.140, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2438.156, -1450.023, 26.265, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2443.664, -1438.828, 26.218, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2458.320, -1262.046, 26.171, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2504.921, -1438.828, 30.773, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2518.578, -1262.148, 37.328, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2376.796, -1249.203, 26.140, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2310.484, -1158.820, 29.195, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2316.367, -1147.429, 29.179, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2382.109, -1167.710, 29.906, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2378.437, -1159.515, 29.804, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2448.703, -1246.054, 26.093, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2458.507, -1189.593, 38.906, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2563.679, -1250.742, 48.218, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2636.453, -1451.851, 32.531, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2648.398, -1397.398, 32.531, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2573.414, -1267.062, 48.273, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2636.781, -1263.851, 52.046, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2648.523, -1248.656, 52.164, 0.250);
    RemoveBuildingForPlayer(playerid, 1297, 2637.890, -1245.765, 52.093, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2716.937, -1265.062, 61.898, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2743.562, -1250.632, 61.898, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2864.140, -1393.187, 13.335, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2578.351, -1189.726, 64.078, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2648.273, -1063.750, 71.757, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2717.007, -1190.648, 71.437, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 1161.589, -1281.359, 15.710, 0.250);
    RemoveBuildingForPlayer(playerid, 1350, 1291.839, -1702.459, 12.250, 0.250);
    RemoveBuildingForPlayer(playerid, 1350, 1286.260, -1718.250, 12.445, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 1335.199, -1731.780, 15.625, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 1345.770, -1740.619, 15.625, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 1357.520, -1732.939, 15.625, 0.250);
    RemoveBuildingForPlayer(playerid, 1226, 1401.760, -1727.670, 16.421, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 1403.369, -1733.010, 15.625, 0.250);
    RemoveBuildingForPlayer(playerid, 1350, 1292.260, -1562.150, 12.460, 0.250);
    RemoveBuildingForPlayer(playerid, 1350, 1286.680, -1577.930, 12.656, 0.250);
    RemoveBuildingForPlayer(playerid, 1350, 760.000, -1591.910, 12.500, 0.250);
    RemoveBuildingForPlayer(playerid, 1350, 756.343, -1596.550, 12.500, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 1411.219, -1872.930, 15.625, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 1405.160, -1871.599, 15.625, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 1952.520, -1751.380, 15.554, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 1962.599, -1823.520, 15.585, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 1972.989, -1811.449, 15.585, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 1960.609, -1802.010, 15.500, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2070.209, -1812.880, 15.601, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2082.270, -1823.910, 15.601, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2080.939, -1800.949, 15.617, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2274.100, -2304.669, 15.812, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 2407.570, -1756.640, 15.781, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2271.770, -1311.819, 26.000, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2260.149, -1301.650, 26.007, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2270.060, -1289.689, 26.015, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2272.149, -1231.479, 26.000, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2269.979, -1209.349, 26.039, 0.250);
    RemoveBuildingForPlayer(playerid, 3385, 235.531, 1819.510, 6.406, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2259.909, -1221.770, 26.023, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2218.270, -1112.520, 27.835, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2200.820, -1127.660, 27.710, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2225.659, -1129.930, 27.843, 0.250);
    RemoveBuildingForPlayer(playerid, 1308, 2185.040, -1184.859, 23.328, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2190.469, -1105.719, 27.070, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2187.060, -1120.810, 26.820, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2176.409, -1132.449, 26.968, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2166.270, -1119.229, 27.570, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2139.300, -1383.579, 26.093, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2092.989, -1604.160, 15.601, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2103.840, -1612.729, 15.601, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 2070.149, -1612.920, 15.609, 0.250);
    RemoveBuildingForPlayer(playerid, 1283, 1962.560, -1974.089, 15.468, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 838.601, -875.890, 70.804, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 1288.030, -598.976, 102.789, 0.250);
    RemoveBuildingForPlayer(playerid, 1315, 889.632, -764.882, 98.062, 0.250);
	return 1;
}

hook LoadMaps()
{
    CreateDynamicObject(19976, 2712.757324, -1189.771484, 68.384498, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2318.367431, -1143.491699, 25.946399, 0.000000, 0.000000, 170.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2259.019287, -1226.500488, 22.818099, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2712.741210, -1264.043579, 58.733299, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2747.739501, -1252.257080, 58.715301, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2747.605468, -1482.408447, 29.432800, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2712.517089, -1514.319580, 29.447900, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2716.935546, -1600.312866, 12.017900, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 2816.873046, -1647.239135, 9.857500, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1350, 2844.698242, -1662.626708, 9.855400, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1315, 2859.750732, -1639.868286, 13.299400, 0.000000, 0.000000, 160.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1315, 2874.308837, -1678.689331, 13.299400, 0.000000, 0.000000, -15.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 2861.852294, -1494.591674, 9.892999, 0.000000, 0.000000, -100.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 2863.968505, -1392.113525, 10.033599, 0.000000, 0.000000, -85.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2858.373291, -1146.106567, 10.080400, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2812.891113, -2055.292968, 10.102700, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2632.814697, -1738.503540, 9.895400, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 2533.492187, -1742.372802, 12.540200, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2352.486328, -1558.206298, 22.996900, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2332.545410, -1569.918823, 23.006299, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2632.736816, -1450.237548, 29.439800, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2652.663330, -1398.694580, 29.443300, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2381.076171, -1250.668212, 22.920200, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1350, 2647.968750, -1266.035888, 48.971401, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1350, 2637.266845, -1245.805664, 48.971401, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1350, 2632.316406, -1262.262207, 48.971401, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1350, 2652.915527, -1249.893310, 48.971401, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 2576.688720, -1266.607177, 45.131500, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 2565.396728, -1246.530029, 45.131500, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2380.916503, -1168.368530, 26.529699, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2376.629882, -1183.382446, 26.529699, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2360.960205, -1306.460449, 22.992399, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2313.437744, -1295.144042, 22.984300, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19971, 2313.437744, -1295.144042, 22.168300, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 2308.889160, -1162.081542, 25.943899, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19971, 2259.019287, -1226.500488, 22.026100, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 2276.593505, -1210.902221, 23.005199, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 2297.919677, -1311.043212, 22.988199, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2260.825439, -1306.410278, 22.981399, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19971, 2260.825439, -1306.410278, 22.165399, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 2276.629150, -1290.439941, 22.977399, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 2276.307861, -1374.243652, 22.971700, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1283, 1294.246215, -1841.990356, 15.562812, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1283, 1287.365600, -1854.060913, 15.562812, 0.000000, 0.000000, 270.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1283, 1319.073486, -1854.060913, 15.562812, 0.000000, 0.000000, 270.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19971, 2314.227294, -1378.544311, 23.054000, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2308.858154, -1162.615112, 25.922700, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2298.399658, -1161.446777, 25.922700, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19971, 2260.858886, -1150.343872, 25.901599, 0.000000, 0.000000, -100.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 2331.922119, -1489.660400, 22.974399, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1350, 2231.581054, -1646.495605, 14.487799, 0.000000, 0.000000, 75.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1352, 2231.430908, -1646.564697, 12.813799, 0.000000, 0.000000, -15.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 2347.913574, -1395.634277, 22.989969, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 2365.731689, -1374.088623, 23.011100, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1857.439941, -1191.114501, 22.728609, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1861.901611, -1335.249511, 12.350600, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1283, 1287.332519, -1715.079101, 15.570610, 0.000000, 0.000000, 270.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1283, 1295.776000, -1699.404296, 15.576875, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1317.999145, -1742.872558, 12.544599, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1306.575317, -1743.428955, 12.538399, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1283, 1295.776000, -1561.754150, 15.576875, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1283, 1314.057373, -1582.735229, 15.576875, 0.000000, 0.000000, 360.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1283, 1322.928710, -1574.154663, 15.576875, 0.000000, 0.000000, 630.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1283, 1287.204833, -1568.754638, 15.576875, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2221.873779, -1759.550659, 12.560299, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 1773.635620, -1459.011718, 12.541999, 0.000000, 0.000000, -20.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2180.880859, -1130.612670, 23.925600, 0.000000, 0.000000, -15.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2213.250732, -1111.963256, 24.784500, 0.000000, 0.000000, -210.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 2185.653564, -1114.632934, 23.842500, 0.000000, 0.000000, -110.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 2223.789062, -1966.837402, 12.547400, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1283, 1965.618774, -1948.323364, 15.710700, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1283, 1963.471069, -1761.997558, 15.554699, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1283, 1952.480224, -1748.505981, 15.554699, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 1972.171386, -1806.912597, 12.548000, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 2091.671875, -1888.867919, 12.547800, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 2071.604492, -1817.841186, 12.543000, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1695.099609, -1602.259887, 12.547320, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1652.578247, -1582.495727, 12.546699, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1663.332031, -1450.833862, 12.547780, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1596.280761, -1430.835083, 12.545200, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1615.266479, -1314.865844, 16.451179, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 1464.883300, -1292.682128, 12.546999, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19976, 1444.691528, -1246.319091, 12.545800, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1476.434204, -1150.891479, 23.073999, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1460.208618, -1170.882202, 22.814720, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1367.652954, -1235.093505, 12.544599, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1283, 1365.342407, -1137.892700, 25.866245, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(1350, 757.151733, -1597.862426, 12.650811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1157.013305, -1130.913085, 22.826700, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 647.731140, -1314.086669, 12.519499, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 617.736145, -1325.177978, 12.811599, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 617.347351, -1597.977539, 15.148200, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 647.448608, -1581.339965, 14.710599, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 88.200843, -1518.939575, 4.087800, 0.000000, 0.000000, 170.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1291.635131, -1842.234008, 12.545499, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1303.100341, -1842.360473, 12.545599, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1352.111938, -1416.715209, 12.604000, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1363.197265, -1415.945678, 12.604000, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1349.291503, -1384.385009, 12.604000, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1337.323730, -1385.032836, 12.604000, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1351.286254, -1291.905883, 12.513099, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1362.877075, -1291.404663, 12.513099, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1348.992675, -1269.943115, 12.513099, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1337.253540, -1270.044311, 12.513099, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1351.439331, -1251.958007, 12.513099, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1363.064575, -1251.818481, 12.513099, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1348.707763, -1230.020019, 12.604000, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1337.371093, -1230.379760, 12.604000, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1351.562622, -1161.389282, 22.787000, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1362.635986, -1161.389282, 22.787000, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1336.843872, -1130.593627, 22.825700, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1347.863769, -1130.930541, 22.825700, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1291.597412, -1562.296386, 12.546099, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1303.182861, -1562.343994, 12.546099, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1318.205810, -1582.452880, 12.546099, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1306.733520, -1582.462158, 12.546099, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1291.589233, -1702.198364, 12.545399, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1303.280761, -1702.222412, 12.545399, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1363.040161, -1049.792602, 25.658199, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1359.191162, -1021.943237, 25.658199, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1348.112670, -1021.734252, 25.658199, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1374.040771, -1049.726196, 25.658199, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1371.347778, -961.641784, 33.337898, 0.000000, 0.000000, 170.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1382.239746, -963.262573, 33.337898, 0.000000, 0.000000, 170.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1072.682739, -1275.341796, 12.542599, 0.000000, 0.000000, 90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1043.777832, -1226.274902, 15.890700, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1185.806518, -1723.129638, 12.542519, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1185.800537, -1723.123657, 12.542499, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1178.444702, -1723.123657, 12.542499, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1169.611328, -1842.042480, 12.567099, 0.000000, 0.000000, 180.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1169.611328, -1842.054443, 12.567099, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19967, 1176.611328, -1842.054443, 12.567099, 0.000000, 0.000000, 0.000000, -1, -1, -1, 300.00, 300.00); 
    CreateDynamicObject(19966, 1811.645629, -1837.721679, 12.578599, 0.000000, 0.000000, -90.000000, -1, -1, -1, 300.00, 300.00); 
	return 1;
}