#if defined _map_comisaria_quince_ext_inc
	#endinput
#endif
#define _map_comisaria_quince_ext_inc

#include <YSI_Coding\y_hooks>

hook RemoveMapsBuildings(playerid) 
{
    RemoveBuildingForPlayer(playerid, 4117, 1375.270, -1452.400, 23.039, 0.250);
    RemoveBuildingForPlayer(playerid, 4119, 1375.270, -1452.400, 23.039, 0.250);
    RemoveBuildingForPlayer(playerid, 4120, 1364.199, -1491.599, 25.601, 0.250);
    RemoveBuildingForPlayer(playerid, 1231, 1263.300, -1520.150, 15.195, 0.250);
    RemoveBuildingForPlayer(playerid, 4221, 1406.709, -1499.560, 69.156, 0.250);

    return 1;
}

hook LoadMaps()
{
    new tmpobjid;
    tmpobjid = CreateDynamicObject(4018, 1373.795532, -1449.437988, 12.466670, 0.000000, 0.000000, -90.500076, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 10101, "2notherbuildsfe", "Bow_Abpave_Gen", 0x00000000);
    SetDynamicObjectMaterial(tmpobjid, 2, 6102, "gazlaw1", "law_gazgrn7", 0x00000000);
    SetDynamicObjectMaterial(tmpobjid, 3, 3603, "bevmans01_la", "sjmlahus21", 0x00000000);
    SetDynamicObjectMaterial(tmpobjid, 4, 14634, "blindinglite", "ws_volumetriclight", 0x00000000);
    SetDynamicObjectMaterial(tmpobjid, 5, 13059, "ce_fact03", "sw_garwind", 0x00000000);
    tmpobjid = CreateDynamicObject(19479, 1369.799194, -1435.766967, 19.392820, 0.000000, 0.000000, 179.499984, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterialText(tmpobjid, 0, "POLICIA FEDERAL ARGENCHOLINA", 130, "Ariel", 34, 1, 0xFF233E99, 0x00000000, 1);
    tmpobjid = CreateDynamicObject(19479, 1369.771240, -1437.129760, 15.650592, 0.000000, 0.000000, 179.500015, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterialText(tmpobjid, 0, "COMISAR�A DE DETENCI�N PRIMARIA N�15", 130, "Ariel", 25, 1, 0xFFFFFFFF, 0x00000000, 0);
    tmpobjid = CreateDynamicObject(19479, 1369.682617, -1447.456665, 14.428631, 0.000000, 0.000000, 179.499969, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterialText(tmpobjid, 0, "GOBIERNO DE MALOS AIRES", 130, "Ariel", 14, 1, 0xFF000000, 0x00000000, 0);
    tmpobjid = CreateDynamicObject(19375, 1371.395874, -1420.660888, 12.467073, 0.000000, 90.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1371.395874, -1430.290161, 12.466873, 0.000000, 90.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1371.395874, -1439.919433, 12.466873, 0.000000, 90.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1371.055541, -1444.191162, 12.467073, 0.000000, 90.000000, -3.599997, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1370.450195, -1453.800903, 12.467073, 0.000000, 90.000000, -3.599997, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1381.777832, -1420.660888, 12.466873, 0.000000, 90.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1381.777832, -1430.289672, 12.466873, 0.000000, 90.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1381.777832, -1439.889526, 12.466873, 0.000000, 90.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1381.777832, -1449.519409, 12.466873, 0.000000, 90.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1380.686279, -1457.586669, 12.467473, 0.000000, 90.000000, -15.399996, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1378.130126, -1466.870361, 12.467269, 0.000000, 90.000000, -15.399996, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1375.575683, -1476.145385, 12.466873, 0.000000, 90.000000, -15.399996, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1369.399414, -1456.346923, 12.467473, 0.000000, 90.000000, -13.699999, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1367.119750, -1465.691772, 12.466873, 0.000000, 90.000000, -13.699999, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1364.754638, -1475.014770, 12.466873, 0.000000, 90.000000, -15.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1363.295166, -1480.469726, 12.467073, 0.000000, 90.000000, -15.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1373.610717, -1483.271972, 12.467073, 0.000000, 90.000000, -15.399996, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1370.582641, -1482.437133, 12.466873, 0.000000, 90.000000, -15.099998, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19479, 1375.580932, -1469.757568, 12.608599, 0.000000, 90.000000, -14.999997, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4835, "airoads_las", "grassdry_128HV", 0x00000000);
    tmpobjid = CreateDynamicObject(19479, 1374.014160, -1475.600219, 12.628602, 0.000000, 90.000000, -14.999997, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4835, "airoads_las", "grassdry_128HV", 0x00000000);
    tmpobjid = CreateDynamicObject(19479, 1369.805664, -1468.209594, 12.588602, 0.000000, 90.000000, -14.999997, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4835, "airoads_las", "grassdry_128HV", 0x00000000);
    tmpobjid = CreateDynamicObject(19479, 1368.245361, -1474.034790, 12.598602, 0.000000, 90.000000, -14.999997, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4835, "airoads_las", "grassdry_128HV", 0x00000000);
    tmpobjid = CreateDynamicObject(19375, 1376.357910, -1457.736938, 12.466873, 0.000000, 90.000000, -15.099998, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4593, "buildblk55", "sl_plazatile01", 0x00000000);
    tmpobjid = CreateDynamicObject(19482, 1367.583618, -1494.882934, 13.856884, 0.000000, 0.000000, 75.100074, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 4227, "graffiti_lan01", "cleargraf01_LA", 0x00000000);
    tmpobjid = CreateObject(18765, 1379.684814, -1450.999145, 26.188270, 0.000000, 0.000000, -90.500076); 
    SetObjectMaterial(tmpobjid, 0, 17606, "lae2roadscoast", "concretedust2_256128", 0xFFFFFFFF);
    SetObjectMaterial(tmpobjid, 15, 2538, "cj_ss_2", "CJ_milk", 0x00000000);
    tmpobjid = CreateObject(18765, 1379.770996, -1441.000488, 26.188270, 0.000000, 0.000000, -90.500076); 
    SetObjectMaterial(tmpobjid, 0, 17606, "lae2roadscoast", "concretedust2_256128", 0xFFFFFFFF);
    SetObjectMaterial(tmpobjid, 15, 2538, "cj_ss_2", "CJ_milk", 0x00000000);
    tmpobjid = CreateObject(18765, 1379.857666, -1431.012329, 26.188270, 0.000000, 0.000000, -90.500076); 
    SetObjectMaterial(tmpobjid, 0, 17606, "lae2roadscoast", "concretedust2_256128", 0xFFFFFFFF);
    SetObjectMaterial(tmpobjid, 15, 2538, "cj_ss_2", "CJ_milk", 0x00000000);
    tmpobjid = CreateObject(18765, 1379.907226, -1425.398559, 26.186210, 0.000000, 0.000000, -90.500076); 
    SetObjectMaterial(tmpobjid, 0, 17606, "lae2roadscoast", "concretedust2_256128", 0xFFFFFFFF);
    SetObjectMaterial(tmpobjid, 15, 2538, "cj_ss_2", "CJ_milk", 0x00000000);
    tmpobjid = CreateDynamicObject(3934, 1379.879272, -1437.591186, 28.688270, 0.000000, 0.000000, -0.500074, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, -1, "none", "none", 0xFFFFFFFF);
    tmpobjid = CreateDynamicObject(3934, 1379.950195, -1429.519165, 28.688270, 0.000000, 0.000000, -0.500074, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, -1, "none", "none", 0xFFFFFFFF);
    tmpobjid = CreateDynamicObject(16096, 1379.977783, -1450.875610, 30.128273, 0.000000, 0.000000, 270.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, -1, "none", "none", 0xFFFFFFFF);
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    tmpobjid = CreateDynamicObject(970, 1368.026245, -1462.218505, 13.096878, 0.000000, 0.000000, 74.899948, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1366.945556, -1466.224731, 13.096878, 0.000000, 0.000000, 74.899948, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1365.862060, -1470.240966, 13.096878, 0.000000, 0.000000, 74.899948, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1364.776123, -1474.267822, 13.096878, 0.000000, 0.000000, 74.899948, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1363.689453, -1478.293945, 13.096878, 0.000000, 0.000000, 74.899948, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1365.160400, -1480.864379, 13.096878, 0.000000, 0.000000, 164.799972, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1369.175048, -1481.955566, 13.096878, 0.000000, 0.000000, 164.799972, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1373.189208, -1483.046752, 13.096878, 0.000000, 0.000000, 164.799972, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1375.796752, -1481.580078, 13.096878, 0.000000, 0.000000, -105.000007, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1376.875488, -1477.551879, 13.096878, 0.000000, 0.000000, -105.000007, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1377.955200, -1473.523559, 13.096878, 0.000000, 0.000000, -105.000007, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1379.034545, -1469.495483, 13.096878, 0.000000, 0.000000, -105.000007, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1380.105834, -1465.496582, 13.096878, 0.000000, 0.000000, -105.000007, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1370.591918, -1460.755981, 13.096878, 0.000000, 0.000000, -14.900044, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1374.612426, -1461.825805, 13.096878, 0.000000, 0.000000, -14.900044, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1378.612426, -1462.890136, 13.096878, 0.000000, 0.000000, -14.900044, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(736, 1370.076171, -1462.772705, 23.596849, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(736, 1372.497070, -1481.523071, 23.596849, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(736, 1363.946777, -1479.852783, 23.596849, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(672, 1376.144531, -1465.454467, 13.456658, 0.000000, 0.000000, 116.400016, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(669, 1369.674804, -1472.538818, 12.553705, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1280, 1372.071166, -1483.159545, 12.956885, 0.000000, 0.000000, 75.499984, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1280, 1366.011108, -1481.590087, 12.956885, 0.000000, 0.000000, 75.499984, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1280, 1363.586303, -1477.221069, 12.956885, 0.000000, 0.000000, -14.500020, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1280, 1365.783691, -1469.132446, 12.956885, 0.000000, 0.000000, -14.500020, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1280, 1367.355346, -1463.145263, 12.956885, 0.000000, 0.000000, -14.500020, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19967, 1387.006958, -1448.678833, 12.454686, 0.000000, 0.000000, 154.499969, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1424, 1392.719970, -1450.694824, 13.084688, 0.000000, 0.000000, 159.100051, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1424, 1390.060668, -1449.401855, 13.084688, 0.000000, 0.000000, 153.699996, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1359, 1369.087402, -1482.411254, 13.272813, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1359, 1364.419921, -1473.047485, 12.962815, 0.000000, 90.000000, -142.400054, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1359, 1366.664672, -1465.277465, 13.232811, 0.000000, 0.000000, -13.899997, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(2672, 1364.067993, -1473.253173, 12.842815, 0.000000, 0.000000, 175.800094, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1345, 1386.533569, -1419.617553, 13.312442, 0.000000, 0.000000, 450.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1297, 1372.617675, -1410.343261, 15.890610, 0.000000, 0.000000, 270.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1297, 1385.870483, -1410.343261, 15.890610, 0.000000, 0.000000, 270.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1297, 1392.061889, -1425.736206, 15.890610, 0.000000, 0.000000, 540.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1297, 1395.744140, -1445.768188, 15.890610, 0.000000, 0.000000, -93.399909, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1297, 1427.063354, -1445.546264, 15.890610, 0.000000, 0.000000, 630.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1297, 1425.742675, -1423.003051, 15.890610, 0.000000, 0.000000, 720.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1297, 1425.742675, -1405.182739, 15.890610, 0.000000, 0.000000, 720.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1297, 1414.729736, -1390.632446, 15.890610, 0.000000, 0.000000, 810.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1297, 1396.895629, -1390.632446, 15.890610, 0.000000, 0.000000, 810.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1297, 1380.782348, -1390.632446, 15.890610, 0.000000, 0.000000, 810.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1231, 1379.336059, -1469.564086, 15.252806, 0.000000, 0.000000, -105.199943, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1231, 1377.149169, -1477.620239, 15.252806, 0.000000, 0.000000, -105.199943, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1231, 1375.389648, -1483.861694, 15.252806, 0.000000, 0.000000, -159.199981, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1231, 1362.956176, -1480.414916, 15.252806, 0.000000, 0.000000, 121.400054, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1231, 1364.486572, -1474.168334, 15.252806, 0.000000, 0.000000, 75.600059, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1231, 1366.553710, -1466.111328, 15.252806, 0.000000, 0.000000, 75.600059, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1231, 1368.287353, -1459.959228, 15.252806, 0.000000, 0.000000, 31.600072, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1231, 1380.814086, -1463.369384, 15.252806, 0.000000, 0.000000, -48.299934, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(3526, 1375.334960, -1441.526489, 28.758260, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(3526, 1375.334960, -1433.545654, 28.758260, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(3526, 1375.334960, -1425.484619, 28.758260, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(3526, 1384.476074, -1425.474609, 28.758260, 0.000000, 0.000000, 180.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(3526, 1384.385986, -1433.715942, 28.758260, 0.000000, 0.000000, 180.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(3526, 1384.355957, -1441.766235, 28.758260, 0.000000, 0.000000, 180.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19868, 1384.505126, -1453.318725, 28.688270, 0.000000, 0.000000, 270.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19868, 1384.505126, -1448.058837, 28.688270, 0.000000, 0.000000, 270.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19868, 1375.082641, -1448.058837, 28.688270, 0.000000, 0.000000, 450.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19868, 1375.082641, -1453.320190, 28.688270, 0.000000, 0.000000, 450.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19868, 1381.864501, -1455.939941, 28.688270, 0.000000, 0.000000, 540.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19868, 1377.755859, -1455.939941, 28.688270, 0.000000, 0.000000, 540.000000, -1, -1, -1, 250.00, 250.00); 

    return 1;
}