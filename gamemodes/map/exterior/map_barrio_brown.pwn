#if defined _map_ext_barrio_brown_inc
	#endinput
#endif
#define _map_ext_barrio_brown_inc

#include <YSI_Coding\y_hooks>

hook RemoveMapsBuildings(playerid)
{
	RemoveBuildingForPlayer(playerid, 3695, 2239.929, -1790.695, 17.007, 0.250);
	RemoveBuildingForPlayer(playerid, 3695, 2282.992, -1790.695, 17.007, 0.250);
	RemoveBuildingForPlayer(playerid, 3695, 2314.820, -1790.695, 17.007, 0.250);
	RemoveBuildingForPlayer(playerid, 3695, 2352.718, -1790.695, 17.007, 0.250);
	RemoveBuildingForPlayer(playerid, 3695, 2387.820, -1790.695, 17.007, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2229.046, -1810.031, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2226.164, -1807.328, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2226.164, -1801.867, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 620, 2234.484, -1817.929, 12.093, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2226.164, -1791.000, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2226.164, -1796.453, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 3584, 2239.929, -1790.695, 17.007, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2226.164, -1775.507, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2226.164, -1780.984, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2228.671, -1767.273, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2226.164, -1770.046, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1307, 2232.515, -1766.054, 12.750, 0.250);
	RemoveBuildingForPlayer(playerid, 1307, 2249.867, -1815.414, 12.750, 0.250);
	RemoveBuildingForPlayer(playerid, 669, 2254.726, -1827.437, 12.562, 0.250);
	RemoveBuildingForPlayer(playerid, 620, 2258.343, -1804.742, 12.093, 0.250);
	RemoveBuildingForPlayer(playerid, 645, 2259.265, -1773.242, 11.125, 0.250);
	RemoveBuildingForPlayer(playerid, 17886, 2264.039, -1789.257, 20.773, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2265.296, -1791.000, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2265.296, -1796.453, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2265.296, -1807.328, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2265.296, -1801.867, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2265.296, -1770.046, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2265.296, -1775.507, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2265.296, -1780.984, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 620, 2275.390, -1820.726, 12.093, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2268.187, -1810.031, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2273.695, -1810.031, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 3584, 2282.992, -1790.695, 17.007, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2267.812, -1767.273, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2273.335, -1767.343, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 620, 2271.648, -1772.398, 8.351, 0.250);
	RemoveBuildingForPlayer(playerid, 645, 2285.757, -1762.125, 12.289, 0.250);
	RemoveBuildingForPlayer(playerid, 1226, 2297.898, -1793.820, 16.421, 0.250);
	RemoveBuildingForPlayer(playerid, 620, 2297.382, -1798.539, 8.351, 0.250);
	RemoveBuildingForPlayer(playerid, 620, 2297.148, -1775.875, 8.351, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2305.062, -1810.031, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2302.171, -1807.328, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2302.171, -1801.867, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2302.171, -1791.000, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2302.171, -1796.453, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 3584, 2314.820, -1790.695, 17.007, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2302.171, -1775.507, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2302.171, -1770.046, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2302.171, -1780.984, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2304.781, -1767.382, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1307, 2322.648, -1815.414, 12.750, 0.250);
	RemoveBuildingForPlayer(playerid, 645, 2332.828, -1817.710, 12.117, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2341.757, -1810.031, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2338.867, -1807.328, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 620, 2341.757, -1817.726, 8.359, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2338.867, -1801.867, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2338.867, -1791.000, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2338.867, -1796.453, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 620, 2334.710, -1785.062, 12.093, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2338.867, -1775.507, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2338.867, -1780.984, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 17887, 2343.609, -1784.507, 20.312, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2338.867, -1770.046, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2341.382, -1767.273, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 669, 2329.187, -1765.523, 12.437, 0.250);
	RemoveBuildingForPlayer(playerid, 673, 2349.617, -1763.343, 11.632, 0.250);
	RemoveBuildingForPlayer(playerid, 3584, 2352.718, -1790.695, 17.007, 0.250);
	RemoveBuildingForPlayer(playerid, 620, 2367.648, -1802.796, 8.359, 0.250);
	RemoveBuildingForPlayer(playerid, 620, 2367.648, -1780.773, 11.046, 0.250);
	RemoveBuildingForPlayer(playerid, 620, 2378.335, -1818.726, 8.359, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2374.101, -1800.468, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2374.101, -1805.929, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2376.992, -1813.929, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2374.101, -1811.382, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2374.101, -1780.984, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2374.101, -1789.601, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2374.101, -1795.054, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2376.617, -1767.273, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2374.101, -1770.046, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 1408, 2374.101, -1775.507, 13.156, 0.250);
	RemoveBuildingForPlayer(playerid, 645, 2399.976, -1815.992, 11.890, 0.250);
	RemoveBuildingForPlayer(playerid, 3584, 2387.820, -1790.695, 17.007, 0.250);
	RemoveBuildingForPlayer(playerid, 673, 2398.578, -1782.773, 10.703, 0.250);
	RemoveBuildingForPlayer(playerid, 645, 2387.023, -1763.640, 12.179, 0.250);
	RemoveBuildingForPlayer(playerid, 17518, 2361.937, -1699.937, 15.921, 0.250);
	RemoveBuildingForPlayer(playerid, 1307, 2295.703, -1742.195, 12.750, 0.250);
	RemoveBuildingForPlayer(playerid, 1307, 2331.265, -1742.414, 12.750, 0.250);
	RemoveBuildingForPlayer(playerid, 1307, 2364.250, -1742.117, 12.750, 0.250);
	RemoveBuildingForPlayer(playerid, 1307, 2403.289, -1741.742, 12.750, 0.250);
	return 1;
}

hook LoadMaps()
{
	// OBJETOS QUE NECESITAN STREAM CONSTANTE
	CreateDynamicObject(3436, 2251.675537, -1786.548095, 18.142099, 0.000000, 0.000000, 90.000000, .streamdistance = -1.0, .drawdistance = 450.0); // EDIFICIO
	CreateDynamicObject(3436, 2283.514892, -1786.577026, 18.125759, 0.000000, 0.000000, 90.000000, .streamdistance = -1.0, .drawdistance = 450.0); // EDIFICIO
	Textura = CreateDynamicObject(3469, 2348.370361, -1802.396728, 15.635700, 0.000000, 0.000000, 180.00000, .streamdistance = -1.0, .drawdistance = 450.0); // EDIFICIO
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	Textura = CreateDynamicObject(3469, 2322.727294, -1765.405029, 15.635700, 0.000000, 0.000000, -90.000000, .streamdistance = -1.0, .drawdistance = 450.0); // EDIFICIO
	SetDynamicObjectMaterial(Textura, 1, -1, "none", "none", 0xFFFFFFFF);
	Textura = CreateDynamicObject(12947, 2353.792968, -1763.973754, 12.536720, 0.000000, 0.000000, 0.000000, .streamdistance = -1.0, .drawdistance = 450.0); // EDIFICIO DE NEGOCIO
	SetDynamicObjectMaterial(Textura, 0, 9496, "boxybld_sfw", "ws_altz_wall8_bot", 0xFFFFFFFF);
	SetDynamicObjectMaterial(Textura, 5, 13363, "cephotoblockcs_t", "sw_wall_05", 0x00000000);
	SetDynamicObjectMaterial(Textura, 6, 5731, "melrose15_lawn", "2winsmel_law", 0x00000000);
	SetDynamicObjectMaterial(Textura, 8, 14742, "mp3", "GB_rapposter02", 0x00000000);
	SetDynamicObjectMaterial(Textura, 9, 8065, "vgsswrehse03", "vgswrhsign01", 0x00000000);
	Textura = CreateDynamicObject(12938, 2381.973144, -1807.144042, 16.439540, 0.000000, 0.000000, -90.000000, .streamdistance = -1.0, .drawdistance = 450.0); // EDIFICIO
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	SetDynamicObjectMaterial(Textura, 3, 1413, "break_f_mesh", "CJ_CORRIGATED", 0x00000000);
	SetDynamicObjectMaterial(Textura, 9, 17298, "weefarmcuntw", "sjmbigold3", 0xFFFFDDDD);
	SetDynamicObjectMaterial(Textura, 10, -1, "none", "none", 0xFFF8D3D9);
	Textura = CreateDynamicObject(12938, 2379.922119, -1772.695068, 16.438999, 0.000015, 0.000000, 90.000000, .streamdistance = -1.0, .drawdistance = 450.0); // EDIFICIO
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	SetDynamicObjectMaterial(Textura, 3, 1413, "break_f_mesh", "CJ_CORRIGATED", 0x00000000);
	SetDynamicObjectMaterial(Textura, 9, 17298, "weefarmcuntw", "sjmbigold3", 0xFFFFDDDD);
	SetDynamicObjectMaterial(Textura, 10, -1, "none", "none", 0xFFF8D3D9);
	CreateDynamicObject(714, 2400.386474, -1762.437500, 11.833334, 0.000000, 0.000000, 0.000000, .streamdistance = -1.0, .drawdistance = 550.0); // EDIFICIO
	CreateDynamicObject(709, 2317.426757, -1792.773803, 6.001638, 0.000000, 0.000000, 57.540000, .streamdistance = -1.0, .drawdistance = 550.0); // EDIFICIO

	//////////////////////////////////////////////////////////////////////////////////////////////////

	Textura = CreateDynamicObject(3459, 2271.423095, -1773.203002, 18.856260, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	SetDynamicObjectMaterial(Textura, 3, -1, "none", "none", 0x00FFFFFF);
	SetDynamicObjectMaterial(Textura, 4, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(19545, 2245.702148, -1790.620361, 12.578418, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0xFFFFFFFF);
	Textura = CreateDynamicObject(10671, 2322.262451, -1758.694946, 13.932808, 0.000000, 0.000000, -90.500000, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 0, 6282, "beafron2_law2", "shutter03LA", 0x00000000);
	Textura = CreateDynamicObject(16317, 2318.468750, -1812.941162, 10.669440, 0.000000, 0.000000, -61.020000, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 0, 4835, "airoads_las", "grassdry_128HV", 0x00000000);
	Textura = CreateDynamicObject(16305, 2315.197021, -1807.956298, 14.700058, 0.000000, 10.000000, -19.620000, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 0, 4835, "airoads_las", "grassdry_128HV", 0x00000000);
	Textura = CreateDynamicObject(16653, 2266.196044, -1776.366943, 20.457239, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 0, 3049, "qrydrx", "Was_scrpyd_barbwire", 0x00000000);
	Textura = CreateDynamicObject(10671, 2310.161865, -1758.669555, 13.932808, 0.000000, 0.000000, -90.599998, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 0, 6404, "beafron1_law2", "shutter02LA", 0x00000000);
	Textura = CreateDynamicObject(10671, 2334.352783, -1758.690185, 13.932808, 0.000000, 0.000000, -90.500000, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 0, 5461, "glenpark6d_lae", "shutter01LA", 0x00000000);
	Textura = CreateDynamicObject(5150, 2286.185546, -1801.245117, 23.720819, 0.000000, 0.000000, -171.799987, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 1, -1, "none", "none", 0x00FFFFFF);
	SetDynamicObjectMaterial(Textura, 2, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(1631, 2366.262207, -1816.370117, 13.588550, 0.000000, 0.000000, -75.700012, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 0, 5122, "ground3_las2", "fossiloil_128", 0xFFFFFFFF);
	Textura = CreateDynamicObject(3459, 2296.783447, -1799.332763, 19.766248, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	SetDynamicObjectMaterial(Textura, 3, -1, "none", "none", 0x00FFFFFF);
	SetDynamicObjectMaterial(Textura, 4, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(3459, 2246.761718, -1821.812255, 13.106263, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	SetDynamicObjectMaterial(Textura, 3, -1, "none", "none", 0x00FFFFFF);
	SetDynamicObjectMaterial(Textura, 4, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(3459, 2268.768554, -1820.832153, 13.106263, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	SetDynamicObjectMaterial(Textura, 3, -1, "none", "none", 0x00FFFFFF);
	SetDynamicObjectMaterial(Textura, 4, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(3459, 2266.396972, -1818.051635, 11.026261, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	SetDynamicObjectMaterial(Textura, 3, -1, "none", "none", 0x00FFFFFF);
	SetDynamicObjectMaterial(Textura, 4, -1, "none", "none", 0x00FFFFFF);
	Textura = CreateDynamicObject(3459, 2344.762939, -1775.672973, 12.836246, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	SetDynamicObjectMaterial(Textura, 0, -1, "none", "none", 0x00FFFFFF);
	SetDynamicObjectMaterial(Textura, 3, -1, "none", "none", 0x00FFFFFF);
	SetDynamicObjectMaterial(Textura, 4, -1, "none", "none", 0x00FFFFFF);
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	CreateDynamicObject(16061, 2259.205322, -1829.405029, 12.647338, 0.000000, 0.000000, 96.000007, -1, -1, -1, 300.00, 300.00);
	CreateDynamicObject(16061, 2310.655517, -1820.784790, 12.647338, 0.000000, 0.000000, 89.340011, -1, -1, -1, 300.00, 300.00);
	CreateDynamicObject(3473, 2348.370361, -1802.396728, 15.635700, 0.000000, -0.000007, 179.999954, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(3473, 2322.727294, -1765.405029, 15.635700, 0.000000, 0.000000, -90.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1307, 2300.705322, -1746.088989, 12.257501, 0.000000, 0.000000, 31.900003, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1307, 2327.694091, -1739.459838, 9.316728, 0.000000, 13.899997, 15.500000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1306, 2381.148193, -1784.372558, 20.004999, 0.000000, 0.000000, -75.900001, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1535, 2274.088134, -1791.170898, 12.067628, 0.000007, 0.000000, 89.999977, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(669, 2233.614990, -1761.509887, 12.668978, 0.000000, 0.000000, -93.660003, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(19456, 2341.007812, -1758.714477, 13.795298, 0.000000, 0.000000, 90.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1535, 2292.965820, -1808.651489, 12.067628, 0.000000, 0.000000, 90.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1535, 2292.965820, -1803.744018, 12.067600, 0.000000, 0.000000, 90.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1535, 2292.965820, -1795.903076, 12.067600, 0.000000, 0.000000, 90.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1535, 2292.965820, -1790.802124, 12.067600, 0.000000, 0.000000, 90.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1535, 2292.965820, -1783.922119, 12.067600, 0.000000, 0.000000, 90.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1535, 2292.965820, -1772.312133, 12.067600, 0.000000, 0.000000, 90.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(833, 2320.397460, -1789.347656, 13.262648, 0.000000, 0.000000, 52.139991, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(837, 2324.237060, -1790.113159, 13.004380, 0.000000, 0.000000, 68.939979, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1368, 2331.968261, -1797.649902, 13.204998, 0.000000, 0.000000, 180.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1368, 2325.920654, -1797.748168, 13.204998, 0.000000, 0.000000, 180.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(829, 2328.885986, -1793.140991, 12.958848, 0.000000, 0.000000, 42.840000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(832, 2329.750732, -1787.924194, 12.471320, 0.000000, 0.000000, 263.099975, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(852, 2334.300781, -1775.765625, 12.422418, 0.000000, 0.000000, 89.880012, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(673, 2334.523925, -1775.539306, 12.632510, 356.858398, 0.000000, -40.728939, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(760, 2340.610595, -1787.286376, 12.392008, 0.000000, 0.000000, -63.480010, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(760, 2328.734619, -1798.912353, 12.392008, 0.000000, 0.000000, 26.099990, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(800, 2320.085205, -1798.019531, 13.908948, 0.000000, 0.000000, 2.460000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(800, 2315.508300, -1798.899414, 13.908948, 0.000000, 0.000000, -23.940000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(5088, 2292.109863, -1761.766479, 21.178720, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(5844, 2314.350097, -1787.233886, 12.773940, 0.000000, 0.000000, 49.980049, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1223, 2323.921142, -1797.116699, 12.525259, 0.000000, 0.000000, 41.880001, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1223, 2323.528076, -1783.707885, 12.525300, 0.000000, 0.000000, -90.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1223, 2310.619384, -1787.392578, 12.525300, 0.000000, 0.000000, 90.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1368, 2306.665283, -1783.608520, 13.204998, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1368, 2313.868164, -1787.619384, 13.204998, 0.000000, 0.000000, 180.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(669, 2303.757568, -1789.229980, 12.222610, 0.000000, 0.000000, 70.079986, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(13450, 2380.950195, -1789.921386, 15.026888, 0.000015, 0.000000, 89.999954, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(800, 2321.122070, -1809.063720, 12.208948, 0.000000, 0.000000, 45.259983, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1438, 2364.046142, -1812.790405, 12.546873, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1440, 2346.400390, -1765.699829, 13.034399, 0.000000, 0.000000, -65.800003, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(3594, 2318.083740, -1780.878417, 13.166875, 0.000000, 0.000000, 94.499992, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(3594, 2315.009033, -1779.593261, 13.848021, -14.199999, 0.000000, 46.800003, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1307, 2254.731445, -1754.912475, 12.158111, 0.099997, -6.099998, 45.100009, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1307, 2340.997558, -1780.593383, 13.523182, -9.699997, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1307, 2271.728271, -1791.393554, 12.810325, 12.200000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1307, 2268.072509, -1833.335449, 11.797169, 0.000000, 0.000000, 34.999996, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(17886, 2263.906494, -1789.153198, 20.689416, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1307, 2298.340820, -1829.991333, 12.631606, 0.000000, 0.000000, 49.700004, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1344, 2338.385253, -1821.027465, 13.324117, 0.000000, 0.000000, -121.599998, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1358, 2328.899658, -1824.076416, 13.744791, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1481, 2380.976806, -1808.124389, 13.222486, 0.000000, 0.000000, 180.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(3694, 2395.698730, -1816.959716, 12.899154, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(17887, 2343.474365, -1784.378051, 20.219806, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1307, 2380.482666, -1804.361938, 20.018892, 180.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1535, 2274.088134, -1786.263427, 12.067600, 0.000007, 0.000000, 89.999977, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1281, 2378.718750, -1791.133789, 13.359341, 0.000000, 0.000000, -40.799991, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1281, 2378.634521, -1798.775024, 13.359341, 0.000000, 0.000000, -40.799991, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1307, 2230.065673, -1762.994995, 12.916646, -0.599999, 0.000000, 12.499972, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1307, 2251.303466, -1820.244140, 12.361853, 0.000000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1307, 2248.962890, -1813.927856, 12.430601, 11.700000, 0.000000, 0.000000, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1307, 2321.817138, -1816.443237, 12.177459, 0.000000, 0.000000, -29.300001, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1307, 2312.067382, -1812.649291, 12.977466, 0.000000, 0.000000, 3.399998, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1535, 2261.119140, -1791.260986, 12.067628, 0.000030, 0.000000, 89.999908, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1535, 2261.119140, -1786.353515, 12.067600, 0.000030, 0.000000, 89.999908, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1535, 2242.270507, -1789.210449, 12.067628, 0.000038, 0.000000, 89.999885, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1535, 2242.270507, -1784.302978, 12.067600, 0.000038, 0.000000, 89.999885, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1535, 2242.270507, -1772.590576, 12.067628, 0.000045, 0.000000, 89.999862, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1535, 2242.270507, -1767.683105, 12.067600, 0.000045, 0.000000, 89.999862, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(16410, 2287.524169, -1823.183837, 12.807964, 0.000000, 0.000000, -55.699996, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1438, 2360.250488, -1781.780517, 12.529273, 0.000000, 0.000000, -118.900001, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1440, 2294.164306, -1811.094726, 13.066482, 0.000000, 0.000000, 94.099983, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1441, 2261.547119, -1811.249389, 13.190574, 0.000000, 0.000000, 89.599990, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(3006, 2251.531982, -1834.369506, 12.340790, 0.000000, 0.000000, 21.299999, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1712, 2341.976562, -1765.738525, 12.536872, 0.000000, 0.000000, 66.900016, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(837, 2332.350097, -1806.445190, 12.956613, 0.000000, 0.000000, -78.800018, -1, -1, -1, 150.00, 150.00);
	CreateDynamicObject(1307, 2232.26318, -1789.99207, 19.05210, 0.00000, 180.00000, 0.00000, -1, -1, -1, 150.00, 150.00);
	return 1;
}