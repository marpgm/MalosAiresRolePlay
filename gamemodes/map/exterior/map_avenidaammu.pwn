#if defined _map_avenidaammu_inc
	#endinput
#endif
#define _map_avenidaammu_inc

#include <YSI_Coding\y_hooks>

hook RemoveMapsBuildings(playerid) //en caso de no tener Removes, Borrar este hook
{   
    RemoveBuildingForPlayer(playerid, 713, 1304.770, -1839.869, 12.437, 0.250);
    RemoveBuildingForPlayer(playerid, 713, 1304.770, -1808.489, 12.437, 0.250);
    RemoveBuildingForPlayer(playerid, 713, 1304.770, -1780.109, 12.437, 0.250);
    RemoveBuildingForPlayer(playerid, 713, 1304.770, -1729.939, 12.437, 0.250);
    RemoveBuildingForPlayer(playerid, 713, 1304.770, -1753.589, 12.437, 0.250);
    RemoveBuildingForPlayer(playerid, 620, 1304.119, -1696.800, 12.289, 0.250);
    RemoveBuildingForPlayer(playerid, 620, 1305.670, -1669.900, 12.585, 0.250);
    RemoveBuildingForPlayer(playerid, 620, 1305.560, -1641.530, 12.289, 0.250);
    RemoveBuildingForPlayer(playerid, 620, 1305.560, -1614.829, 12.289, 0.250);
    RemoveBuildingForPlayer(playerid, 620, 1305.670, -1583.699, 12.585, 0.250);
    RemoveBuildingForPlayer(playerid, 6046, 1305.469, -1619.739, 13.398, 0.250);
    RemoveBuildingForPlayer(playerid, 6253, 1305.469, -1619.739, 13.398, 0.250);
	return 1;
}

hook LoadMaps()
{   
    new tmpobjid;
    tmpobjid = CreateDynamicObject(19445, 1305.712768, -1712.380859, 12.470606, 360.000000, 90.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 9514, "711_sfw", "ws_carpark2", 0x00000000);
    tmpobjid = CreateDynamicObject(19445, 1304.201293, -1712.380859, 12.472599, 360.000000, 90.000000, 180.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 9514, "711_sfw", "ws_carpark2", 0x00000000);
    tmpobjid = CreateDynamicObject(19445, 1304.942016, -1712.380859, 12.474598, 360.000000, 90.000000, 180.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 10778, "airportcpark_sfse", "ws_carpark3", 0x00000000);
    tmpobjid = CreateDynamicObject(19445, 1305.712768, -1732.283813, 12.470606, 0.000000, 90.000015, 0.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 9514, "711_sfw", "ws_carpark2", 0x00000000);
    tmpobjid = CreateDynamicObject(19445, 1304.201293, -1732.283813, 12.472599, 0.000000, 89.999984, 179.999908, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 9514, "711_sfw", "ws_carpark2", 0x00000000);
    tmpobjid = CreateDynamicObject(19445, 1304.942016, -1732.283813, 12.474598, 0.000000, 89.999984, 179.999908, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 10778, "airportcpark_sfse", "ws_carpark3", 0x00000000);
    tmpobjid = CreateDynamicObject(19445, 1305.712768, -1572.354248, 12.470606, 0.000000, 90.000022, 0.000000, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 9514, "711_sfw", "ws_carpark2", 0x00000000);
    tmpobjid = CreateDynamicObject(19445, 1304.201293, -1572.354248, 12.472599, 0.000000, 89.999977, 179.999862, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 9514, "711_sfw", "ws_carpark2", 0x00000000);
    tmpobjid = CreateDynamicObject(19445, 1304.942016, -1572.354248, 12.474598, 0.000000, 89.999977, 179.999862, -1, -1, -1, 250.00, 250.00); 
    SetDynamicObjectMaterial(tmpobjid, 0, 10778, "airportcpark_sfse", "ws_carpark3", 0x00000000);
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    tmpobjid = CreateDynamicObject(970, 1304.927856, -1727.435913, 13.076875, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1304.927856, -1737.116577, 13.076875, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1306.987915, -1739.207397, 13.076875, 0.000000, 0.000000, 90.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1306.987915, -1725.337158, 13.076875, 0.000000, 0.000000, 90.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1302.877075, -1725.337158, 13.076875, 0.000000, 0.000000, 90.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1302.877075, -1739.177490, 13.076875, 0.000000, 0.000000, 90.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1304.927856, -1707.543090, 13.076875, 0.000000, 0.000007, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1304.927856, -1717.223754, 13.076875, 0.000000, 0.000007, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1306.987915, -1719.314575, 13.076875, 0.000007, 0.000000, 89.999977, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1306.987915, -1705.444335, 13.076875, 0.000007, 0.000000, 89.999977, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1302.877075, -1705.444335, 13.076875, 0.000007, 0.000000, 89.999977, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1302.877075, -1719.284667, 13.076875, 0.000007, 0.000000, 89.999977, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1304.927856, -1567.529907, 13.076875, 0.000000, 0.000014, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1304.927856, -1577.210571, 13.076875, 0.000000, 0.000014, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1306.987915, -1579.301391, 13.076875, 0.000014, 0.000000, 89.999954, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1306.987915, -1565.431152, 13.076875, 0.000014, 0.000000, 89.999954, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1302.877075, -1565.431152, 13.076875, 0.000014, 0.000000, 89.999954, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1302.877075, -1579.271484, 13.076875, 0.000014, 0.000000, 89.999954, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1350.018310, -1416.290649, 13.076875, 0.000000, 0.000045, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1351.927246, -1418.351440, 13.076875, 0.000068, 0.000000, 85.599807, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1347.784545, -1418.354003, 13.076875, 0.000068, 0.000000, 85.599807, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1304.927856, -1842.466796, 13.076875, 0.000000, 0.000007, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1306.987915, -1840.368041, 13.076875, 0.000007, 0.000000, 89.999977, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(970, 1302.877075, -1840.368041, 13.076875, 0.000007, 0.000000, 89.999977, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19967, 1306.998657, -1737.126953, 12.542817, 0.000000, 0.000000, 90.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19950, 1307.007568, -1727.426391, 12.532814, 0.000000, 0.000000, 90.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19957, 1302.864257, -1737.108398, 12.522815, 0.000000, 0.000000, -90.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19955, 1302.853393, -1727.429321, 12.382811, 0.000000, 0.000000, -90.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19967, 1302.862182, -1707.526489, 12.542817, 0.000000, -0.000007, -90.000007, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19950, 1302.853271, -1717.227050, 12.532814, 0.000000, -0.000007, -90.000007, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19957, 1306.996582, -1707.545043, 12.522815, 0.000000, 0.000007, 89.999938, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19955, 1307.007446, -1717.224121, 12.382811, 0.000000, 0.000007, 89.999938, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19962, 1306.996337, -1577.215454, 12.542816, 0.000000, 0.000000, 90.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19976, 1307.010009, -1567.504028, 12.512812, 0.000000, 0.000000, 90.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19962, 1302.869628, -1567.504028, 12.542816, 0.000000, -0.000007, -90.000007, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(19976, 1302.855957, -1577.215454, 12.512812, 0.000000, -0.000007, -90.000007, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1722.655883, 12.280603, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1744.408203, 12.280603, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1837.149291, 12.280603, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1701.136230, 12.280603, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1718.981567, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1726.142211, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1739.862915, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1705.981933, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1584.096191, 12.280603, 0.000000, 0.000014, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1560.774780, 12.280603, 0.000000, 0.000007, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1578.981079, 12.382811, 0.000000, 0.000014, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1565.620483, 12.382811, 0.000000, 0.000007, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1841.384033, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1290, 1305.016113, -1762.412841, 18.232801, 0.000000, 0.000007, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1290, 1305.016113, -1742.501708, 18.232801, 0.000000, 0.000007, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1290, 1305.016113, -1802.207153, 18.232801, 0.000000, 0.000014, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1290, 1305.016113, -1782.296020, 18.232801, 0.000000, 0.000014, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1290, 1305.016113, -1843.200561, 18.232801, 0.000000, 0.000022, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(1290, 1305.016113, -1823.289428, 18.232801, 0.000000, 0.000022, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1748.753417, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1756.325195, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1761.259643, 12.280603, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1766.226318, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1775.498535, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1780.910522, 12.280603, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1785.759155, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1794.600585, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1800.520996, 12.280603, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1805.701293, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1814.441894, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1819.791381, 12.280603, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1824.972167, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1832.742553, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1695.600585, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1684.514038, 12.280603, 0.000000, 0.000007, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1689.359741, 12.382811, 0.000000, 0.000007, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1678.978393, 12.382811, 0.000000, 0.000007, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1664.891723, 12.280603, 0.000000, 0.000014, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1669.737426, 12.382811, 0.000000, 0.000014, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1659.356079, 12.382811, 0.000000, 0.000014, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1644.469238, 12.280603, 0.000000, 0.000022, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1649.314941, 12.382811, 0.000000, 0.000022, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1638.933593, 12.382811, 0.000000, 0.000022, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1625.158447, 12.280603, 0.000000, 0.000029, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1630.004150, 12.382811, 0.000000, 0.000029, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1619.622802, 12.382811, 0.000000, 0.000029, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1304.876342, -1604.803955, 12.280603, 0.000000, 0.000045, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1609.649658, 12.382811, 0.000000, 0.000045, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1599.268310, 12.382811, 0.000000, 0.000045, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1304.886352, -1589.198608, 12.382811, 0.000000, 0.000037, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1305.836791, -1555.211303, 12.382811, 0.000000, 0.000007, -8.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1307.730224, -1541.756713, 12.280603, 0.000000, 0.000059, -8.100001, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1307.057495, -1546.555419, 12.382811, 0.000000, 0.000059, -8.100001, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1309.048461, -1537.215454, 12.382811, 0.000000, 0.000059, -20.100002, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1314.956542, -1521.836669, 12.280603, -0.000018, 0.000111, -20.999984, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1313.229248, -1526.363769, 12.382811, -0.000018, 0.000111, -20.999984, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1317.989868, -1516.382446, 12.382811, -0.000018, 0.000111, -33.999984, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1325.715820, -1504.940673, 12.280603, -0.000014, 0.000087, -33.999977, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1323.014526, -1508.963623, 12.382811, -0.000014, 0.000087, -33.999977, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1328.819702, -1500.356689, 12.382811, -0.000014, 0.000087, -33.999977, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1335.034423, -1488.661376, 12.280603, -0.000034, 0.000119, -26.999963, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1332.843627, -1492.983398, 12.382811, -0.000034, 0.000119, -26.999963, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1337.379882, -1484.080932, 12.382811, -0.000034, 0.000119, -26.999963, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1342.000122, -1473.771728, 12.280603, -0.000054, 0.000176, -12.999949, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1340.536987, -1478.191894, 12.382811, -0.000037, 0.000127, -26.999958, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1343.255126, -1468.379394, 12.382811, -0.000054, 0.000176, -12.999949, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1346.125488, -1455.735717, 12.280603, -0.000007, 0.000097, -11.999993, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1345.127563, -1460.477661, 12.382811, -0.000007, 0.000097, -11.999993, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1347.286254, -1450.322875, 12.382811, -0.000007, 0.000097, -11.999993, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1348.553466, -1438.033447, 12.280603, -0.000012, 0.000142, -3.999988, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1348.225219, -1442.868408, 12.382811, -0.000012, 0.000142, -3.999988, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1348.949584, -1432.511718, 12.382811, -0.000012, 0.000142, -3.999988, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1349.968627, -1417.946899, 12.382811, -0.000012, 0.000142, -0.999988, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(621, 1349.611938, -1422.890625, 12.280603, -0.000012, 0.000142, -3.999988, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(716, 1349.348876, -1426.796142, 12.382811, -0.000012, 0.000142, -3.999988, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1305.123168, -1828.650024, 12.372813, 0.000000, 0.000000, 90.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1306.872192, -1828.180908, 12.382811, 0.000000, 0.000000, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1305.123168, -1809.859130, 12.372813, 0.000007, 0.000000, 89.999977, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1306.872192, -1809.390014, 12.382811, 0.000000, 0.000007, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1305.123168, -1790.057861, 12.372813, 0.000014, 0.000000, 89.999954, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1306.872192, -1789.588745, 12.382811, 0.000000, 0.000014, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1305.123168, -1770.687011, 12.372813, 0.000022, 0.000000, 89.999931, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1306.872192, -1770.217895, 12.382811, 0.000000, 0.000022, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1305.123168, -1752.375732, 12.372813, 0.000029, 0.000000, 89.999908, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1306.872192, -1751.906616, 12.382811, 0.000000, 0.000029, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1305.123168, -1692.505859, 12.372813, 0.000037, 0.000000, 89.999885, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1306.872192, -1692.036743, 12.382811, 0.000000, 0.000037, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1305.123168, -1674.564697, 12.372813, 0.000045, 0.000000, 89.999862, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1306.872192, -1674.095581, 12.382811, 0.000000, 0.000045, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1305.123168, -1654.503662, 12.372813, 0.000052, 0.000000, 89.999839, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1306.872192, -1654.034545, 12.382811, 0.000000, 0.000052, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1305.123168, -1634.372314, 12.372813, 0.000060, 0.000000, 89.999816, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1306.872192, -1633.903198, 12.382811, 0.000000, 0.000060, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1305.123168, -1614.562500, 12.372813, 0.000068, 0.000000, 89.999794, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1306.872192, -1614.093383, 12.382811, 0.000000, 0.000068, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1305.123168, -1594.591552, 12.372813, 0.000075, 0.000000, 89.999771, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1306.872192, -1594.122436, 12.382811, 0.000000, 0.000075, 0.000000, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1306.676879, -1550.594238, 12.372813, 0.000098, 0.000000, 84.999702, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1308.460205, -1550.279174, 12.382811, 0.000000, 0.000098, -4.999999, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1311.277587, -1531.118286, 12.372813, 0.000128, 0.000004, 69.999626, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1313.081665, -1531.275512, 12.382811, -0.000004, 0.000128, -19.999996, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1320.878417, -1512.218383, 12.372813, 0.000157, 0.000017, 54.999603, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1322.580444, -1512.837402, 12.382811, -0.000017, 0.000157, -34.999984, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1331.267333, -1496.618774, 12.372813, 0.000174, 0.000029, 56.999568, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1332.989868, -1497.178222, 12.382811, -0.000029, 0.000174, -32.999965, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1339.208251, -1480.977050, 12.372813, 0.000182, 0.000034, 56.999557, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1340.930786, -1481.536499, 12.382811, -0.000034, 0.000182, -32.999954, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1344.315673, -1464.808105, 12.372813, 0.000241, 0.000066, 76.999481, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1346.125244, -1464.744995, 12.382811, -0.000066, 0.000241, -12.999915, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1348.231079, -1446.179687, 12.372813, 0.000265, 0.000071, 80.999435, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1350.031616, -1445.990356, 12.382811, -0.000071, 0.000265, -8.999912, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(748, 1349.342163, -1429.370239, 12.372813, 0.000272, 0.000072, 80.999412, -1, -1, -1, 250.00, 250.00); 
    tmpobjid = CreateDynamicObject(634, 1351.142700, -1429.180908, 12.382811, -0.000072, 0.000272, -8.999912, -1, -1, -1, 250.00, 250.00); 
	return 1;
}