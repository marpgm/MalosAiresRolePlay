#if defined _map_concesionarias_inc
	#endinput
#endif
#define _map_concesionarias_inc

#include <YSI_Coding\y_hooks>

hook RemoveMapsBuildings(playerid)
{
	//===========================CONCESIONARIA PINAR============================
	RemoveBuildingForPlayer(playerid, 5873, 1022.5078, -1078.8906, 34.6797, 0.25);
	RemoveBuildingForPlayer(playerid, 5876, 1022.5313, -1096.3672, 34.6250, 0.25);
	RemoveBuildingForPlayer(playerid, 5963, 982.3672, -1090.9219, 28.9219, 0.25);
	RemoveBuildingForPlayer(playerid, 1287, 974.2813, -1131.1328, 23.3125, 0.25);
	RemoveBuildingForPlayer(playerid, 5722, 982.3672, -1090.9219, 28.9219, 0.25);

	//========================CONCESIONARIA DE UTILITARIOS======================
	RemoveBuildingForPlayer(playerid, 3744, 2241.2969, -2183.9766, 15.1016, 0.25);
	RemoveBuildingForPlayer(playerid, 3744, 2226.3203, -2168.9922, 15.1016, 0.25);
	RemoveBuildingForPlayer(playerid, 3574, 2226.3203, -2168.9922, 15.1016, 0.25);
	RemoveBuildingForPlayer(playerid, 3574, 2241.2969, -2183.9766, 15.1016, 0.25);

	//==========================CONCESIONARIA GROTTI============================
	RemoveBuildingForPlayer(playerid, 6357, 505.0547, -1269.9375, 28.6875, 0.25);

	return 1;
}

hook LoadMaps()
{
	//===========================CONCESIONARIA PINAR============================
	// Paredes
	Textura = CreateDynamicObject(18766, 991.80701, -1124.63293, 25.30030,   0.00000, 0.00000, 90.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(18766, 991.80701, -1114.63611, 25.30030,   0.00000, 0.00000, 90.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(18766, 991.80701, -1124.63293, 30.29900,   0.00000, 0.00000, 90.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(18766, 991.80701, -1114.63611, 30.29900,   0.00000, 0.00000, 90.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(18766, 988.64069, -1130.28955, 30.29900,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(18766, 986.30811, -1110.14075, 30.29900,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(18766, 977.65460, -1110.13684, 25.30030,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(18766, 986.30811, -1110.13684, 25.30030,   0.00000, 0.00000, 0.18000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(18766, 977.65460, -1110.13684, 30.29990,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(18766, 973.15472, -1125.62512, 30.29900,   0.00000, 0.00000, 90.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(18766, 978.64508, -1130.28955, 30.29900,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(18766, 973.15472, -1115.63245, 30.30950,   0.00000, 0.00000, 90.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(18766, 991.81403, -1125.79285, 25.30030,   0.00000, 0.00000, 90.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);

	// Columnas
	Textura = CreateDynamicObject(18762, 973.15472, -1115.15784, 25.34030,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 5720, "sunrise10_lawn", "holbuild02d_law", 0xFF848383);
	Textura = CreateDynamicObject(18762, 977.24652, -1130.28955, 25.32030,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 5720, "sunrise10_lawn", "holbuild02d_law", 0xFF848383);
	Textura = CreateDynamicObject(18762, 987.06982, -1130.14856, 25.30030,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 5720, "sunrise10_lawn", "holbuild02d_law", 0xFF848383);
	Textura = CreateDynamicObject(18762, 973.15472, -1120.44763, 25.34030,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 5720, "sunrise10_lawn", "holbuild02d_law", 0xFF848383);
	Textura = CreateDynamicObject(18762, 982.09161, -1130.22961, 25.32030,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 5720, "sunrise10_lawn", "holbuild02d_law", 0xFF848383);
	Textura = CreateDynamicObject(18762, 973.15472, -1130.34827, 25.34030,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 5720, "sunrise10_lawn", "holbuild02d_law", 0xFF848383);
	Textura = CreateDynamicObject(18762, 973.15472, -1125.33655, 25.34030,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 5720, "sunrise10_lawn", "holbuild02d_law", 0xFF848383);
	Textura = CreateDynamicObject(18762, 982.57910, -1119.79663, 23.61890,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 5720, "sunrise10_lawn", "holbuild02d_law", 0xFF848383);
	Textura = CreateDynamicObject(18762, 991.80701, -1130.11230, 30.29900,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(18762, 973.16467, -1130.34827, 30.29900,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);

	//Pisos y techos
	Textura = CreateObject(19380, 978.87250, -1115.39685, 31.89020,   0.00000, 90.00000, 0.00000);
	SetObjectMaterial(Textura, 0, 9953, "ottos_sfw", "sfe_arch10", -1);
	Textura = CreateObject(19380, 989.36987, -1115.39685, 31.89020,   0.00000, 90.00000, 0.00000);
	SetObjectMaterial(Textura, 0, 9953, "ottos_sfw", "sfe_arch10", -1);
	Textura = CreateObject(19380, 989.36987, -1125.03223, 31.89020,   0.00000, 90.00000, 0.00000);
	SetObjectMaterial(Textura, 0, 9953, "ottos_sfw", "sfe_arch10", -1);
	Textura = CreateObject(19380, 987.34009, -1115.47510, 26.20400,   0.00000, 90.00000, 0.00000);
	SetObjectMaterial(Textura, 0, 9953, "ottos_sfw", "sfe_arch10", -1);
	Textura = CreateObject(19380, 978.87250, -1125.03223, 31.89020,   0.00000, 90.00000, 0.00000);
	SetObjectMaterial(Textura, 0, 9953, "ottos_sfw", "sfe_arch10", -1);
	Textura = CreateObject(18981, 984.70538, -1119.10376, 22.32720,   0.00000, 90.00000, 0.00000);
	SetObjectMaterial(Textura, 0, 9953, "ottos_sfw", "sfe_arch10", -1);

	//Vidrios
	CreateDynamicObject(3858, 984.64172, -1130.66467, 25.71500,   0.00000, 0.00000, -134.16010);
	CreateDynamicObject(3859, 972.67029, -1113.04810, 25.54560,   0.00000, 0.00000, -162.95999);
	CreateDynamicObject(3858, 972.67029, -1122.89050, 25.54560,   0.00000, 0.00000, -225.00011);

	CreateDynamicObject(8572, 988.42908, -1121.20203, 25.04080,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3850, 982.10431, -1111.64331, 26.82750,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3850, 982.09949, -1115.10095, 26.82750,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3850, 982.10431, -1118.55469, 26.82750,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3850, 983.83807, -1120.28149, 26.82750,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(3850, 987.27301, -1120.28149, 26.82750,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(632, 982.20081, -1121.75195, 23.27902,   0.00000, 0.00000, -42.96004);
	CreateDynamicObject(625, 983.78735, -1126.34192, 23.68988,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(957, 983.55847, -1113.06970, 26.06710,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(957, 989.01184, -1113.62463, 26.06710,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(957, 989.65625, -1118.73450, 26.06710,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(957, 984.10297, -1119.58533, 26.06710,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(632, 983.91675, -1120.43384, 26.70586,   0.00000, 0.00000, -42.96004);
	CreateDynamicObject(625, 979.75787, -1112.49756, 23.68990,   0.00000, 0.00000, 34.80000);
	CreateDynamicObject(632, 983.84589, -1112.96643, 26.70590,   0.00000, 0.00000, -42.96000);
	CreateDynamicObject(1999, 986.48529, -1112.27844, 26.29420,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(632, 990.11322, -1113.36194, 26.70590,   0.00000, 0.00000, -42.96000);
	CreateDynamicObject(1999, 989.83832, -1115.74060, 26.29420,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1703, 983.16852, -1117.33386, 26.27140,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1703, 987.47821, -1119.42065, 26.27140,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(1704, 988.61761, -1116.27563, 26.29040,   0.00000, 0.00000, 109.56000);
	CreateDynamicObject(1704, 986.47101, -1113.97656, 26.29040,   0.00000, 0.00000, 173.09990);
	CreateDynamicObject(1714, 990.93958, -1115.23584, 26.26350,   0.00000, 0.00000, -96.36000);
	CreateDynamicObject(1714, 985.92603, -1111.05396, 26.26350,   0.00000, 0.00000, -6.78000);
	CreateDynamicObject(957, 984.10297, -1119.58533, 31.75777,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(957, 983.25287, -1115.75854, 31.75777,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(957, 989.76978, -1114.03479, 31.75777,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(957, 990.18121, -1119.85132, 31.75777,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2773, 975.83643, -1118.95898, 23.34522,   0.00000, 0.00000, 25.14000);
	CreateDynamicObject(2773, 980.79926, -1116.32153, 23.34522,   0.00000, 0.00000, 28.92000);
	CreateDynamicObject(2773, 979.18811, -1119.00903, 23.34520,   0.00000, 0.00000, 117.00000);
	CreateDynamicObject(2773, 977.74878, -1116.32788, 23.34520,   0.00000, 0.00000, 117.90001);
	CreateDynamicObject(2773, 985.91449, -1115.38745, 23.34520,   0.00000, 0.00000, 169.55994);
	CreateDynamicObject(2773, 988.62054, -1115.76416, 23.34520,   0.00000, 0.00000, 163.98000);
	CreateDynamicObject(2773, 986.38550, -1118.34155, 23.34520,   0.00000, 0.00000, 254.33990);
	CreateDynamicObject(2773, 987.80438, -1113.04932, 23.34520,   0.00000, 0.00000, 254.33990);
	CreateDynamicObject(2773, 987.25909, -1124.52551, 23.34520,   0.00000, 0.00000, 237.83994);
	CreateDynamicObject(2773, 984.76428, -1126.14221, 23.34520,   0.00000, 0.00000, 326.69974);
	CreateDynamicObject(2773, 986.93604, -1127.47217, 23.34520,   0.00000, 0.00000, 328.79965);
	CreateDynamicObject(2773, 984.53308, -1129.02612, 23.34520,   0.00000, 0.00000, 237.77968);

	Textura = CreateDynamicObject(18762, 980.20923, -1130.89600, 30.92920,   0.00000, 90.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 2164, "cj_office", "white32", 0xFF020202);
	Textura = CreateDynamicObject(18762, 983.60919, -1130.89673, 30.92920,   0.00000, 90.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 2164, "cj_office", "white32", 0xFF020202);
	Textura = CreateDynamicObject(18244, 981.91711, -1130.90259, 29.70910,   90.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 6357, "sunstrans_law2", "SunBillB10", -1);

	Textura = CreateDynamicObject(18762, 972.51880, -1120.21082, 30.92920,   0.00000, 90.00000, 90.00000);
	SetDynamicObjectMaterial(Textura, 0, 2164, "cj_office", "white32", 0xFF020202);
	Textura = CreateDynamicObject(18762, 972.51813, -1123.60864, 30.92920,   0.00000, 90.00000, 90.00000);
	SetDynamicObjectMaterial(Textura, 0, 2164, "cj_office", "white32", 0xFF020202);
	Textura = CreateDynamicObject(18244, 972.53247, -1121.77551, 29.70910,   90.00000, 0.00000, -90.00000);
	SetDynamicObjectMaterial(Textura, 0, 6357, "sunstrans_law2", "SunBillB10", -1);

	CreateDynamicObject(18263, 987.17511, -1116.72034, 35.08197,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1635, 982.00391, -1109.08154, 29.44268,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(2254, 986.28003, -1110.65857, 25.00790,   0.00000, 0.00000, 0.00000);
	Textura = CreateDynamicObject(19172, 991.29651, -1127.43079, 25.82215,   0.00000, 0.00000, -90.00000);
	SetDynamicObjectMaterial(Textura, 0, 10249, "ottos2_sfw", "ottos_pics_sfe", -1);
	Textura = CreateDynamicObject(19172, 991.30505, -1114.54138, 25.05154,   0.00000, 0.00000, -90.00000);
	SetDynamicObjectMaterial(Textura, 0, 10249, "ottos2_sfw", "ottos_pics_sfe", -1);
	Textura = CreateDynamicObject(19172, 978.07458, -1110.67651, 25.05150,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 10249, "ottos2_sfw", "ottos_pics_sfe", -1);

	Textura = CreateDynamicObject(19361, 979.7401, -1131.3258, 31.0649,   0.00000, 0.00000, 90.00000);
	SetDynamicObjectMaterialText(Textura, 0, "Pinar", 140, "Arial", 115, 1, 0xFFFFFFFF, 0, 1);
	Textura = CreateDynamicObject(19361, 982.1201, -1131.3258, 31.0649,   0.00000, 0.00000, 90.00000);
	SetDynamicObjectMaterialText(Textura, 0, "Automo", 140, "Arial", 115, 1, 0xFFFFFFFF, 0, 1);
	Textura = CreateDynamicObject(19361, 984.0581, -1131.3258, 31.0649,   0.00000, 0.00000, 90.00000);
	SetDynamicObjectMaterialText(Textura, 0, "tores", 140, "Arial", 115, 1, 0xFFFFFFFF, 0, 1);

	Textura = CreateDynamicObject(19361, 972.08038, -1119.75085, 31.06490,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterialText(Textura, 0, "Pinar", 140, "Arial", 115, 1, 0xFFFFFFFF, 0, 1);
	Textura = CreateDynamicObject(19361, 972.08038, -1122.10229, 31.06490,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterialText(Textura, 0, "Automo", 140, "Arial", 115, 1, 0xFFFFFFFF, 0, 1);
	Textura = CreateDynamicObject(19361, 972.08038, -1124.03880, 31.06490,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterialText(Textura, 0, "tores", 140, "Arial", 115, 1, 0xFFFFFFFF, 0, 1);

	CreateDynamicObject(2835, 985.35791, -1116.50293, 26.28590,   0.00000, 0.00000, -43.98000);
	CreateDynamicObject(2231, 973.45038, -1114.91162, 26.63880,   10.00000, 0.00000, 90.00000);
	CreateDynamicObject(2231, 986.83710, -1129.85364, 26.63880,   10.00000, 0.00000, 180.00000);
	CreateDynamicObject(3920, 973.55292, -1116.97046, 30.76000,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(3920, 973.55292, -1123.54248, 30.76000,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(3920, 980.07599, -1129.90332, 30.76000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3920, 985.03003, -1129.90332, 30.76000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3920, 991.37750, -1123.27576, 30.76000,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(3920, 991.37750, -1116.95081, 30.76000,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(3920, 984.75940, -1110.38550, 30.76000,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(3920, 980.10748, -1110.39758, 30.76000,   0.00000, 0.00000, 180.00000);

	//Plazita
	CreateDynamicObject(800, 986.92218, -1053.95911, 31.82136,   0.00000, 0.00000, 28.25999);
	CreateDynamicObject(800, 985.79871, -1056.37598, 30.17197,   0.00000, 0.00000, 81.30000);
	CreateDynamicObject(800, 983.13440, -1060.45203, 29.33634,   0.00000, 0.00000, 135.48000);
	CreateDynamicObject(800, 980.95825, -1064.23047, 29.33634,   0.00000, 0.00000, 198.12004);
	CreateDynamicObject(800, 978.33264, -1068.02747, 27.22262,   0.00000, 0.00000, 198.12004);
	CreateDynamicObject(616, 986.36066, -1058.18799, 24.93420,   0.00000, 0.00000, 70.61999);
	CreateDynamicObject(617, 984.87097, -1064.41638, 22.70635,   0.00000, 0.00000, 211.50000);
	CreateDynamicObject(656, 984.70905, -1074.24316, 24.80081,   0.00000, 0.00000, 66.12002);
	CreateDynamicObject(714, 992.67664, -1092.25183, 22.73260,   0.00000, 0.00000, 210.89990);
	CreateDynamicObject(6965, 980.36792, -1090.88672, 21.42196,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3472, 987.62378, -1085.45459, 23.23172,   0.00000, 0.00000, 165.77995);
	CreateDynamicObject(3472, 987.60406, -1096.10205, 23.18963,   0.00000, 0.00000, 73.79996);
	CreateDynamicObject(1280, 987.44208, -1091.00427, 23.27345,   0.00000, 0.00000, 0.60000);
	CreateDynamicObject(1280, 980.87811, -1085.90063, 23.27350,   0.00000, 0.00000, 91.97999);
	CreateDynamicObject(1280, 980.68573, -1096.05664, 23.31806,   0.00000, 0.00000, -89.52001);
	CreateDynamicObject(669, 978.99579, -1054.64417, 28.61099,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(803, 976.05499, -1051.99854, 29.17070,   0.00000, 0.00000, 167.70000);

	//=======================CONCESIONARIA DE UTILITARIOS=======================
	Textura = CreateDynamicObject(12843, 2244.64673, -2189.55737, 12.44380,   0.00000, 0.00000, -135.12000);
    SetDynamicObjectMaterial(Textura, 0, 5310, "warehus_las2", "dockwall1", -1);
    SetDynamicObjectMaterial(Textura, 1, 18646, "MatColours", "red-2", 0xFF8F8F91);
 	Textura = CreateDynamicObject(18765, 2253.34570, -2180.89502, 12.44320,   0.00000, 90.00000, 44.88000);
 	SetDynamicObjectMaterial(Textura, 0, 5310, "warehus_las2", "dockwall1", -1);
	Textura = CreateDynamicObject(18765, 2244.64771, -2189.56494, 10.07240,   0.00000, 0.00000, 44.88000);
	SetDynamicObjectMaterial(Textura, 0, 5131, "imrancomp_las2", "lastat97", -1);
	Textura = CreateDynamicObject(18765, 2251.73364, -2182.50830, 10.07240,   0.00000, 0.00000, 44.88000);
	SetDynamicObjectMaterial(Textura, 0, 5131, "imrancomp_las2", "lastat97", -1);
	Textura = CreateDynamicObject(18244, 2242.94238, -2185.63745, 20.35989,   90.00000, 0.00000, 135.00000);
	SetDynamicObjectMaterial(Textura, 0, 10936, "stadiumground_sfse", "ws_RShaul_dirt", 0xFF656666);

	CreateDynamicObject(2009, 2252.26123, -2186.34302, 12.50070,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(2009, 2248.88818, -2181.58203, 12.50070,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(2773, 2243.47314, -2191.40601, 13.08160,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2773, 2243.47314, -2194.04590, 13.08160,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2773, 2246.17920, -2191.40601, 13.08160,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2773, 2246.17920, -2193.79395, 13.08160,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2773, 2244.91284, -2189.88477, 13.08160,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1715, 2252.60986, -2185.23193, 12.54900,   0.00000, 0.00000, -66.53999);
	CreateDynamicObject(1715, 2247.91699, -2181.08398, 12.54900,   0.00000, 0.00000, 17.52001);
	CreateDynamicObject(2310, 2249.20337, -2183.71313, 13.03359,   0.00000, 0.00000, -70.19999);
	CreateDynamicObject(2310, 2248.04712, -2183.85400, 13.03359,   0.00000, 0.00000, -103.74000);
	CreateDynamicObject(2310, 2250.03662, -2186.62646, 13.03359,   0.00000, 0.00000, -157.14000);
	CreateDynamicObject(2310, 2249.91211, -2185.26465, 13.03359,   0.00000, 0.00000, -212.09990);
	CreateDynamicObject(1702, 2250.68579, -2189.64233, 12.55980,   0.00000, 0.00000, -135.00000);
	CreateDynamicObject(2002, 2248.64282, -2191.30176, 12.55212,   0.00000, 0.00000, -135.00000);
	CreateDynamicObject(2007, 2254.40698, -2186.20190, 12.55120,   0.00000, 0.00000, -135.00000);
	CreateDynamicObject(2007, 2253.69775, -2186.90283, 12.55120,   0.00000, 0.00000, -135.00000);
	CreateDynamicObject(2007, 2253.69775, -2186.90283, 13.94960,   0.00000, 0.00000, -135.00000);
	CreateDynamicObject(2007, 2254.40698, -2186.20190, 13.94960,   0.00000, 0.00000, -135.00000);
	CreateDynamicObject(2260, 2248.46436, -2180.20239, 14.62702,   0.00000, 0.00000, -45.00000);
	CreateDynamicObject(2261, 2250.53540, -2182.31934, 14.15376,   0.00000, 0.00000, -45.00000);
	CreateDynamicObject(2262, 2253.46313, -2185.23145, 14.15380,   0.00000, 0.00000, -45.00000);
	CreateDynamicObject(633, 2251.18433, -2189.09351, 13.53231,   0.00000, 0.00000, 142.08003);
	CreateDynamicObject(638, 2246.12158, -2180.53052, 13.24700,   0.00000, 0.00000, -224.70010);
	CreateDynamicObject(638, 2244.25366, -2182.41870, 13.24700,   0.00000, 0.00000, -224.70010);
	CreateDynamicObject(638, 2242.36865, -2184.32422, 13.24700,   0.00000, 0.00000, -224.70010);
	CreateDynamicObject(631, 2247.31274, -2179.40869, 13.42943,   0.00000, 0.00000, 381.29993);
	CreateDynamicObject(982, 2222.27930, -2174.86743, 13.24150,   0.00000, 0.00000, 44.99998);
	CreateDynamicObject(983, 2233.61328, -2186.18750, 13.24150,   0.00000, 0.00000, 45.00000);
	CreateDynamicObject(2773, 2221.02710, -2166.37134, 13.08160,   0.00000, 0.00000, 55.07996);
	CreateDynamicObject(2773, 2224.36108, -2164.50391, 13.08160,   0.00000, 0.00000, 147.41972);
	CreateDynamicObject(2773, 2221.70044, -2162.44678, 13.08160,   0.00000, 0.00000, 145.73972);
	CreateDynamicObject(2773, 2224.64185, -2161.03735, 13.08160,   0.00000, 0.00000, 234.89973);
	CreateDynamicObject(1290, 2226.06494, -2166.69238, 18.54370,   0.00000, 0.00000, -41.52000);
	CreateDynamicObject(1290, 2235.44360, -2176.29810, 18.54370,   0.00000, 0.00000, -41.52000);
	CreateDynamicObject(2773, 2230.31299, -2175.13013, 13.08160,   0.00000, 0.00000, 86.21999);
	CreateDynamicObject(2773, 2228.68799, -2169.26709, 13.08160,   0.00000, 0.00000, -4.14000);
	CreateDynamicObject(2773, 2232.08521, -2169.32080, 13.08160,   0.00000, 0.00000, -4.14000);
	CreateDynamicObject(2773, 2231.11084, -2164.51587, 13.08160,   0.00000, 0.00000, -94.32001);
	CreateDynamicObject(2773, 2234.75293, -2179.60205, 13.08160,   0.00000, 0.00000, 23.57997);
	CreateDynamicObject(2773, 2239.04663, -2179.31250, 13.08160,   0.00000, 0.00000, 116.39996);
	CreateDynamicObject(2773, 2237.91064, -2176.73438, 13.08160,   0.00000, 0.00000, 115.19997);
	CreateDynamicObject(2773, 2241.19189, -2176.69531, 13.08160,   0.00000, 0.00000, 204.05978);

	//==========================CONCESIONARIA GROTTI============================
	CreateDynamicObject(2774, 545.78308, -1294.63306, 18.42860,   180.00000, 0.00000, 0.00000);
	CreateDynamicObject(2774, 538.77887, -1294.63306, 18.42860,   180.00000, 0.00000, 0.00000);
	CreateDynamicObject(2774, 538.77887, -1300.93811, 18.42860,   180.00000, 0.00000, 0.00000);
	CreateDynamicObject(2774, 566.43018, -1294.64209, 12.27500,   180.00000, 0.00000, 0.00000);
	CreateDynamicObject(2774, 559.50732, -1294.64209, 12.27500,   180.00000, 0.00000, 0.00000);
	CreateDynamicObject(2774, 545.78308, -1300.93811, 18.42860,   180.00000, 0.00000, 0.00000);
	CreateDynamicObject(3857, 542.23468, -1294.32739, 28.61620,   0.00000, 0.00000, 45.00000);
	CreateDynamicObject(3858, 549.53162, -1294.34131, 16.96000,   45.00000, 90.00000, 0.06000);
	CreateDynamicObject(3858, 562.71753, -1294.34131, 16.96000,   45.00000, 90.00000, 0.06000);
	CreateDynamicObject(2774, 552.53760, -1294.64209, 12.27500,   180.00000, 0.00000, 0.00000);
	CreateDynamicObject(3858, 555.71503, -1294.34131, 16.96000,   45.00000, 90.00000, 0.06000);
	CreateDynamicObject(2774, 518.36377, -1294.64209, 12.27500,   180.00000, 0.00000, 0.00000);
	CreateDynamicObject(2774, 531.88098, -1294.64209, 12.27500,   180.00000, 0.00000, 0.00000);
	CreateDynamicObject(2774, 524.92938, -1294.64209, 12.27500,   180.00000, 0.00000, 0.00000);
	CreateDynamicObject(3858, 535.52802, -1294.34131, 16.96000,   45.00000, 90.00000, 0.06000);
	CreateDynamicObject(3858, 528.20618, -1294.34131, 16.96000,   45.00000, 90.00000, 0.06000);
	CreateDynamicObject(3858, 521.66901, -1294.34131, 16.96000,   45.00000, 90.00000, 0.06000);
	CreateDynamicObject(3857, 546.61572, -1297.87561, 28.49520,   0.00000, 0.00000, -45.00300);
	CreateDynamicObject(3857, 542.34241, -1301.75586, 28.49520,   0.00000, 0.00000, -134.99390);
	CreateDynamicObject(3857, 537.97650, -1297.87561, 28.49520,   0.00000, 0.00000, -45.26700);
	CreateDynamicObject(3857, 542.23468, -1294.32739, 22.80410,   0.00000, 0.00000, 45.00000);

	// Extras
	Textura = CreateDynamicObject(19381, 542.33282, -1297.97742, 31.33120,   0.00000, 90.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 6337, "sunset02_law2", "tarmacplain2_bank", -1);
	Textura = CreateDynamicObject(19379, 566.50421, -1299.30640, 19.20000,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 6337, "sunset02_law2", "concpanel_la", 0xFF7D7C7D);
	Textura = CreateDynamicObject(19379, 518.05872, -1299.02954, 19.20000,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 6337, "sunset02_law2", "concpanel_la", 0xFF7D7C7D);
	Textura = CreateDynamicObject(1251, 542.01862, -1294.32544, 19.82030,   0.00000, 0.00000, 89.94000);
	SetDynamicObjectMaterial(Textura, 0, 2774, "airp_prop", "CJ_GALVANISED", -1);

	// Piso
	Textura = CreateObject(19379, 561.18457, -1299.14465, 16.15710,   0.00000, 90.00000, 0.00000);
	SetObjectMaterial(Textura, 0, 9953, "ottos_sfw", "sfe_arch10", -1);
	Textura = CreateObject(19379, 550.68341, -1299.14465, 16.15710,   0.00000, 90.00000, 0.00000);
	SetObjectMaterial(Textura, 0, 9953, "ottos_sfw", "sfe_arch10", -1);
	Textura = CreateObject(19379, 540.18329, -1299.14465, 16.15710,   0.00000, 90.00000, 0.00000);
	SetObjectMaterial(Textura, 0, 9953, "ottos_sfw", "sfe_arch10", -1);
	Textura = CreateObject(19379, 529.68732, -1299.14465, 16.15710,   0.00000, 90.00000, 0.00000);
	SetObjectMaterial(Textura, 0, 9953, "ottos_sfw", "sfe_arch10", -1);
	Textura = CreateObject(19379, 523.39539, -1299.14465, 16.15410,   0.00000, 90.00000, 0.00000);
	SetObjectMaterial(Textura, 0, 9953, "ottos_sfw", "sfe_arch10", -1);

	// Cuadros
	Textura = CreateDynamicObject(19172, 557.40881, -1303.72681, 19.68690,   0.00000, 0.00000, 180.00000);
	SetDynamicObjectMaterial(Textura, 0, 10249, "ottos2_sfw", "ottos_pics_sfe", -1);
	Textura = CreateDynamicObject(19172, 526.57123, -1303.72681, 19.68690,   0.00000, 0.00000, 180.00000);
	SetDynamicObjectMaterial(Textura, 0, 10249, "ottos2_sfw", "ottos_pics_sfe", -1);

	CreateDynamicObject(2773, 559.31097, -1297.84802, 16.76360,   0.00000, 0.00000, -51.29999);
	CreateDynamicObject(2773, 560.08740, -1300.94983, 16.76360,   0.00000, 0.00000, 38.58000);
	CreateDynamicObject(2773, 562.22430, -1299.09082, 16.76360,   0.00000, 0.00000, 38.58000);
	CreateDynamicObject(2773, 563.04675, -1302.21680, 16.76360,   0.00000, 0.00000, -50.04001);
	CreateDynamicObject(2773, 550.89874, -1297.43689, 16.76360,   0.00000, 0.00000, -51.29999);
	CreateDynamicObject(2773, 554.45319, -1302.37122, 16.76360,   0.00000, 0.00000, -51.29999);
	CreateDynamicObject(2773, 553.95538, -1299.18103, 16.76360,   0.00000, 0.00000, 39.60000);
	CreateDynamicObject(2773, 551.69287, -1301.07263, 16.76360,   0.00000, 0.00000, 36.78001);
	CreateDynamicObject(2773, 535.30817, -1297.82410, 16.76360,   0.00000, 0.00000, 39.12000);
	CreateDynamicObject(2773, 530.06641, -1301.80835, 16.76360,   0.00000, 0.00000, 39.12000);
	CreateDynamicObject(2773, 531.64850, -1298.47363, 16.76360,   0.00000, 0.00000, -52.44001);
	CreateDynamicObject(2773, 533.70892, -1301.03442, 16.76360,   0.00000, 0.00000, -54.00001);
	CreateDynamicObject(2773, 525.68390, -1297.95398, 16.76360,   0.00000, 0.00000, 39.05999);
	CreateDynamicObject(2773, 521.01514, -1301.66333, 16.76360,   0.00000, 0.00000, 36.89999);
	CreateDynamicObject(2773, 524.02271, -1301.10205, 16.76360,   0.00000, 0.00000, -55.32001);
	CreateDynamicObject(2773, 522.27087, -1298.77686, 16.76360,   0.00000, 0.00000, -54.48001);
	CreateDynamicObject(3920, 560.09912, -1303.92664, 23.36920,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3920, 547.49951, -1303.92664, 23.36920,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3920, 536.42572, -1303.92664, 23.36920,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3920, 524.32098, -1303.92664, 23.36920,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3920, 518.03668, -1301.08313, 23.36920,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(3920, 566.56897, -1301.25574, 23.36920,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(2231, 546.52203, -1300.64673, 19.73078,   0.00000, 0.00000, 91.38000);
	CreateDynamicObject(2231, 545.51563, -1300.22229, 19.73078,   0.00000, 0.00000, 180.71997);
	CreateDynamicObject(2231, 538.54749, -1300.19104, 19.73078,   0.00000, 0.00000, 180.71997);
	CreateDynamicObject(2231, 538.02795, -1301.17456, 19.73078,   0.00000, 0.00000, 266.58005);
	CreateDynamicObject(948, 545.65039, -1299.68762, 16.24010,   0.00000, 0.00000, 1.08000);
	CreateDynamicObject(948, 538.92383, -1299.63879, 16.24010,   0.00000, 0.00000, 1.08000);
	CreateDynamicObject(2009, 543.46527, -1300.62073, 16.24380,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(2009, 540.47632, -1300.62073, 16.24380,   0.00000, 0.00000, -89.76000);
	CreateDynamicObject(1714, 543.25226, -1301.79858, 16.25020,   0.00000, 0.00000, 132.60002);
	CreateDynamicObject(1714, 540.30841, -1301.93652, 16.25020,   0.00000, 0.00000, 162.53995);
	CreateDynamicObject(1704, 543.58289, -1299.01306, 16.22850,   0.00000, 0.00000, -8.94000);
	CreateDynamicObject(1704, 540.35052, -1299.07739, 16.22850,   0.00000, 0.00000, 13.98000);
	CreateDynamicObject(3920, 518.03668, -1301.08313, 23.36920,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(3920, 566.56897, -1301.25574, 23.36920,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(8328, 560.97943, -1256.30811, 27.39410,   0.00000, 0.00000, 294.06009);
	CreateDynamicObject(8292, 560.21332, -1261.45850, 36.30280,   0.00000, 0.00000, 13.62000);
	CreateDynamicObject(8330, 521.20508, -1307.51111, 35.15574,   0.00000, 0.00000, 40.62000);
	CreateDynamicObject(8332, 520.94812, -1307.48645, 34.78401,   0.00000, 0.00000, -139.32007);
	CreateDynamicObject(8292, 455.07184, -1287.41675, 31.19804,   0.00000, 0.00000, 271.73993);

	//====================CONCESIONARIA CIUDAD MOTO=============================
	CreateDynamicObject(984, 1303.34326, -1862.31421, 13.23780,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(984, 1294.51978, -1868.69653, 13.23780,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1223, 1295.00879, -1865.07898, 12.50750,   0.00000, 0.00000, 358.14001);
	CreateDynamicObject(2773, 1297.03442, -1867.22119, 13.10160,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(2773, 1295.81238, -1866.90686, 13.08060,   0.00000, 0.00000, -50.16000);
	CreateDynamicObject(2773, 1295.85779, -1868.87634, 13.08060,   0.00000, 0.00000, -50.16000);
	CreateDynamicObject(2773, 1302.70776, -1866.15955, 13.10160,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(2773, 1303.94128, -1865.80652, 13.10160,   0.00000, 0.00000, 51.48000);
	CreateDynamicObject(2773, 1303.89697, -1868.01416, 13.10160,   0.00000, 0.00000, 51.48000);
	CreateDynamicObject(1731, 1301.87805, -1872.08264, 14.27550,   0.00000, 0.00000, 89.76000);
	CreateDynamicObject(1705, 1304.04504, -1874.14392, 12.55878,   0.00000, 0.00000, -80.21998);
	CreateDynamicObject(2251, 1304.20435, -1875.66821, 13.41850,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1705, 1304.21851, -1876.07776, 12.55878,   0.00000, 0.00000, -102.18002);
	CreateDynamicObject(16779, 1300.70911, -1875.69788, 15.66510,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2001, 1304.26782, -1879.81970, 12.55880,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1721, 1300.65442, -1877.52930, 12.55869,   0.00000, 0.00000, 199.32004);
	CreateDynamicObject(1721, 1302.03857, -1877.37598, 12.55869,   0.00000, 0.00000, 176.34004);
	CreateDynamicObject(2008, 1301.04626, -1878.78638, 12.56019,   0.00000, 0.00000, 7.86000);
	CreateDynamicObject(1671, 1301.74158, -1879.73267, 12.99780,   0.00000, 0.00000, 170.15999);
	CreateDynamicObject(2773, 1296.63196, -1878.80884, 13.10160,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(2773, 1298.12891, -1879.72815, 13.10160,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(2008, 1297.70251, -1874.10571, 12.56019,   0.00000, 0.00000, -83.82000);
	CreateDynamicObject(1671, 1296.57214, -1874.60522, 12.99780,   0.00000, 0.00000, 72.24001);
	CreateDynamicObject(1721, 1299.21606, -1874.22791, 12.55869,   0.00000, 0.00000, 96.96003);
	CreateDynamicObject(2001, 1296.00989, -1872.35681, 12.55875,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3857, 1298.22302, -1870.95752, 13.50110,   0.00000, 0.00000, 45.00000);
	Textura = CreateDynamicObject(18762, 1304.49585, -1871.43311, 13.96590,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 5720, "sunrise10_lawn", "holbuild02d_law", 0xFF848383);
	Textura = CreateDynamicObject(18762, 1301.88098, -1871.43311, 13.96590,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 5720, "sunrise10_lawn", "holbuild02d_law", 0xFF848383);
	Textura = CreateDynamicObject(18762, 1295.94116, -1871.43481, 13.96590,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 5720, "sunrise10_lawn", "holbuild02d_law", 0xFF848383);
	Textura = CreateDynamicObject(18762, 1294.94116, -1871.43481, 13.96590,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 5720, "sunrise10_lawn", "holbuild02d_law", 0xFF848383);
	Textura = CreateDynamicObject(19377, 1294.52307, -1876.62720, 10.32470,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 5720, "sunrise10_lawn", "holbuild02d_law", 0xFF848383);
	Textura = CreateDynamicObject(18981, 1306.98621, -1880.80432, 3.97300,   0.00000, 0.00000, 90.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(19377, 1304.98474, -1876.62720, 10.32470,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 10948, "skyscrapper_sfs", "ws_tunnelwall2", 0xFF747374);
	Textura = CreateDynamicObject(18981, 1306.93274, -1883.41272, 15.97780,   0.00000, 90.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 3976, "lanbloke", "parking1plain", -1);
	Textura = CreateDynamicObject(19379, 1299.72302, -1875.78284, 12.47190,   0.00000, 90.00000, 0.00000);
	SetDynamicObjectMaterial(Textura, 0, 9953, "ottos_sfw", "sfe_arch10", -1);
	Textura = CreateDynamicObject(19361, 1305.00098, -1865.74512, 17.22590,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterialText(Textura, 0, "Ciudad", 140, "Arial", 115, 1, 0xFFFFFFFF, 0, 1);
	Textura = CreateDynamicObject(19361, 1305.00098, -1868.07336, 17.22590,   0.00000, 0.00000, 0.00000);
	SetDynamicObjectMaterialText(Textura, 0, "Moto", 140, "Arial", 115, 1, 0xFFFFFFFF, 0, 1);
	Textura = CreateDynamicObject(18762, 1305.45129, -1866.82800, 17.17690,   0.00000, 90.00000, 90.00000);
	SetDynamicObjectMaterial(Textura, 0, 2164, "cj_office", "white32", 0xFF020202);

	return 1;
}