/*
 				ooo        ooooo           oooo                                               
				`88.       .888'           `888                                               
				 888b     d'888   .oooo.    888   .ooooo.   .oooo.o                           
				 8 Y88. .P  888  `P  )88b   888  d88' `88b d88(  "8                           
				 8  `888'   888   .oP"888   888  888   888 `"Y88b.                            
				 8    Y     888  d8(  888   888  888   888 o.  )88b                           
				o8o        o888o `Y888""8o o888o `Y8bod8P' 8""888P'                           
				      .o.        o8o                                                          
				     .888.       `"'                                                          
				    .8"888.     oooo  oooo d8b  .ooooo.   .oooo.o                             
				   .8' `888.    `888  `888""8P d88' `88b d88(  "8                             
				  .88ooo8888.    888   888     888ooo888 `"Y88b.                              
				 .8'     `888.   888   888     888    .o o.  )88b                             
				o88o     o8888o o888o d888b    `Y8bod8P' 8""888P'                             
      ooooooooo.             oooo                       oooo                        
	  `888   `Y88.           `888                       `888                        
	   888   .d88'  .ooooo.   888   .ooooo.  oo.ooooo.   888   .oooo.   oooo    ooo 
	   888ooo88P'  d88' `88b  888  d88' `88b  888' `88b  888  `P  )88b   `88.  .8'  
	   888`88b.    888   888  888  888ooo888  888   888  888   .oP"888    `88..8'   
	   888  `88b.  888   888  888  888    .o  888   888  888  d8(  888     `888'    
	  o888o  o888o `Y8bod8P' o888o `Y8bod8P'  888bod8P' o888o `Y888""8o     .8'     
	                                          888                       .o..P'      
	                                         o888o                      `Y8P'


 ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ 
|______|______|______|______|______|______|______|______|______|______|______|______|


			  ________     ____                       _       _     _           
			/ / ___\ \   / ___|___  _ __  _   _ _ __(_) __ _| |__ | |_         
		   | | |    | | | |   / _ \| '_ \| | | | '__| |/ _` | '_ \| __|        
		   | | |___ | | | |__| (_) | |_) | |_| | |  | | (_| | | | | |_         
		   | |\____|| |  \____\___/| .__/ \__, |_|  |_|\__, |_| |_|\__|        
			\_\    /_/             |_|    |___/        |___/                   
				  ____   ___  _  ___       ____   ___ ____   ____                     
				 |___ \ / _ \/ |/ _ \     |___ \ / _ \___ \ |___ \              
				   __) | | | | | | | |_____ __) | | | |__) |  __) |           
				  / __/| |_| | | |_| |_____/ __/| |_| / __/| / __/|         
				 |_____|\___/|_|\___/     |_____|\___/_____|/_____|              
								  _                                                                  
								 | |__  _   _                                                        
								 | '_ \| | | |                                                       
								 | |_) | |_| |                                                       
								 |_.__/ \__, |                                                                                                         
						  ____  _       |___/   _                                            
						 |  _ \| |__   ___  ___| | __                                        
						 | |_) | '_ \ / _ \/ _ \ |/ /                                        
						 |  __/| | | |  __/  __/   <                                         
						 |_|   |_| |_|\___|\___|_|\_|                                        
					   ____                 _                                            
					  / ___| __ _ _ __ ___ (_)_ __   __ _                                
					 | |  _ / _` | '_ ` _ \| | '_ \ / _` |                               
					 | |_| | (_| | | | | | | | | | | (_| |                               
					  \____|\__,_|_| |_| |_|_|_| |_|\__, |                           
		 _          _   _                           |___/__      _           
		| |    __ _| |_(_)_ __   ___   __ _ _ __ ___   _/_/ _ __(_) ___ __ _ 
		| |   / _` | __| | '_ \ / _ \ / _` | '_ ` _ \ / _ \ '__| |/ __/ _` |
		| |__| (_| | |_| | | | | (_) | (_| | | | | | |  __/ |  | | (_| (_| |
		|_____\__,_|\__|_|_| |_|\___/ \__,_|_| |_| |_|\___|_|  |_|\___\__,_|

*/

/*******************************************************************************
********************************************************************************
***********************                                 ************************
*********************    MALOS AIRES ROLEPLAY GAMEMODE    **********************
**********										 			         ***********
********    (C) Copyright 2010 - 2020 by Pheek Gaming Latinoam�rica    *********
**********                                            				 ***********
***********************    @Do not remove this label    ************************
***********************    @No remueva esta etiqueta    ************************
*************************                             **************************
********************************************************************************
*******************************************************************************/

#pragma warning disable 214 // warning 214: possibly a "const" array argument was intended: "array_name". Solo con pawncc ^3.10.4
#pragma warning disable 239 // warning 239: literal array/string passed to a non-const parameter. Solo con pawncc ^3.10.4

#define MAX_PLAYERS (150)

#include <a_samp>
#include <a_mysql>
#include <sscanf2>


#define YSI_NO_VERSION_CHECK
#define YSI_NO_CACHE_MESSAGE
#define YSI_NO_MODE_CACHE
#define CGEN_MEMORY (60000) // Requerido por librer�a YSI para reservar mayor tama�o de memoria para su c�digo

#define FOREACH_NO_LOCALS
#define FOREACH_NO_ACTORS
#define FOREACH_NO_BOTS

#include <PawnPlus>
#include <YSI_Data\y_iterate> // provides foreach
#include <zcmd>
#include <streamer>
#include <Dini>
#include <cstl>
#include <anti_flood>
#include <easyDialog>
#include <progress2>
//#include <nex-ac>
#include "util/marp_util.pwn" // Siempre arriba del resto de los includes de marp

#define HP_GAIN           		2         	                                	// Vida que ganas por segundo al estar hospitalizado.
#define GAS_UPDATE_TIME         36000                                           // Tiempo de actualizacion de la gasolina.
#define MAX_LOGIN_ATTEMPTS      5

// Posiciones.
#define POS_BANK_X              1479.6465
#define POS_BANK_Y              -1134.2802
#define POS_BANK_Z              1015.4130
#define POS_BANK_I              1
#define POS_BANK_W              1002

#define POS_SPAWN_X				1742.97
#define POS_SPAWN_Y				-1860.21
#define POS_SPAWN_Z				13.57
#define POS_SPAWN_A				0.0
#define POS_SPAWN_WORLD			0
#define POS_SPAWN_INTERIOR		0
/* #define POS_SPAWN_X				-12.21
#define POS_SPAWN_Y				3199.52
#define POS_SPAWN_Z				2412.72
#define POS_SPAWN_A				270.56
#define POS_SPAWN_WORLD			0
#define POS_SPAWN_INTERIOR		1032 */

// Tiempos de jail.
#define DM_JAILTIME 			300 	// 5 minutos



// Precios.
#define PRICE_FIGHTSTYLE        3000
#define PRICE_TREATMENT         500

#include "util\marp_zones.pwn"              	//Informacion de las diferentes zonas y barrios
#include "util\marp_fade_screen.pwn"
#include "system/marp_previewmodelmenu.pwn"
#include "marp_database.pwn" 					//Funciones varias para acceso a datos
#include "system\marp_shutdown.pwn"
#include "util/marp_commands_list.pwn"
#include "system\streamer\marp_streamer_handling.pwn"
#include "system\streamer\marp_dyn_obj_handling.pwn"
#include "system\marp_button.pwn"
#include "player/marp_players.pwn" 				//Contiene definiciones y l�gica de negocio para todo lo que involucre a los jugadores (Debe ser incluido antes de cualquier include que dependa de playerInfo)
#include "system\marp_server_logs.pwn"
#include "system/marp_login_screen.pwn"
#include "system/marp_login_camera.pwn"
#include "system/marp_logo.pwn"
#include "player/marp_toggle.pwn"
#include "system\notification\marp_noti.pwn"
#include "system\marp_cmd_cooldown.pwn"
#include "system\damage\marp_damage.pwn"
#include "item\marp_item.pwn"
#include "item\marp_item_admin.pwn"
#include "item\marp_aiming_camera_fix.pwn"
#include "player/marp_hidden_name.pwn"
#include "system\marp_teleport.pwn"
#include "container/marp_container.pwn"
#include "util/marp_streamings.pwn"
#include "player/marp_mano.pwn" 				//Sistema de items en la mano
#include "system\phone\marp_phone_core.pwn"
#include "system\phone\marp_phone_call.pwn"
#include "system\phone\marp_phone_contacts.pwn"
#include "system\phone\marp_phone_sms.pwn"
#include "system\phone\marp_phone_gui.pwn"
#include "item\marp_toy.pwn" 					//Sistema de toys
#include "system\marp_key_chain.pwn" 			//Sistema de llaveros
#include "player/marp_inventory.pwn" 			//Sistema de inventario
#include "player/marp_duty_belt.pwn"
#include "vehicle/marp_vehicles.pwn" 			//Sistema de vehiculos
#include "vehicle/marp_speedo.pwn"
#include "system\marp_gate.pwn"
#include "business\marp_biz.pwn" 			//Sistema de negocios
#include "map/marp_interiors_list.pwn"
#include "house/marp_houses.pwn" 				//Sistema de casas
#include "building/marp_buildings.pwn"          //Sistema de edificios
#include "faction/marp_factions.pwn" 			//Sistema de facciones
#include "system\marp_mapro.pwn"
#include "job/marp_jobs.pwn" 					//Definiciones y funciones para los JOBS
#include "player\marp_player_guide.pwn"
#include "house\marp_armarios.pwn" 				//Sistema de armarios en las casas
#include "job/marp_thiefjob.pwn"
#include "util/marp_animations.pwn" 			//Sistema de animaciones
#include "system/marp_licenses.pwn"
#include "vehicle/marp_sprintrace.pwn"			//Sistema de picadas (carreras)
#include "faction\marp_gangzones.pwn"  					//Sistema de control de barrios
#include "map/marp_maps.pwn"  					//Mapeos del GM
#include "player/marp_saludocoordinado.pwn" 	//Sistema de saludo coordinado
#include "player/marp_descripcionyo.pwn" 		//Sistema de descripci�n /yo.
#include "player/marp_chat.pwn"
#include "item/marp_maletin.pwn" 				//sistema maletin
#include "item/marp_objects.pwn"            	//Sistema de objetos en el suelo
#include "job/marp_robobanco.pwn"         		//Robo a banco.
#include "job/marp_carthief.pwn"          		//Robo de autos.
#include "system\marp_car_lift.pwn"
#include "system\marp_gas_station.pwn"
#include "vehicle/marp_racesystem.pwn"          //Sistema de carreras
#include "player\marp_back.pwn"      	//Sistema de espalda/guardado de armas largas
#include "system/marp_afk.pwn"          		//Sistema de AFK
#include "system/marp_cmdpermissions.pwn"     	//Permisos din�micos para comandos
#include "vehicle/marp_concesionaria.pwn"		
#include "job/marp_garbjob.pwn"
#include "job/marp_tranjob.pwn"
#include "job/marp_farmjob.pwn"
#include "job/marp_drugfjob.pwn"
#include "job/marp_delijob.pwn"
#include "job/marp_taxijob.pwn"
#include "job/marp_pilotjob.pwn"
//#include "job/marp_fishjob.pwn"
#include "job\marp_busjob.pwn"
#include "util/marp_cronometro.pwn"
#include "system/marp_rolepoints.pwn"
#include "system\marp_warnings.pwn"
#include "system/marp_time_and_weather.pwn"
#include "faction/marp_same.pwn"
#include "system/marp_lifts.pwn"
#include "util/marp_actors.pwn"
#include "item/marp_parlantes.pwn"
#include "admin/marp_acmds.pwn"
#include "admin/marp_debug.pwn"
#include "admin/marp_admin_spectate.pwn"
#include "faction/marp_police.pwn"
#include "faction\equipment\marp_equipment.pwn"
#include "faction/marp_gob.pwn"
#include "faction/marp_gen.pwn"
#include "faction/marp_ctr.pwn"
#include "player/marp_pcmds.pwn"
#include "system/money/marp_money.pwn"
#include "system/money/marp_money_bars.pwn"
#include "system/money/marp_atm_gui.pwn"
#include "system/phone/marp_phone.pwn"
#include "system/marp_player_creation.pwn"
#include "system/marp_scenes.pwn"
#include "player\marp_player_update.pwn"
#include "player\marp_player_anticheat.pwn"
#include "player\marp_player_save_account.pwn"
#include "player\marp_player_basic_needs.pwn"
#include "player\marp_player_jail.pwn"
#include "vehicle/marp_police_deposit.pwn"
#include "house/marp_plantation.pwn"
#include "player/marp_player_drugs.pwn" 
#include "faction/traffic/marp_traffic.pwn"
#include "business/marp_biz_thief.pwn"
#include "system/marp_black_market.pwn"
#include "system/marp_map_marker.pwn"
#include "system\furniture\marp_furniture.pwn"
#include "system\marp_grenade_effect.pwn"
#include "system\marp_blackjack.pwn"
#include "player\marp_player_payday.pwn"
#include "system\marp_paynspray.pwn"
#include "system\tuning\marp_tuning_gui.pwn"
#include "garage\marp_garages.pwn"
#include "system/marp_firstlogin_test.pwn"
#include "vehicle/marp_altimeter.pwn"
#include "system/marp_peajes.pwn"
#include "player/marp_ayuda.pwn"
#include "system\marp_lightbar.pwn"




new timersID[24];



new	LastDeath[MAX_PLAYERS],
	DeathSpam[MAX_PLAYERS char],
	gPlayerLogged[MAX_PLAYERS],
	pLoginTransitionTimer[MAX_PLAYERS];

IsPlayerMuted(playerid) {
	return Muted[playerid];
}

new
	P_BANK,
	P_FIGHT_STYLE,
	P_HOSP_HEAL,
	P_HOSP_HEAL_2,
	P_LICENSE_CENTER,
	P_POLICE_ARREST,
	P_POLICE_ARREST2,
	P_JAIL_EAT,
	P_GEN_EAT,
	P_CAR_RENT1,
	P_CAR_RENT2,
	P_CAR_RENT3,
	P_DRUGFARM_MATS,
	P_CAR_DEMOLITION,
	P_CARPART_SHOP,
	P_POLICE_CAMERAS;

new successStart = 0;

// Timers
forward robberyCancel(playerid);
forward healTimer(playerid);
forward AceptarPipeta(playerid);
forward SoplandoPipeta(playerid);

//==============================================================================

main() {
    AntiDeAMX();
	return 1;
}

public OnGameModeInit()
{
	if(GetMaxPlayers() > MAX_PLAYERS)
    {
        printf("[ERROR] 'maxplayers' (%i) excede MAX_PLAYERS (%i). Arreglar.", GetMaxPlayers(), MAX_PLAYERS);
        SendRconCommand("exit");
		return 1;
    }

	successStart = 1;

    SQLDB_LoadConfig();
    SQLDB_Connect();

    GenerateCommandsListVector();
	DumpCommandsListToDB();

    Streamer_SetVisibleItems(.type = STREAMER_TYPE_OBJECT, .items = 850);
    CallLocalFunction("LoadMaps", "");
	LoadPickups();
	LoadGangZones();

	ShowPlayerMarkers(PLAYER_MARKERS_MODE_GLOBAL);
	EnableStuntBonusForAll(0);
    DisableInteriorEnterExits();
    AllowInteriorWeapons(1);
	ManualVehicleEngineAndLights();
	SetNameTagDrawDistance(30.0);
	AddPlayerClass(0, 0.0, 0.0, 0.0, 0.0, 0, 0, 0, 0, 0, 0);

	LoadServerInfo();
	LoadAllFactions();
	LoadAllVehicles();
	LoadAllHouses();
	LoadAllBusiness();
	
	CallLocalFunction("LoadSystemData", "");

	//===================================[TIMERS]===============================

	timersID[0] = SetTimer("VehicleFuelTimer", GAS_UPDATE_TIME, true); // 15 seg. - Actualiza la gasolina de los veh�culos.
	timersID[1] = SetTimer("GlobalUpdate", 997, true);	// 1 seg. - Actualiza el score y la hora/fecha.
	timersID[2] = SetTimer("commandPermissionsUpdate", 3600000, true); // 60 min. - Refresca los permisos de los comandos
	timersID[3] = SetTimer("VehicleDamageTimer", 1009, true); // 1 seg. - Actualiza motores da�ados y evita explosiones.
	timersID[4] = SetTimer("rentRespawn", 1000 * 60 * 20, true); // Respawn de veh�culos de renta.
	timersID[5] = SetTimer("ServerObjectsCleaningTimer", SERVER_OBJECT_UPD_TIME * 60 * 1000, true); // Borrado de objetos con mucho tiempo de vida

	ResetServerRacesVariables();
	InitializeServerSpeakers();
	CallLocalFunction("OnGameModeInitEnded", "");
	return 1;
}

forward OnGameModeInitEnded();
public OnGameModeInitEnded() {
	return 1;
}

CALLBACK:LoadSystemData() {
	return 1;
}

public OnGameModeExit()
{
	for(new i; i < sizeof(timersID); i++)
	{
		if(timersID[i]) {
			KillTimer(timersID[i]);
		}
	}

	if(successStart)
	{
		SaveServerInfo();
		SaveAllFactions();
		SaveAllVehicles();
		SaveAllHouses();
		SaveAllBusiness();
		
		CallLocalFunction("SaveSystemData", "");
	}

	DestroyGangZones();
	ServerObjects_OnServerShutDown();

	Streamer_DestroyAllItems(STREAMER_TYPE_OBJECT);
	Streamer_DestroyAllItems(STREAMER_TYPE_PICKUP);
	Streamer_DestroyAllItems(STREAMER_TYPE_CP);
	Streamer_DestroyAllItems(STREAMER_TYPE_RACE_CP);
	Streamer_DestroyAllItems(STREAMER_TYPE_MAP_ICON);
	Streamer_DestroyAllItems(STREAMER_TYPE_3D_TEXT_LABEL);
	Streamer_DestroyAllItems(STREAMER_TYPE_AREA);
	Streamer_DestroyAllItems(STREAMER_TYPE_ACTOR);

	SQLDB_Close();
	return 1;
}

CALLBACK:SaveSystemData() {
	return 1;
}

public OnPlayerRequestClass(playerid, classid)
{
	if(IsPlayerLogged(playerid)) {
		return SpawnPlayer(playerid);
	} else {
		//KickPlayer(playerid, "el sistema", "intento de selecci�n de clase sin iniciar sesi�n");
		return 0;
	}
}

public OnPlayerRequestSpawn(playerid)
{
	if(IsPlayerLogged(playerid)) {
		return 1;
	} else {
		KickPlayer(playerid, "el sistema", "intento de spawn sin iniciar sesi�n");
		return 0;
	}
}

public OnPlayerConnect(playerid)
{
	if(!AntiFlood(playerid))
		return 0;    
	if(!IsNameRoleplayValid(GetPlayerNameEx(playerid))) {
		KickPlayer(playerid, "el sistema", "nombre inv�lido. Las iniciales deben estar en may�sculas, un y solo un gui�n bajo separando nombre y apellido, y lo dem�s en min�sculas. Ej: Roberto_Martinez");
		return 0;
	}

	PlayAudioStreamForPlayer(playerid, "https://dl.dropbox.com/s/j7bia0bysvvt0pa/marp_intro_short.mp3?dl=0"); //Original: https://dl.dropbox.com/s/ml70x04z1r4orvf/marp_intro_short.mp3?dl=0
										
	OnPlayerResetStats(playerid);

	GetPlayerName(playerid, PlayerInfo[playerid][pName], MAX_PLAYER_NAME);
	SetPlayerCleanName(playerid, PlayerInfo[playerid][pName]);
	SetPlayerChatName(playerid, GetPlayerCleanName(playerid));

	GetPlayerIp(playerid, PlayerInfo[playerid][pIP], 16);
	TogglePlayerSpectating(playerid, true);
	pLoginTransitionTimer[playerid] = SetTimerEx("OnPlayerConnectDelayed", 750, false, "i", playerid);
	return 1;
}

forward OnPlayerConnectDelayed(playerid);
public OnPlayerConnectDelayed(playerid)
{
	LoginScreen_Start(playerid);
	ClearScreen(playerid);
	CallLocalFunction("RemoveMapsBuildings", "i", playerid);
	pLoginTransitionTimer[playerid] = 0;
	CheckAccountExistence(playerid);
	return 1;
}

CheckAccountExistence(playerid)
{
	new query[128];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), "SELECT Id FROM accounts WHERE Name='%s' LIMIT 1", PlayerInfo[playerid][pName]);
	mysql_tquery(MYSQL_HANDLE, query, "OnAccountExistenceChecked", "i", playerid);
}

forward OnAccountExistenceChecked(playerid);
public OnAccountExistenceChecked(playerid)
{
	if(!IsPlayerConnected(playerid))
		return 1;

	new string[128];

	if(cache_num_rows())
	{
		format(string, sizeof(string), "** %s (%d) se ha conectado al servidor (/verip ID). Registrado: si **", PlayerInfo[playerid][pName], playerid, PlayerInfo[playerid][pIP]);
		pLoginTransitionTimer[playerid] = SetTimerEx("StartAccountLogin", 2500, false, "i", playerid);
	}
	else
	{
		format(string, sizeof(string), "** %s (%d) se ha conectado al servidor (/verip ID). Registrado: no **", PlayerInfo[playerid][pName], playerid, PlayerInfo[playerid][pIP]);
		pLoginTransitionTimer[playerid] = SetTimerEx("CheckForMaxAccounts", 2500, false, "i", playerid);
	}

	new sqlid = (cache_num_rows()) ? (cache_index_int(0, 0)) : (0);

	CheckAccountBans(playerid, sqlid);
	AdministratorMessage(COLOR_GREY, string, 2);
	return 1;
}



CheckAccountBans(playerid, playersqlid)
{
	new query[128];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), "SELECT * FROM bans WHERE (pID=%i OR pIP='%s') AND banActive=1 LIMIT 1", playersqlid, PlayerInfo[playerid][pIP]);
	mysql_tquery(MYSQL_HANDLE, query, "OnAccountBansChecked", "ii", playerid, playersqlid);
}

forward OnAccountBansChecked(playerid, playersqlid);
public OnAccountBansChecked(playerid, playersqlid)
{
	if(!IsPlayerConnected(playerid))
		return 1;

	if(cache_num_rows())
	{
		new issuerName[MAX_PLAYER_NAME], banReason[128], banEndDate[32], banEndUnix;

		cache_get_value_name(0, "banIssuerName", issuerName, MAX_PLAYER_NAME);
		cache_get_value_name(0, "banReason", banReason, 128);
		cache_get_value_name(0, "banEnd", banEndDate, 32);
		cache_get_value_name_int(0, "banEndUnix", banEndUnix);
	    
	    if(gettime() > banEndUnix)
	    {
		    SendFMessage(playerid, COLOR_ADMINCMD, "[SERVIDOR] has sido desbaneado ya que el baneo temporal aplicado por %s finaliz� el %s.", issuerName, banEndDate);

		    new query[128];  
	        mysql_format(MYSQL_HANDLE, query, sizeof(query), "UPDATE bans SET banActive=0 WHERE (pID=%i OR pIP='%s') AND banActive=1 LIMIT 1", playersqlid, PlayerInfo[playerid][pIP]);
			mysql_tquery(MYSQL_HANDLE, query);
		}
		else
		{
			SendFMessage(playerid, COLOR_ADMINCMD, "Te encuentras baneado/a hasta el %s por %s, raz�n: %s", banEndDate, issuerName, banReason);
			SendClientMessage(playerid, COLOR_ADMINCMD, "Ser�s desbaneado autom�ticamente por el servidor en el momento de finalizaci�n del baneo.");
			SendClientMessage(playerid, COLOR_ADMINCMD, "Para m�s informaci�n o para realizar un reclamo/descargo, dir�gete a nuestro canal de Discord o al foro en www.pheek.net");
			SetTimerEx("kickTimer", 1000, false, "d", playerid);
			return 1;
		}
	}
	
	return 1;
}

CALLBACK:CheckForMaxAccounts(playerid)
{
	new query[128];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), "SELECT Id FROM accounts WHERE Ip='%e' LIMIT 3", PlayerInfo[playerid][pIP]);
	mysql_tquery(MYSQL_HANDLE, query, "OnMaxAccountsChecked", "i", playerid);
}

CALLBACK:OnMaxAccountsChecked(playerid)
{
	pLoginTransitionTimer[playerid] = 0;

	if (cache_num_rows() > 2)
	{
		SendClientMessage(playerid, COLOR_ADMINCMD, "Se alcanzo el m�ximo de cuentas posibles registradas por IP.");
		SendClientMessage(playerid, COLOR_ADMINCMD, "Si consideras que esto es un error, realiaz un ticket via discord.");
		SetTimerEx("kickTimer", 1000, false, "d", playerid);
		return 1;
	}

	CallLocalFunction("AccountRegister", "i", playerid);
	return 1;
}

forward AccountRegister(playerid);
public AccountRegister(playerid)
{
	pLoginTransitionTimer[playerid] = 0;
	new str[561+1];
 	format(str, sizeof(str), "Malos Aires Roleplay es un servidor de rol basado en Buenos Aires, Argentina (IC conocidos como Malos Aires y Argencholina).\nEs importante tener en cuenta que cualquier rol fuera del contexto del ambiente Argentino/Latino, podr� ser considerado NIP y sancionado en consecuencia.\n\nEntiendase: pandillero americano, ex militar, ex fbi, entre otros.\n\n\nSi tienes dudas utiliza /duda o contacta a un staff por discord.\nPara m�s informaci�n ingresa al foro (www.pheek.net) o al discord del servidor.\n\n\nA continuaci�n deber�s realizar un corto examen de rol", str);
    Dialog_Show(playerid, DLG_TUT, DIALOG_STYLE_MSGBOX, "�Bienvenido a Malos Aires!", str, "Aceptar", "");
    return 1;
}

forward StartAccountLogin(playerid);
public StartAccountLogin(playerid)
{
	pLoginTransitionTimer[playerid] = 0;
	Dialog_Show(playerid, DLG_LOGIN, DIALOG_STYLE_PASSWORD, "�Bienvenido a Malos Aires!", "Para comenzar, por favor ingresa tu contrase�a:", "Ingresar", "");
	return 1;
}


Dialog:DLG_LOGIN(playerid, response, listitem, inputtext[])
{
    if(!response)
    	return KickPlayer(playerid, "el sistema", "evadir inicio de sesi�n");
	if(gPlayerLogged[playerid])
		return 1;

	new query[256];
	strcat(query, inputtext, sizeof(query));
	mysql_escape_string(query, query, sizeof(query), MYSQL_HANDLE);
	mysql_format(MYSQL_HANDLE, query, sizeof(query), "SELECT FirstLogin FROM accounts WHERE Name = '%s' AND Password = MD5('%s') LIMIT 1", PlayerInfo[playerid][pName], query);
	mysql_tquery(MYSQL_HANDLE, query, "OnAccountPasswordChecked", "i", playerid);
    return 1;
}

forward OnAccountPasswordChecked(playerid);
public OnAccountPasswordChecked(playerid)
{
	if(!IsPlayerConnected(playerid))
		return 1;

	if(cache_num_rows())
	{
		if(cache_name_int(0, "FirstLogin")) {
			StartAccountFirstLogin(playerid);
		} else {
			LoadPlayerAccountData(playerid);
		}
	}
	else
	{
	    SetPVarInt(playerid, "LoginAttempts", GetPVarInt(playerid, "LoginAttempts") + 1);

	    if(GetPVarInt(playerid, "LoginAttempts") > MAX_LOGIN_ATTEMPTS)
	        return KickPlayer(playerid, "el sistema", "demasiados intentos de iniciar sesi�n");

		Dialog_Show(playerid, DLG_LOGIN, DIALOG_STYLE_PASSWORD, "�Bienvenido a Malos Aires!", "{E44A4A}�Contrase�a incorrecta!\n\n"COLOR_EMB_DLG_DEFAULT"Ingresa tu contrase�a por favor:", "Ingresar", "");
	}
	return 1;
}

StartAccountFirstLogin(playerid)
{
	LoginScreen_End(playerid, 3000); // In 3000 ms hide and destroy textdraws
	pLoginTransitionTimer[playerid] = PCC_Start(playerid, 5000); // delay character creation panel to 5000 ms
	FadeScreen_StartForPlayer(playerid, 0x0, 2500, 1000); // Color 0 = black, 2500 ms transition time, 1000 hold on max time
	
}

OnPlayerCreationSuccess(playerid, skin, sex, age)
{
	new query[128];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), "UPDATE accounts SET FirstLogin=0, Skin=%i, Age=%i, Sex=%i WHERE Name = '%s' LIMIT 1", skin, age, sex, PlayerInfo[playerid][pName]);
	mysql_tquery(MYSQL_HANDLE, query, "OnAccountCreationSucceded", "i", playerid);
	pLoginTransitionTimer[playerid] = 0;
	return 1;
}

forward OnAccountCreationSucceded(playerid);
public OnAccountCreationSucceded(playerid)
{
	if(!IsPlayerConnected(playerid))
		return 1;

	LoadPlayerAccountData(playerid);
	return 1;
}

LoadPlayerAccountData(playerid)
{
	LoginScreen_End(playerid, 3000); // In 3000 ms hide and destroy textdraws
	FadeScreen_StartForPlayer(playerid, 0x0, 2500, 4000); // Color 0 = black, 2500 ms transition time, 4000ms hold on max time
	pLoginTransitionTimer[playerid] = SetTimerEx("LoadPlayerAccountDataDelayed", 2700, false, "i", playerid);
}

forward LoadPlayerAccountDataDelayed(playerid);
public LoadPlayerAccountDataDelayed(playerid)
{
	new query[128];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), "SELECT * FROM accounts WHERE Name = '%s' LIMIT 1", PlayerInfo[playerid][pName]);
	mysql_tquery(MYSQL_HANDLE, query, "OnPlayerAccountDataLoad", "i", playerid);
	pLoginTransitionTimer[playerid] = 0;
	return 1;
}

forward OnPlayerAccountDataLoad(playerid);
public OnPlayerAccountDataLoad(playerid)
{
	if(!IsPlayerConnected(playerid))
		return 1;
	if(!cache_num_rows())
		return KickPlayer(playerid, "el sistema", "cuenta no encontrada en la base de datos.");

	DeletePVar(playerid, "LoginAttempts");

    cache_get_value_name_int(0, "Id", PlayerInfo[playerid][pID]);
	cache_get_value_name_int(0, "Level", PlayerInfo[playerid][pLevel]);
	cache_get_value_name_int(0, "AdminLevel", PlayerInfo[playerid][pAdmin]);
	cache_get_value_name_int(0, "Sex", PlayerInfo[playerid][pSex]);
	cache_get_value_name_int(0, "Age", PlayerInfo[playerid][pAge]);
	cache_get_value_name_int(0, "Exp", PlayerInfo[playerid][pExp]);
	cache_get_value_name_int(0, "CashMoney", PlayerInfo[playerid][pCash]);
	cache_get_value_name_int(0, "BankMoney", PlayerInfo[playerid][pBank]);
	cache_get_value_name_int(0, "Skin", PlayerInfo[playerid][pSkin]);
	cache_get_value_name_int(0, "pThirst", PlayerInfo[playerid][pThirst]);
	cache_get_value_name_int(0, "pHunger", PlayerInfo[playerid][pHunger]);
	cache_get_value_name_int(0, "Job", PlayerInfo[playerid][pJob]);
	PlayerInfo[playerid][pJobSkin] = 0; // Por ahora sin entrada en base de datos.
	cache_get_value_name_int(0, "JobTime", PlayerInfo[playerid][pJobTime]);
	cache_get_value_name_int(0, "pTimePlayed", PlayerInfo[playerid][pTimePlayed]);
	cache_get_value_name_int(0, "PayCheck", PlayerInfo[playerid][pPayCheck]);
	cache_get_value_name_int(0, "pPayTime", PlayerInfo[playerid][pPayTime]);
	cache_get_value_name_int(0, "Faction", PlayerInfo[playerid][pFaction]);
	cache_get_value_name_int(0, "Rank", PlayerInfo[playerid][pRank]);
	cache_get_value_name_int(0, "HouseKey", PlayerInfo[playerid][pHouseKey]);
	cache_get_value_name_int(0, "pRolePoints", PlayerInfo[playerid][pRolePoints]);
	cache_get_value_name_int(0, "Warnings", PlayerInfo[playerid][pWarnings]);
	cache_get_value_name_int(0, "CarLic", PlayerInfo[playerid][pCarLic]);
	cache_get_value_name_int(0, "FlyLic", PlayerInfo[playerid][pFlyLic]);
	cache_get_value_name_int(0, "WepLic", PlayerInfo[playerid][pWepLic]);
	cache_get_value_name_int(0, "PhoneNumber", PlayerInfo[playerid][pPhoneNumber]);
	cache_get_value_name_int(0, "Jailed", PlayerInfo[playerid][pJailed]);
	cache_get_value_name_int(0, "JailedTime", PlayerInfo[playerid][pJailTime]);
	cache_get_value_name_int(0, "pInterior", PlayerInfo[playerid][pInterior]);
	cache_get_value_name_int(0, "pWorld", PlayerInfo[playerid][pVirtualWorld]);
	cache_get_value_name_int(0, "pHospitalized", PlayerInfo[playerid][pHospitalized]);
	cache_get_value_name_int(0, "pCrack", PlayerInfo[playerid][pCrack]);
	cache_get_value_name_int(0, "pWantedLevel", PlayerInfo[playerid][pWantedLevel]);
	cache_get_value_name_int(0, "pCantWork", PlayerInfo[playerid][pCantWork]);
	cache_get_value_name_int(0, "pMuteB", PlayerInfo[playerid][pMuteB]);
	cache_get_value_name_int(0, "pRentCarID", PlayerInfo[playerid][pRentCarID]);
	cache_get_value_name_int(0, "pRentCarRID", PlayerInfo[playerid][pRentCarRID]);
	cache_get_value_name_int(0, "pFightStyle", PlayerInfo[playerid][pFightStyle]);
	
	cache_get_value_name(0, "Name", PlayerInfo[playerid][pName], MAX_PLAYER_NAME);
	cache_get_value_name(0, "AdminNickName", PlayerInfo[playerid][aNick], MAX_PLAYER_NAME);
	cache_get_value_name(0, "LastConnected", PlayerInfo[playerid][pLastConnected], 32);
	cache_get_value_name(0, "pAccusedOf", PlayerInfo[playerid][pAccusedOf], 64);
	cache_get_value_name(0, "pAccusedBy", PlayerInfo[playerid][pAccusedBy], 24);
	cache_get_value_name(0, "pQuestion", PlayerInfo[playerid][pQuestion], 144);

    cache_get_value_name_int(0, "pHaveQuestion", PlayerInfo[playerid][pHaveQuestion]);
	cache_get_value_name_float(0, "pX", PlayerInfo[playerid][pX]);
	cache_get_value_name_float(0, "pY", PlayerInfo[playerid][pY]);
	cache_get_value_name_float(0, "pZ", PlayerInfo[playerid][pZ]);
	cache_get_value_name_float(0, "pA", PlayerInfo[playerid][pA]);
	cache_get_value_name_float(0, "pHealth", PlayerInfo[playerid][pHealth]);
	cache_get_value_name(0, "pWounds", PlayerInfo[playerid][pWounds], 32);


	
	cache_get_value_name_int(0, "pContainerSQLID", PlayerInfo[playerid][pContainerSQLID]);
	cache_get_value_name_int(0, "pBeltSQLID", PlayerInfo[playerid][pBeltSQLID]);
	

	//=============================MANO DERECHA=============================
	cache_get_value_name_int(0, "r_hand_item", HandInfo[playerid][HAND_RIGHT][Item]);
	cache_get_value_name_int(0, "r_hand_param", HandInfo[playerid][HAND_RIGHT][Amount]);

	if(ItemModel_GetType(HandInfo[playerid][HAND_RIGHT][Item]) == ITEM_CONTAINER) {
	    HandInfo[playerid][HAND_RIGHT][Amount] = Container_Load(HandInfo[playerid][HAND_RIGHT][Amount]);
	}

    //============================MANO IZQUIERDA============================
	cache_get_value_name_int(0, "l_hand_item", HandInfo[playerid][HAND_LEFT][Item]);
	cache_get_value_name_int(0, "l_hand_param", HandInfo[playerid][HAND_LEFT][Amount]);

	if(ItemModel_GetType(HandInfo[playerid][HAND_LEFT][Item]) == ITEM_CONTAINER) {
	    HandInfo[playerid][HAND_LEFT][Amount] = Container_Load(HandInfo[playerid][HAND_LEFT][Amount]);
	}

    //===============================ESPALDA================================
	cache_get_value_name_int(0, "back_carry", BackInfo[playerid][backCarryType]);
    cache_get_value_name_int(0, "back_item", BackInfo[playerid][backItem]);
	cache_get_value_name_int(0, "back_param", BackInfo[playerid][backAmount]);

	if(ItemModel_GetType(BackInfo[playerid][backItem]) == ITEM_CONTAINER) {
	    BackInfo[playerid][backAmount] = Container_Load(BackInfo[playerid][backAmount]);
	}

	//======================================================================
	
	gPlayerLogged[playerid] = 1;
	LoadPlayerJobData(playerid); // Info del job
   	
   	if(Faction_IsValidId(PlayerInfo[playerid][pFaction]))
   	{
   		if(Faction_HasTag(PlayerInfo[playerid][pFaction], FAC_TAG_ALLOW_GANGZONE))
   		    ShowGangZonesToPlayer(playerid);
	}

	//===========================CARGA DE CONTENEDOR========================

	if(PlayerInfo[playerid][pContainerSQLID] > 0)
	    PlayerInfo[playerid][pContainerID] = Container_Load(PlayerInfo[playerid][pContainerSQLID]);
	else
	    Container_Create(CONTAINER_INV_SPACE, 1, PlayerInfo[playerid][pContainerID], PlayerInfo[playerid][pContainerSQLID]);

	if (PlayerInfo[playerid][pFaction] == FAC_PMA || PlayerInfo[playerid][pFaction] == FAC_SIDE)
	{
		if(PlayerInfo[playerid][pBeltSQLID] > 0)
			PlayerInfo[playerid][pBeltID] = Container_Load(PlayerInfo[playerid][pBeltSQLID]);
		else
			Container_Create(CONTAINER_BELT_SPACE, 1, PlayerInfo[playerid][pBeltID], PlayerInfo[playerid][pBeltSQLID]);
	}

	//======================================================================
    
    SetPlayerCash(playerid,PlayerInfo[playerid][pCash]);
    SetPlayerHealth(playerid, PlayerInfo[playerid][pHealth]);
	SetPlayerScore(playerid, PlayerInfo[playerid][pLevel]);
	SetSpawnInfo(playerid, 1, PlayerInfo[playerid][pSkin], PlayerInfo[playerid][pX], PlayerInfo[playerid][pY], PlayerInfo[playerid][pZ], PlayerInfo[playerid][pA], 0, 0, 0, 0, 0, 0);

	if(PlayerInfo[playerid][pAdmin]) {
	    SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" �Bienvenido! Para ver los comandos de administraci�n escribe /acmds.");
	} else {
	    SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" �Bienvenido! Si necesitas asistencia escribe '/ayuda', o usa '/gps' para conocer distintas ubicaciones.");
		SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Record� unirte a nuestro servidor de Discord para estar al tanto de las novedades: "COLOR_EMB_ALERT"https://discord.gg/QXTkRamKMe");
		SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Nuestro agradecimiendo a los desarrolladores de Malos Aires a lo largo del tiempo: "COLOR_EMB_ALERT"/marp");
	}
	
	if(PlayerInfo[playerid][pRentCarID] > 0)
	{
	    if(RentCarInfo[PlayerInfo[playerid][pRentCarRID]][rRented] == 1 && RentCarInfo[PlayerInfo[playerid][pRentCarRID]][rOwnerSQLID] == PlayerInfo[playerid][pID])
	        SendFMessage(playerid, COLOR_WHITE, "Te quedan %d minutos de renta del veh�culo que alquilaste.", RentCarInfo[PlayerInfo[playerid][pRentCarRID]][rTime]);
		else
	    {
	    	PlayerInfo[playerid][pRentCarRID] = 0;
	    	PlayerInfo[playerid][pRentCarID] = 0;
	    	SendClientMessage(playerid, COLOR_WHITE, "Se ha acabado el tiempo de renta de tu veh�culo alquilado.");
		}
	}
	
	SendClientMessage(playerid, COLOR_WHITE, " ");

	if(GetPlayerState(playerid) == PLAYER_STATE_SPECTATING) {
		TogglePlayerSpectating(playerid, false);
	} else {
		SpawnPlayer(playerid);
	}

	SyncPlayerTimeAndWeather(playerid);
	CallLocalFunction("LoadAccountDataEnded", "i", playerid);
	return 1;
}

forward LoadAccountDataEnded(playerid);
public LoadAccountDataEnded(playerid)
{
	StopAudioStreamForPlayer(playerid);
	LoginCamera_Start(playerid, 1000, 5500); // 1000ms delay to init login camera, and then an extra 5500ms to start interpolation
	return 1;
}

OnPlayerResetStats(playerid)
{
    MedDuty[playerid] = 0;
    
	/* Vehiculos */
    OfferingVehicle[playerid] = false;
    VehicleOfferPrice[playerid] = -1;
    VehicleOffer[playerid] = INVALID_PLAYER_ID;
    VehicleOfferID[playerid] = -1;
	startingEngine[playerid] = false;
    
    /* Venta de casas */
	ResetHouseOffer(playerid);

	/* Venta de negocios */
	ResetBusinessOffer(playerid);

	BlowingPipette[playerid] = 0;
    OfferingPipette[playerid] = 0;
	
	smoking[playerid] = 0;
	LastVeh[playerid] = 0;
	
	LastCP[playerid] = -1;
	CollectedProds[playerid] = 0;
		
	/* Saludo */
	saluteOffer[playerid] = INVALID_PLAYER_ID;
	saluteStyle[playerid] = 0;
	
	/*Sistema de robo al banco*/
	ResetRobberyGroupVariables(playerid);

	/* Licencia de armas */
	wepLicOffer[playerid] = INVALID_PLAYER_ID;
	
	/* Revision de usuarios */
	ReviseOffer[playerid] = 999;
	
	/* Sistema de camaras */
	usingCamera[playerid] = false;
	
	/* Sistema de Picadas */
	resetSprintRace(playerid);

	/* Sistema de carreras */
	ResetPlayerRaceVariables(playerid);
	
	/* Descripciones de 3Dtexts */
	ResetDescVariables(playerid);
	
	/* Sistema de stream de radios */
	Radio_Reset(playerid);
	
	/* Sistema de entrevistas para CTRMAN */
	InterviewOffer[playerid] = 999;
	InterviewActive[playerid] = false;
	
	/* Sistema de casino */
	isBetingRoulette[playerid] = false;
	isBetingFortune[playerid] = false;
	isBetingTragamonedas[playerid] = false;
	
	/* Sistema de hambre y sed */
    PlayerInfo[playerid][pThirst] = 100;
	PlayerInfo[playerid][pHunger] = 100;
	
	/* Cintur�n de Seguridad */
	SeatBelt[playerid] = false;

	/* Sistema de toggle */
	p_toggle[playerid] = e_ToggleFlags:0xFFFFFFFF; // All flags in p_toggle ON
	
	/* Administraci�n */
	AdminDuty[playerid] = false;
	AdminPMsEnabled[playerid] = false;
	AdminWhispersEnabled[playerid] = false;
	AdminFactionEnabled[playerid] = false;
	AdminSMSEnabled[playerid] = false;
	Admin911Enabled[playerid] = false;

    jobDuty[playerid] = false;
	TicketOffer[playerid] = 999;
	TicketMoney[playerid] = 0;
	PlayerCuffed[playerid] = 0;
	CopDuty[playerid] = 0;
	SIDEDuty[playerid] = 0;
	Muted[playerid] = 0;
	HospHealing[playerid] = 0;
	SetPlayerColor(playerid, COLOR_NOTLOGGED);
	FactionRequest[playerid] = 0;
	Mobile[playerid] = 255;
	gPlayerLogged[playerid] = 0;
	
	PlayerInfo[playerid][pFightStyle] = 0;
	PlayerInfo[playerid][pMuteB] = 0;

	PlayerInfo[playerid][pID] = 0;
	PlayerInfo[playerid][pCantWork] = 0;
	PlayerInfo[playerid][pWantedLevel] = 0;
	PlayerInfo[playerid][pWarnings] = 0;
	PlayerInfo[playerid][pLevel] = 1;
	PlayerInfo[playerid][pName] = "XXXXXXXXXXXXXXXXXXXXXXX";
	PlayerInfo[playerid][pLastConnected] = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	PlayerInfo[playerid][pIP] = "XXXXXXXXXXXXXXX";
	PlayerInfo[playerid][aNick] ="XXXXXXXXXXXXXXXXXXXXXXX";
	PlayerInfo[playerid][pAdmin] = 0;
	PlayerInfo[playerid][pQuestion][0] = '\0';
	PlayerInfo[playerid][pHaveQuestion] = 0;
	PlayerInfo[playerid][pReport] = 0;
	PlayerInfo[playerid][pSex] = 1;
	PlayerInfo[playerid][pAge] = 0;
	PlayerInfo[playerid][pExp] = 0;
	PlayerInfo[playerid][pCash] = 0;
	PlayerInfo[playerid][pBank] = 0;
	PlayerInfo[playerid][pSkin] = 0;
	PlayerInfo[playerid][pJob] = 0;
	PlayerInfo[playerid][pJobSkin] = 0;
	PlayerInfo[playerid][pJobTime] = 0;
	PlayerInfo[playerid][pTimePlayed] = 0;
	PlayerInfo[playerid][pPayCheck] = 0;
	PlayerInfo[playerid][pPayTime] = 0;
	PlayerInfo[playerid][pDead] = 0;
	PlayerInfo[playerid][pDisabled] = DISABLE_NONE;
	PlayerInfo[playerid][pFaction] = 0;
	PlayerInfo[playerid][pRank] = 0;
	PlayerInfo[playerid][pHouseKey] = 0;
	PlayerInfo[playerid][pWarnings] = 0;
	PlayerInfo[playerid][pCarLic] = 0;
	PlayerInfo[playerid][pWepLic] = 0;
	PlayerInfo[playerid][pFlyLic] = 0;
	PlayerInfo[playerid][pPhoneNumber] = 0;
	PlayerInfo[playerid][pJailed] = JAIL_NONE;
	PlayerInfo[playerid][pJailTime] = 0;
	PlayerInfo[playerid][pX] = 1481.2136;
	PlayerInfo[playerid][pY] = -1751.6758;
	PlayerInfo[playerid][pZ] = 15.4453;
	PlayerInfo[playerid][pA] = 358.1794;
	PlayerInfo[playerid][pInterior] = 0;
	PlayerInfo[playerid][pVirtualWorld] = 0;
	PlayerInfo[playerid][pHospitalized] = 0;
	PlayerInfo[playerid][pCrack] = 0;
	PlayerInfo[playerid][pHealth] = 100.0;
	PlayerInfo[playerid][pArmour] = 0.0;
	PlayerInfo[playerid][pRentCarID] = 0;
	PlayerInfo[playerid][pRentCarRID] = 0;
	PlayerInfo[playerid][pRolePoints] = 0;
	PlayerInfo[playerid][pContainerSQLID] = 0;
	PlayerInfo[playerid][pContainerID] = 0;
	PlayerInfo[playerid][pBeltSQLID] = 0;
	PlayerInfo[playerid][pBeltID] = 0;
	
 	ResetJobVariables(playerid);
 	
 	ResetContainerSelection(playerid);

	J_Garb_ResetVars(playerid);
	J_Garb_ResetInfo(playerid);
	return 0;
}

public OnPlayerDisconnect(playerid, reason)
{
	if(pLoginTransitionTimer[playerid])
	{
		KillTimer(pLoginTransitionTimer[playerid]);
		pLoginTransitionTimer[playerid] = 0;
	}

	ResetDescLabel(playerid);
	AdminDutyNickOff(playerid);
	
    KillTimer(GetPVarInt(playerid, "CancelVehicleTransfer"));
    KillTimer(GetPVarInt(playerid, "CancelDrugTransfer"));
    KillTimer(GetPVarInt(playerid, "robberyCancel"));
    KillTimer(GetPVarInt(playerid, "fuelCar"));
	KillTimer(GetPVarInt(playerid, "fuelCarWithCan"));
	KillTimer(ReplenishDescTimer[playerid]);
	ResetThiefCrime(playerid);

	if(jobDuty[playerid])
	{
		if(GetJobType(PlayerInfo[playerid][pJob]) == JOB_TYPE_LEGAL)
		{
			SetVehicleToRespawn(jobVehicle[playerid]);
		}
	}

	OnPlayerLeaveRobberyGroup(playerid, 1);

	EndPlayerDuty(playerid);
	
	deleteAbandonedSprintRace(playerid);
	OnPlayerLeaveRace(playerid);
	
	Radio_Stop(playerid);
		
	HideGangZonesToPlayer(playerid);
	
	Cronometro_Borrar(playerid);

 	if(gPlayerLogged[playerid])
	{
		switch(reason)
		{
	        case 0: PlayerLocalMessage(playerid, 30.0, "se ha desconectado (raz�n: timeout/crash).");
			case 1: PlayerLocalMessage(playerid, 30.0, "se ha desconectado (raz�n: a voluntad).");
			case 2: PlayerLocalMessage(playerid, 30.0, "se ha desconectado (raz�n: kick/ban).");
	    }

		SaveAccount(playerid);
	}

	SetPlayerCarrying(playerid, false);

	DestroyPlayerInventory(playerid);
	DestroyPlayerDutyBelt(playerid);
	DestroyPlayerHands(playerid);
	Back_DestroyContainer(playerid);

	Job_WorkingPlayerDisconnect(playerid);

	J_Garb_OnPlayerDisconnect(playerid);
	J_Pilot_OnPlayerDisconnect(playerid);
	J_Bus_OnPlayerDisconnect(playerid);

	ResetJobVariables(playerid);
	gPlayerLogged[playerid] = 0;
	return 1;
}

public OnPlayerSpawn(playerid)
{
	if(!gPlayerLogged[playerid])
		return 0;

	PlayerInfo[playerid][pDead] = 0;

	SetNormalPlayerGunSkills(playerid);
	SetPlayerFightingStyle(playerid, PlayerInfo[playerid][pFightStyle]);

	if((jobDuty[playerid] || isPlayerCopOnDuty(playerid) || IsMedicOnDuty(playerid) || isPlayerSideOnDuty(playerid)) && PlayerInfo[playerid][pJobSkin] != 0) {
		SetPlayerSkin(playerid, PlayerInfo[playerid][pJobSkin]);
	} else {
		SetPlayerSkin(playerid, PlayerInfo[playerid][pSkin]);
	}

	if(AdminDuty[playerid])
	{
		SetPlayerColor(playerid, COLOR_ADMINDUTY);
		SetPlayerHealthEx(playerid, 50000);
	}  else {
		SetPlayerColor(playerid, 0xFFFFFF00);
	}

	if(PlayerInfo[playerid][pJailed])
	{
		SetPlayerHealthEx(playerid, 100.0);
		TeleportPlayerTo(playerid, PlayerInfo[playerid][pX], PlayerInfo[playerid][pY], PlayerInfo[playerid][pZ], PlayerInfo[playerid][pA], PlayerInfo[playerid][pInterior], PlayerInfo[playerid][pVirtualWorld], .forceLoadingTime = true);
		return 1;
	}

	if(WasPlayerSpectating(playerid)) {
		ResetPlayerSpectate(playerid);
	} else {
		if(PlayerInfo[playerid][pHospitalized] >= 1) {
			InitiateHospital(playerid);
		} else  {
			TeleportPlayerTo(playerid, PlayerInfo[playerid][pX], PlayerInfo[playerid][pY], PlayerInfo[playerid][pZ], PlayerInfo[playerid][pA], PlayerInfo[playerid][pInterior], PlayerInfo[playerid][pVirtualWorld]);
		}
	}

	LoadHandItem(playerid, HAND_RIGHT);
	LoadHandItem(playerid, HAND_LEFT);
	Back_ShowGraphicObject(playerid);
	return 1;
}

public OnPlayerDeath(playerid, killerid, reason)
{
	new time = gettime();

	PlayerInfo[playerid][pDead] = 1;
	PlayerInfo[playerid][pHealth] = 10.0;
	PlayerInfo[playerid][pDisabled] = DISABLE_NONE;

	if(0 <= (time - LastDeath[playerid]) <= 3)
	{
		DeathSpam[playerid]++;

		if(DeathSpam{playerid} == 3)
			return BanPlayer(playerid, INVALID_PLAYER_ID, "fake kills cheat", 0);
	} else {
		DeathSpam{playerid} = 0;
	}
    
    LastDeath[playerid] = time;

	GetPlayerPos(playerid, PlayerInfo[playerid][pX], PlayerInfo[playerid][pY], PlayerInfo[playerid][pZ]);
	PlayerInfo[playerid][pVirtualWorld] = GetPlayerVirtualWorld(playerid);
	PlayerInfo[playerid][pInterior] = GetPlayerInterior(playerid);
    
	if(AdminDuty[playerid])
		return 1;

	if(IsPlayerLogged(killerid))
	{
		new string[128];

	    if(PlayerInfo[killerid][pJailed] == JAIL_OOC)
		{
		    SendFMessage(killerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Tu condena ha sido aumentada en %i segundos. Raz�n: DM.", DM_JAILTIME);
	        PlayerInfo[killerid][pJailTime] += DM_JAILTIME;
	    }
		else if(killerid != playerid)
		{
			format(string, sizeof(string), "[STAFF] %s(ID %d) mat� a %s (ID %d).", GetPlayerCleanName(killerid), killerid, GetPlayerCleanName(playerid), playerid);
			AdministratorMessage(COLOR_ADMINCMD, string, 2);
            if(PlayerInfo[playerid][pWantedLevel] > 0 && isPlayerCopOnDuty(killerid))
			{
				SendFMessage(killerid, COLOR_WHITE, "Has reducido a %s y ha sido arrestado por miembros del departamento de polic�a.", GetPlayerCleanName(playerid));
				PlayerInfo[playerid][pJailTime] = PlayerInfo[playerid][pWantedLevel] * 3 * 60;
				SendClientMessage(playerid, COLOR_LIGHTBLUE, "Has sido reducido y arrestado por miembros de la polic�a perdiendo todas las armas y drogas en el inventario.");
				format(string, sizeof(string), "[CENTRAL] %s ha reducido y arrestado al criminal %s (%i minutos).", GetPlayerCleanName(killerid), GetPlayerCleanName(playerid), PlayerInfo[playerid][pJailTime] / 60);
				SendFactionMessage(FAC_PMA, COLOR_PMA, string);
				PlayerInfo[playerid][pJailed] = JAIL_IC_PMA;
				PlayerInfo[playerid][pX] = 193.5868;
				PlayerInfo[playerid][pY] = 175.3084;
				PlayerInfo[playerid][pZ] = 1003.1221;
				PlayerInfo[playerid][pA] = 180.0000;
				
				ResetPlayerWantedLevelEx(playerid);
				Container_Empty_Weapons(PlayerInfo[playerid][pContainerID]);
				Faction_GiveMoney(FAC_GOB, PlayerInfo[playerid][pJailTime]);
				SaveFaction(FAC_GOB);
			}
	    }
 	}
 	
	if(PlayerInfo[playerid][pJailed] == JAIL_NONE) {
		PlayerInfo[playerid][pHospitalized] = 1;
	}

	// Los policias en servicio que mueren no pierden las armas (no se las confiscan en el hospital)
	if(!isPlayerCopOnDuty(playerid))
	{
	    ResetHandsWeapons(playerid);
  		Back_ResetWeapon(playerid);
	}

	EndPlayerDuty(playerid);
	ResetThiefCrime(playerid);

	Radio_Stop(playerid);
		
	OnPlayerLeaveRobberyGroup(playerid, 2);

	BusJob_PlayerDeath(playerid);
	ResetJobVariables(playerid);
	return 1;
}

public OnPlayerText(playerid, text[])
{
    if(!gPlayerLogged[playerid]) {
    	return 0;
    }

	if(Muted[playerid])	{
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] No puedes hablar, has sido silenciado.");
		return 0;
	}

	if(usingCamera[playerid]) {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] No puedes hablar mientras estas viendo una c�mara.");
		return 0;
	}

	new string[256], name[MAX_PLAYER_NAME]; name = GetPlayerChatName(playerid);

    if(!IsPlayerInAnyVehicle(playerid) || Veh_GetModelType(GetPlayerVehicleID(playerid)) != VTYPE_CAR)
	{
		if(BitFlag_Get(p_toggle[playerid], FLAG_TOGGLE_TALKANIM) && GetPlayerState(playerid) == PLAYER_STATE_ONFOOT) {
			ApplyPlayerTalkAnimation(playerid, 1250 + strlen(text) * 50);
		}

        format(string, sizeof(string), "%s dice: %s", name, text);
		SendPlayerMessageInRange(15.0, playerid, string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5);
	}
	else
	{
		new vehSeat = GetPlayerVehicleSeat(playerid), winState[4];

        GetVehicleParamsCarWindows(GetPlayerVehicleID(playerid), winState[0], winState[1], winState[2], winState[3]);

	  	if(vehSeat < 0 || vehSeat > 3 || winState[vehSeat] != 0)
	    {
	        format(string, sizeof(string), "[Ventanillas cerradas] %s dice: %s", name, text);
			SendPlayerMessageInRange(5.0, playerid, string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5);
		}
		else
		{
			format(string, sizeof(string), "[Ventanillas abiertas] %s dice: %s", name, text);
			SendPlayerMessageInRange(15.0, playerid, string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5);
		}
	}
    return 0;
}

public OnPlayerCommandReceived(playerid, cmdtext[]) 
{    
    if(!gPlayerLogged[playerid]) {
        return 0;
    }

    new cmd_str[130];
	sscanf(cmdtext, "s[130] ", cmd_str);

	if(strlen(cmd_str) > (MAX_FUNC_NAME - 4)) { // {strlen("cmd_"+cmd_str-"/") <= 31} <=> {strlen(cmd_str) <= 28}
		return 0;
	}

    if(checkCmdPermission(cmd_str, PlayerInfo[playerid][pAdmin]) == 0) {
        SendClientMessage(playerid, COLOR_ERROR, "[ERROR] No tienes acceso a este comando");
        return 0;
    }

	if(usingCamera[playerid] && strcmp(cmdtext,"/salircam") != 0) {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] Para utilizar un comando antes debes salir de la c�mara.");
	    return 0;
	}
    return 1;
}

public OnPlayerCommandPerformed(playerid, cmdtext[], success) 
{
	if(!success) {
		SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "Comando desconocido. Para ver una lista de comandos usa [/ayuda] o envia [/duda] a un administrador.");
	}
	return 1;
}

AntiDeAMX() {
    new b;
    #emit load.pri b
    #emit stor.pri b
}

forward SaveAccount(playerid);
public SaveAccount(playerid)
{
	if(gPlayerLogged[playerid])
	{
		new query[1700];
		
        if(AdminDuty[playerid])
        {
			PlayerInfo[playerid][pHealth] = GetPVarFloat(playerid, "tempHealth");
			AdminDuty[playerid] = false;
			AdminDutyNickOff(playerid);
		}

		if(GetPlayerState(playerid) != PLAYER_STATE_SPECTATING && gettime() >= pAllowPosDataSyncTime[playerid])
		{
			GetPlayerPos(playerid, PlayerInfo[playerid][pX], PlayerInfo[playerid][pY], PlayerInfo[playerid][pZ]);
			GetPlayerFacingAngle(playerid, PlayerInfo[playerid][pA]);
			PlayerInfo[playerid][pInterior] = GetPlayerInterior(playerid);

			if(IsPlayerAFK(playerid)) {
				PlayerInfo[playerid][pVirtualWorld] = AFK_GetReturnWorld(playerid);
			} else {
				PlayerInfo[playerid][pVirtualWorld] = GetPlayerVirtualWorld(playerid);
			}
		}

		// TODO: fix container destroy in DestroyPlayerHands getting called before this:

		if(ItemModel_GetType(HandInfo[playerid][HAND_RIGHT][Item]) == ITEM_CONTAINER) {
			HandInfo[playerid][HAND_RIGHT][Amount] = Container_GetSQLID(HandInfo[playerid][HAND_RIGHT][Amount]);
		}
		if(ItemModel_GetType(HandInfo[playerid][HAND_LEFT][Item]) == ITEM_CONTAINER) {
			HandInfo[playerid][HAND_LEFT][Amount]  = Container_GetSQLID(HandInfo[playerid][HAND_LEFT][Amount]);
		}
		if(ItemModel_GetType(BackInfo[playerid][backItem]) == ITEM_CONTAINER) {
			BackInfo[playerid][backAmount] = Container_GetSQLID(BackInfo[playerid][backAmount]);
		}

		mysql_format(MYSQL_HANDLE, query, sizeof(query), "UPDATE `accounts` SET \
			`Ip`='%s',\
			`Name`='%s',\
			`Level`=%i,\
			`AdminLevel`=%i,\
			`Sex`=%i,\
			`Age`=%i,\
			`Exp`=%i,\
			`CashMoney`=%i,\
			`BankMoney`=%i,\
			`Skin`=%i,\
			`pHunger`=%i,\
			`Job`=%i,\
			`JobTime`=%i,\
			`pTimePlayed`=%i,\
			`PayCheck`=%i,\
			`pPayTime`=%i,\
			`Faction`=%i,\
			`Rank`=%i,\
			`HouseKey`=%i,\
			`Warnings`=%i,\
			`pMuteB`=%i,\
			`pRentCarID`=%i,\
			`pRentCarRID`=%i,\
			`pFightStyle`=%i,\
			`pRolePoints`=%i,\
			`pContainerSQLID`=%i,\
			`pBeltSQLID`=%i,\
			`CarLic`=%i,\
			`FlyLic`=%i,\
			`WepLic`=%i,\
			`PhoneNumber`=%i,\
			`Jailed`=%i,\
			`JailedTime`=%i,\
			`pThirst`=%i,\
			`pInterior`=%i,\
			`pWorld`=%i,\
			`pHospitalized`=%i,\
			`pCrack`=%i,\
			`pWantedLevel`=%i,\
			`pCantWork`=%i,\
			`pQuestion`='%e',\
			`pHaveQuestion`=%i,\
			`LastConnected`=CURRENT_TIMESTAMP,\
			`pAccusedOf`='%e',\
			`pAccusedBy`='%s',\
			`pX`=%f,\
			`pY`=%f,\
			`pZ`=%f,\
			`pA`=%f,\
			`pHealth`=%f,\
			`pWounds`='%s',\
			`r_hand_item`=%i,\
			`r_hand_param`=%i,\
			`l_hand_item`=%i,\
			`l_hand_param`=%i,\
			`back_carry`=%i,\
			`back_item`=%i,\
			`back_param`=%i WHERE `Id`=%i;",
			PlayerInfo[playerid][pIP],
			PlayerInfo[playerid][pName],
			PlayerInfo[playerid][pLevel],
			PlayerInfo[playerid][pAdmin],
			PlayerInfo[playerid][pSex],
			PlayerInfo[playerid][pAge],
			PlayerInfo[playerid][pExp],
			PlayerInfo[playerid][pCash],
			PlayerInfo[playerid][pBank],
			PlayerInfo[playerid][pSkin],
			PlayerInfo[playerid][pHunger],
			PlayerInfo[playerid][pJob],
			PlayerInfo[playerid][pJobTime],
			PlayerInfo[playerid][pTimePlayed],
			PlayerInfo[playerid][pPayCheck],
			PlayerInfo[playerid][pPayTime],
			PlayerInfo[playerid][pFaction],
			PlayerInfo[playerid][pRank],
			PlayerInfo[playerid][pHouseKey],
			PlayerInfo[playerid][pWarnings],
			PlayerInfo[playerid][pMuteB],
			PlayerInfo[playerid][pRentCarID],
			PlayerInfo[playerid][pRentCarRID],
			PlayerInfo[playerid][pFightStyle],
			PlayerInfo[playerid][pRolePoints],
			PlayerInfo[playerid][pContainerSQLID],
			PlayerInfo[playerid][pBeltSQLID],
			PlayerInfo[playerid][pCarLic],
			PlayerInfo[playerid][pFlyLic],
			PlayerInfo[playerid][pWepLic],
			PlayerInfo[playerid][pPhoneNumber],
			PlayerInfo[playerid][pJailed],
			PlayerInfo[playerid][pJailTime],
			PlayerInfo[playerid][pThirst],
			PlayerInfo[playerid][pInterior],
			PlayerInfo[playerid][pVirtualWorld],
			PlayerInfo[playerid][pHospitalized],
			PlayerInfo[playerid][pCrack],
			PlayerInfo[playerid][pWantedLevel],
			PlayerInfo[playerid][pCantWork],
			PlayerInfo[playerid][pQuestion],
			PlayerInfo[playerid][pHaveQuestion],
			PlayerInfo[playerid][pAccusedOf],
			PlayerInfo[playerid][pAccusedBy],
			PlayerInfo[playerid][pX],
			PlayerInfo[playerid][pY],
			PlayerInfo[playerid][pZ],
			PlayerInfo[playerid][pA],
			PlayerInfo[playerid][pHealth],
			PlayerInfo[playerid][pWounds],
			HandInfo[playerid][HAND_RIGHT][Item],
			HandInfo[playerid][HAND_RIGHT][Amount],
			HandInfo[playerid][HAND_LEFT][Item],
			HandInfo[playerid][HAND_LEFT][Amount],
			BackInfo[playerid][backCarryType],
			BackInfo[playerid][backItem],
			BackInfo[playerid][backAmount],
			PlayerInfo[playerid][pID]
		);

		mysql_tquery(MYSQL_HANDLE, query);

		SavePlayerJobData(playerid); // Info del job
	}
	return 1;
}

forward GlobalUpdate();
public GlobalUpdate() {
	return 1;
}

public OnPlayerInteriorChange(playerid, newinteriorid, oldinteriorid) {
	return 1;
}

public OnPlayerEnterVehicle(playerid, vehicleid, ispassenger)
{
	if(AdminDuty[playerid]) {
	    return 1;
	}

	if(VehicleInfo[vehicleid][VehLocked] == 1)
	{
	    new vehModelType = Veh_GetModelType(vehicleid);
	    
		if(vehModelType == VTYPE_BMX || vehModelType == VTYPE_BIKE || vehModelType == VTYPE_QUAD)
		    return 1;

		new Float:pos[3];
		GetPlayerPos(playerid, pos[0], pos[1], pos[2]);
		SetPlayerPos(playerid, pos[0], pos[1], pos[2]);
		GameTextForPlayer(playerid, "~w~Vehiculo cerrado", 1000, 4);
	}

	if(PlayerInfo[playerid][pCrack]) 
	{
		new Float:pos[3];
		GetPlayerPos(playerid, pos[0], pos[1], pos[2]);
		SetPlayerPos(playerid, pos[0], pos[1], pos[2]);
		Damage_ApplyCrackAnimation(playerid);
	}

	return 1;
}

public OnPlayerExitVehicle(playerid, vehicleid) {
	return 1;
}

/* NOTA MENTAL. SI SE DEVUELVE ~1 O ~0 EN CUALQUIER HOOK SEA A UN PUBLIC, O CUALQUIER VALOR EXCEPTO 'return continue(parametros originales)'
SEA UN FUNCTION, SE INTERRUMPE TODA LA CADENA QUE VENGA DESPUES, INCLUIDA TAMBIEN EL PUBLIC/FUNCTION ORIGINAL */

// TODO: cambiar checkpoints a dynamic
public OnPlayerEnterCheckpoint(playerid)
{
	DisablePlayerCheckpoint(playerid);
	OnPlayerEnterCPId(playerid, GetPlayerActiveCheckpointID(playerid));
	return 1;
}

OnPlayerEnterCPId(playerid, checkpointid)
{
	#pragma unused playerid
	#pragma unused checkpointid

	return 1;
}

public OnPlayerLeaveCheckpoint(playerid) {
	return 1;
}

public OnPlayerEnterRaceCheckpoint(playerid) {
	return 1;
}

public OnPlayerLeaveRaceCheckpoint(playerid) {
	return 1;
}

public OnObjectMoved(objectid) {
	return 1;
}

public OnPlayerObjectMoved(playerid, objectid) {
	return 1;
}

public OnPlayerClickTextDraw(playerid, Text:clickedid) {
	return 0;
}

public OnPlayerClickPlayerTextDraw(playerid, PlayerText:playertextid) {
	return 0;
}

public OnPlayerClickPlayer(playerid, clickedplayerid, source) {
	return 1;
}

LoadPickups() {

	/* C�maras de Seguridad PMA */
	P_POLICE_CAMERAS = CreateDynamicPickup(1239, 1, -2811.67, 3211.25, 2412.73, -1);
	CreateDynamic3DTextLabel("C�maras de Seguridad de la Ciudad", COLOR_WHITE, 219.36, 188.31, 1003.75, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, 16002, 3, -1, 100.0);

	/* Gimnasio */
	P_FIGHT_STYLE = CreateDynamicPickup(1239, 1, 766.3723, 13.8237, 1000.7015, -1);

	// Curarse en hospital
	P_HOSP_HEAL = CreateDynamicPickup(1240, 1, POS_HOSP_HEAL_X_1, POS_HOSP_HEAL_Y_1, POS_HOSP_HEAL_Z_1, -1);
	P_HOSP_HEAL_2 = CreateDynamicPickup(1240, 1, POS_HOSP_HEAL_X_2, POS_HOSP_HEAL_Y_2, POS_HOSP_HEAL_Z_2, -1);

	// Robo de autos
	P_CAR_DEMOLITION = CreateDynamicPickup(1239, 1, POS_CAR_DEMOLITION_X, POS_CAR_DEMOLITION_Y, POS_CAR_DEMOLITION_Z, -1);
	
	/* Banco de Malos Aires */
	P_BANK = CreateDynamicPickup(1239, 1, POS_BANK_X, POS_BANK_Y, POS_BANK_Z, -1);
	
	/* C�rcel de la Polic�a Metropolitana */
	P_POLICE_ARREST = CreateDynamicPickup(1239, 1, POS_POLICE_ARREST_X, POS_POLICE_ARREST_Y, POS_POLICE_ARREST_Z, -1);
	P_POLICE_ARREST2 = CreateDynamicPickup(1239, 1, POS_POLICE_ARREST2_X, POS_POLICE_ARREST2_Y, POS_POLICE_ARREST2_Z, -1);
	P_JAIL_EAT = CreateDynamicPickup(1239, 1, 1202.45, 3166.91, 2416.58, -1);
	
	/* Gendarmer�a */
	P_GEN_EAT = CreateDynamicPickup(1239, 1, -492.45, -515.54, 4217.67, -1);

	/* Centro de Licencias de Malos Aires */
	P_LICENSE_CENTER = CreateDynamicPickup(1239, 1, -2033.2118, -117.4678, 1035.1719, -1);

    /* Cosechador de materia prima */
	P_DRUGFARM_MATS = CreateDynamicPickup(1239, 1, -1060.9709, -1195.5382, 129.6939);

	// Renta de autos
	P_CAR_RENT1 = CreateDynamicPickup(1239, 1, 1569.8145, -2243.8796, 13.5184, -1);
	P_CAR_RENT2	= CreateDynamicPickup(1239, 1, 1276.8502, -1309.8553, 13.3107, -1);
	P_CAR_RENT3 = CreateDynamicPickup(1239, 1, 611.9272, -1294.7240, 15.2081, -1);
	
	// Jobs
	Jobs_LoadPickups();
	return 1;
}

public OnPlayerPickUpDynamicPickup(playerid, pickupid)
{
	if(pickupid == P_BANK) {
		GameTextForPlayer(playerid, "~w~/ayudabanco", 2000, 4);
		return 1;

	} else if(pickupid == P_FIGHT_STYLE) {
		GameTextForPlayer(playerid, "~w~Escribe /aprender para adquirir nuevos conocimientos de pelea.", 2000, 4);
		return 1;

	} else if(pickupid == P_POLICE_ARREST && PlayerInfo[playerid][pFaction] == FAC_PMA) {
		GameTextForPlayer(playerid, "~w~/arrestar aqui para arrestar.", 2000, 4);
		return 1;

	} else if(pickupid == P_POLICE_ARREST2 && PlayerInfo[playerid][pFaction] == FAC_PMA) {
		GameTextForPlayer(playerid, "~w~/arrestar aqui para arrestar.", 2000, 4);
		return 1;
		
	} else if(pickupid == P_JAIL_EAT) {
		GameTextForPlayer(playerid, "~w~Usa /carcelcomer para recibir tu bandeja con alimentos.", 2000, 4);
		return 1;

	} else if(pickupid == P_GEN_EAT) {
		GameTextForPlayer(playerid, "~w~Usa /gcomer para recibir tu bandeja con alimentos.", 2000, 4);
		return 1;

	} else if(pickupid == P_LICENSE_CENTER) {
		GameTextForPlayer(playerid, "~w~/licencias para ver las licencias disponibles. ~n~/manuales para ver los manuales.", 2000, 4);
		return 1;

	} else if(pickupid == P_POLICE_CAMERAS) {
		GameTextForPlayer(playerid, "~w~/camaras para seleccionar una camara de la ciudad.", 2000, 4);
		return 1;

	} else if(pickupid == P_HOSP_HEAL || pickupid == P_HOSP_HEAL_2) {
		new string[128];
		format(string, sizeof(string), "~w~/curarse para solicitar un medico que atienda tus heridas ($%d)", PRICE_HOSP_HEAL);
		GameTextForPlayer(playerid, string, 2000, 4);
		return 1;

	} else if(pickupid == P_CAR_DEMOLITION) {
		if(PlayerInfo[playerid][pJob] == JOB_FELON && ThiefJobInfo[playerid][pFelonLevel] >= 5)
			GameTextForPlayer(playerid, "~w~Utiliza /desarmar para desarmar el vehiculo robado.", 2000, 4);
		return 1;

	} else if(pickupid == P_CARPART_SHOP) {
		if(PlayerInfo[playerid][pFaction] == FAC_MECH)
			GameTextForPlayer(playerid, "~w~Utiliza /meccomprar para comprar repuestos de auto.", 2000, 4);
		return 1;

	} else if(pickupid == P_CAR_RENT1 || pickupid == P_CAR_RENT2 || pickupid == P_CAR_RENT3) {
		GameTextForPlayer(playerid, "~w~Alquiler de vehiculos", 2000, 4);
		return 1;

	} else if(pickupid == P_DRUGFARM_MATS) {
	    if(PlayerInfo[playerid][pJob] == JOB_DRUGF) {
	    	new string[128];
			format(string, sizeof(string), "~w~bolsas de materia prima: %d", ServerInfo[sDrugRawMats]);
		    GameTextForPlayer(playerid, string, 2000, 4);
 	    }

	} else if(pickupid == Depo_PickUp) {
        GameTextForPlayer(playerid, "~w~/sacarvehiculo aqui para retirar tu vehiculo del deposito.", 2000, 4);
		return 1;
	} else {
		if(IsBlackMarketPickup(pickupid)) {
			GameTextForPlayer(playerid, "~w~Mercado negro - Utiliza /comprar o /vender.", 2000, 4);
			return 1;
		}
	}
	return 1;
}

SetNormalPlayerGunSkills(playerid)
{
    SetPlayerSkillLevel(playerid, WEAPONSKILL_PISTOL, 998);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_PISTOL_SILENCED, 999);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_DESERT_EAGLE, 999);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_SHOTGUN, 999);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_SAWNOFF_SHOTGUN, 998);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_SPAS12_SHOTGUN, 999);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_MP5, 999);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_AK47, 999);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_M4, 999);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_MICRO_UZI, 998);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_SNIPERRIFLE, 999);
}

InitiateHospital(playerid)
{
	PlayerInfo[playerid][pHospitalized] = 2;
	SendClientMessage(playerid, COLOR_YELLOW2, "Debes reposar un tiempo en el hospital hasta recuperarte.");
	SendClientMessage(playerid, COLOR_YELLOW2, "Antes de ser dado de alta el personal del hospital te quitar� las armas y te cobrar� una suma por el tratamiento recibido.");
	SetPlayerHealthEx(playerid, 10.0);
	TogglePlayerControllable(playerid, false);

	if(random(2))
	{
		TeleportPlayerTo(playerid, 1188.4574, -1309.2242, 10.5625, 0.0, 0, 0);
		SetPlayerCameraPos(playerid, 1188.4574, -1309.2242, 13.5625 + 6.0);
		SetPlayerCameraLookAt(playerid, 1175.5581, -1324.7922, 18.1610);
		SetPVarInt(playerid, "hosp", 1);
	}
	else
	{
		TeleportPlayerTo(playerid, 1999.5308, -1449.3281, 10.5594, 0.0, 0, 0);
		SetPlayerCameraPos(playerid, 1999.5308, -1449.3281, 13.5594 + 6.0);
		SetPlayerCameraLookAt(playerid, 2036.2179, -1410.3223, 17.1641);
	    SetPVarInt(playerid, "hosp", 2);
	}
	return 1;
}

LoadServerInfo()
{
	new query[64];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), "SELECT * FROM `server` WHERE `ID`=1;");
	mysql_tquery(MYSQL_HANDLE, query, "OnServerDataLoad");
	print("[INFO] Cargando datos del servidor...");
	return 1;
}

forward OnServerDataLoad();
public OnServerDataLoad()
{
	if(cache_num_rows())
	{
		cache_get_value_name_int(0, "sVehiclePricePercent", ServerInfo[sVehiclePricePercent]);
		cache_get_value_name_int(0, "sPlayersRecord", ServerInfo[sPlayersRecord]);
		cache_get_value_name_int(0, "svLevelExp", ServerInfo[svLevelExp]);
		cache_get_value_name_int(0, "sDrugRawMats", ServerInfo[sDrugRawMats]);

		print("[INFO] Carga de datos del servidor finalizada.");
	}
	return 1;
}

SaveServerInfo()
{	
	new query[256];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), "UPDATE `server` SET `sVehiclePricePercent`=%i,`sPlayersRecord`=%i,`svLevelExp`=%i,`sDrugRawMats`=%i WHERE `ID`=1;",
		ServerInfo[sVehiclePricePercent],
		ServerInfo[sPlayersRecord],
		ServerInfo[svLevelExp],
		ServerInfo[sDrugRawMats]
	);
	mysql_tquery(MYSQL_HANDLE, query);

	print("[INFO] Datos del servidor guardados.");
	return 1;
}

ShowStats(playerid, targetid, bool:adminInfo)
{
	if(!IsPlayerLogged(playerid) || !IsPlayerLogged(targetid))
		return 0;

	new location[MAX_ZONE_NAME], factionText[64], jobText[32];

	GetPlayer2DZone(targetid, location, MAX_ZONE_NAME);

	if(PlayerInfo[targetid][pFaction]) {
		format(factionText, sizeof(factionText), "Facci�n: %s | Rango: %s", FactionInfo[PlayerInfo[targetid][pFaction]][fName], Faction_GetRankName(PlayerInfo[targetid][pFaction], PlayerInfo[targetid][pRank]));
	} else {
		strcat(factionText, "Facci�n: Ninguna | Rango: -", sizeof(factionText));
	}

	if(PlayerInfo[targetid][pJob]) {
		strcat(jobText, JobInfo[PlayerInfo[targetid][pJob]][jName], sizeof(jobText));
	} else {
		strcat(jobText, "No", sizeof(jobText));
	}

	SendClientMessage(playerid, COLOR_LIGHTYELLOW, "============================[General IC]=============================");
	SendFMessage(playerid, COLOR_WHITE, "Nombre: %s | Zona: %s | Dinero: $%d | Banco: $%d | Edad: %d | Sexo: %s", GetPlayerCleanName(targetid), location, GetPlayerCash(targetid), PlayerInfo[targetid][pBank], PlayerInfo[targetid][pAge], (PlayerInfo[targetid][pSex]) ? ("Masculino") : ("Femenino"));
	SendFMessage(playerid, COLOR_WHITE, "Tel�fono: %i | Empleo: %s | %s", PlayerInfo[targetid][pPhoneNumber], jobText, factionText);
	SendFMessage(playerid, COLOR_WHITE,	"[Licencias] Conducci�n: %s | Vuelo: %s | Portaci�n de armas: %s", (PlayerInfo[targetid][pCarLic]) ? ("Si") : ("No"), (PlayerInfo[targetid][pFlyLic]) ? ("Si") : ("No"), (PlayerInfo[targetid][pWepLic]) ? ("Si") : ("No"));
	SendClientMessage(playerid, COLOR_LIGHTYELLOW, "============================[General OOC]===========================");
	SendFMessage(playerid, COLOR_WHITE, "Salud: %.1f | Chaleco: %.1f | Nivel: %d | Advertencias: %d | Puntos de Rol: %d | Experiencia: %d/%d | Tiempo de juego: %d horas", GetPlayerHealthEx(targetid), PlayerInfo[targetid][pArmour], PlayerInfo[targetid][pLevel], PlayerInfo[targetid][pWarnings], PlayerInfo[targetid][pRolePoints], PlayerInfo[targetid][pExp], (PlayerInfo[targetid][pLevel] + 1) * ServerInfo[svLevelExp], PlayerInfo[targetid][pTimePlayed] / 3600);

	if(adminInfo)
	{
		SendClientMessage(playerid, COLOR_LIGHTYELLOW, "==============================[DEBUG]==============================");
		SendFMessage(playerid, COLOR_WHITE,	"Negocio actual: %d | Skin: %d | Mundo: %d | Interior: %d | Ultveh: %d | pCantWork: %d | pID %d", Biz_IsPlayerInsideAny(targetid), PlayerInfo[targetid][pSkin], GetPlayerVirtualWorld(targetid), GetPlayerInterior(targetid), LastVeh[targetid], PlayerInfo[targetid][pCantWork], PlayerInfo[targetid][pID]);
		SendFMessage(playerid, COLOR_WHITE, "Hambre: %d | Sed: %d | Crack: %d", PlayerInfo[targetid][pHunger], PlayerInfo[targetid][pThirst], PlayerInfo[playerid][pCrack]);

		KeyChain_Show(targetid, playerid);
	}

	SendClientMessage(playerid, COLOR_LIGHTYELLOW, "===================================================================");
	return 1;
}

forward kickTimer(playerid);
public kickTimer(playerid) {
	return Kick(playerid);
}

forward banTimer(playerid);
public banTimer(playerid) {
	return Ban(playerid);
}

forward KickPlayer(playerid, const kickedby[], const reason[]);
public KickPlayer(playerid, const kickedby[], const reason[])
{
	foreach(new i : Player)
	{
	    if(i == playerid) {
	        SendFMessage(i, COLOR_ADMINCMD, "Has sido expulsado/a por %s, raz�n: %s", kickedby, reason);
	    } else if(PlayerInfo[i][pAdmin] > 1) {
	        SendFMessage(i, COLOR_ADMINCMD, "%s ha sido expulsado/a por %s, raz�n: %s.", GetPlayerNameEx(playerid), kickedby, reason);
	    }
	}
	SetTimerEx("kickTimer", 1000, false, "d", playerid);
	return 1;
}

forward BanPlayer(playerid, issuerid, const reason[], days);
public BanPlayer(playerid, issuerid, const reason[], days)
{
	new	issuerSQLID,
		issuerName[MAX_PLAYER_NAME],
		playerName[MAX_PLAYER_NAME],
		playerIP[16],
		str[128];
	
	if(issuerid == INVALID_PLAYER_ID)
	{
		issuerName = "el servidor";
		issuerSQLID = -1;
	}
	else
	{
        GetPlayerName(issuerid, issuerName, sizeof(issuerName));
	    issuerSQLID = PlayerInfo[issuerid][pID];
	}
		
	GetPlayerName(playerid, playerName, sizeof(playerName));
	
	mysql_escape_string(issuerName, issuerName, sizeof(issuerName), MYSQL_HANDLE);
	mysql_escape_string(playerName, playerName, sizeof(playerName), MYSQL_HANDLE);
	
	GetPlayerIp(playerid, playerIP, sizeof(playerIP));

	if(days == 0) // Perma ban
	{
	    days = 2000; // Una fecha lejana
		format(str, sizeof(str), "%s ha sido baneado/a permanentemente por %s, raz�n: %s.", playerName, issuerName, reason);
	} else {
        format(str, sizeof(str), "%s ha sido baneado/a %d d�as por %s, raz�n: %s.", playerName, days, issuerName, reason);
	}

	new query[512];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), \
		"INSERT INTO `bans` \
			(`pID`,\
			`pName`,\
			`pIP`,\
			`banDate`,\
			`banEnd`,\
			`banEndUnix`,\
			`banReason`,\
			`banIssuerID`,\
			`banIssuerName`,\
			`banActive`) \
		VALUES \
			(%i,'%s','%s',CURRENT_TIMESTAMP,TIMESTAMPADD(DAY,%i,CURRENT_TIMESTAMP),%i,'%e',%i,'%s',1);",
		PlayerInfo[playerid][pID],
		playerName,
		playerIP,
		days,
		gettime() + 86400 * days,
		reason,
		issuerSQLID,
		issuerName
	);
	mysql_tquery(MYSQL_HANDLE, query);

	SendClientMessageToAll(COLOR_ADMINCMD, str);
	TogglePlayerControllable(playerid, false);
	SendClientMessage(playerid, COLOR_WHITE, "En el caso de ser un baneo temporal, ser�s desbaneado automaticamente por el servidor en la fecha l�mite.");
	SendClientMessage(playerid, COLOR_WHITE, "Para m�s informaci�n o para realizar un reclamo/descargo, dir�gete a nuestro canal de Discord o al foro en www.pheek.net");
	SetTimerEx("kickTimer", 1000, false, "d", playerid);
	return 1;
}

public healTimer(playerid) {
    if(GetPVarInt(playerid, "isHealing") != 0)
	{
		SendClientMessage(playerid, COLOR_WHITE, "Tu oferta se ha cancelado, el herido no la ha aceptado.");
		SendClientMessage(GetPVarInt(playerid, "healTarget"), COLOR_WHITE, "Ha pasado demasiado tiempo y has rechazado la oferta del m�dico.");
	}
	DeletePVar(DeletePVar(playerid, "healTarget"), "healIssuer");
	DeletePVar(DeletePVar(playerid, "healTarget"), "healCost");
	DeletePVar(playerid, "isHealing");
	DeletePVar(playerid, "healTarget");
	return 1;
}

ResetPlayerWantedLevelEx(playerid)
{
	PlayerInfo[playerid][pAccusedOf][0] = EOS;
	strcat(PlayerInfo[playerid][pAccusedOf], "Sin cargos", 64);
	PlayerInfo[playerid][pAccusedBy][0] = EOS;
	strcat(PlayerInfo[playerid][pAccusedBy], "Nadie", 24);
	PlayerInfo[playerid][pWantedLevel] = 0;
	return 1;
}

SetPlayerWantedLevelEx(playerid, level) {
	PlayerInfo[playerid][pWantedLevel] = level;
}

GetPlayerWantedLevelEx(playerid) {
	return PlayerInfo[playerid][pWantedLevel];
}

public OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if(newkeys & KEY_JUMP && !(oldkeys & KEY_JUMP) && GetPlayerSpecialAction(playerid) == SPECIAL_ACTION_CUFFED) {
		ApplyAnimationEx(playerid, "GYMNASIUM", "gym_jog_falloff", 4.1, 0, 1, 1, 0, 0, .forcesync = 1, .autofinish = true, .finishAnimId = 0);
	}

	if(KEY_PRESSED_SINGLE(KEY_WALK))
	{
		if(J_Garb_OnPlayerPressKeyWalk(playerid)) {
		    return 1;
		}
    }

	if(KEY_PRESSED_SINGLE(KEY_ACTION))
	{
		if(IsPlayerInAnyVehicle(playerid))
		{
			new vehicleid = GetPlayerVehicleID(playerid);

			if(GetVehicleModel(vehicleid) == 481 || GetVehicleModel(vehicleid) == 509 || GetVehicleModel(vehicleid) == 510)
			{
				RemovePlayerFromVehicle(playerid);
				SetVehicleVelocity(vehicleid, 0.0, 0.0, 0.0);
			}
		}
	}
	return 1;
}

forward Unfreeze(playerid);
public Unfreeze(playerid)
{
	TogglePlayerControllable(playerid, true);
    return 1;
}

public OnPlayerUpdate(playerid) {
	return 1;
}

public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) { // Dont handle dialogs here, use easyDialogs
    return 0;
}

CMD:pos(playerid, params[])
{
	new Float:x, Float:y, Float:z, Float:a, vehicleid;

	GetPlayerPos(playerid, x, y, z);
	GetPlayerFacingAngle(playerid, a);
	SendFMessage(playerid, COLOR_WHITE, "Tu posici�n es [X: %.2f - Y: %.2f - Z: %.2f - Angle: %.2f - Int: %i - VWorld: %i]", x, y, z, a, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));

	if((vehicleid = GetPlayerVehicleID(playerid)))
	{
		GetVehiclePos(vehicleid, x, y, z);
		GetVehicleZAngle(vehicleid, a);
		SendFMessage(playerid, COLOR_WHITE, "La posici�n de tu veh�culo es [X: %.2f - Y: %.2f - Z: %.2f - Angle: %.2f - Int: %i - VWorld: %i]", x, y, z, a, GetPlayerInterior(playerid), GetVehicleVirtualWorld(vehicleid));
	}
	return 1;
}

CMD:stats(playerid, params[]) {
	return ShowStats(playerid, playerid, .adminInfo = false);
}

CMD:duda(playerid,params[])
{
	new string[144], string2[200];
	
	if(sscanf(params, "s[144]", string))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO]" COLOR_EMB_GREY" /duda [texto]");

	SendClientMessage(playerid, COLOR_INFO, "[INFO]" COLOR_EMB_GREY" La duda ha sido enviada. Recuerda que si ten�as otra duda antes, ser� reemplazada por la �ltima.");
	PlayerInfo[playerid][pQuestion][0] = EOS;
	strcat(PlayerInfo[playerid][pQuestion], string, 144);
	PlayerInfo[playerid][pHaveQuestion] = 1;
	format(string2, sizeof(string2), "[DUDA] %s (ID: %i): "COLOR_EMB_GREY" %s", GetPlayerCleanName(playerid), playerid, string);
	AdministratorMessage(COLOR_DOUBT, string2, 1);
	return 1;
}

CMD:marp(playerid, params[]){
	return marp_show(playerid);
}

marp_show(playerid)
{
	new str[514+1];
	format(str, sizeof(str), "%sNuestro humilde agradecimiento a todos los desarrolladores que usaron parte de su tiempo en aportar su granito de arena para el servidor que somos hoy por ", str);
	format(str, sizeof(str), "%shoy,\npasando desde ImperiumGames, Pheek, hasta nuestro presente siendo una comunidad independiente.\n\n{C21120}2010 - 2016:{FFFFFF} Nevermore, Krizzen, Forkus, Froda, WarHeith, Jukiro, Mushe, Chelusa", str);
	format(str, sizeof(str), "%s, JBracone, APalopoli, House, Huevazo, Sopa\n\n{C21120}2020 - 2021:{FFFFFF} Huevazo, Sopa, JBracone  \n\n{C21120}2021 - Actualidad:{FFFFFF} Russo, Rawnker", str);
	ShowPlayerDialog(playerid, 2101, DIALOG_STYLE_MSGBOX, "Desarrolladores de Malos Aires", str, "Cerrar", "");
	return 1;
}

CMD:reportar(playerid,params[])
{
	new id,
		string[256],
		reason[256];
		
	if(sscanf(params, "us[256]", id, reason))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /reportar [ID/ParteDelNombre] [raz�n]");
	if(!IsPlayerConnected(id))
	    return SendClientMessage(playerid, COLOR_ERROR,"[ERROR] "COLOR_EMB_GREY" nombre incorrecto o el jugador no se encuentra conectado.");

	PlayerInfo[playerid][pReport]=1;
	format(string, sizeof(string), "[Reporte] %s (%d) ha reportado a %s (%d), raz�n: %s", GetPlayerCleanName(playerid), playerid, GetPlayerCleanName(id), id, reason);
	AdministratorMessage(COLOR_ADMINCMD, string, 2);
	
	format(string, sizeof(string), "Has reportado a %s (ID:%d), raz�n: %s", GetPlayerCleanName(id), id, reason);
	SendClientMessage(playerid, COLOR_WHITE, string);
	return 1;
}


IsPlayerLogged(playerid) {
	return (IsPlayerConnected(playerid) && gPlayerLogged[playerid]);
}

CMD:hora(playerid, params[])
{
	PlayerActionMessage(playerid, 15.0, "toma su reloj y se fija la hora.");
	SendFMessage(playerid, COLOR_WHITE, "La hora actual es %s. {5CACC8}Pr�ximo d�a de Pago en %d minutos.", GetHourString(), 60 - (PlayerInfo[playerid][pPayTime] / 60));
	return 1;
}

CMD:servicios(playerid, params[]) {
	SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "Emergencias: 911 | Taller mec�nico: sms al 555 | Taxi: sms al 444 | Estaci�n de radiodifusi�n: sms al 3900");
}

OnPlayerCmdComprar(playerid, const params[])
{
	#pragma unused playerid
	#pragma unused params

	return 1;
}

CMD:comprar(playerid, params[])
{
	if(PlayerInfo[playerid][pDisabled] != DISABLE_NONE)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "No puedes hacerlo en este momento.");

	OnPlayerCmdComprar(playerid, params);
	return 1;
}

//=============================RENTA DE VEHICULOS===============================

CMD:rentar(playerid, params[])
{
	new vehicleid, rentcarid, price, time;

	if(sscanf(params, "i", time))
	    return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /rentar [tiempo] (en horas)");
	if(PlayerInfo[playerid][pRentCarID] != 0)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "�Ya has rentado un veh�culo!");
	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar subido a un veh�culo de renta disponible para alquilar.");
 	vehicleid = GetPlayerVehicleID(playerid);
	if(VehicleInfo[vehicleid][VehType] != VEH_RENT)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar subido a un veh�culo de renta disponible para alquilar.");
	for(new i = 1; i < MAX_RENTCAR; i++)
	{
	    if(RentCarInfo[i][rVehicleID] == vehicleid)
		{
	        if(RentCarInfo[i][rRented] == 1)
	            return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar subido a un veh�culo de renta disponible para alquiler.");
			else
		    {
		        rentcarid = i;
		    	break;
			}
		}
	}
	if(time < 1 || time > 3)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "Solo puedes alquilar por un m�nimo de una hora, o un m�ximo de tres.");
	price = Veh_GetPrice(vehicleid) / 200;
	if(price < 100)
		price = 100; // Seteamos un m�nimo de precio
	if(GetPlayerCash(playerid) < price * time)
    	return SendClientMessage(playerid, COLOR_YELLOW2, "No tienes el dinero necesario.");

	GivePlayerCash(playerid, -(price * time));
	SendClientMessage(playerid, COLOR_WHITE, "�Rentaste el veh�culo! Usa '/motor' o presiona (~k~~TOGGLE_SUBMISSIONS~) para encenderlo. Ser� devuelto al acabarse el tiempo.");
    SendClientMessage(playerid, COLOR_WHITE, "(( Si el veh�culo respawnea, lo encontrar�s en la agencia donde lo rentaste en primer lugar. ))");
	RentCarInfo[rentcarid][rRented] = 1;
	RentCarInfo[rentcarid][rOwnerSQLID] = PlayerInfo[playerid][pID];
	RentCarInfo[rentcarid][rTime] = time * 60; // Guardamos el tiempo en minutos
	PlayerInfo[playerid][pRentCarID] = vehicleid;
	PlayerInfo[playerid][pRentCarRID] = rentcarid;
	return 1;
}

TIMER:rentRespawn()
{
	new ownerid;
	for(new i = 1; i < MAX_RENTCAR; i++)
	{
		if(RentCarInfo[i][rRented] == 1)
		{
 		    RentCarInfo[i][rTime] -= 20;
            if(RentCarInfo[i][rTime] < 30)
            {
                ownerid = -1; // Por default el -1 que significa no est� conectado
	           	foreach(new playerid : Player)
			    {
			        if(PlayerInfo[playerid][pID] == RentCarInfo[i][rOwnerSQLID])
			        {
			            ownerid = playerid;
			            break;
					}
				}
				if(RentCarInfo[i][rTime] > 0 && ownerid != 1)
 					SendFMessage(ownerid, COLOR_WHITE, "A tu veh�culo de renta le quedan %d minutos de alquiler. Al finalizar ser� devuelto a la agencia.", RentCarInfo[i][rTime]);
				if(RentCarInfo[i][rTime] <= 0)
				{
				    RentCarInfo[i][rRented] = 0;
				    RentCarInfo[i][rTime] = 0;
				    RentCarInfo[i][rOwnerSQLID] = 0;
				    if(ownerid != -1)
				    {
       					PlayerInfo[ownerid][pRentCarID] = 0;
						PlayerInfo[ownerid][pRentCarRID] = 0;
						SendClientMessage(ownerid, COLOR_WHITE, "Se ha acabado el tiempo de alquiler del veh�culo de renta.");
				    }
				}
			}
		}
    	if(RentCarInfo[i][rRented] == 0)
    	{
			VehicleInfo[RentCarInfo[i][rVehicleID]][VehLocked] = 0;
			SetVehicleToRespawn(RentCarInfo[i][rVehicleID]);
			VehicleInfo[RentCarInfo[i][rVehicleID]][VehFuel] = 100;
		}
	}
	return 1;
}

CMD:aceptar(playerid, params[])
{
	new subcmd[32];

	if(sscanf(params, "s[32]", subcmd))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /aceptar [comando]");

	OnPlayerCmdAccept(playerid, subcmd);
	return 1;
}

OnPlayerCmdAccept(playerid, const subcmd[])
{
	if(isnull(subcmd)) {
		SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /aceptar [comando]");
	}
	return 1;
}

CMD:cancelar(playerid, params[])
{
	new subcmd[32];

	if(sscanf(params, "s[32]", subcmd))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /cancelar [comando]");

	OnPlayerCmdCancel(playerid, subcmd);
	return 1;
}

OnPlayerCmdCancel(playerid, const subcmd[])
{
	if(isnull(subcmd)) {
		SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /cancelar [comando]");
	}
	return 1;
}

public OnPlayerStreamIn(playerid, forplayerid) {
	return 1;
}

GetPlayerCount() {
	return Iter_Count(Player);
}