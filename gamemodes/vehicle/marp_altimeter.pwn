#if defined _marp_altimeter_included
	#endinput
#endif
#define _marp_altimeter_included

#include <YSI_Coding\y_hooks>


static Altimeter_timer[MAX_PLAYERS];

static PlayerText:plane_altimeter_box[MAX_PLAYERS];
static PlayerText:plane_altimeter_altitude[MAX_PLAYERS];
static PlayerText:plane_altimeter_usebox[MAX_PLAYERS];
static PlayerText:plane_altimeter_value[MAX_PLAYERS];
static PlayerText:plane_altimeter_feet[MAX_PLAYERS];

forward Altimeter_Update(playerid);
public Altimeter_Update(playerid)
{
	static vehicleid;
	static string[32];
	new
	Float:ppX,
	Float:ppY,
	Float:ppZ;	//Para sacar la altura del avi�n usamos el eje Z.

	if(!BitFlag_Get(p_toggle[playerid], FLAG_TOGGLE_HUD))
		return 0;

	if(!(vehicleid = GetPlayerVehicleID(playerid)))
	{
		if(Altimeter_timer[playerid])
		{
			HidePlayerAltimeter(playerid);
			KillTimer(Altimeter_timer[playerid]);
			Altimeter_timer[playerid] = 0;
		}
		return 0;
	}
	GetVehiclePos(vehicleid, ppX, ppY, ppZ);
	format(string, sizeof(string), "%.1f", ppZ);
	PlayerTextDrawSetString(playerid, plane_altimeter_value[playerid], string);
	return 1;
}

HidePlayerAltimeter(playerid)
{
	PlayerTextDrawHide(playerid, plane_altimeter_box[playerid]);
	PlayerTextDrawHide(playerid, plane_altimeter_altitude[playerid]);
	PlayerTextDrawHide(playerid, plane_altimeter_usebox[playerid]);
	PlayerTextDrawHide(playerid, plane_altimeter_value[playerid]);
	PlayerTextDrawHide(playerid, plane_altimeter_feet[playerid]);

}

ShowPlayerAltimeter(playerid)
{
	if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER && BitFlag_Get(p_toggle[playerid], FLAG_TOGGLE_HUD))
	{
		Altimeter_Update(playerid);
		PlayerTextDrawShow(playerid, plane_altimeter_box[playerid]);
		PlayerTextDrawShow(playerid, plane_altimeter_altitude[playerid]);
		PlayerTextDrawShow(playerid, plane_altimeter_usebox[playerid]);
		PlayerTextDrawShow(playerid, plane_altimeter_value[playerid]);
		PlayerTextDrawShow(playerid, plane_altimeter_feet[playerid]);
	}
}

hook OnPlayerStateChange(playerid, newstate, oldstate)
{
	if(oldstate == PLAYER_STATE_DRIVER)
	{
		if(Altimeter_timer[playerid])
		{
			HidePlayerAltimeter(playerid);
			KillTimer(Altimeter_timer[playerid]);
			Altimeter_timer[playerid] = 0;
		}
	}
	else if(newstate == PLAYER_STATE_DRIVER)
	{
		if(Veh_GetModelType(GetPlayerVehicleID(playerid)) == VTYPE_PLANE || Veh_GetModelType(GetPlayerVehicleID(playerid)) == VTYPE_HELI)
		{
			if(Altimeter_timer[playerid]) {
				KillTimer(Altimeter_timer[playerid]);
			}

			ShowPlayerAltimeter(playerid);
			Altimeter_timer[playerid] = SetTimerEx("Altimeter_Update", 800, true, "i", playerid);
		}
	}
	return 1;
}

hook OnPlayerDisconnect(playerid, reason)
{
	if(Altimeter_timer[playerid])
	{
		KillTimer(Altimeter_timer[playerid]);
		Altimeter_timer[playerid] = 0;
	}
	return 1;
}

hook OnPlayerSpawn(playerid)
{
	if(Altimeter_timer[playerid])
	{
		HidePlayerAltimeter(playerid);
		KillTimer(Altimeter_timer[playerid]);
		Altimeter_timer[playerid] = 0;
	}
	return 1;
}

hook OnPlayerConnectDelayed(playerid)
{
	plane_altimeter_box[playerid] = CreatePlayerTextDraw(playerid, 693.000000, 419.000000, "Preview_Model");
	PlayerTextDrawFont(playerid, plane_altimeter_box[playerid], 5);
	PlayerTextDrawLetterSize(playerid, plane_altimeter_box[playerid], 0.600000, 2.000000);
	PlayerTextDrawTextSize(playerid, plane_altimeter_box[playerid], -257.500000, -131.500000);
	PlayerTextDrawSetOutline(playerid, plane_altimeter_box[playerid], 0);
	PlayerTextDrawSetShadow(playerid, plane_altimeter_box[playerid], 0);
	PlayerTextDrawAlignment(playerid, plane_altimeter_box[playerid], 1);
	PlayerTextDrawColor(playerid, plane_altimeter_box[playerid], 50);
	PlayerTextDrawBackgroundColor(playerid, plane_altimeter_box[playerid], 0);
	PlayerTextDrawBoxColor(playerid, plane_altimeter_box[playerid], 0);
	PlayerTextDrawUseBox(playerid, plane_altimeter_box[playerid], 0);
	PlayerTextDrawSetProportional(playerid, plane_altimeter_box[playerid], 1);
	PlayerTextDrawSetSelectable(playerid, plane_altimeter_box[playerid], 0);
	PlayerTextDrawSetPreviewModel(playerid, plane_altimeter_box[playerid], 1625);
	PlayerTextDrawSetPreviewRot(playerid, plane_altimeter_box[playerid], 0.000000, 90.000000, 0.000000, 2.000000);
	PlayerTextDrawSetPreviewVehCol(playerid, plane_altimeter_box[playerid], 1, 1);

	plane_altimeter_altitude[playerid] = CreatePlayerTextDraw(playerid, 546.000000, 356.000000, "altura");
	PlayerTextDrawFont(playerid, plane_altimeter_altitude[playerid], 2);
	PlayerTextDrawLetterSize(playerid, plane_altimeter_altitude[playerid], 0.183332, 1.250000);
	PlayerTextDrawTextSize(playerid, plane_altimeter_altitude[playerid], 400.000000, 17.000000);
	PlayerTextDrawSetOutline(playerid, plane_altimeter_altitude[playerid], 0);
	PlayerTextDrawSetShadow(playerid, plane_altimeter_altitude[playerid], 0);
	PlayerTextDrawAlignment(playerid, plane_altimeter_altitude[playerid], 1);
	PlayerTextDrawColor(playerid, plane_altimeter_altitude[playerid], -1);
	PlayerTextDrawBackgroundColor(playerid, plane_altimeter_altitude[playerid], 255);
	PlayerTextDrawBoxColor(playerid, plane_altimeter_altitude[playerid], 50);
	PlayerTextDrawUseBox(playerid, plane_altimeter_altitude[playerid], 0);
	PlayerTextDrawSetProportional(playerid, plane_altimeter_altitude[playerid], 1);
	PlayerTextDrawSetSelectable(playerid, plane_altimeter_altitude[playerid], 0);

	plane_altimeter_usebox[playerid] = CreatePlayerTextDraw(playerid, 536.000000, 358.000000, "HUD:radar_airyard");
	PlayerTextDrawFont(playerid, plane_altimeter_usebox[playerid], 4);
	PlayerTextDrawLetterSize(playerid, plane_altimeter_usebox[playerid], 0.600000, 2.000000);
	PlayerTextDrawTextSize(playerid, plane_altimeter_usebox[playerid], 8.500000, 8.500000);
	PlayerTextDrawSetOutline(playerid, plane_altimeter_usebox[playerid], 1);
	PlayerTextDrawSetShadow(playerid, plane_altimeter_usebox[playerid], 0);
	PlayerTextDrawAlignment(playerid, plane_altimeter_usebox[playerid], 1);
	PlayerTextDrawColor(playerid, plane_altimeter_usebox[playerid], -1);
	PlayerTextDrawBackgroundColor(playerid, plane_altimeter_usebox[playerid], 255);
	PlayerTextDrawBoxColor(playerid, plane_altimeter_usebox[playerid], 50);
	PlayerTextDrawUseBox(playerid, plane_altimeter_usebox[playerid], 1);
	PlayerTextDrawSetProportional(playerid, plane_altimeter_usebox[playerid], 1);
	PlayerTextDrawSetSelectable(playerid, plane_altimeter_usebox[playerid], 0);

	plane_altimeter_value[playerid] = CreatePlayerTextDraw(playerid, 579.000000, 354.000000, "%.1f");
	PlayerTextDrawFont(playerid, plane_altimeter_value[playerid], 2);
	PlayerTextDrawLetterSize(playerid, plane_altimeter_value[playerid], 0.237499, 1.549999);
	PlayerTextDrawTextSize(playerid, plane_altimeter_value[playerid], 400.000000, 17.000000);
	PlayerTextDrawSetOutline(playerid, plane_altimeter_value[playerid], 0);
	PlayerTextDrawSetShadow(playerid, plane_altimeter_value[playerid], 0);
	PlayerTextDrawAlignment(playerid, plane_altimeter_value[playerid], 1);
	PlayerTextDrawColor(playerid, plane_altimeter_value[playerid], -1);
	PlayerTextDrawBackgroundColor(playerid, plane_altimeter_value[playerid], 255);
	PlayerTextDrawBoxColor(playerid, plane_altimeter_value[playerid], 50);
	PlayerTextDrawUseBox(playerid, plane_altimeter_value[playerid], 0);
	PlayerTextDrawSetProportional(playerid, plane_altimeter_value[playerid], 1);
	PlayerTextDrawSetSelectable(playerid, plane_altimeter_value[playerid], 0);

	plane_altimeter_feet[playerid] = CreatePlayerTextDraw(playerid, 619.000000, 359.000000, "mts.");
	PlayerTextDrawFont(playerid, plane_altimeter_feet[playerid], 2);
	PlayerTextDrawLetterSize(playerid, plane_altimeter_feet[playerid], 0.133332, 0.899999);
	PlayerTextDrawTextSize(playerid, plane_altimeter_feet[playerid], 400.000000, 17.000000);
	PlayerTextDrawSetOutline(playerid, plane_altimeter_feet[playerid], 0);
	PlayerTextDrawSetShadow(playerid, plane_altimeter_feet[playerid], 0);
	PlayerTextDrawAlignment(playerid, plane_altimeter_feet[playerid], 1);
	PlayerTextDrawColor(playerid, plane_altimeter_feet[playerid], -1);
	PlayerTextDrawBackgroundColor(playerid, plane_altimeter_feet[playerid], 255);
	PlayerTextDrawBoxColor(playerid, plane_altimeter_feet[playerid], 50);
	PlayerTextDrawUseBox(playerid, plane_altimeter_feet[playerid], 0);
	PlayerTextDrawSetProportional(playerid, plane_altimeter_feet[playerid], 1);
	PlayerTextDrawSetSelectable(playerid, plane_altimeter_feet[playerid], 0);

	return 1;
}