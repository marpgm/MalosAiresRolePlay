#if defined _marp_vehicles_admin_included
	#endinput
#endif
#define _marp_vehicles_admin_included

CMD:av(playerid, params[])
{
	SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "_____________________________________[ ADMINISTRACION DE VEHICULOS ]_____________________________________");
	SendClientMessage(playerid, COLOR_INFO, "[COMANDOS] "COLOR_EMB_GREY" /avmotor - /avinfo - /avfix - /avfuel - /aventrar - /avtraer - /avrespawn - /avestacionar - /avhp");
	SendClientMessage(playerid, COLOR_INFO, "[COMANDOS] "COLOR_EMB_GREY" /avpatente - /avrespawnall - /avfuelcars - /avfixcars - /avresetplates- /avcolor - /avnitro");
	SendClientMessage(playerid, COLOR_INFO, "[COMANDOS] "COLOR_EMB_GREY" /avtipo - /avmodelo - /avfaccion - /avempleo - /avsirena - /avcrear - /avborrar - /avowner - /avgoto");
	SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "_________________________________________________________________________________________________________");
	return 1;
}

CMD:avmotor(playerid, params[])
{
	new vehicleid;

	if(!(vehicleid = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Debes estar en un veh�culo para utilizar este comando.");

	GetVehicleParamsEx(vehicleid, VehicleInfo[vehicleid][VehEngine], VehicleInfo[vehicleid][VehLights], VehicleInfo[vehicleid][VehAlarm], vlocked, VehicleInfo[vehicleid][VehBonnet], VehicleInfo[vehicleid][VehBoot], VehicleInfo[vehicleid][VehObjective]);
 	
 	if(VehicleInfo[vehicleid][VehEngine] != 1)
 	{
		SetEngine(vehicleid, 1);
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" veh�culo %i encendido correctamente.", vehicleid);
 	}
 	else
 	{
		SetEngine(vehicleid, 0);
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" veh�culo %i apagado correctamente.", vehicleid);
	}
	return 1;
}

CMD:avinfo(playerid, params[])
{
	new Float:hp, id, string[512];

	if(sscanf(params, "i", id) && !(id = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avinfo [idvehiculo]");
	if(!Veh_IsValidId(id, true))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de veh�culo inv�lida.");

	GetVehicleHealth(id, hp);

	new lastDriverName[MAX_PLAYER_NAME];

	Veh_GetLastDriverName(id, lastDriverName);

	format(string, sizeof(string),"\n\
		- Id: %i (SQLID: %i)\n\
		- Tipo: %i (%s)\n\
		- Tiempo de respawn sin conductor: %i segs\n\
		- Modelo: %s (%i)\n\
		- Colores: %i (%s####"COLOR_EMB_DLG_DEFAULT") - %i (%s####"COLOR_EMB_DLG_DEFAULT")\n\
		- Faccion: %i\n\
		- Empleo: %i\n\
		- due�o: %s (SQLID: %i)\n\
		- Patente: %s\n\
		- HP: %.2f - HP2: %.2f\n\
		- Gasolina: %i\n\
		- Cerrado: %i\n\
		- Sirena: %i\n\
		- Ultimo conductor: %s\n\
		- Container SQLID: %i",
		id, VehicleInfo[id][VehSQLID],
		VehicleInfo[id][VehType], Veh_GetSystemTypeName(VehicleInfo[id][VehType]),
		VehicleInfo[id][VehRespawnTime],
		Veh_GetModelName(VehicleInfo[id][VehModel]), VehicleInfo[id][VehModel],
		VehicleInfo[id][VehColor1], GetEmbeddedVehicleColor(VehicleInfo[id][VehColor1]), VehicleInfo[id][VehColor2], GetEmbeddedVehicleColor(VehicleInfo[id][VehColor2]),
		VehicleInfo[id][VehFaction],
		VehicleInfo[id][VehJob],
		VehicleInfo[id][VehOwnerName], VehicleInfo[id][VehOwnerSQLID],
		VehicleInfo[id][VehPlate],
		hp, VehicleInfo[id][VehHP],
		VehicleInfo[id][VehFuel],
		VehicleInfo[id][VehLocked],
		VehicleInfo[id][VehSiren],
		lastDriverName,
		VehicleInfo[id][VehContainerSQLID]
	);

	Dialog_Show(playerid, DLG_CMD_AVINFO, DIALOG_STYLE_MSGBOX, "Informaci�n de veh�culo", string, "Cerrar", "");
	return 1;
}

CMD:aventrar(playerid, params[])
{
	new vehicleid;

    if(sscanf(params, "i", vehicleid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /aventrar [idvehiculo]");
	if(!Veh_IsValidId(vehicleid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de veh�culo inv�lida.");
	
	new seat = Veh_GetFreeSeat(vehicleid);

	if(seat == -1)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Todos los asientos estan ocupados.");

	new Float:pos[4];
	GetPlayerPos(playerid, pos[0], pos[1], pos[2]);
	GetPlayerFacingAngle(playerid, pos[3]);
	TeleportPlayerTo(playerid, pos[0], pos[1], pos[2], pos[3], Veh_GetInterior(vehicleid), GetVehicleVirtualWorld(vehicleid));
	
	PutPlayerInVehicle(playerid, vehicleid, seat);
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Entraste correctamente al veh�culo %i.", vehicleid);
	return 1;
}

CMD:avrespawn(playerid, params[])
{
	new vehicleid;

	if(sscanf(params, "i", vehicleid) && !(vehicleid = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avrespawn [idvehiculo]");
	if(!Veh_IsValidId(vehicleid, true))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de veh�culo inv�lida.");

	Veh_Respawn(vehicleid);
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" veh�culo %i respawneado correctamente.", vehicleid);
	ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avrespawn", .playerid=playerid);
	return 1;
}

CMD:avrespawnall(playerid, params[])
{
	for(new i = 1; i < MAX_VEH; i++)
	{
 		if(IsVehicleOccupied(i) == 0) {
 			Veh_Respawn(i);
		}
	}
	SendClientMessageToAll(COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Todos los veh�culos desocupados han sido respawneados por un administrador.");
	return 1;
}

CMD:avtraer(playerid, params[])
{
	new vehicleid;

	if(sscanf(params, "i", vehicleid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avtraer [idvehiculo]");
	if(!Veh_IsValidId(vehicleid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de veh�culo inv�lida.");
	if(Depo_IsVehicle(vehicleid))
		return SendClientMessage(playerid, COLOR_YELLOW2, "Este veh�culo se encuentra en el deposito policial.");

	new Float:x, Float:y, Float:z, Float:angle, Float:dist, interior, vworld;

	GetVehicleModelInfo(GetVehicleModel(vehicleid), VEHICLE_MODEL_INFO_SIZE, x, y, z);
	dist = (x / 2.0) + 4.0;
	GetPlayerPos(playerid, x, y, z);
	GetPlayerFacingAngle(playerid, angle);
	GetXYInFrontOfPoint(x, y, angle, x, y, dist);
	interior = GetPlayerInterior(playerid);
	vworld = GetPlayerVirtualWorld(playerid);

	TeleportVehicleTo(vehicleid, x, y, z, angle, interior, vworld);
	
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" veh�culo %i tra�do correctamente.", vehicleid);
	return 1;
}

CMD:avfix(playerid, params[])
{
	new vehicleid;

	if(!(vehicleid = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Debes estar en un veh�culo para utilizar este comando.");

	RepairVehicle(vehicleid);
	VehicleInfo[vehicleid][VehHP] = 1000.0;
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" veh�culo %i reparado correctamente.", vehicleid);
	ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avfix", .playerid=playerid);
	return 1;
}

CMD:avfuel(playerid, params[])
{
	new vehicleid;

	if(!(vehicleid = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" debes estar en un veh�culo para utilizar este comando.");

	VehicleInfo[vehicleid][VehFuel] = 100;
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" veh�culo %i llenado correctamente con combustible.", vehicleid);
	ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avfuel", .playerid=playerid);
	return 1;
}

CMD:avfuelcars(playerid, params[])
{
	for(new c = 0; c < MAX_VEH; c++) {
		VehicleInfo[c][VehFuel] = 100;
	}

	SendClientMessageToAll(COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Todos los veh�culos han sido llenados con gasolina por un administrador.");
	ServerLog(LOG_TYPE_ID_VEHICLES, .entry="/avfuelcars", .playerid=playerid);
	return 1;
}

CMD:avfixcars(playerid, params[])
{
	for(new c = 0; c < MAX_VEH; c++)
	{
		RepairVehicle(c);
		VehicleInfo[c][VehHP] = 1000.0;
	}

	SendClientMessageToAll(COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Todos los veh�culos han sido reparados por un administrador.");
	ServerLog(LOG_TYPE_ID_VEHICLES, .entry="/avfixcars", .playerid=playerid);
	return 1;
}

CMD:avnitro(playerid, params[])
{
	new vehicleid;

	if(!(vehicleid = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Debes estar en un veh�culo para utilizar este comando.");

	AddVehicleComponent(vehicleid, 1010);
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Nitro temporal correctamente instalado en el veh�culo %i.", vehicleid);
	ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avnitro", .playerid=playerid);
	return 1;
}

CMD:avempleo(playerid, params[])
{
	new vehicleid, jobid;

	if(sscanf(params, "i", jobid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avempleo [idempleo]");
	if(!(vehicleid = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Debes estar en un veh�culo para utilizar este comando.");
	if(jobid < 0 || jobid > 10)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de empleo inv�lida.");

	VehicleInfo[vehicleid][VehJob] = jobid;
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El empleo del veh�culo %i ha sido ajustado a %i.", vehicleid, jobid);
	SaveVehicle(vehicleid);
	ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avempleo", .playerid=playerid, .params=params);
	return 1;
}

CMD:avfaccion(playerid, params[])
{
	new vehicleid, factionid;

	if(sscanf(params, "i", factionid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avfaccion [idfaccion]");
	if(!(vehicleid = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Debes estar en un veh�culo para utilizar este comando.");
	if(factionid < 0 || factionid > 20)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de facci�n inv�lida.");

	VehicleInfo[vehicleid][VehFaction] = factionid;
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" La facci�n del veh�culo %i ha sido ajustada a %i.", vehicleid, factionid);
	SaveVehicle(vehicleid);
	ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avfaccion", .playerid=playerid, .params=params);
	return 1;
}

CMD:avcolor(playerid, params[])
{
	new vehicleid, color1, color2;

	if(sscanf(params, "ii", color1, color2))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avcolor [color1] [color2]");
	if(!(vehicleid = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Debes estar en un veh�culo para utilizar este comando.");
	if(!(0 <= color1 <= 255 && 0 <= color2 <= 255))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Ingresa colores v�lidos: [0 - 255].");

	if(!Veh_SetColor(vehicleid, color1, color2))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" veh�culo inv�lido.");

	PutPlayerInVehicle(playerid, vehicleid, 0);
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El color del veh�culo %i ha sido ajustado a %i-%i.", vehicleid, color1, color2);
	ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avcolor", .playerid=playerid, .params=params);
	return 1;
}

CMD:avmodelo(playerid, params[])
{
	new vehicleid, modelid;

	if(sscanf(params, "ii", vehicleid, modelid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avmodelo [idvehiculo] [idmodelo]");
	if(!Veh_IsValidId(vehicleid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de veh�culo inv�lida.");
	if(!Veh_IsValidModel(modelid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de modelo incorrecta, debe estar en el rango de 400-611.");
	if(Veh_GetModelModelType(modelid) == VTYPE_TRAIN)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de modelo incorrecta, no es posible crear trenes.");

	if(!Veh_SetModel(vehicleid, modelid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" veh�culo inv�lido.");
	
	PutPlayerInVehicle(playerid, vehicleid, 0);
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El modelo del veh�culo %i ha sido cambiado a %i.", vehicleid, modelid);
	ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avmodelo", .playerid=playerid, .params=params);
	return 1;
}

CMD:avsirena(playerid, params[])
{
	new vehicleid, siren;

	if(sscanf(params, "i", siren))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avsirena [1 o 0]");
	if(!(vehicleid = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Debes estar en un veh�culo para utilizar este comando.");
	if(!(0 <= siren <= 1))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Usa 1 para instalar sirena o 0 para desinstalar.");

	if(!Veh_SetSiren(vehicleid, siren))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" veh�culo inv�lido.");

	PutPlayerInVehicle(playerid, vehicleid, 0);
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" La sirena del veh�culo %i ha sido cambiada a %i.", vehicleid, siren);
	ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avsirena", .playerid=playerid, .params=params);
	return 1;
}

CMD:avtipo(playerid, params[])
{
	new vehicleid, type;

	if(sscanf(params, "i", type))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avtipo [ID tipo] (1: est�tico - 2: Personal - 3: facci�n - 4: Licencia - 5: Temporal - 6: Empleo - 7: Renta)");
	if(!(vehicleid = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Debes estar en un veh�culo para utilizar este comando.");
	if(type < 1 || type > 7)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de tipo incorrecta.");

	if(!Veh_SetType(vehicleid, type))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" veh�culo inv�lido o no se pueden poner m�s veh�culos de renta.");

	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El tipo del veh�culo %i ha sido ajustado a %i.", vehicleid, type);
	ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avtipo", .playerid=playerid, .params=params);
	return 1;
}

CMD:avestacionar(playerid, params[])
{
	new vehicleid, Float:x, Float:y, Float:z, Float:angle, vworld, interior;

	if(!(vehicleid = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid,  COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Debes estar en un veh�culo para utilizar este comando.");

	GetVehiclePos(vehicleid, x, y, z);
	GetVehicleZAngle(vehicleid, angle);
	vworld = GetPlayerVirtualWorld(playerid);
	interior = GetPlayerInterior(playerid);

	if(!Veh_SetRespawnPoint(vehicleid, x, y, z, angle, vworld, interior))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" veh�culo inv�lido.");

	PutPlayerInVehicle(playerid, vehicleid, 0);
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" veh�culo %i estacionado correctamente.", vehicleid);
	ServerFormattedLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avestacionar", .playerid=playerid, .params=<"(x: %.2f y: %.2f z: %2.f)", x, y, z>);
	return 1;
}

CMD:avcrear(playerid, params[])
{
	new vehicleid, Float:x, Float:y, Float:z, Float:angle, modelid, color1, color2, respawn_secs, interior, vworld;

	if(sscanf(params, "iiii", modelid, color1, color2, respawn_secs))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avcrear [idmodelo] [color1] [color2] [respawn en segundos sin conductor] (para PERMANENTE use -1).");
	if(!Veh_IsValidModel(modelid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de modelo incorrecta, debe estar en el rango de [400 - 611].");
	if(!(0 <= color1 <= 255 && 0 <= color2 <= 255))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Ingresa colores v�lidos: [0 - 255].");
	if(Veh_GetModelModelType(modelid) == VTYPE_TRAIN)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de modelo incorrecta, no es posible crear trenes.");

	GetPlayerPos(playerid, x, y, z);
	GetPlayerFacingAngle(playerid, angle);

	interior = GetPlayerInterior(playerid);
	vworld = GetPlayerVirtualWorld(playerid);

	if(!(vehicleid = Veh_Create(modelid, color1, color2, x, y, z, angle, interior, vworld, respawn_secs)))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" L�mite de veh�culos alcanzado.");

	PutPlayerInVehicle(playerid, vehicleid, 0);
	if(respawn_secs > 0)
	{
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" veh�culo %i creado correctamente (luego de %i segundos sin un conductor ser� respawneado).", vehicleid, respawn_secs);
		SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El veh�culo fue creado como tipo temporal: al respawnear se borrar�. Para evitarlo, cambia el tipo con /avtipo.");
	} else {
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" veh�culo %i de tipo permanente creado correctamente. No posee tiempo de respawn sin conductor.", vehicleid);
	}
	ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avcrear", .playerid=playerid, .params=params);
	return 1;
}

CMD:avborrar(playerid, params[])
{
	new vehicleid;

	if(!(vehicleid = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Debes estar en un veh�culo para utilizar este comando.");

	if(Veh_Delete(vehicleid))
	{
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" veh�culo %i borrado correctamente.", vehicleid);
		ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avborrar", .playerid=playerid);
	} else {
		SendFMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Hubo un error en el borrado del veh�culo %i.", vehicleid);
	}
	return 1;
}

CMD:avhp(playerid, params[])
{
	new vehicleid, vhp;

	if(sscanf(params, "ii", vehicleid, vhp))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avhp [idvehiculo] [hp]");
	if(!Veh_IsValidId(vehicleid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de vehiculo incorrecta.");
	if(vhp < 250 || vhp > 10000)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Solo valores comprendidos en intervalo (250 - 10000).");

	SetVehicleHealth(vehicleid, float(vhp));
	// VehicleInfo[vehicleid][VehMaxHp] = float(vhp);
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" La vida del veh�culo %i ha sido cambiada a %i.", vehicleid, vhp);
	ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avhp", .playerid=playerid, .params=params);
	return 1;
}

CMD:avpatente(playerid, params[])
{
	new vehicleid, string[MAX_VEH_PLATE_LENGTH];
	
	if(sscanf(params, "is[10]", vehicleid, string))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avpatente [idvehiculo] [patente] (m�x 10 char)");

	if(Veh_SetCustomPlate(vehicleid, string))
	{
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" La patente del veh�culo %i ha sido cambiada a %s.", vehicleid, string);
		ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avpatente", .playerid=playerid, .params=params);
	} else {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de veh�culo o patente inv�lida.");
	}
	return 1;
}

CMD:avowner(playerid, params[])
{
	new vehicleid, ownerid, ownername[MAX_PLAYER_NAME];
	    
	if(!(vehicleid = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Debes estar en un veh�culo para utilizar este comando.");

	if(Veh_GetSystemType(vehicleid) == VEH_OWNED)
	{
		if(sscanf(params, "u", ownerid))
        	return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avowner [ID/Jugador]");
		if(!IsPlayerLogged(ownerid))
		    return SendClientMessage(playerid, COLOR_YELLOW2, "Jugador no conectado.");
		if(!KeyChain_GetFreeSlots(ownerid))
			return SendClientMessage(playerid, COLOR_YELLOW2, "El jugador no tiene m�s espacio en su llavero.");

		KeyChain_DeleteAll(KEY_TYPE_VEHICLE, vehicleid, .allowIfOwnerKey = true);
		GetPlayerName(ownerid, VehicleInfo[vehicleid][VehOwnerName], MAX_PLAYER_NAME);
		VehicleInfo[vehicleid][VehOwnerSQLID] = PlayerInfo[ownerid][pID];
		KeyChain_Add(ownerid, KEY_TYPE_VEHICLE, vehicleid, .label = Veh_GetName(vehicleid), .owner = true);
		SaveVehicle(vehicleid);
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Has borrado toda llave existente del veh�culo y has hecho a %s due�o del %s (ID %d).", GetPlayerCleanName(ownerid), Veh_GetName(vehicleid), vehicleid);
		SendFMessage(ownerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El administrador %s te ha hecho due�o del veh�culo %i (%s).", GetPlayerCleanName(playerid), vehicleid, Veh_GetName(vehicleid));
		ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avowner", .playerid=playerid, .targetid=ownerid);
	}
	else if(Veh_GetSystemType(vehicleid) != VEH_NONE)
	{
    	if(sscanf(params, "s[24]", ownername))
        	return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avowner [Nombre] (m�x 24 caracteres)");

        Veh_SetOwnerName(vehicleid, ownername);
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Has puesto al veh�culo %i (%s) a nombre de %s.", vehicleid, Veh_GetName(vehicleid), ownername);
		ServerLog(LOG_TYPE_ID_VEHICLES, .id=vehicleid, .entry="/avowner", .playerid=playerid, .params=ownername);
	}
	return 1;
}

CMD:avresetplates(playerid, params[])
{
	Veh_SetAllRandomPlates();
	SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Se han reseteado las patentes de todos los veh�culos.");
	return 1;
}

CMD:avgoto(playerid, params[])
{
	new Float:x, Float:y, Float:z, Float:angle, vehicleid;

	if(sscanf(params, "i", vehicleid) && !(vehicleid = GetPlayerVehicleID(playerid)))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /avgoto [idvehiculo]");
	if(!Veh_IsValidId(vehicleid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de veh�culo inv�lida.");
	
	GetVehiclePos(vehicleid, x, y, z); 
	GetVehicleZAngle(playerid, angle);
	x += 3.5 * floatsin(angle, degrees);
	y += 3.5 * floatcos(angle, degrees);

	TeleportPlayerTo(playerid, x, y, z, angle, Veh_GetInterior(vehicleid), GetVehicleVirtualWorld(vehicleid));
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Teletransportado al vehiculo ID: %i.", vehicleid);
	return 1;
}