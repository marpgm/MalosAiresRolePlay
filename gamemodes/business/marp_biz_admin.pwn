#if defined _marp_biz_admin_included
	#endinput
#endif
#define _marp_biz_admin_included

CMD:an(playerid, params[])
{
	SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "_____________________________________[ ADMINISTRACION DE NEGOCIOS ]______________________________________");
	SendClientMessage(playerid, COLOR_USAGE, "[COMANDOS] "COLOR_EMB_GREY" /angetid - /antele - /aninfo - /annombre - /anhabilitado - /ancaja - /anvender - /anprecioentrada");
	SendClientMessage(playerid, COLOR_USAGE, "[COMANDOS] "COLOR_EMB_GREY" /ancrear - /anborrar - /anprecio - /antipo - /anmapeo - /anentrada - /ansalida - /anpuntocompra");
	SendClientMessage(playerid, COLOR_USAGE, "[COMANDOS] "COLOR_EMB_GREY" /anstock - /anitem - /anrandomstock");
	SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Seteando posiciones deber�s estar mirando hacia la puerta para que el �ngulo de salida sea el correcto.");
	SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "_________________________________________________________________________________________________________");
	return 1;
}

CMD:ancrear(playerid, params[])
{
	new name[BIZ_MAX_NAME_LENGTH], price, type, mapid;

	if(sscanf(params, "iiis["#BIZ_MAX_NAME_LENGTH"]", price, type, mapid, name))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /ancrear [precio] [tipo] [ID mapeo] [nombre] (hasta "#BIZ_MAX_NAME_LENGTH" caracteres).");
	if(Util_HasInvalidSQLCharacter(name))
		return Util_PrintInvalidSQLCharacter(playerid);

	new Float:x, Float:y, Float:z, Float:angle, bizid;

	GetPlayerPos(playerid, x, y, z);
	GetPlayerFacingAngle(playerid, angle);

	if((bizid = Biz_Create(name, price, type, x, y, z, angle + 180.0, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid), mapid))) {
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Negocio ID %i creado correctamente.", bizid);
	} else {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" L�mite de negocios alcanzado o alg�n par�metro es incorrecto.");
	}

	return 1;
}

CMD:anborrar(playerid, params[])
{
	new bizid;

	if(sscanf(params, "i", bizid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /anborrar [ID negocio]");

	if(Biz_Delete(bizid)) {
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Negocio ID %i borrado correctamente.", bizid);
	} else {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio inv�lida.");
	}
	return 1;
}

CMD:anentrada(playerid, params[])
{
	new bizid;

	if(sscanf(params, "i", bizid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /anentrada [ID negocio] - Setea la entrada a tu posici�n. Debes estar mirando hacia la puerta de entrada.");

	new Float:x, Float:y, Float:z, Float:angle;
	GetPlayerPos(playerid, x, y, z);
	GetPlayerFacingAngle(playerid, angle);

	if(Biz_SetOutsidePoint(bizid, x, y, z, angle + 180.0, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid)))
	{
		new string[128];
		Biz_GetOutsidePointInfo(bizid, string, sizeof(string));
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Has seteado la entrada del negocio ID %i a: %s", bizid, string);
	} else {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio inv�lida.");
	}
	return 1;
}

CMD:ansalida(playerid, params[])
{
	new bizid;

	if(sscanf(params, "i", bizid))
	{
		SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /ansalida [ID negocio] - Setea la salida a tu posici�n. Debes estar mirando hacia la puerta de salida.");
		SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" Usa este comando con cuidado, teniendo en cuenta lo que implica si cambias el interior o mundo virtual.");
		return 1;
	}

	new Float:x, Float:y, Float:z, Float:angle;
	GetPlayerPos(playerid, x, y, z);
	GetPlayerFacingAngle(playerid, angle);

	if(Biz_SetInsidePoint(bizid, x, y, z, angle + 180.0, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid)))
	{
		new string[128];
		Biz_GetInsidePointInfo(bizid, string, sizeof(string));
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Has seteado la salida del negocio ID %i a: %s", bizid, string);
		SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Recuerda re-configurar las dem�s entidades del interior del negocio, como el punto de compra, etc.");
	} else {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio inv�lida.");
	}
	return 1;
}

CMD:anpuntocompra(playerid, params[])
{
	new bizid;

	if(sscanf(params, "i", bizid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /anpuntocompra [ID negocio]");
	if(!Biz_IsValidId(bizid) || Biz_GetPlayerLastId(playerid) != bizid)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio inv�lida o no fue el �ltimo negocio visitado.");

	new Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x, y, z);

	if(Biz_SetBuyingPoint(bizid, x, y, z))
	{
		new string[128];
		Biz_GetBuyingPointInfo(bizid, string, sizeof(string));
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Punto de compra del negocio ID %i seteado: %s.", bizid, string);
	} else {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio inv�lida.");
	}
	return 1;
}

CMD:anmapeo(playerid, params[])
{
	new bizid, mapid;

	if(sscanf(params, "ii", bizid, mapid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /anmapeo [ID negocio] [ID mapeo]");

	if(Biz_SetInsideMap(bizid, mapid))
	{
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El mapeo del negocio ID %i ha sido correctamente configurado al ID %i.", bizid, mapid);
		SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Recuerda reconfigurar las dem�s entidades del interior del negocio, como el punto de compra, etc.");
	} else {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio o mapeo inv�lida.");
	}
	return 1;
}

CMD:ancaja(playerid, params[])
{
	new bizid, money;

	if(sscanf(params, "ii", bizid, money))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /ancaja [ID negocio] [dinero]");

	if(Biz_SetTill(bizid, money)) {
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Dinero en caja del negocio ID %i configurado correctamente. Caja actual: $%i.", bizid, Biz_GetTill(bizid));
	} else {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio inv�lida.");
	}
	return 1;
}

CMD:annombre(playerid, params[])
{
	new bizid, name[BIZ_MAX_NAME_LENGTH];

	if(sscanf(params, "is["#BIZ_MAX_NAME_LENGTH"]", bizid, name))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /annombre [ID negocio] [nombre] (hasta "#BIZ_MAX_NAME_LENGTH" caracteres).");
	if(Util_HasInvalidSQLCharacter(name))
		return Util_PrintInvalidSQLCharacter(playerid);

	if(Biz_SetName(bizid, name)) {
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Nombre del negocio ID %i seteado con �xito a '%s'.", bizid, name);
	} else {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio inv�lida.");
	}
	return 1;
}

CMD:anprecio(playerid, params[])
{
	new bizid, price;

	if(sscanf(params, "ii", bizid, price))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /anprecio [ID negocio] [precio]");

	if(Biz_SetPrice(bizid, price)) {
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Has seteado correctamente el precio del negocio ID %i en $%i.", bizid, price);
	} else {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio o precio inv�lido.");
	}
	return 1;
}

CMD:anprecioentrada(playerid,params[])
{
	new bizid, price;

	if(sscanf(params, "ii", bizid, price))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /anprecioentrada [ID negocio] [precio]");

	if(Biz_SetEntranceFee(bizid, price)) {
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El precio de entrada del negocio ID %i ahora es de $%i.", bizid, price);
	} else {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio o precio inv�lido.");
	}
	return 1;
}

CMD:antipo(playerid, params[])
{
	new bizid, type;

	if(sscanf(params, "ii", bizid, type))
	{
		SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /antipo [ID negocio] [tipo]");
		Biz_PrintTypesForPlayer(playerid);
		return 1;
	}

	if(Biz_SetType(bizid, type)) {
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Has seteado correctamente el tipo del negocio ID %i en %i (%s).", bizid, type, Biz_GetTypeName(Business[bizid][bType]));
	} else {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio o tipo inv�lido.");
	}
	return 1;
}

CMD:angetid(playerid, params[])
{
	new bizid = Biz_IsPlayerOutsideOrInsideAny(playerid);

	if(bizid) {
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" ID del negocio actual: %i.", bizid);
	} else {
		SendClientMessage(playerid, COLOR_WHITE, "No se ha encontrado ning�n negocio en tu posici�n.");
	}
	return 1;
}

CMD:aninfo(playerid, params[])
{
	new bizid;

	if(sscanf(params, "i", bizid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /aninfo [ID negocio]");

	if(!Biz_PrintInfoForPlayer(playerid, bizid)) {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio inv�lida.");
	}
	return 1;
}

CMD:anvender(playerid, params[])
{
	new bizid;

	if(sscanf(params, "i", bizid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /anvender [ID negocio]");

	if(Biz_Sell(bizid))
	{
		Biz_SetRandomItemsStock(bizid, 100000);

		new string[128];
		format(string, sizeof(string), "[ADMIN INFO] %s (ID %i - SQLID %i): venta forzada de negocio ID %i.", GetPlayerCleanName(playerid), playerid, PlayerInfo[playerid][pID], bizid);
		AdministratorMessage(COLOR_ADMINCMD, string, 2);
	} else {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio inv�lida o no ten�a due�o.");
	}
	return 1;
}

CMD:antele(playerid, params[])
{
	new bizid;

	if(sscanf(params, "i", bizid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /antele [ID negocio]");
	if(!Biz_IsValidId(bizid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio inv�lida.");

	Biz_TpPlayerToOutsideDoorId(playerid, bizid);
	return 1;
}

CMD:anhabilitado(playerid, params[])
{
	new bizid, status;

	if(sscanf(params, "ii", bizid, status))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /anhabilitado [ID negocio] [1 = SI, 0 = NO]");

	if(Biz_SetEnterable(bizid, status)) {
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Negocio ID %i %s"COLOR_EMB_GREY" correctamente.", bizid, (Biz_IsEnterable(bizid)) ? ("{33FF33}habilitado") : ("{FF3333}deshabilitado"));
	} else {
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de negocio o par�metro inv�lido.");
	}
	return 1;
}