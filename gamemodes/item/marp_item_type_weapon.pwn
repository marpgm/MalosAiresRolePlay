#if defined _marp_item_type_weapon_included
	#endinput
#endif
#define _marp_item_type_weapon_included

#include <YSI_Coding\y_hooks>

static enum e_RELOAD_ANIMS
{
    reloadLibery[32],
    reloadAnim[32]
}

static const ItemWeapon_ReloadAnims[][e_RELOAD_ANIMS] = {
/*22*/ {
        /*reloadLibery*/ "colt45",
        /*reloadAnim*/ "colt45_reload"
    },
/*23*/ {
        /*reloadLibery*/ "silenced",
        /*reloadAnim*/ "silence_reload"
    },
/*24*/ {
        /*reloadLibery*/ "python",
        /*reloadAnim*/ "python_reload"
    },
/*25*/ {
        /*reloadLibery*/ "buddy",
        /*reloadAnim*/ "buddy_reload"
    },
/*26*/ {
        /*reloadLibery*/ "colt45",
        /*reloadAnim*/ "sawnoff_reload"
    },
/*27*/ {
        /*reloadLibery*/ "buddy",
        /*reloadAnim*/ "buddy_reload"
    },
/*28*/ {
        /*reloadLibery*/ "colt45",
        /*reloadAnim*/ "colt45_reload"
    },
/*29*/ {
        /*reloadLibery*/ "rifle",
        /*reloadAnim*/ "rifle_load"
    },
/*30*/ {
        /*reloadLibery*/ "rifle",
        /*reloadAnim*/ "rifle_load"
    },
/*31*/ {
        /*reloadLibery*/ "rifle",
        /*reloadAnim*/ "rifle_load"
    },
/*32*/ {
        /*reloadLibery*/ "uzi",
        /*reloadAnim*/ "uzi_reload"
    },
/*33*/ {
        /*reloadLibery*/ "rifle",
        /*reloadAnim*/ "rifle_load"
    },
/*34*/ {
        /*reloadLibery*/ "rifle",
        /*reloadAnim*/ "rifle_load"
    }
};

ItemWeapon_SearchInventory(playerid, itemid, &slot, &containerID)
{
    slot = -1;
    if(isPlayerCopOnDuty(playerid) || isPlayerSideOnDuty(playerid))
    {
        slot = Container_SearchItem(PlayerInfo[playerid][pBeltID], itemid);

        if(slot != -1) 
        {
            containerID = PlayerInfo[playerid][pBeltID];
            return 1;
        }
    }

    slot = Container_SearchItem(PlayerInfo[playerid][pContainerID], itemid);
    containerID = PlayerInfo[playerid][pContainerID];
    return 1;
}

hook function Item_OnUsed(playerid, hand, itemid, itemType)
{
	if(itemType != ITEM_WEAPON || Weapon_GetMagazineId(ItemModel_GetExtraId(itemid)) == ITEM_ID_NULL)
		return continue(playerid, hand, itemid, itemType);

	if(Item_IsHandlingCooldownOn(playerid))
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes esperar un tiempo antes de volver a interactuar con otro item!");

    new ItemWeapon_dataId = ItemModel_GetExtraId(itemid);
    new magazineitem = Weapon_GetMagazineId(ItemWeapon_dataId);
    new maxmunition = ItemModel_GetParamDefaultValue(itemid);

    new maghand = SearchHandsForItem(playerid, magazineitem);
    new maginvslot, magcontainerid, munition;

    ItemWeapon_SearchInventory(playerid, magazineitem, maginvslot, magcontainerid);

    if(maghand == -1 && maginvslot == -1)
    {
		SendFMessage(playerid, COLOR_YELLOW2, "Debes tener un %s en una de tus manos o inventario.", ItemModel_GetName(magazineitem));
		return 0;
	}

    if(maghand != -1)
    {
        munition = GetHandParam(playerid, maghand);

        if(munition <= maxmunition)
        {
            if(GetHandParam(playerid, hand) > 0) {
                SetHandItemAndParam(playerid, maghand, magazineitem, GetHandParam(playerid, hand));
            } else {
                SetHandItemAndParam(playerid, maghand, 0, 0); // Borrado l�gico y grafico
            }
        }
        else 
        {
            SetHandItemAndParam(playerid, maghand, magazineitem, GetHandParam(playerid, hand) + munition - maxmunition);
            munition = maxmunition;
        }
    }
    else 
    {
        Container_TakeItem(magcontainerid, maginvslot, magazineitem, munition);
        
        if(munition <= maxmunition)
        {
            if(GetHandParam(playerid, hand) > 0) {
                Container_AddItemAndParam(magcontainerid, magazineitem, GetHandParam(playerid, hand));
            }
        }
        else 
        {
            Container_AddItemAndParam(magcontainerid, magazineitem, GetHandParam(playerid, hand) + munition - maxmunition);
            munition = maxmunition;
        }
    }
    
    new animindex = GetPlayerAnimationIndex(playerid);
    new weaponid = Weapon_GetEngineWeaponId(ItemWeapon_dataId);      

    if(22 <= weaponid < 35)
    {   
        if(animindex != 1274 && animindex != 1159) { // PED/WEAPON_CROUCH = 1274 - PED/GUNCROUCHFWD = 1159
            ApplyAnimationEx(playerid, ItemWeapon_ReloadAnims[weaponid - 22][reloadLibery], ItemWeapon_ReloadAnims[weaponid - 22][reloadAnim], 4.0, 0, 0, 0, 0, 0, 1);
        }
    }

	new str[128];
	format(str, sizeof(str), "Recarga su %s", ItemModel_GetName(itemid));
	PlayerCmeMessage(playerid, 15.0, 4000, str);

    SetHandItemAndParam(playerid, hand, itemid, munition);
    Item_ApplyHandlingCooldown(playerid);
    return 1;
}

CMD:descargar(playerid, params[])
{
    new rightitem = GetHandItem(playerid, HAND_RIGHT),
        rightparam = GetHandParam(playerid, HAND_RIGHT);

    if(GetHandItem(playerid, HAND_LEFT))
        return SendClientMessage(playerid, COLOR_YELLOW2, "Debes tener la mano izquierda libre.");
    if((ItemModel_GetType(rightitem) != ITEM_WEAPON || !rightparam))
        return SendClientMessage(playerid, COLOR_YELLOW2, "Debes tener un arma con un cargador en tu mano derecha.");
	if(Item_IsHandlingCooldownOn(playerid))
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes esperar un tiempo antes de volver a interactuar con otro item!");

    new ItemWeapon_dataId = ItemModel_GetExtraId(rightitem);
    new magazineitem = Weapon_GetMagazineId(ItemWeapon_dataId);

    if(magazineitem == ITEM_ID_NULL)
        return SendClientMessage(playerid, COLOR_YELLOW2, "Esta arma no utiliza cargadores.");

    new str[128];
	format(str, sizeof(str), "Descarga su %s", ItemModel_GetName(rightitem));
	PlayerCmeMessage(playerid, 15.0, 4000, str);

    SetHandItemAndParam(playerid, HAND_RIGHT, rightitem, 0);
    SetHandItemAndParam(playerid, HAND_LEFT, magazineitem, rightparam);

    new weaponid = Weapon_GetEngineWeaponId(ItemWeapon_dataId);
    new animindex = GetPlayerAnimationIndex(playerid);

    if(22 <= weaponid < 35)
    {
        if(animindex != 1274 && animindex != 1159) { // PED/WEAPON_CROUCH = 1274 - PED/GUNCROUCHFWD = 1159
            ApplyAnimationEx(playerid, ItemWeapon_ReloadAnims[weaponid - 22][reloadLibery], ItemWeapon_ReloadAnims[weaponid - 22][reloadAnim], 4.0, 0, 0, 0, 0, 0, 1);
        }
    }

	Item_ApplyHandlingCooldown(playerid);
    return 1;
}