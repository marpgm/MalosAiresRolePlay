#if defined _marp_peajes_included
	#endinput
#endif
#define _marp_peajes_included

#include <YSI_Coding\y_hooks>

const PEAJE_MAX_AMOUNT = 128;
const PEAJE_RESET_ID = PEAJE_MAX_AMOUNT;
const Float:PEAJE_DEFAULT_SPEED = 2.0;
const INVALID_PEAJE_ID = -1;

enum
{
	/*0*/ PEAJE_TYPE_NONE,
	/*1*/ PEAJE_TYPE_BUSINESS,
	/*2*/ PEAJE_TYPE_HOUSE,
	/*3*/ PEAJE_TYPE_FACTION,

	/*LEAVE LAST*/ 	PEAJE_TYPES_AMOUNT
};

static enum e_PEAJE_DATA
{
	pjState,
	pjType,
	pjExtraid,
	Float:pjPosClosed[6],
	Float:pjPosOpen[6],
	Float:pjSpeed,
	STREAMER_TAG_OBJECT:pjObject,
	Button:pjFrontButton,
	STREAMER_TAG_OBJECT:pjFrontPanelObject,
	Button:pjBackButton,
	STREAMER_TAG_OBJECT:pjBackPanelObject,
	STREAMER_TAG_AREA:pjVehArea,
	pjAutoCloseTime,
	pjAutoCloseTimer,
	pjAsociatePeaje
}

static PeajeData[PEAJE_MAX_AMOUNT + 1][e_PEAJE_DATA] = {
	{
		/*pjState*/ 0,
		/*pjType*/ PEAJE_TYPE_NONE,
		/*pjExtraid*/ 0,
		/*Float:pjPosClosed*/ {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
		/*Float:pjPosOpen*/ {0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
		/*Float:pjSpeed*/ PEAJE_DEFAULT_SPEED,
		/*STREAMER_TAG_OBJECT:pjObject*/ INVALID_STREAMER_ID,
		/*Button:pjFrontButton*/ INVALID_BUTTON_ID,
		/*STREAMER_TAG_OBJECT:pjFrontPanelObject*/ INVALID_STREAMER_ID,
		/*Button:pjBackButton*/ INVALID_BUTTON_ID,
		/*STREAMER_TAG_OBJECT:pjBackPanelObject*/ INVALID_STREAMER_ID,
		/*STREAMER_TAG_AREA:pjVehArea*/ INVALID_STREAMER_ID,
		/*pjAutoCloseTime*/ 0,
		/*pjAutoCloseTimer*/ 0,
		/*pjAsociatePeaje*/ INVALID_PEAJE_ID,
	}, ...
};

static Iterator:PeajeData<PEAJE_MAX_AMOUNT>;

static PeajePlayerData[MAX_PLAYERS] = {INVALID_PEAJE_ID, ...};

Peaje_Create(modelid,
			Float:cx, Float:cy, Float:cz, Float:crx, Float:cry, Float:crz,
			Float:ox, Float:oy, Float:oz, Float:orx, Float:ory, Float:orz,
			worldid = -1,
			Float:speed = PEAJE_DEFAULT_SPEED,
			type = PEAJE_TYPE_NONE,
			extraid = 0,
			autoCloseTime = 0,
			bool:staticObject = false)
{
	if(type <= PEAJE_TYPE_NONE)
	{
		print("[ERROR] Intentando crear puerta sin tipo.");
		return INVALID_PEAJE_ID;
	}

	new peajeid = Iter_Free(PeajeData);

	if(peajeid == ITER_NONE)
	{
		printf("[ERROR] Alcanzado PEAJE_MAX_AMOUNT (%i). Iter_Count: %i.", PEAJE_MAX_AMOUNT, Iter_Count(PeajeData));
		return INVALID_PEAJE_ID;
	}

	PeajeData[peajeid][pjState] = 0;

	if(staticObject) {
		PeajeData[peajeid][pjObject] = CreateDynamicObject(modelid, cx, cy, cz, crx, cry, crz, .worldid = -1, .streamdistance = -1.0, .drawdistance = 300.0);
	} else {
		PeajeData[peajeid][pjObject] = CreateDynamicObject(modelid, cx, cy, cz, crx, cry, crz, .worldid = worldid, .streamdistance = 300.0, .drawdistance = 300.0);
	}

	PeajeData[peajeid][pjPosClosed][0] = cx, PeajeData[peajeid][pjPosClosed][1] = cy, PeajeData[peajeid][pjPosClosed][2] = cz, PeajeData[peajeid][pjPosClosed][3] = crx, PeajeData[peajeid][pjPosClosed][4] = cry, PeajeData[peajeid][pjPosClosed][5] = crz;
	PeajeData[peajeid][pjPosOpen][0] = ox, PeajeData[peajeid][pjPosOpen][1] = oy, PeajeData[peajeid][pjPosOpen][2] = oz, PeajeData[peajeid][pjPosOpen][3] = orx, PeajeData[peajeid][pjPosOpen][4] = ory, PeajeData[peajeid][pjPosOpen][5] = orz;
	PeajeData[peajeid][pjSpeed] = (speed <= 0.0) ? (0.1) : (speed);
	PeajeData[peajeid][pjType] = type;
	PeajeData[peajeid][pjExtraid] = (extraid < 0) ? (0) : (extraid);
	PeajeData[peajeid][pjAutoCloseTime] = (autoCloseTime < 400) ? (0) : (autoCloseTime);
	PeajeData[peajeid][pjAutoCloseTimer] = 0;

	Iter_Add(PeajeData, peajeid);

	return peajeid;
}

Peaje_SetVehicleDetection(peajeid, Float:x, Float:y, Float:z, Float:size = 6.0)
{
	if(!Iter_Contains(PeajeData, peajeid))
		return 0;

	if(PeajeData[peajeid][pjVehArea]) {
		DestroyDynamicArea(PeajeData[peajeid][pjVehArea]);
	}

	new worldid = Streamer_GetIntData(STREAMER_TYPE_OBJECT, PeajeData[peajeid][pjObject], E_STREAMER_WORLD_ID);
	PeajeData[peajeid][pjVehArea] = CreateDynamicCylinder(x, y, z - 2.0, z + 2.0, .size = size, .worldid = worldid);
	Streamer_SetExtraIdArray(STREAMER_TYPE_AREA, PeajeData[peajeid][pjVehArea], STREAMER_ARRAY_TYPE_PEAJE, peajeid);
	return 1;
}

//    Peaje_SetTexture(peajeid, index, txdmodelid, const txdname[], const texturename[], materialcolor = 0x00000000)
//    {
//    	if(!Iter_Contains(PeajeData, peajeid))
//    		return 0;
//    
//    	SetDynamicObjectMaterial(PeajeData[peajeid][pjObject], index, txdmodelid, txdname, texturename, materialcolor);
//    	return 1;
//    }

//Peaje_AssociateToPeaje(peajeid, associatepeaje)
//{
//    	if(!Iter_Contains(PeajeData, peajeid) || !Iter_Contains(PeajeData, associatepeaje) || peajeid == associatepeaje)
//  		return 0;
//	
//	    PeajeData[peajeid][pjAsociatePeaje] = associatepeaje;
//	    return 1;
//}

Peaje_Destroy(peajeid)
{
	if(!Iter_Contains(PeajeData, peajeid))
		return 0;

	Iter_Remove(PeajeData, peajeid); // Lo ponemos primero por si hubiese una asociaci�n circular y fuese invocado nuevamente el destroy del ID original
	DestroyDynamicObject(PeajeData[peajeid][pjObject]);

	if(PeajeData[peajeid][pjVehArea])
	{
		if(IsAnyPlayerInDynamicArea(PeajeData[peajeid][pjVehArea]))
		{
			foreach(new playerid : Player)
			{
				if(peajeid == PeajePlayerData[playerid]) {
					PeajePlayerData[playerid] = INVALID_PEAJE_ID;
				}
			}
		}

		DestroyDynamicArea(PeajeData[peajeid][pjVehArea]);
	}

	if(PeajeData[peajeid][pjFrontPanelObject]) {
		DestroyDynamicObject(PeajeData[peajeid][pjFrontPanelObject]);
	}

	if(PeajeData[peajeid][pjFrontButton] != INVALID_BUTTON_ID) {
		Button_Destroy(PeajeData[peajeid][pjFrontButton]);
	}

	if(PeajeData[peajeid][pjBackPanelObject]) {
		DestroyDynamicObject(PeajeData[peajeid][pjBackPanelObject]);
	}
	
	if(PeajeData[peajeid][pjBackButton] != INVALID_BUTTON_ID) {
		Button_Destroy(PeajeData[peajeid][pjBackButton]);
	}
	
	if(PeajeData[peajeid][pjAutoCloseTimer]) {
		KillTimer(PeajeData[peajeid][pjAutoCloseTimer]);
	}

	if(PeajeData[peajeid][pjAsociatePeaje] != INVALID_PEAJE_ID) {
		Peaje_Destroy(PeajeData[peajeid][pjAsociatePeaje]);
	}

	PeajeData[peajeid] = PeajeData[PEAJE_RESET_ID];
	return 1;
}

Peaje_OnPlayerEnterArea(playerid, peajeid) {
	PeajePlayerData[playerid] = peajeid;
}

Peaje_OnPlayerLeaveArea(playerid, peajeid)
{
	if(peajeid == PeajePlayerData[playerid]) {
		PeajePlayerData[playerid] = INVALID_PEAJE_ID;
	}
}

Peaje_CanPlayerUse(playerid, peajeid) 
{
	switch(PeajeData[peajeid][pjType])
	{
		case PEAJE_TYPE_BUSINESS: return Biz_CanUse(playerid, PeajeData[peajeid][pjExtraid]);
		case PEAJE_TYPE_HOUSE: return (KeyChain_Contains(playerid, KEY_TYPE_HOUSE, PeajeData[peajeid][pjExtraid]) || AdminDuty[playerid]);
		case PEAJE_TYPE_FACTION: return (!PeajeData[peajeid][pjExtraid] || PeajeData[peajeid][pjExtraid] == PlayerInfo[playerid][pFaction] || AdminDuty[playerid]);
		default: return 0;
	}
			
	return 0;
}

Peaje_UseOnVehicle(playerid, peajeid)
{   
	if(!Iter_Contains(PeajeData, peajeid))
		return 0;
	if(!Peaje_CanPlayerUse(playerid, peajeid))
		return 0;

    if(GetPlayerCash(playerid) < (150))
	   	return SendClientMessage(playerid, COLOR_YELLOW2, "No ten�s dinero suficiente para pagar el peaje, no podr�s pasar por ac�.");
    
	Peaje_Move(peajeid);
	PlayerPlaySound(playerid, 6002, 0.0, 0.0, 0.0);
	PlayerCmeMessage(playerid, 15.0, 4000, "Toma unos billetes de su bolsillo y se los cede al empleado del peaje.");
    PlayerDoMessage(playerid, 15.0, "El empleado toma los billetes y posteriormente presiona un bot�n levantando as� la barrera.");
    if(VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_PMA && VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_SIDE && VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_GOB && VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_HOSP) {
		Faction_GiveMoney(PlayerInfo[playerid][pFaction], -150);
		SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El dinero se descontara del fondo de tu faccion.");
	} else {
		GivePlayerCash(playerid, -150);
	}
	return 1;
}

static Peaje_Move(peajeid, chainMoveId = 0)
{
	static moveIdCounter;
	static moveId[PEAJE_MAX_AMOUNT];

	if(!chainMoveId) {
		chainMoveId = (++moveIdCounter);
	}

	moveId[peajeid] = chainMoveId;

	if(PeajeData[peajeid][pjState])
	{
		MoveDynamicObject(PeajeData[peajeid][pjObject], PeajeData[peajeid][pjPosClosed][0], PeajeData[peajeid][pjPosClosed][1], PeajeData[peajeid][pjPosClosed][2], PeajeData[peajeid][pjSpeed], PeajeData[peajeid][pjPosClosed][3], PeajeData[peajeid][pjPosClosed][4], PeajeData[peajeid][pjPosClosed][5]);

		if(PeajeData[peajeid][pjAutoCloseTimer])
		{
			KillTimer(PeajeData[peajeid][pjAutoCloseTimer]);
			PeajeData[peajeid][pjAutoCloseTimer] = 0;
		}
	}
	else
	{
		MoveDynamicObject(PeajeData[peajeid][pjObject], PeajeData[peajeid][pjPosOpen][0], PeajeData[peajeid][pjPosOpen][1], PeajeData[peajeid][pjPosOpen][2], PeajeData[peajeid][pjSpeed], PeajeData[peajeid][pjPosOpen][3], PeajeData[peajeid][pjPosOpen][4], PeajeData[peajeid][pjPosOpen][5]);

		if(PeajeData[peajeid][pjAutoCloseTime])
		{
			if(PeajeData[peajeid][pjAutoCloseTimer]) {
				KillTimer(PeajeData[peajeid][pjAutoCloseTimer]);
			}

			PeajeData[peajeid][pjAutoCloseTimer] = SetTimerEx("Peaje_AutoClose", PeajeData[peajeid][pjAutoCloseTime], false, "i", peajeid);
		}
	}

	PeajeData[peajeid][pjState] = !PeajeData[peajeid][pjState];	

	if(PeajeData[peajeid][pjAsociatePeaje] != INVALID_PEAJE_ID && moveId[PeajeData[peajeid][pjAsociatePeaje]] != chainMoveId) {
		Peaje_Move(PeajeData[peajeid][pjAsociatePeaje], chainMoveId);
	}
}

forward Peaje_AutoClose(peajeid);
public Peaje_AutoClose(peajeid)
{
	if(!Iter_Contains(PeajeData, peajeid))
		return 0;

	PeajeData[peajeid][pjAutoCloseTimer] = 0;

	if(PeajeData[peajeid][pjState])
	{
		MoveDynamicObject(PeajeData[peajeid][pjObject], PeajeData[peajeid][pjPosClosed][0], PeajeData[peajeid][pjPosClosed][1], PeajeData[peajeid][pjPosClosed][2], PeajeData[peajeid][pjSpeed], PeajeData[peajeid][pjPosClosed][3], PeajeData[peajeid][pjPosClosed][4], PeajeData[peajeid][pjPosClosed][5]);
		PeajeData[peajeid][pjState] = 0;
	}
	return 1;
}

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if(PeajePlayerData[playerid] != INVALID_PEAJE_ID && IsPlayerInAnyVehicle(playerid) && KEY_PRESSED_SINGLE(KEY_CROUCH)) {
		Peaje_UseOnVehicle(playerid, PeajePlayerData[playerid]);
	}
	return 1;
}

hook OnPlayerDisconnect(playerid, reason)
{
	PeajePlayerData[playerid] = INVALID_PEAJE_ID;
	return 1;
}

hook OnGameModeInitEnded()
{
	new peajeid;

    //====================================PEAJE RUTA 205======================================//

	peajeid = Peaje_Create(968,
				2881.789794, -743.317504, 10.591885, 0.000000, 90.000000, 180.000000,
				2881.769775, -743.317504, 10.590837, 0.299999, 2.999992, 180.000000,
				.worldid = -1, .speed = 0.025, .type = PEAJE_TYPE_FACTION, .extraid = -1, .autoCloseTime = 4000);

	Peaje_SetVehicleDetection(peajeid, 2877.5291, -737.7806,   10.9019, .size = 4.5);

    peajeid = Peaje_Create(968,
				2867.757080, -743.317504, 10.591885, 0.000000, 90.000000, 360.000000,
				2867.787109, -743.317687, 10.588437, -1.599999, 6.600017, 360.000000,
				.worldid = -1, .speed = 0.025, .type = PEAJE_TYPE_FACTION, .extraid = -1, .autoCloseTime = 4000);

	Peaje_SetVehicleDetection(peajeid, 2870.4133, -738.1749,   10.9019, .size = 4.5);

    peajeid = Peaje_Create(968,
				2888.099609, -732.258483, 10.591885, 0.000000, 90.000000, 360.000000,
				2888.129638, -732.258483, 10.591621, 0.299999, 0.500013, 360.000000,
				.worldid = -1, .speed = 0.025, .type = PEAJE_TYPE_FACTION, .extraid = -1, .autoCloseTime = 4000);

	Peaje_SetVehicleDetection(peajeid, 2892.6682, -736.3461,   10.9019, .size = 4.5);

    peajeid = Peaje_Create(968,
				2902.014404, -732.258483, 10.591885, 0.000000, 90.000000, 540.000000,
				2901.964355, -732.258483, 10.588136, -0.399999, 4.300013, 540.000000,
				.worldid = -1, .speed = 0.025, .type = PEAJE_TYPE_FACTION, .extraid = -1, .autoCloseTime = 4000);

	Peaje_SetVehicleDetection(peajeid, 2899.5098, -736.4572,   10.9019, .size = 4.5);

    //=====================================PEAJE AU. 25 DE MAYO LS====================================//
    
    peajeid = Peaje_Create(968,
				1700.561157, -476.375518, 33.138996, 0.000000, 90.000000, -169.199966,
				1700.541625, -476.379241, 33.137115, 0.199999, 5.400002, -169.199966,
				.worldid = -1, .speed = 0.025, .type = PEAJE_TYPE_FACTION, .extraid = -1, .autoCloseTime = 4000);

	Peaje_SetVehicleDetection(peajeid, 1695.1105,-472.1425,33.3468, .size = 4.5);

    peajeid = Peaje_Create(968,
				1687.116943, -479.062225, 33.138996, 0.000000, 90.000000, 11.500035,
				1687.184448, -479.048522, 33.127807, -0.399999, 9.200000, 11.500035,
				.worldid = -1, .speed = 0.025, .type = PEAJE_TYPE_FACTION, .extraid = -1, .autoCloseTime = 4000);

	Peaje_SetVehicleDetection(peajeid, 1688.7284,-473.1215,33.3468, .size = 4.5);

    peajeid = Peaje_Create(968,
				1698.809448, -464.158386, 33.053588, 0.000000, 90.000000, 11.699996,
				1698.828979, -464.154357, 33.052330, 0.299999, 3.599994, 11.699996,
				.worldid = -1, .speed = 0.025, .type = PEAJE_TYPE_FACTION, .extraid = -1, .autoCloseTime = 4000);

	Peaje_SetVehicleDetection(peajeid, 1704.2207,-468.0153,33.3487, .size = 4.5);

    peajeid = Peaje_Create(968,
				1712.371459, -461.349975, 33.053588, 0.000000, 90.000000, 191.699996,
				1712.316650, -461.360595, 33.016307, -0.999999, 7.200010, 191.699996,
				.worldid = -1, .speed = 0.025, .type = PEAJE_TYPE_FACTION, .extraid = -1, .autoCloseTime = 4000);

	Peaje_SetVehicleDetection(peajeid, 1711.0227,-467.0537,33.3487, .size = 4.5);
    return 1;
}