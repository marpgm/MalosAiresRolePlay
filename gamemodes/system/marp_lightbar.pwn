#if defined _marp_lightbar
	#endinput
#endif
#define _marp_lightbar

#include <YSI_Coding\y_hooks>

new
NOSOUNDLIGHT[MAX_VEHICLES],
POLICELIGHT[11][MAX_VEHICLES];

CMD:sir(playerid, params[])
{
    return cmd_sirena(playerid, params);
}

CMD:sirena(playerid, params[])
{
    if(!Faction_HasTag(PlayerInfo[playerid][pFaction], FAC_TAG_GOB_TYPE))
        return SendClientMessage(playerid, COLOR_YELLOW2, "No eres miembro de una organizaci�n legal o no estas autorizado a utilizar este comando.");
	{
        if(VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_PMA && VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_SIDE && VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_GOB && VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_HOSP)
            return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar dentro de un veh�culo faccionario para utilizar este comando.");
        {
            if(IsPlayerInAnyVehicle(playerid))
            {
                new vehicleid = GetPlayerVehicleID(playerid);

                if(NOSOUNDLIGHT[vehicleid] == 0)
                {
                    POLICELIGHT[0][vehicleid] = CreateObject(19292,0.0,0.0,-1000.0,0.0,0.0,0.0); //Luz Azul 1
                    POLICELIGHT[1][vehicleid] = CreateObject(19292,0.0,0.0,-1000.0,0.0,0.0,0.0); //Luz Azul 2
                    POLICELIGHT[2][vehicleid] = CreateObject(19290,0.0,0.0,-1000.0,0.0,0.0,0.0); //Luz Roja 1
                    POLICELIGHT[3][vehicleid] = CreateObject(19290,0.0,0.0,-1000.0,0.0,0.0,0.0); //Luz Roja 2
                    POLICELIGHT[4][vehicleid] = CreateObject(19291,0.0,0.0,-1000.0,0.0,0.0,0.0); //Luz Verde 1
                    POLICELIGHT[5][vehicleid] = CreateObject(19291,0.0,0.0,-1000.0,0.0,0.0,0.0); //Luz Verde 2
                    POLICELIGHT[6][vehicleid] = CreateObject(18646,0.0,0.0,-1000.0,0.0,0.0,0.0); //Luz Unmarked 1
                    POLICELIGHT[7][vehicleid] = CreateObject(19288,0.0,0.0,-1000.0,0.0,0.0,0.0); //Luz Azul 8
                    POLICELIGHT[8][vehicleid] = CreateObject(19288,0.0,0.0,-1000.0,0.0,0.0,0.0); //Luz Azul 9
                    POLICELIGHT[9][vehicleid] = CreateObject(19288,0.0,0.0,-1000.0,0.0,0.0,0.0); //Luz Azul 10
                    POLICELIGHT[10][vehicleid] = CreateObject(19288,0.0,0.0,-1000.0,0.0,0.0,0.0); //Luz Azul 11
                    
                    if(GetVehicleModel(vehicleid) == 596) //LSPD
                    {
                        AttachObjectToVehicle(POLICELIGHT[0][vehicleid],vehicleid,  0.510, -0.380, 0.950, 0.000, 0.000, 0.000);
                        AttachObjectToVehicle(POLICELIGHT[1][vehicleid],vehicleid,  -0.510, -0.380, 0.950, 0.000, 0.000, 0.000);
                        PlayerCmeMessage(playerid, 15.0, 4000, "Presiona un bot�n del control y enciende las sirenas lum�nicas.");
                        NOSOUNDLIGHT[vehicleid] = 1;
                    }
                    else if(GetVehicleModel(vehicleid) == 597)  //SFPD
                    {
                        AttachObjectToVehicle(POLICELIGHT[0][vehicleid],vehicleid,  0.510, -0.380, 0.950, 0.000, 0.000, 0.000);
                        AttachObjectToVehicle(POLICELIGHT[1][vehicleid],vehicleid,  -0.510, -0.380, 0.950, 0.000, 0.000, 0.000);
                        PlayerCmeMessage(playerid, 15.0, 4000, "Presiona un bot�n del control y enciende las sirenas lum�nicas.");
                        NOSOUNDLIGHT[vehicleid] = 1;
                    }
                    else if(GetVehicleModel(vehicleid) == 598)  //LVPD
                    {
                        AttachObjectToVehicle(POLICELIGHT[0][vehicleid],vehicleid,  0.510, -0.310, 0.950, 0.000, 0.000, 0.000);
                        AttachObjectToVehicle(POLICELIGHT[1][vehicleid],vehicleid,  -0.510, -0.310, 0.950, 0.000, 0.000, 0.000);
                        PlayerCmeMessage(playerid, 15.0, 4000, "Presiona un bot�n del control y enciende las sirenas lum�nicas.");
                        NOSOUNDLIGHT[vehicleid] = 1;
                    }
                    else if(GetVehicleModel(vehicleid) == 599)  //Police Ranger
                    {
                        AttachObjectToVehicle(POLICELIGHT[0][vehicleid],vehicleid,  0.510, 0.050, 1.180, 0.000, 0.000, 0.000);
                        AttachObjectToVehicle(POLICELIGHT[1][vehicleid],vehicleid,  -0.510, 0.050, 1.180, 0.000, 0.000, 0.000);
                        PlayerCmeMessage(playerid, 15.0, 4000, "Presiona un bot�n del control y enciende las sirenas lum�nicas.");
                        NOSOUNDLIGHT[vehicleid] = 1;
                    }
                    else if(GetVehicleModel(vehicleid) == 407)  //FireTruck (Sin escalera)
                    {
                        AttachObjectToVehicle(POLICELIGHT[2][vehicleid],vehicleid,  0.610, 3.200, 1.380, 0.000, 0.000, 0.000);
                        AttachObjectToVehicle(POLICELIGHT[3][vehicleid],vehicleid,  -0.610, 3.200, 1.380, 0.000, 0.000, 0.000);
                        PlayerCmeMessage(playerid, 15.0, 4000, "Presiona un bot�n del control y enciende las sirenas lum�nicas.");
                        NOSOUNDLIGHT[vehicleid] = 1;
                    }
                    else if(GetVehicleModel(vehicleid) == 416)  //Ambulance
                    {
                        AttachObjectToVehicle(POLICELIGHT[4][vehicleid],vehicleid,  0.330, 0.900, 1.250, 0.000, 0.000, 0.000);
                        AttachObjectToVehicle(POLICELIGHT[5][vehicleid],vehicleid,  -0.330, 0.900, 1.250, 0.000, 0.000, 0.000);
                        PlayerCmeMessage(playerid, 15.0, 4000, "Presiona un bot�n del control y enciende las sirenas lum�nicas.");
                        NOSOUNDLIGHT[vehicleid] = 1;
                    }
                    else if(GetVehicleModel(vehicleid) == 427)  //Enforcer
                    {
                        AttachObjectToVehicle(POLICELIGHT[0][vehicleid],vehicleid,  0.380, 1.100, 1.450, 0.000, 0.000, 0.000);
                        AttachObjectToVehicle(POLICELIGHT[1][vehicleid],vehicleid,  -0.380, 1.100, 1.450, 0.000, 0.000, 0.000);
                        PlayerCmeMessage(playerid, 15.0, 4000, "Presiona un bot�n del control y enciende las sirenas lum�nicas.");
                        NOSOUNDLIGHT[vehicleid] = 1;
                    }
                    else
                    {
                        SendClientMessage(playerid, COLOR_YELLOW2, "Este veh�culo no cuenta con sirenas lum�nicas.");
                    }
                    return 1;
                }
                else if(NOSOUNDLIGHT[vehicleid] == 1)
                {
                    DestroyObject(POLICELIGHT[0][vehicleid]);
                    DestroyObject(POLICELIGHT[1][vehicleid]);
                    DestroyObject(POLICELIGHT[2][vehicleid]);
                    DestroyObject(POLICELIGHT[3][vehicleid]);
                    DestroyObject(POLICELIGHT[4][vehicleid]);
                    DestroyObject(POLICELIGHT[5][vehicleid]);
                    DestroyObject(POLICELIGHT[6][vehicleid]);
                    DestroyObject(POLICELIGHT[7][vehicleid]);
                    DestroyObject(POLICELIGHT[8][vehicleid]);
                    DestroyObject(POLICELIGHT[9][vehicleid]);
                    DestroyObject(POLICELIGHT[10][vehicleid]);
				    NOSOUNDLIGHT[vehicleid] = 0;
                    PlayerCmeMessage(playerid, 15.0, 4000, "Presiona un bot�n del control y apaga las sirenas lum�nicas.");
                    return 1;
                }
            }
        }
    }
    return 1;
}

hook OnVehicleSpawn(vehicleid)
{
    
    DestroyObject(POLICELIGHT[0][vehicleid]);
    DestroyObject(POLICELIGHT[1][vehicleid]);
    DestroyObject(POLICELIGHT[2][vehicleid]);
    DestroyObject(POLICELIGHT[3][vehicleid]);
    DestroyObject(POLICELIGHT[4][vehicleid]);
    DestroyObject(POLICELIGHT[5][vehicleid]);
    DestroyObject(POLICELIGHT[6][vehicleid]);
    DestroyObject(POLICELIGHT[7][vehicleid]);
    DestroyObject(POLICELIGHT[8][vehicleid]);
    DestroyObject(POLICELIGHT[9][vehicleid]);
    DestroyObject(POLICELIGHT[10][vehicleid]);
	NOSOUNDLIGHT[vehicleid] = 0;

    return 1;
}