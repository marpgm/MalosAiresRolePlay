#if defined _marp_money_included
	#endinput
#endif
#define _marp_money_included

#include <YSI_Coding\y_hooks>

static const MONEY_MIN_ALERT_VALUE = 500000; // Valor absoluto m�nimo en una sola transacci�n de GivePlayerCash para elevar una alerta administrativo y generar un log
static const MONEY_MAX_ALLOWED_VALUE = 10000000; // Valor absoluto m�ximopermitido en una sola transacci�n de GivePlayerCash antes de bloquear la cuenta

#define ResetMoneyBar ResetPlayerMoney
#define UpdateMoneyBar GivePlayerMoney

CMD:cajero(playerid, params[])
{
	if(!IsAtATM(playerid))
		return 1;

	ATM_Show(playerid, 0);
	return 1;
}

hook OnGameModeInitEnded()
{
	new onATMShowAddress = GetPublicAddressFromName("ATM_Show");

	Button_Create(0, 1350.3433, -1758.5756, 13.5078, .size = 0.7, .labelText = "Cajero", .onEnterText = "(H) Usar", .onPressCallbackAddress = onATMShowAddress);
	Button_Create(0, 1144.1825, -1765.3004, 13.6190, .size = 0.7, .labelText = "Cajero", .onEnterText = "(H) Usar", .onPressCallbackAddress = onATMShowAddress);
	Button_Create(0, 1834.2802, -1851.6484, 13.3897, .size = 0.7, .labelText = "Cajero", .onEnterText = "(H) Usar", .onPressCallbackAddress = onATMShowAddress);
	Button_Create(0, 527.4268, -1739.4935, 11.7066, .size = 0.7, .labelText = "Cajero", .onEnterText = "(H) Usar", .onPressCallbackAddress = onATMShowAddress);
	Button_Create(0, 1317.4628, -898.6657, 39.5781, .size = 0.7, .labelText = "Cajero", .onEnterText = "(H) Usar", .onPressCallbackAddress = onATMShowAddress);
	Button_Create(0, 486.4542, -1271.3389, 15.6990, .size = 0.7, .labelText = "Cajero", .onEnterText = "(H) Usar", .onPressCallbackAddress = onATMShowAddress);
	Button_Create(0, 1594.2991, -2334.9038, 13.5398, .size = 0.7, .labelText = "Cajero", .onEnterText = "(H) Usar", .onPressCallbackAddress = onATMShowAddress);
	Button_Create(0, 2423.6157, -1220.1835, 25.4946, .size = 0.7, .labelText = "Cajero", .onEnterText = "(H) Usar", .onPressCallbackAddress = onATMShowAddress);
	Button_Create(0, 2232.5903, -1161.5964, 25.8906, .size = 0.7, .labelText = "Cajero", .onEnterText = "(H) Usar", .onPressCallbackAddress = onATMShowAddress);
	Button_Create(0, 2422.7905, -1959.7603, 13.5375, .size = 0.7, .labelText = "Cajero", .onEnterText = "(H) Usar", .onPressCallbackAddress = onATMShowAddress);
	Button_Create(0, 1175.1900, -1382.0300, 13.5400, .size = 0.7, .labelText = "Cajero", .onEnterText = "(H) Usar", .onPressCallbackAddress = onATMShowAddress);
	Button_Create(0, 695.14789, -539.28339, 15.98150, .size = 0.7, .labelText = "Cajero", .onEnterText = "(H) Usar", .onPressCallbackAddress = onATMShowAddress);
	return 1;
}

stock IsAtATM(playerid)
{
	if(IsPlayerInRangeOfPoint(playerid, 1.0,1350.3433,-1758.5756,13.5078) ||
		IsPlayerInRangeOfPoint(playerid, 1.0,1144.1825,-1765.3004,13.6190) ||
		IsPlayerInRangeOfPoint(playerid, 1.0,1834.2802,-1851.6484,13.3897) ||
		IsPlayerInRangeOfPoint(playerid, 1.0,527.4268,-1739.4935,11.7066) ||
		IsPlayerInRangeOfPoint(playerid, 1.0,1317.4628,-898.6657,39.5781) ||
		IsPlayerInRangeOfPoint(playerid, 1.0,486.4542,-1271.3389,15.6990) ||
		IsPlayerInRangeOfPoint(playerid, 1.0,1594.2991,-2334.9038,13.5398) ||
		IsPlayerInRangeOfPoint(playerid, 1.0,2423.6157,-1220.1835,25.4946) ||
		IsPlayerInRangeOfPoint(playerid, 1.0,2232.5903,-1161.5964,25.8906) ||
		IsPlayerInRangeOfPoint(playerid, 1.0,2422.7905,-1959.7603,13.5375) ||
		IsPlayerInRangeOfPoint(playerid, 1.0,1175.1900,-1382.0300,13.5400) ||
		IsPlayerInRangeOfPoint(playerid, 1.5,695.14789,-539.28339,15.98150)) {
		return 1;
	}
	return 0;
}

CMD:pagar(playerid,params[])
{
	new targetID, amount;

    if(sscanf(params, "ud", targetID, amount))
        return SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "{5CCAF1}[Sintaxis]{C8C8C8} /pagar [ID/Jugador] [cantidad]");
    if(GetPlayerCash(playerid) < amount || amount < 1 || amount > 500000)
        return SendClientMessage(playerid, COLOR_YELLOW2, " �Cantidad de dinero inv�lida! aseg�rate de tener dicha suma y que sea de m�s de $0 y menos de $500,000).");
	if(!IsPlayerLogged(targetID) || targetID == playerid)
		return SendClientMessage(playerid, COLOR_YELLOW2, "ID/Jugador inv�lido.");
    if(!IsPlayerInRangeOfPlayer(2.0, playerid, targetID))
        return SendClientMessage(playerid, COLOR_YELLOW2, " �Deben estar cerca!");

	GivePlayerCash(playerid, -amount);
	GivePlayerCash(targetID, amount);
	SendFMessage(playerid, COLOR_WHITE, "Le has pagado $%d a %s.", amount, GetPlayerCleanName(targetID));
	SendFMessage(targetID, COLOR_WHITE, "%s te ha pagado $%d.", GetPlayerCleanName(playerid), amount);
    PlayerPlayerCmeMessage(playerid, targetID, 15.0, 4000, "Toma algo de dinero y se lo entrega a");
    PlayerPlaySound(targetID, 1052, 0.0, 0.0, 0.0);
    PlayerPlaySound(playerid, 1052, 0.0, 0.0, 0.0);
    ServerFormattedLog(LOG_TYPE_ID_MONEY, .entry="PAGO", .playerid=playerid, .targetid=targetID, .params=<"$%d", amount>);
    return 1;
}

CMD:billetera(playerid, params[])
{
	new money;

	if(sscanf(params, "i", money))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /billetera [cantidad de dinero]");
	if(!(100 <= money <= 20000 && money <= GetPlayerCash(playerid)))
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Cantidad de dinero inv�lida! (m�nimo $100, m�ximo$20.000)");

	new freehand = SearchFreeHand(playerid);

	if(freehand == -1)
		return SendClientMessage(playerid, COLOR_YELLOW2, "Tienes ambas manos ocupadas.");

	GivePlayerCash(playerid, -money);
	SetHandItemAndParam(playerid, freehand, ITEM_ID_DINERO, money);
	PlayerCmeMessage(playerid, 15.0, 4000, "Toma algo de dinero de su billetera.");
	ServerFormattedLog(LOG_TYPE_ID_MONEY, .entry="/billetera", .playerid=playerid, .params=<"$%i", money>);
	return 1;
}

hook function Item_OnUsed(playerid, hand, itemid, itemType)
{
	if(itemid != ITEM_ID_DINERO)
		return continue(playerid, hand, itemid, itemType);

	new money = GetHandParam(playerid, hand);

	GivePlayerCash(playerid, money);
	SetHandItemAndParam(playerid, hand, 0, 0);
	PlayerCmeMessage(playerid, 15.0, 4000, "Guarda algo de dinero en su billetera.");
	ServerFormattedLog(LOG_TYPE_ID_MONEY, .entry="/consumir", .playerid=playerid, .params=<"$%i", money>);
	return 1;
}

CMD:depositar(playerid,params[])
{
	new amount;

	if(!IsPlayerInRangeOfPoint(playerid, 5.0, POS_BANK_X, POS_BANK_Y, POS_BANK_Z) && !IsAtATM(playerid))
	    return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en un banco o cajero autom�tico!");
 	if(sscanf(params, "i", amount))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /depositar [cantidad]");

	if(!Bank_PlayerDeposit(playerid, amount))
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Cantidad de dinero inv�lida!");

    return 1;
}

CMD:retirar(playerid,params[])
{
	new amount;
	
	if(!IsPlayerInRangeOfPoint(playerid, 5.0, POS_BANK_X, POS_BANK_Y, POS_BANK_Z) && !IsAtATM(playerid))
	    return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en un banco o cajero autom�tico!");
 	if(sscanf(params, "i", amount))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /retirar [cantidad]");

	if(!Bank_PlayerWithdraw(playerid, amount))
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Cantidad de dinero inv�lida!");

    return 1;
}

CMD:transferir(playerid, params[])
{
	new targetid, amount;

	if(!IsPlayerInRangeOfPoint(playerid, 5.0, POS_BANK_X, POS_BANK_Y, POS_BANK_Z) && !IsAtATM(playerid))
	    return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en un banco o cajero autom�tico!");
	if(sscanf(params, "ud", targetid, amount))
 		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /transferir [ID/Jugador] [cantidad]");

	if(!Bank_PlayerTransferTo(playerid, targetid, amount))
		return SendClientMessage(playerid, COLOR_YELLOW2, " �ID/Jugador o cantidad de dinero inv�lida!");

 	return 1;
}

CMD:verbalance(playerid,params[])
{
	if(!IsPlayerInRangeOfPoint(playerid, 5.0, POS_BANK_X, POS_BANK_Y, POS_BANK_Z) && !IsAtATM(playerid))
	    return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en un banco o cajero autom�tico!");

	SendFMessage(playerid, COLOR_WHITE, "Tu balance actual es de $%d.", PlayerInfo[playerid][pBank]);
	PlayerActionMessage(playerid, 15.0, "recibe un papel con el estado de su cuenta bancaria.");
    return 1;
}

CMD:donar(playerid, params[])
{
	new money, str[128];
	
	if(sscanf(params, "i", money))
		return SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "{5CCAF1}[Sintaxis]{C8C8C8} /donar [cantidad]");
	if(money < 0)
	    return SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "{FF4600}[Error]{C8C8C8} cantidad inv�lida.");
	if(GetPlayerCash(playerid) < money)
	    return SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "{FF4600}[Error]{C8C8C8} no tienes esa cantidad.");
	    
	GivePlayerCash(playerid, -money);
	format(str, sizeof(str), "{878EE7}[INFO]{C8C8C8} %s ha donado $%d.", GetPlayerCleanName(playerid), money);
	AdministratorMessage(COLOR_LIGHTYELLOW2, str, 2);
	SendFMessage(playerid, COLOR_WHITE, "Has donado $%i.", money);
	ServerFormattedLog(LOG_TYPE_ID_MONEY, .entry="DONACION", .playerid=playerid, .params=<"$%d", money>);
	return 1;
}

stock GivePlayerCash(playerid, money)
{
	SanityCashCheck(playerid, money);
	PlayerInfo[playerid][pCash] += money;
	ResetMoneyBar(playerid);
	UpdateMoneyBar(playerid, PlayerInfo[playerid][pCash]);
	MoneyBars_NewChange(playerid, money);
	return PlayerInfo[playerid][pCash];
}

stock SetPlayerCash(playerid, money)
{
	PlayerInfo[playerid][pCash] = money;
	ResetMoneyBar(playerid);
	UpdateMoneyBar(playerid, PlayerInfo[playerid][pCash]);
	return PlayerInfo[playerid][pCash];
}

stock ResetPlayerCash(playerid)
{
	PlayerInfo[playerid][pCash] = 0;
	ResetMoneyBar(playerid);
	UpdateMoneyBar(playerid, PlayerInfo[playerid][pCash]);
	return PlayerInfo[playerid][pCash];
}

stock GetPlayerCash(playerid) {
	return PlayerInfo[playerid][pCash];
}

stock SyncPlayerCash(playerid)
{
	ResetMoneyBar(playerid);
	UpdateMoneyBar(playerid, PlayerInfo[playerid][pCash]);
}

stock SanityCashCheck(playerid, money)
{
	if(money < (-MONEY_MIN_ALERT_VALUE) || money > MONEY_MIN_ALERT_VALUE)
	{
		new string[128];
		format(string, sizeof(string), "[ALERTA] "COLOR_EMB_GREY" Transacci�n de dinero elevada en %s (ID %i) por $%i.", GetPlayerCleanName(playerid), playerid, money);
		AdministratorMessage(COLOR_ALERT, string, 2);

		ServerFormattedLog(LOG_TYPE_ID_MONEY, .entry="SanityCashCheck", .playerid=playerid, .params=<"$%i", money>);

		if(money < (-MONEY_MAX_ALLOWED_VALUE) || money > MONEY_MAX_ALLOWED_VALUE) {
			BanPlayer(playerid, INVALID_PLAYER_ID, "Bloqueo preventivo por movimiento de dinero irregular.", .days = 0);
		}
	}
}

stock Money_IsValidValue(money, bool:allownegative = false)
{
	if(allownegative) {
		return ((-MONEY_MAX_ALLOWED_VALUE) <= money <= MONEY_MAX_ALLOWED_VALUE);
	} else {
		return (1 <= money <= MONEY_MAX_ALLOWED_VALUE);
	}
}

Bank_PlayerWithdraw(playerid, money)
{
	if(!(1 <= money <= PlayerInfo[playerid][pBank]))
		return 0;

	GivePlayerCash(playerid, money);
	PlayerInfo[playerid][pBank] -= money;
	SendFMessage(playerid, COLOR_WHITE, "Has retirado $%i. Nuevo balance: $%i.", money, PlayerInfo[playerid][pBank]);
	PlayerActionMessage(playerid, 15.0, "realiza una operaci�n en su cuenta bancaria.");
	Bank_OnUpdate(playerid, PlayerInfo[playerid][pBank], PlayerInfo[playerid][pBank] + money);

	ServerFormattedLog(LOG_TYPE_ID_MONEY, .entry="RETIRO", .playerid=playerid, .params=<"$%d", money>);
	return 1;
}

Bank_PlayerDeposit(playerid, money)
{
	if(!(1 <= money <= GetPlayerCash(playerid)))
		return 0;

	GivePlayerCash(playerid, -money);
	PlayerInfo[playerid][pBank] += money;
	SendFMessage(playerid, COLOR_WHITE, "Has depositado $%i. Nuevo balance: $%i.", money, PlayerInfo[playerid][pBank]);
	PlayerActionMessage(playerid, 15.0, "realiza una operaci�n en su cuenta bancaria.");
	Bank_OnUpdate(playerid, PlayerInfo[playerid][pBank], PlayerInfo[playerid][pBank] - money);

	ServerFormattedLog(LOG_TYPE_ID_MONEY, .entry="DEPOSITO", .playerid=playerid, .params=<"$%d", money>);
	return 1;
}

Bank_PlayerTransferTo(playerid, targetid, money)
{
	if(!(1 <= money <= PlayerInfo[playerid][pBank]))
		return 0;
	if(!IsPlayerLogged(targetid) || targetid == playerid)
		return 0;

	PlayerActionMessage(playerid, 15.0, "aprieta algunos botones del cajero y realiza una operaci�n bancaria.");

	PlayerInfo[playerid][pBank] -= money;
	PlayerInfo[targetid][pBank] += money;

	SendFMessage(playerid, COLOR_WHITE, "Has realizado una transferencia de $%i a la cuenta de %s.", money, GetPlayerCleanName(targetid));
	SendFMessage(targetid, COLOR_WHITE, "[Mensaje del Banco] Has recibido una transferencia de la cuenta de %s por $%i.", GetPlayerCleanName(playerid), money);
   
    Bank_OnUpdate(playerid, PlayerInfo[playerid][pBank], PlayerInfo[playerid][pBank] + money);
    Bank_OnUpdate(targetid, PlayerInfo[targetid][pBank], PlayerInfo[targetid][pBank] - money);

	ServerFormattedLog(LOG_TYPE_ID_MONEY, .entry="TRANSFERENCIA", .playerid=playerid, .targetid=targetid, .params=<"$%d", money>);
	return 1;
}