#if defined _marp_damage_included
	#endinput
#endif
#define _marp_damage_included

#include "system\damage\marp_damage_wound.pwn"
#include "system\damage\marp_damage_weapon.pwn"

#include <YSI_Coding\y_hooks>

#define DAMAGE_FIREGUN_EXTREMITY_NERF	(0.6)
#define DAMAGE_MIN_TO_PROCCESS			(0.15) // Evita floodeo por da�os de fuego en damage taken. Omite cerca de la mitad de las ocurrencias

#define DAMAGE_CRACK_HP_MIN_LIMIT		(1.0)
#define DAMAGE_CRACK_HP					(15.0)
#define DAMAGE_CRACK_HP_LOSS			(0.1)

static bool:Damage_dyingCamera[MAX_PLAYERS];

SetPlayerHealthEx(playerid, Float:health)
{
	PlayerInfo[playerid][pHealth] = health;
	SetPlayerHealth(playerid, health);
	return 1;
}

Float:GetPlayerHealthEx(playerid) {
	return PlayerInfo[playerid][pHealth];
}

SetPlayerArmourEx(playerid, Float:armour)
{
	PlayerInfo[playerid][pArmour] = armour;
	SetPlayerArmour(playerid, armour);
	return 1;
}

Float:GetPlayerArmourEx(playerid) {
	return PlayerInfo[playerid][pArmour];
}

hook OnGameModeInit()
{
    SetTeamCount(1);
    return 1;
}

hook OnPlayerSpawn(playerid)
{
    SetPlayerTeam(playerid, 1);
    Damage_dyingCamera[playerid] = false;
    return 1;
}

public OnPlayerGiveDamage(playerid, damagedid, Float:amount, weaponid, bodypart)
{
	static woundtype, itemid;

	if(!WeaponData[weaponid][wProccessWhenGiven] || AdminDuty[damagedid] || PlayerInfo[damagedid][pCrack])
		return 0;

	if(WeaponData[weaponid][wItemIdLinked])
	{
		itemid = GetHandItem(playerid, HAND_RIGHT);

		if(ItemModel_GetType(itemid) == ITEM_WEAPON && weaponid == WeaponData[ItemModel_GetExtraId(itemid)][wEngineWeaponId]) {
			weaponid = ItemModel_GetExtraId(itemid); // Paso del weaponid de samp a la del servidor
		} else {
			return 0;
		}
	}

	if(WeaponData[weaponid][wHasArmedElbowHit] && amount == 2.6400001049041748046875) {
		woundtype = WOUND_TYPE_HIT;
	}
	else
	{
		amount = (WeaponData[weaponid][wUsesDamageMultiplier]) ? (amount * WeaponData[weaponid][wDamage]) : (WeaponData[weaponid][wDamage]);
		woundtype = WeaponData[weaponid][wWoundType];

		if(WeaponData[weaponid][wHasSpecialEffect]) {
			Weapon_ApplySpecialEffect(weaponid, playerid, damagedid);
		}

		if(bodypart == BODY_PART_HEAD) {
			amount = (WeaponData[weaponid][wUsesDamageMultiplier]) ? (amount * WeaponData[weaponid][wHeadShotDamage]) : (WeaponData[weaponid][wHeadShotDamage]);
		}
		else if(bodypart == BODY_PART_TORSO) // else if(bodypart == BODY_PART_TORSO || bodypart == BODY_PART_GROIN)
		{
			if(WeaponData[weaponid][wArmourAffected])
			{
				if(PlayerInfo[damagedid][pArmour] > 0.0)
				{
					if((PlayerInfo[damagedid][pArmour] -= amount) <= 0.0)
					{
						amount = -PlayerInfo[damagedid][pArmour];
						PlayerInfo[damagedid][pArmour] = 0.0;
					} else {
						amount = 0.0;
					}

					SetPlayerArmour(damagedid, PlayerInfo[damagedid][pArmour]);
				}
			}
		}
		else if(bodypart != BODY_PART_GROIN && WeaponData[weaponid][wType] == WEAPON_TYPE_FIREGUN) { // En piernas o brazos, el da�o se reduce
			amount *= DAMAGE_FIREGUN_EXTREMITY_NERF;
		}
	}

	if(amount > DAMAGE_MIN_TO_PROCCESS)
	{
		if((PlayerInfo[damagedid][pHealth] -= amount) <= DAMAGE_CRACK_HP_MIN_LIMIT) {
			Damage_ApplyCrackEffect(damagedid);
		}

		SetPlayerHealth(damagedid, PlayerInfo[damagedid][pHealth]);
		Wound_OnPlayerReceived(damagedid, amount, bodypart, woundtype);
	}
	return 1;
}

public OnPlayerTakeDamage(playerid, issuerid, Float:amount, weaponid, bodypart)
{
	if(!WeaponData[weaponid][wProccessWhenTaken] || AdminDuty[playerid] || PlayerInfo[playerid][pCrack])
	{
		SetPlayerHealth(playerid, PlayerInfo[playerid][pHealth]);
		return 0;
	}

	// No se realiza an�lisis de headhsot, torso, 2.6400... , chaleco y dem�s ya que por la configuraci�n actual todos
	// los da�os que se procesen ac� son "naturales" o del ambiente, y no lo requieren. Si fuese necesario, el c�digo
	// a agregar ser�a mas o menos el de OnPlayerGiveDamage

	if(amount > DAMAGE_MIN_TO_PROCCESS)
	{
		// El siguiente c�digo para da�os auto-inflingidos no tiene sentido sin setear vida alta pues el cliente reporta la muerte instantanea:
		// if((PlayerInfo[playerid][pHealth] -= amount) <= DAMAGE_CRACK_HP_MIN_LIMIT) {
		// 	Damage_ApplyCrackEffect(playerid);
		// }
		// SetPlayerHealth(playerid, PlayerInfo[playerid][pHealth]);

		PlayerInfo[playerid][pHealth] -= amount;
		SetPlayerHealth(playerid, PlayerInfo[playerid][pHealth]);
		Wound_OnPlayerReceived(playerid, amount, bodypart, WeaponData[weaponid][wWoundType]);
	}

	// En el caso de da�os de fuego omitidos, esto actualizar�a en tiempo real la barra de vida. Poco necesario dado que son 500ms hasta la sincronizaci�n del anticheat:
	// else {
	// 	SetPlayerHealth(playerid, PlayerInfo[playerid][pHealth]);
	// }

	return 1;
}

IsPlayerCracked(playerid) {
	return (PlayerInfo[playerid][pCrack] || PlayerInfo[playerid][pDisabled] == DISABLE_DYING || PlayerInfo[playerid][pDisabled] == DISABLE_DEATHBED);
}

Damage_ApplyCrackEffect(playerid)
{
	PlayerInfo[playerid][pHealth] = DAMAGE_CRACK_HP;
	PlayerInfo[playerid][pCrack] = 1;
	Wound_UpdateLabel(playerid, "Gravemente herido");
	TogglePlayerControllable(playerid, false);
	Damage_ApplyCrackAnimation(playerid);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, " �Te encuentras herido e incapaz de moverte!, con cada segundo que pase perder�s algo de sangre.");
	PlayerInfo[playerid][pDisabled] = DISABLE_DYING;
	//Damage_ApplyDyingCamera(playerid);
	Dialog_Show(playerid, DLG_DYING, DIALOG_STYLE_MSGBOX, "Estas desangr�ndote", "Teniendo en cuenta el entorno en el que te encuentras, decide si es posible que alguien te haya visto y llame a emergencias.", "Avisar", "Cancelar");
}

Dialog:DLG_DYING(playerid, response, listitem, inputtext[])
{
	if(!response)
		return SendClientMessage(playerid, COLOR_YELLOW2, "Desafortunadamente nadie ha notado tu agon�a.");

	new Float:x, Float:y, Float:z, area[MAX_ZONE_NAME];

	PlayerPos_GetExteriorPos(playerid, x, y, z);
	GetCoords2DZone(x, y, area, MAX_ZONE_NAME);

	foreach(new i : Player)
	{
		if(IsMedicOnDuty(i) && PlayerHasRadio(i))
		{
			SendFMessage(i, COLOR_WHITE, "[EMERGENCIAS] Se solicita una ambulancia en el barrio de %s. Lo marcamos con rojo en su GPS.", area);
			MapMarker_CreateForPlayer(i, x, y, z, .color = COLOR_RED, .time = 300000);
		}
		else if(isPlayerCopOnDuty(i) && PlayerHasRadio(i))
		{
			SendFMessage(i, COLOR_CENTRALRED, "[911] Persona herida reportada en barrio de %s. Lo marcamos con rojo en su GPS.", area);
			MapMarker_CreateForPlayer(i, x, y, z, .color = COLOR_RED, .time = 300000);
		}
	}

	SendClientMessage(playerid, COLOR_YELLOW2, " �Un ciudadano not� tu agon�a y ha reportado tu situaci�n al 911!");
	return 1;
}

Damage_ApplyCrackAnimation(playerid)
{
	if(!IsPlayerInAnyVehicle(playerid))
	{
		ClearAnimations(playerid, 1);
		ApplyAnimationEx(playerid, "WUZI", "CS_DEAD_GUY", 4.0, 1, 1, 1, 1, 0, .forcesync = 1, .autofinish = true, .finishAnimId = 0);
	}
	else 
	{
		new modelid = GetVehicleModel(GetPlayerVehicleID(playerid));

		switch (modelid) 
		{
			case 509, 481, 510, 462, 448, 581, 522, 461, 521, 523, 463, 586, 468, 471: 
			{
				new Float:vx, Float:vy, Float:vz;
				GetVehicleVelocity(GetPlayerVehicleID(playerid), vx, vy, vz);

				ClearAnimations(playerid, 1);
				if(VectorSize(vx, vy, vz) >= 0.63) {
					ApplyAnimationEx(playerid, "PED", "BIKE_fallR", 4.0, 0, 0, 0, 1, 0, .forcesync = 1, .autofinish = true, .finishAnimId = 0);
				} else {
					ApplyAnimationEx(playerid, "PED", "BIKE_fall_off", 4.0, 0, 0, 0, 1, 0, .forcesync = 1, .autofinish = true, .finishAnimId = 0);
				}
			}
			default: 
			{
				if(GetPlayerVehicleSeat(playerid) & 1) {
					ApplyAnimationEx(playerid, "PED", "CAR_dead_LHS", 4.0, 0, 0, 0, 1, 0, .forcesync = 1, .autofinish = true, .finishAnimId = 0);
				} else {
					ApplyAnimationEx(playerid, "PED", "CAR_dead_RHS", 4.0, 0, 0, 0, 1, 0, .forcesync = 1, .autofinish = true, .finishAnimId = 0);
				}
			}
		}
	}
	return 1;
}

hook function SetPlayerHealthEx(playerid, Float:health)
{
	if(health >= 100.0 && PlayerInfo[playerid][pCrack])
	{
		SendClientMessage(playerid, COLOR_WHITE, " �Has sido curado!, ten m�s cuidado la pr�xima vez.");
		TogglePlayerControllable(playerid, true);
		ClearAnimations(playerid, 1);
		PlayerInfo[playerid][pDisabled] = DISABLE_NONE;
		PlayerInfo[playerid][pCrack] = 0;
	}
	return continue(playerid, health);
}

Damage_ApplyDeathEffect(playerid)
{
	TogglePlayerControllable(playerid, false);
	ClearAnimations(playerid, 1);
	ApplyAnimationEx(playerid, "PED", "FLOOR_hit_f", 4.0, 0, 0, 0, 1, 0, .forcesync = 1, .autofinish = true, .finishAnimId = 0);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, "est�s en tu lecho de muerte por lo que ya no podr�n salvarte, puedes utilizar {FFFFFF}/morir{87CEFA} o continuar roleando.");
	PlayerInfo[playerid][pDisabled] = DISABLE_DEATHBED;
	Wound_UpdateLabel(playerid, "Muerto");
}

/*
Damage_ApplyDyingCamera(playerid)
{
	if(!GetPlayerInterior(playerid) && !GetPlayerVirtualWorld(playerid)) 
	{
		GetPlayerPos(playerid, PlayerInfo[playerid][pX], PlayerInfo[playerid][pY], PlayerInfo[playerid][pZ]);
		SetPlayerCameraPos(playerid, PlayerInfo[playerid][pX] - 5.0, PlayerInfo[playerid][pY] - 5.0, PlayerInfo[playerid][pZ] + 6.0);
		SetPlayerCameraLookAt(playerid, PlayerInfo[playerid][pX], PlayerInfo[playerid][pY], PlayerInfo[playerid][pZ], CAMERA_MOVE);
		Damage_dyingCamera[playerid] = true;
	}
}*/

CMD:morir(playerid, params[])
{
	if(PlayerInfo[playerid][pDisabled] == DISABLE_DEATHBED)
	{
		PlayerInfo[playerid][pDisabled] = DISABLE_NONE;
		SetPlayerHealthEx(playerid, 0.0);
	} else {
		SendClientMessage(playerid, COLOR_YELLOW2, "No puedes utilizarlo en este momento.");
	}
	return 1;
}

hook OnPlayerGlobalUpdate(playerid)
{
	if(!PlayerInfo[playerid][pHospitalized] && PlayerInfo[playerid][pJailed] != JAIL_OOC && !PlayerInfo[playerid][pDead])
	{
		if(Damage_dyingCamera[playerid])
		{
			if(PlayerInfo[playerid][pHealth] > DAMAGE_CRACK_HP || IsPlayerInAnyVehicle(playerid))
			{
				Damage_dyingCamera[playerid] = false;
				SetCameraBehindPlayer(playerid);
			}
		}

		if(PlayerInfo[playerid][pCrack])
		{
			if(PlayerInfo[playerid][pDisabled] != DISABLE_DYING && PlayerInfo[playerid][pDisabled] != DISABLE_DEATHBED) {
				Damage_ApplyCrackEffect(playerid);
			}
			else if(PlayerInfo[playerid][pDisabled] == DISABLE_DYING && PlayerInfo[playerid][pHealth] > DAMAGE_CRACK_HP_MIN_LIMIT) {
				PlayerInfo[playerid][pHealth] -= DAMAGE_CRACK_HP_LOSS;
			}
			else if(PlayerInfo[playerid][pDisabled] != DISABLE_DEATHBED && PlayerInfo[playerid][pHealth] <= DAMAGE_CRACK_HP_MIN_LIMIT) {
				Damage_ApplyDeathEffect(playerid);
			}
		}
	}
	return 1;
}