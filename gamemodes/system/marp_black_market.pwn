#if defined _marp_black_market_included
	#endinput
#endif
#define _marp_black_market_included

#include <YSI_Coding\y_hooks>

static const BM_PICKUP_MODEL = 1210;
static const Float:BM_BUY_PERCENTAGE = 1.25;
static const Float:BM_SELL_PERCENTAGE = 0.5;

static const BM_MAX_BUY_AMOUNT = 100;

static enum e_BLACK_MARKET_INFO {
    STREAMER_TAG_PICKUP:blackmarketPickup,
    Float:blackmarketX,
    Float:blackmarketY,
    Float:blackmarketZ
};

static BlackMarket_Info[][e_BLACK_MARKET_INFO] = {
    {STREAMER_TAG_PICKUP:0, 2659.6992, -2056.4814, 13.4214},
    {STREAMER_TAG_PICKUP:0, 1296.0912, -990.3329, 32.6260},
    {STREAMER_TAG_PICKUP:0, 1677.5022, -2154.9429, 13.5601}

};

static BlackMarket_Items[] = {
    ITEM_ID_BARRETA,
    ITEM_ID_WEED_SEEDS,
    ITEM_ID_FACA_TUMBERA,
    ITEM_ID_CHEF_KNIFE
};

static BlackMarket_Item[MAX_PLAYERS];
static BlackMarket_Dialog[4096];

hook function LoadPickups()
{
    for(new i = 0; i < sizeof(BlackMarket_Info); i++) {
        BlackMarket_Info[i][blackmarketPickup] = CreateDynamicPickup(BM_PICKUP_MODEL, 1, BlackMarket_Info[i][blackmarketX], BlackMarket_Info[i][blackmarketY], BlackMarket_Info[i][blackmarketZ]);
    }
    return continue();
}

hook LoadAccountDataEnded(playerid) {
    BlackMarket_Item[playerid] = -1;
    return 1;
}

BlackMarket_IsPlayerAt(playerid)
{
    for(new i = 0; i < sizeof(BlackMarket_Info); i++) {
        if(IsPlayerInRangeOfPoint(playerid, 1.4, BlackMarket_Info[i][blackmarketX], BlackMarket_Info[i][blackmarketY], BlackMarket_Info[i][blackmarketZ]))
            return 1;
    }
    return 0;
}

IsBlackMarketPickup(pickupid)
{
    for(new i = 0; i < sizeof(BlackMarket_Info); i++) {
        if(pickupid == BlackMarket_Info[i][blackmarketPickup])
            return 1;
    }
    return 0;
}

CMD:vender(playerid, params[])
{
	if(BlackMarket_IsPlayerAt(playerid))
	{
	    new item = GetHandItem(playerid, HAND_RIGHT);
	    
		if(item == 0)
		    return SendClientMessage(playerid, COLOR_YELLOW2, "No tienes nada en tu mano derecha para vender.");
		if(!ItemModel_HasTag(item, ITEM_TAG_BLACK_MARKET))
		    return SendClientMessage(playerid, COLOR_WHITE, "Comprador: No estoy interesado.");

        new cash;
        if(ItemModel_HasTag(item, ITEM_TAG_FIX_PRICE)) {
            cash = floatround(ItemModel_GetPrice(item) * BM_SELL_PERCENTAGE,  floatround_ceil);
        }
        else {
            cash = floatround(ItemModel_GetPrice(item) * GetHandParam(playerid, HAND_RIGHT) * BM_SELL_PERCENTAGE,  floatround_ceil);
        }

    	GivePlayerCash(playerid, cash);
		SendFMessage(playerid, COLOR_WHITE, "Comprador: Bien, te dar� $%d por tu %s - %s: %d", cash, ItemModel_GetName(item), ItemModel_GetParamName(item), GetHandParam(playerid, HAND_RIGHT));
        ServerFormattedLog(LOG_TYPE_ID_MONEY, .entry="VENTA MERCADO NEGRO", .playerid=playerid, .params=<"%s - %s: %d por $%d", ItemModel_GetName(item), ItemModel_GetParamName(item), GetHandParam(playerid, HAND_RIGHT), cash>);

        new str[128];
        format(str, sizeof(str), "Le entrega un/a %s al sujeto", ItemModel_GetName(item));
	    PlayerCmeMessage(playerid, 15.0, 4000, str);
		SetHandItemAndParam(playerid, HAND_RIGHT, 0, 0);
		return 1;
	}
	return 1;
}

hook function OnPlayerCmdComprar(playerid, const params[])
{
    if(!BlackMarket_IsPlayerAt(playerid))
        return continue(playerid, params);
    
    new freehand = SearchFreeHand(playerid);

    if(freehand == -1)
        return SendClientMessage(playerid, COLOR_YELLOW2, "Tienes ambas manos ocupadas y no puedes agarrar el item.");
    BlackMarket_ShowBuyDialog(playerid);
    return 1;
}

stock BlackMarket_ShowBuyDialog(playerid) 
{
    new bmstr[256];
    new price = 0;
    format(BlackMarket_Dialog, sizeof(BlackMarket_Dialog), "");
    strcat(BlackMarket_Dialog, "Item\tPrecio por unidad\n", sizeof(BlackMarket_Dialog));

    for(new i = 0; i < sizeof(BlackMarket_Items); i++) {
        price = floatround(ItemModel_GetPrice(BlackMarket_Items[i])*BM_BUY_PERCENTAGE,  floatround_ceil);
        format(bmstr, sizeof(bmstr), "%s\t$%d\n", ItemModel_GetName(BlackMarket_Items[i]), price);
        strcat(BlackMarket_Dialog, bmstr, sizeof(BlackMarket_Dialog));
    }
    Dialog_Show(playerid, DLG_BM_SHOW, DIALOG_STYLE_TABLIST_HEADERS, "Items del mercado negro:", BlackMarket_Dialog, "Seleccionar", "Cerrar"); 
    return 1;
}

Dialog:DLG_BM_SHOW(playerid, response, listitem, inputtext[]) 
{
    if(response)
    {
        BlackMarket_Item[playerid] = listitem;
        new str[128];
        format(str, sizeof(str), "Ingrese la cantidad a comprar de %s", ItemModel_GetName(BlackMarket_Items[listitem]));
        Dialog_Show(playerid, DLB_BM_AMOUNT, DIALOG_STYLE_INPUT, "Compra:", str, "Ingresar", "Cerrar"); 
    }
    return 1;
}

Dialog:DLB_BM_AMOUNT(playerid, response, listitem, inputtext[]) 
{
    if(response)
    {
        new bm_item = BlackMarket_Item[playerid];
        new cant = 0;
        if(sscanf(inputtext, "i", cant) || !(1 <= cant <= BM_MAX_BUY_AMOUNT))
        {
            new str[128];
            format(str, sizeof(str), "Ingrese la cantidad a comprar de %s (m�x: %i).", ItemModel_GetName(BlackMarket_Items[bm_item]), BM_MAX_BUY_AMOUNT);
            Dialog_Show(playerid, DLB_BM_AMOUNT, DIALOG_STYLE_INPUT, "Compra:", str, "Ingresar", "Cerrar");
        }
        else 
        {
            
            new price = floatround(ItemModel_GetPrice(BlackMarket_Items[bm_item])*cant*BM_BUY_PERCENTAGE,  floatround_ceil);
            new freehand = SearchFreeHand(playerid);

            if(GetPlayerCash(playerid) < price)
                return SendClientMessage(playerid, COLOR_WHITE, "Vendedor: Tomatela de ac� y volv� cuando tengas el dinero.");
            if(freehand == -1)
                return SendClientMessage(playerid, COLOR_YELLOW2, "Tienes ambas manos ocupadas y no puedes agarrar el item.");
            
            SetHandItemAndParam(playerid, freehand, BlackMarket_Items[bm_item], cant);
            GivePlayerCash(playerid, -price);
            SendFMessage(playerid, COLOR_WHITE, "Vendedor: Ac� tenes tu %s, son $%i.", ItemModel_GetName(BlackMarket_Items[bm_item]), price);
        }
    }
    else 
    {
        BlackMarket_Item[playerid] = -1;
        BlackMarket_ShowBuyDialog(playerid);
    }

    return 1;
}