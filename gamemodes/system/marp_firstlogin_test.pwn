#if defined _marp_firstlogin_test_included
	#endinput
#endif
#define _marp_firstlogin_test_included 

new cAnswers[MAX_PLAYERS];

Dialog:DLG_TUT(playerid, response, listitem)
{
    cAnswers[playerid]=0;
    if(!response)
        return KickPlayer(playerid, "el sistema", "evadir test");
    
    new str[229+1];

    format(str, sizeof(str), "%sA) Sales corriendo intentando esquivar al personal policial.\nB) Sacas un arma de fuego y comenz�s a dispararles.\nC) Roleas las heridas producidas por el impacto y segu�s las ordenes de los oficiales.\nD) A y B.", str);
    Dialog_Show(playerid, DLG_TEST1, DIALOG_STYLE_LIST, "Escapas de la polic�a luego de robar, chocas y te rodean �Qu� debes hacer?", str, "Siguiente", "");
    return 1;
    
}

Dialog:DLG_TEST1(playerid, response, listitem)
{
    if(!response)
        return KickPlayer(playerid, "el sistema", "evadir test");
    if(listitem==2)
        cAnswers[playerid]++;
    new str[239+1];

    format(str, sizeof(str), "%sA) Saco mi arma y les disparo. \nB) Continuo el rol y les entrego mis pertenencias.\nC) Llamo a mis amigos por alg�n medio externo al juego para que me salven.\nD) Me niego a continuar el rol ya que no me parece correcto que me roben.", str);
    Dialog_Show(playerid, DLG_TEST2, DIALOG_STYLE_LIST, "Est�s caminando por una Villa, de repente 4 ladrones te apuntan con un arma.", str, "Siguiente", "");
    return 1;
}

Dialog:DLG_TEST2(playerid, response, listitem)
{
    if(!response)
        return KickPlayer(playerid, "el sistema", "evadir test");
    if(listitem==1)
        cAnswers[playerid]++;
    new str[336+1];

    format(str, sizeof(str), "%s\nA) Estoy en medio de un tiroteo y me queda menos del 20% de vid", str);
    format(str, sizeof(str), "%sa, sigo disparando hasta que ellos mueran o yo quede en crack.\nB) Choco contra otro veh�culo a m�s de 120 KM/H,  me voy como si nada hubiese pasado.\nC) Todas son correctas", str);

    Dialog_Show(playerid, DLG_TEST3, DIALOG_STYLE_LIST, "�Cual de los siguientes ejemplos NO ser�a considerado NRH (No Rol de Heridas)?", str, "Siguiente", "");
    return 1;
}

Dialog:DLG_TEST3(playerid, response, listitem)
{
    if(!response)
        return KickPlayer(playerid, "el sistema", "evadir test");
    if(listitem==0)
        cAnswers[playerid]++;
    new str[238+1];

    format(str, sizeof(str), "%sA) Estar con un arma en mano dentro de una comisaria. \nB) Escapar de un robo a un negocio durante la noche sin llamar a la polic�a.\nC) Conducir mi veh�culo por la vereda, ignorando se�ales de tr�nsito y los NPC.\nD) Todas son correctas.", str);
    Dialog_Show(playerid, DLG_TEST4, DIALOG_STYLE_LIST, "�Qu� ejemplo ser�a correcto para la definici�n de NRE?", str, "Siguiente", "");
    return 1;
}

Dialog:DLG_TEST4(playerid, response, listitem)
{
    if(!response)
        return KickPlayer(playerid, "el sistema", "evadir test");
    if(listitem==3)
        cAnswers[playerid]++;
    new str[238+1];

    format(str, sizeof(str), "%sA) Hacer algo que no podr�a hacer en la vida real.\nB) Realizar algo que no concuerda con la historia y caracter�stica de mi PJ.\nC) Aprovecharse de un jugador nuevo.\nD) Todas son correctas.", str);
    Dialog_Show(playerid, DLG_TEST5, DIALOG_STYLE_LIST, "�Qu� se entiende por NIP (Nula Interpretaci�n del Personaje)?", str, "Siguiente", "");
    return 1;
}

Dialog:DLG_TEST5(playerid, response, listitem, inputtext[])
{
    if(!response)
        return KickPlayer(playerid, "el sistema", "evadir test");
    
    if(listitem==1)
        cAnswers[playerid]++;
    
    if(cAnswers[playerid]>=4)
    {
        SendClientMessage(playerid, COLOR_GREEN, "�Felicidades!, completaste el test de rol, ahora deber�s registrar tu contrase�a y crear tu personaje.");
        Dialog_Show(playerid, DLG_REGISTER, DIALOG_STYLE_PASSWORD, "�Bienvenido a Malos Aires!", "Para registrarte, por favor ingresa una contrase�a:", "Registrarse", "");
    }
    else{
        SendFMessage(playerid, COLOR_ERROR, "Has tenido %d/5 preguntas correctas, necesitas al menos 4 para pasar el test. Deber�s realizarlo nuevamente", cAnswers[playerid]);
        AccountRegister(playerid);
    }
    return 1;
}

Dialog:DLG_REGISTER(playerid, response, listitem, inputtext[])
{
    if(!response)
        return KickPlayer(playerid, "el sistema", "evadir registro");

    new query[256];
    strcat(query, inputtext, sizeof(query));    
    mysql_escape_string(query, query, sizeof(query), MYSQL_HANDLE);
    mysql_format(MYSQL_HANDLE, query, sizeof(query), "INSERT INTO accounts (Name, Password) VALUES ('%s', MD5('%s'))", PlayerInfo[playerid][pName], query);	
    mysql_tquery(MYSQL_HANDLE, query);
    StartAccountFirstLogin(playerid);

    return 1;
}