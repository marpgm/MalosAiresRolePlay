#if defined _marp_logo_inc
	#endinput
#endif
#define _marp_logo_inc

#include <YSI_Coding\y_hooks>

static Text:Logo_TD[6];

hook OnGameModeInitEnded()
{
	Logo_TD[0] = TextDrawCreate(568.000000, 0.000000, "Ma");
	TextDrawFont(Logo_TD[0], 0);
	TextDrawLetterSize(Logo_TD[0], 0.433333, 1.700000);
	TextDrawTextSize(Logo_TD[0], 400.000000, 17.000000);
	TextDrawSetOutline(Logo_TD[0], 1);
	TextDrawSetShadow(Logo_TD[0], 2);
	TextDrawAlignment(Logo_TD[0], 3);
	TextDrawColor(Logo_TD[0], 1097458175);
	TextDrawBackgroundColor(Logo_TD[0], 255);
	TextDrawBoxColor(Logo_TD[0], 50);
	TextDrawUseBox(Logo_TD[0], 0);
	TextDrawSetProportional(Logo_TD[0], 1);
	TextDrawSetSelectable(Logo_TD[0], 0);

	Logo_TD[1] = TextDrawCreate(613.000000, 15.000000, "Roleplay");
	TextDrawFont(Logo_TD[1], 0);
	TextDrawLetterSize(Logo_TD[1], 0.341666, 1.250000);
	TextDrawTextSize(Logo_TD[1], 400.000000, 17.000000);
	TextDrawSetOutline(Logo_TD[1], 1);
	TextDrawSetShadow(Logo_TD[1], 2);
	TextDrawAlignment(Logo_TD[1], 3);
	TextDrawColor(Logo_TD[1], -2686721);
	TextDrawBackgroundColor(Logo_TD[1], 255);
	TextDrawBoxColor(Logo_TD[1], 50);
	TextDrawUseBox(Logo_TD[1], 0);
	TextDrawSetProportional(Logo_TD[1], 1);
	TextDrawSetSelectable(Logo_TD[1], 0);

	Logo_TD[2] = TextDrawCreate(582.000000, 0.000000, "los");
	TextDrawFont(Logo_TD[2], 0);
	TextDrawLetterSize(Logo_TD[2], 0.433333, 1.700000);
	TextDrawTextSize(Logo_TD[2], 400.000000, 17.000000);
	TextDrawSetOutline(Logo_TD[2], 1);
	TextDrawSetShadow(Logo_TD[2], 2);
	TextDrawAlignment(Logo_TD[2], 3);
	TextDrawColor(Logo_TD[2], -1);
	TextDrawBackgroundColor(Logo_TD[2], 255);
	TextDrawBoxColor(Logo_TD[2], 50);
	TextDrawUseBox(Logo_TD[2], 0);
	TextDrawSetProportional(Logo_TD[2], 1);
	TextDrawSetSelectable(Logo_TD[2], 0);

	Logo_TD[3] = TextDrawCreate(608.000000, 0.000000, "Air");
	TextDrawFont(Logo_TD[3], 0);
	TextDrawLetterSize(Logo_TD[3], 0.433333, 1.700000);
	TextDrawTextSize(Logo_TD[3], 400.000000, 17.000000);
	TextDrawSetOutline(Logo_TD[3], 1);
	TextDrawSetShadow(Logo_TD[3], 2);
	TextDrawAlignment(Logo_TD[3], 3);
	TextDrawColor(Logo_TD[3], -1);
	TextDrawBackgroundColor(Logo_TD[3], 255);
	TextDrawBoxColor(Logo_TD[3], 50);
	TextDrawUseBox(Logo_TD[3], 0);
	TextDrawSetProportional(Logo_TD[3], 1);
	TextDrawSetSelectable(Logo_TD[3], 0);

	Logo_TD[4] = TextDrawCreate(619.000000, 0.000000, "es");
	TextDrawFont(Logo_TD[4], 0);
	TextDrawLetterSize(Logo_TD[4], 0.433333, 1.700000);
	TextDrawTextSize(Logo_TD[4], 400.000000, 17.000000);
	TextDrawSetOutline(Logo_TD[4], 1);
	TextDrawSetShadow(Logo_TD[4], 2);
	TextDrawAlignment(Logo_TD[4], 3);
	TextDrawColor(Logo_TD[4], 1097458175);
	TextDrawBackgroundColor(Logo_TD[4], 255);
	TextDrawBoxColor(Logo_TD[4], 50);
	TextDrawUseBox(Logo_TD[4], 0);
	TextDrawSetProportional(Logo_TD[4], 1);
	TextDrawSetSelectable(Logo_TD[4], 0);

	Logo_TD[5] = TextDrawCreate(14.000000, 433.000000, "PCU.MALOSAIRES.COM.AR");
	TextDrawFont(Logo_TD[5], 2);
	TextDrawLetterSize(Logo_TD[5], 0.183333, 1.000000);
	TextDrawTextSize(Logo_TD[5], 400.000000, 17.000000);
	TextDrawSetOutline(Logo_TD[5], 0);
	TextDrawSetShadow(Logo_TD[5], 0);
	TextDrawAlignment(Logo_TD[5], 1);
	TextDrawColor(Logo_TD[5], -1);
	TextDrawBackgroundColor(Logo_TD[5], 255);
	TextDrawBoxColor(Logo_TD[5], 50);
	TextDrawUseBox(Logo_TD[5], 0);
	TextDrawSetProportional(Logo_TD[5], 1);
	TextDrawSetSelectable(Logo_TD[5], 0);
	return 1;
}

hook LoginCamera_OnEnd(playerid)
{
	Logo_ShowForPlayer(playerid);
	return 1;
}

Logo_ShowForPlayer(playerid)
{
	TextDrawShowForPlayer(playerid, Logo_TD[0]);
	TextDrawShowForPlayer(playerid, Logo_TD[1]);
	TextDrawShowForPlayer(playerid, Logo_TD[2]);
	TextDrawShowForPlayer(playerid, Logo_TD[3]);
	TextDrawShowForPlayer(playerid, Logo_TD[4]);
	TextDrawShowForPlayer(playerid, Logo_TD[5]);
}

Logo_HideForPlayer(playerid)
{
	TextDrawHideForPlayer(playerid, Logo_TD[0]);
	TextDrawHideForPlayer(playerid, Logo_TD[1]);
	TextDrawHideForPlayer(playerid, Logo_TD[2]);
	TextDrawHideForPlayer(playerid, Logo_TD[3]);
	TextDrawHideForPlayer(playerid, Logo_TD[4]);
	TextDrawHideForPlayer(playerid, Logo_TD[5]);
}