#if defined _marp_cellphone_included
	#endinput
#endif
#define _marp_cellphone_included

#include <YSI_Coding\y_hooks>

OnSMSSent(PHONE_HANDLE:phone, to_number, const message[])
{
	new fromPlayerid = Phone_GetPlayerid(phone);

	if(fromPlayerid != INVALID_PLAYER_ID)
	{
		new str[180];
		format(str, sizeof(str), "[SMS] %s (ID %i) a n�mero %i: %s", GetPlayerCleanName(fromPlayerid), fromPlayerid, to_number, message);
		foreach(new i : Player)
		{
			if(AdminSMSEnabled[i]) {
				SendClientMessage(i, COLOR_ADMINREAD, str);
			}
		}
	}
	return 1;
}

CMD:llamar(playerid, params[])
{
	new number;

	if(GetHandItem(playerid, HAND_RIGHT) != ITEM_ID_TELEFONO_CELULAR && GetHandItem(playerid, HAND_LEFT) != ITEM_ID_TELEFONO_CELULAR)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �No tienes un tel�fono celular en tu mano!");
    if(sscanf(params, "i", number))
		return SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "{5CCAF1}[Sintaxis]{C8C8C8} /llamar [n�mero de tel�fono]");

	Phone_PlayerCallNumber(playerid, number);
	return 1;
}

CMD:tel(playerid, params[]) {
	return cmd_telefono(playerid, params);
}

CMD:telefono(playerid, params[])
{
	if(!PlayerInfo[playerid][pPhoneNumber])
		return SendClientMessage(playerid, COLOR_YELLOW2, " �No tienes un tel�fono celular! consigue uno en un 24/7.");

	new hand = SearchHandsForItem(playerid, ITEM_ID_TELEFONO_CELULAR);
	if(hand != -1 )
	{
		if(Phone_IsOnCall(Phone_id[playerid])) {
			return SendClientMessage(playerid, COLOR_YELLOW2, "est�s en una llamada en curso, cuelga primero con /colgar.");
		}
		PlayerActionMessage(playerid, 15.0, "guarda su tel�fono celular en el bolsillo.");
		SetHandItemAndParam(playerid, hand, 0, 0);
		PhoneGUI_Close(playerid);
		return 1;
	}

	hand = SearchFreeHand(playerid);
	if(hand == -1)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Tienes ambas manos ocupadas!");

	PlayerActionMessage(playerid, 15.0, "toma su tel�fono celular del bolsillo.");
	SendClientMessage(playerid, -1, "Usa /tel para guardarlo y /t para hablar en una llamada. Usa ESC o click en (X) para poder moverte. Con 'N' reestableces el foco.");
	SetHandItemAndParam(playerid, hand, ITEM_ID_TELEFONO_CELULAR, 1);
	PhoneGUI_Open(playerid);
	return 1;
}

Dialog:DLG_CALL_911(playerid, response, listitem, inputtext[])
{
	if(!response)
		return 1;

	switch(listitem)
	{
		case 0:
		{
			new string[80];
			format(string, sizeof(string), "[Al tel�fono] %s dice: hola, con la polic�a por favor.", GetPlayerChatName(playerid));
			SendPlayerMessageInRange(15.0, playerid, string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5);
			Dialog_Show(playerid, DLG_CALL_911_POLICE, DIALOG_STYLE_INPUT, "[911] Polic�a Federal", "Operadora: Polic�a Federal, por favor de un breve informe de lo ocurrido.", "Continuar", "Cerrar"); 
		}
		case 1:
		{
			new string[80];
			format(string, sizeof(string), "[Al tel�fono] %s dice: hola, con emergencias m�dicas por favor.", GetPlayerChatName(playerid));
			SendPlayerMessageInRange(15.0, playerid, string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5);
			Dialog_Show(playerid, DLG_CALL_911_PARAMEDIC, DIALOG_STYLE_INPUT, "[911] Servicios m�dicos de Emergencia", "Operadora: departamento de emergencias m�dicas, por favor de un breve informe de lo ocurrido.", "Continuar", "Cerrar");
		}
	}
	return 1;
}

Dialog:DLG_CALL_911_POLICE(playerid, response, listitem, inputtext[])
{
	if(!response || isnull(inputtext))
		return 1;

	new string[180];
	format(string, sizeof(string), "[Al tel�fono] %s dice: %s", GetPlayerChatName(playerid), inputtext);
	SendPlayerMessageInRange(15.0, playerid, string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5);

	Dialog_Show(playerid, DLG_CALL_911_END, DIALOG_STYLE_MSGBOX, "[911] polic�a Metropolitana", "Operadora dice: gracias, hemos alertado a todas las unidades, mantenga la calma y espere en el lugar.", "Cerrar", "");
	
	format(string, sizeof(string), "[Llamada al 911 del %i] %s", PlayerInfo[playerid][pPhoneNumber], inputtext);
	SendFactionRadioMessage(FAC_PMA, COLOR_PMA, string);

	format(string, sizeof(string), "[911 - POLIC�A del ID %i] %s", playerid, inputtext);
	foreach(new i : Player) {
		if(Admin911Enabled[i] && i != playerid && PlayerInfo[i][pFaction] != FAC_PMA) {
			SendClientMessage(i, COLOR_ADMINREAD, string);
		}
	}

	lastPoliceCallNumber = PlayerInfo[playerid][pPhoneNumber];
	PlayerPos_GetExteriorPos(playerid, lastPoliceCallPos[0], lastPoliceCallPos[1], lastPoliceCallPos[2]);
	return 1;
}

Dialog:DLG_CALL_911_PARAMEDIC(playerid, response, listitem, inputtext[])
{
	if(!response || isnull(inputtext))
		return 1;

	new string[180];
	format(string, sizeof(string), "[Al tel�fono] %s dice: %s", GetPlayerChatName(playerid), inputtext);
	SendPlayerMessageInRange(15.0, playerid, string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5);

	Dialog_Show(playerid, DLG_CALL_911_END, DIALOG_STYLE_MSGBOX, "[911] Servicios m�dicos de Emergencia", "Operadora dice: gracias, hemos alertado a todas las unidades, mantenga la calma y espere en el lugar.", "Cerrar", ""); 
	
	format(string, sizeof(string), "[Llamada al 911 del %i] %s", PlayerInfo[playerid][pPhoneNumber], inputtext);
	SendFactionRadioMessage(FAC_HOSP, COLOR_PMA, string);

	format(string, sizeof(string), "[911 - SAME del ID %i] %s", playerid, inputtext);
	foreach(new i : Player) {
		if(Admin911Enabled[i] && i != playerid && PlayerInfo[i][pFaction] != FAC_HOSP) {
			SendClientMessage(i, COLOR_ADMINREAD, string);
		}
	}

	lastMedicCallNumber = PlayerInfo[playerid][pPhoneNumber];
	PlayerPos_GetExteriorPos(playerid, lastMedicCallPos[0], lastMedicCallPos[1], lastMedicCallPos[2]);
	return 1;
}

CMD:atender(playerid, params[])
{
	if(GetHandItem(playerid, HAND_RIGHT) != ITEM_ID_TELEFONO_CELULAR && GetHandItem(playerid, HAND_LEFT) != ITEM_ID_TELEFONO_CELULAR)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �No tienes un tel�fono celular en tu mano!");

	if(Phone_IsOnCall(Phone_id[playerid])) {
		PhoneCall_Answer(Phone_GetCurrentCall(Phone_id[playerid]), Phone_id[playerid]);
	}
	return 1;
}

CMD:t(playerid, params[])
{
	if(GetHandItem(playerid, HAND_RIGHT) != ITEM_ID_TELEFONO_CELULAR && GetHandItem(playerid, HAND_LEFT) != ITEM_ID_TELEFONO_CELULAR)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �No tienes un tel�fono celular en tu mano!");

	if(Phone_IsOnCall(Phone_id[playerid])) {
		new string[180];
		format(string, sizeof(string), "[Al tel�fono] %s dice: %s", GetPlayerChatName(playerid), params);
		SendPlayerMessageInRange(15.0, playerid, string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5);
		PhoneCall_NewMessage(Phone_GetCurrentCall(Phone_id[playerid]), Phone_id[playerid], params);
	}
	return 1;
}

CMD:colgar(playerid, params[])
{
	if(GetHandItem(playerid, HAND_RIGHT) != ITEM_ID_TELEFONO_CELULAR && GetHandItem(playerid, HAND_LEFT) != ITEM_ID_TELEFONO_CELULAR)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �No tienes un tel�fono celular en tu mano!");

	if(Phone_IsOnCall(Phone_id[playerid])) {
		PhoneCall_Hang(Phone_GetCurrentCall(Phone_id[playerid]), Phone_id[playerid]);
	}
	return 1;
}