#if defined _marp_garages_admin_inc
	#endinput
#endif
#define _marp_garages_admin_inc

CMD:agaraje(playerid, params[]) {
    return cmd_ag(playerid, params);
}

CMD:ag(playerid, params[]) 
{
    SendClientMessage(playerid, COLOR_WHITE, "_____________________________________[ ADMINISTRACION DE GARAJES ]_____________________________________");
	SendClientMessage(playerid, COLOR_USAGE, "[COMANDOS] "COLOR_EMB_GREY" /aggetid - /agcrear - /agborrar - /agareaexterior - /agareainterior - /agpuntoexterior - /agpuntointerior");
	SendClientMessage(playerid, COLOR_USAGE, "[COMANDOS] "COLOR_EMB_GREY" /agtipo - /agextraid - /aginfo - /agcerrado - /agtele");
	SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Seteando posiciones deber�s estar mirando hacia la puerta para que el �ngulo de salida sea el correcto.");
	SendClientMessage(playerid, COLOR_WHITE, "________________________________________________________________________________________________________");
    return 1;
}

CMD:aggetid(playerid, params[])
{
    new garageid = Garage_IsInAnyArea(playerid);

    if(garageid) {
		SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" ID del garaje actual: %i.", garageid);
	} else {
		SendClientMessage(playerid, COLOR_WHITE, "No se ha encontrado ning�n edificio en tu posici�n.");
	}
    return 1;
}

CMD:agcrear(playerid, params[])
{
    new mapid, type, extraid, locked, Float:outsideX, Float:outsideY, Float:outsideZ, Float:outsideAng, outsideVW, outsideInt;
    if(sscanf(params, "iiii", mapid, type, extraid, locked))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /agcrear [mapeo] [tipo] [id extra] [cerrado]");
    if(locked != 0 && locked != 1)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" El valor 'cerrado' no puede ser diferente a 1 o 0.");
    if(!Garage_IsValidType(type)) {
        SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" El valor 'tipo' invalido.");
        return 1;
    }
    
    GetPlayerPos(playerid, outsideX, outsideY, outsideZ);
	GetPlayerFacingAngle(playerid, outsideAng);
	outsideVW = GetPlayerVirtualWorld(playerid);
	outsideInt = GetPlayerInterior(playerid);

    new garageid = Garage_Create(type, extraid, outsideX, outsideY, outsideZ, outsideAng + 180, outsideVW, outsideInt, mapid, locked);
    if(!garageid)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Imposible  crear el garaje. Se alcanz� el m�ximo de garajes o ocurri� alg�n problema.");
	
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Garaje ID: %i creado correctamente.", garageid);
    return 1;
}

CMD:agborrar(playerid, params[])
{
	new garageid;

	if(sscanf(params, "i", garageid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /agborrar [ID garaje]");
	if(!Garage_Delete(garageid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de garaje invalida.");
	
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Edificio ID: %i garaje correctamente.", garageid);
	return 1;
}

CMD:agareaexterior(playerid, params[])
{
    new garageid;
    if(sscanf(params, "i", garageid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /agareaexterior [ID garaje]");
    if(!Garage_IsValidId(garageid))
        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Id de garaje invalida.");

    new Float:areaX, Float:areaY, Float:areaZ;
    GetPlayerPos(playerid, areaX, areaY, areaZ);
    Garage_SetOutsideArea(garageid, areaX, areaY, areaZ);

    new str[128];
    Garage_GetOutsideAreaInfo(garageid, str, sizeof(str));
    SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Area de entrada de garage Id: %i seteada en: %s", garageid, str);
    return 1;
}

CMD:agareainterior(playerid, params[])
{
    new garageid;
    if(sscanf(params, "i", garageid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /agareainterior [ID garaje]");
    if(!Garage_IsValidId(garageid))
        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Id de garaje invalida.");

    new Float:areaX, Float:areaY, Float:areaZ;
    GetPlayerPos(playerid, areaX, areaY, areaZ);
    Garage_SetInsideArea(garageid, areaX, areaY, areaZ);

    new str[128];
    Garage_GetInsideAreaInfo(garageid, str, sizeof(str));
    SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Area de salida de garage Id: %i seteada en: %s", garageid, str);
    return 1;
}

CMD:agpuntoexterior(playerid, params[])
{
    new garageid;
    if(sscanf(params, "i", garageid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /agpuntoexterior [ID garaje]");
    if(!Garage_IsValidId(garageid))
        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Id de garaje invalida.");

    new Float:x, Float:y, Float:z, Float:angle, int, world;
    GetPlayerPos(playerid, x, y, z);
    GetPlayerFacingAngle(playerid, angle);
	world = GetPlayerVirtualWorld(playerid);
	int = GetPlayerInterior(playerid);

    Garage_SetOutsidePoint(garageid, x, y, z, angle + 180, world, int);
    Garage_SetOutsideArea(garageid, x, y, z);

    new str[128];
    Garage_GetOutsideAreaInfo(garageid, str, sizeof(str));
    SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Area de entrada y punto de salida de garage Id: %i seteado en: %s", garageid, str);
    return 1;
}

CMD:agpuntointerior(playerid, params[])
{
    new garageid;
    if(sscanf(params, "i", garageid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /agpuntointerior [ID garaje]");
    if(!Garage_IsValidId(garageid))
        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Id de garaje invalida.");

    new Float:x, Float:y, Float:z, Float:angle, int, world;
    GetPlayerPos(playerid, x, y, z);
    GetPlayerFacingAngle(playerid, angle);
	world = GetPlayerVirtualWorld(playerid);
	int = GetPlayerInterior(playerid);

    Garage_SetInsidePoint(garageid, x, y, z, angle + 180, world, int);
    Garage_SetInsideArea(garageid, x, y, z);

    new str[128];
    Garage_GetOutsideAreaInfo(garageid, str, sizeof(str));
    SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Area de salida y punto de entrada de garage Id: %i seteado en: %s", garageid, str);
    return 1;
}

CMD:agtipo(playerid, params[])
{
    new garageid, type;
    if(sscanf(params, "ii", garageid, type))
    {
		SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /agtipo [ID garaje] [tipo]");
        return Garage_PrintTypesFor(playerid);
    }

    if(!Garage_SetType(garageid, type))
        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Id de garaje inv�lida.");

    SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Configurado tipo: %s al garaje ID: %i", Garage_GetTypeName(type), garageid);
    return 1;
}

CMD:agextraid(playerid, params[])
{
    new garageid, extraid;
    if(sscanf(params, "ii", garageid, extraid))
    {
		SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /agextraid [ID garaje] [extraid]");
        return SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El par�metro 'extraid' debe ser una id en 'tipo'. Para garaje libre, colocar 'tipo = faccion y extraid = 0'");
    }

    switch(GarageInfo[garageid][gType])
    {
        case GARAGE_TYPE_HOUSE: {if(!House_IsValidId(extraid)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Id de casa inv�lida.");}
        case GARAGE_TYPE_BUSINESS: {if(!Biz_IsValidId(extraid)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Id de negocio inv�lida.");}
        case GARAGE_TYPE_FACTION: {if(!(0 <= extraid <= Faction_GetMaxAmount())) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Id de facci�n inv�lida.");}
        default: return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Tipo de garaje inv�lido. Reportar a un scripter.");
    }

    if(!Garage_SetExtraId(garageid, extraid))
        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Id de garaje inv�lida.");

    SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Configurado extraid: %i al garaje ID: %i", extraid, garageid);
    return 1;
}

CMD:aginfo(playerid, params[])
{
    new garageid;
    if(sscanf(params, "i", garageid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /aginfo [ID garaje]");
    if(!Garage_PrintInfoForPlayer(playerid, garageid))
        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Id de garaje inv�lida.");

    return 1;
}

CMD:agcerrado(playerid, params[])
{
    new garageid, locked;
    if(sscanf(params, "ii", garageid, locked))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /agcerrado [ID garaje] [cerrado]");
    if(locked != 0 && locked != 1)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" El valor 'cerrado' no puede ser diferente a 1 o 0.");
    if(!Garage_SetLocked(garageid, locked))
        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Id de garaje inv�lida.");

    SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Has %s"COLOR_EMB_GREY" el garaje ID %i.", (!locked) ? ("{33FF33}abierto") : ("{FF3333}cerrado"), garageid);
    return 1;
}

CMD:agtele(playerid, params[])
{
    new garageid;
    if(sscanf(params, "i", garageid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /agtele [ID garaje]");
    if(!Garage_IsValidId(garageid))
        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Id de garaje inv�lida.");

    Garage_TpPlayerToOutsideId(playerid, garageid);    
    return 1;
}