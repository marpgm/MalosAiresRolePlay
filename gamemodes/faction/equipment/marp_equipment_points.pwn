#if defined _marp_equipment_points_included
	#endinput
#endif
#define _marp_equipment_points_included

const EQUIP_POINTS_MAX_AMOUNT = 16;
const INVALID_EQUIP_POINT_ID = -1;

static EQUIP_USE_ADDR;
static EQUIP_ON_ENTER_ADDR;
static EQUIP_ON_LEAVE_ADDR;

static enum e_EQUIP_POINT_DATA
{
    epFaction,
    STREAMER_TAG_AREA:epPickup,
    Button:epButton
}

static EquipmentPointData[EQUIP_POINTS_MAX_AMOUNT + 1][e_EQUIP_POINT_DATA] = {
    {
        /*epFaction*/ 0,
        /*STREAMER_TAG_AREA:epPickup*/ INVALID_STREAMER_ID,
        /*STREAMER_TAG_AREA:epButton*/ INVALID_BUTTON_ID
    }, ...
};

static Iterator:EquipmentPointData<EQUIP_POINTS_MAX_AMOUNT>;

static EquipmentPointPlayerData[MAX_PLAYERS] = {INVALID_EQUIP_POINT_ID, ...};

hook OnGameModeInit()
{
    EQUIP_USE_ADDR = GetPublicAddressFromName("EquipmentPnt_OnUse");
    EQUIP_ON_ENTER_ADDR = GetPublicAddressFromName("EquipmentPnt_OnPlayerEnter");
    EQUIP_ON_LEAVE_ADDR = GetPublicAddressFromName("EquipmentPnt_OnPlayerLeave");
	return 1;
}

EquipmentPoint_Create(Float:x, Float:y, Float:z, Float:r, vworld, factionid = 0, modelid = 1239)
{
    new equipid = Iter_Free(EquipmentPointData);

    if(equipid == ITER_NONE)
    {
		printf("[ERROR] Alcanzado EQUIP_POINTS_MAX_AMOUNT (%i). Iter_Count: %i.", EQUIP_POINTS_MAX_AMOUNT, Iter_Count(EquipmentPointData));
		return INVALID_EQUIP_POINT_ID;
	}

    EquipmentPointData[equipid][epPickup] = CreateDynamicPickup(modelid, 1, x, y, z, .worldid = vworld);
    EquipmentPointData[equipid][epButton] = Button_Create(equipid, x, y, z + 1, .size = r, .worldid = vworld, .streamDistance = 4.0, .labelText = "Casillero", .onPressCallbackAddress = EQUIP_USE_ADDR, .onEnterCallbackAddress = EQUIP_ON_ENTER_ADDR, .onExitCallbackAddress = EQUIP_ON_LEAVE_ADDR);
    EquipmentPointData[equipid][epFaction] = factionid;
    
    Iter_Add(EquipmentPointData, equipid);
    return equipid;
}

stock EquipmentPoint_Destroy(equipid)
{
    if(!Iter_Contains(EquipmentPointData, equipid))
		return 0;

    if(EquipmentPointData[equipid][epButton] != INVALID_BUTTON_ID) {
		Button_Destroy(EquipmentPointData[equipid][epButton]);
	}
    
    if(EquipmentPointData[equipid][epPickup]) {
        DestroyDynamicPickup(EquipmentPointData[equipid][epPickup]);
    }

    EquipmentPointData[equipid] = EquipmentPointData[EQUIP_POINTS_MAX_AMOUNT];
	Iter_Remove(EquipmentPointData, equipid);
    return 1;
}

forward EquipmentPnt_OnUse(playerid, equipid);
public EquipmentPnt_OnUse(playerid, equipid)
{
	if(!Faction_IsValidId(PlayerInfo[playerid][pFaction]))
        return 1;
	if(!Faction_HasTag(PlayerInfo[playerid][pFaction], FAC_TAG_EQUIPMENT))
		return SendClientMessage(playerid, COLOR_YELLOW2, "Tu faccion no tiene acceso a este comando.");
	if(!EquipmentPoint_CanPlayerUse(playerid, equipid))
		return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en un casillero de tu faccion.");
	if(GetHandItem(playerid, HAND_RIGHT))
        return SendClientMessage(playerid, COLOR_YELLOW2, "No debes tener nada en tu mano derecha.");

	new callbackaddr = Equipment_GetFactionCallback(PlayerInfo[playerid][pFaction]);

	if(callbackaddr == -1)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Tu faccion no tiene asociada una funcion de equipamiento. Contacta con un scripter.");
	
	CallFunction(callbackaddr, playerid);
	return 1;
}

forward EquipmentPnt_OnPlayerEnter(playerid, equipmentid);
public EquipmentPnt_OnPlayerEnter(playerid, equipmentid)
{
    if(EquipmentPointData[equipmentid][epFaction] && EquipmentPointData[equipmentid][epFaction] == PlayerInfo[playerid][pFaction])
    {
        Noti_Create(playerid, 2000, "Usa '/equipar' � la tecla (H)");
        EquipmentPointPlayerData[playerid] = equipmentid;
    }
}

forward EquipmentPnt_OnPlayerLeave(playerid, equipmentid);
public EquipmentPnt_OnPlayerLeave(playerid, equipmentid)
{
	if(equipmentid == EquipmentPointPlayerData[playerid]) {
		EquipmentPointPlayerData[playerid] = INVALID_EQUIP_POINT_ID;
	}
}

stock EquipmentPoint_IsPlayerAt(playerid) {
    return (EquipmentPointPlayerData[playerid] != INVALID_EQUIP_POINT_ID);
}

stock EquipmentPoint_CanPlayerUse(playerid, equipmentid) {
    return (EquipmentPointData[equipmentid][epFaction] && EquipmentPointData[equipmentid][epFaction] == PlayerInfo[playerid][pFaction]);
}

stock EquipmentPoint_GetId(playerid) {
    return EquipmentPointPlayerData[playerid];
}