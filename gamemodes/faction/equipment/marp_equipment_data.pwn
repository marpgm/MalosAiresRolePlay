#if defined _marp_equipment_data_included
	#endinput
#endif
#define _marp_equipment_data_included

static const Float:EQUIPMENT_PRICE_DISCOUNT = 0.4;

static Equipment_FactionCallbackAddr[MAX_FACTIONS] = {
    -1, ...
};

Equipment_SetFactionCallback(factionid, callback[])
{
    if(!isnull(callback) && strlen(callback) < MAX_FUNC_NAME) 
    {
        Equipment_FactionCallbackAddr[factionid] = GetPublicAddressFromName(callback);
        return 1;
	}
    return 0;
}

Equipment_GetFactionCallback(factionid) {
   return Equipment_FactionCallbackAddr[factionid];
}

static enum e_EQUIP_DATA
{
    equipItem,
    equipRank
}

static const Equipment_Police[][e_EQUIP_DATA] = {
    {
        /*equipItem*/ ITEM_ID_NITESTICK,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_RADIO,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_CAMARA,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_CHALECO_4,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_COLT_MAGAZINE,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_DEAGLE_MAGAZINE,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_TAZER_CHARGE,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_SHOTGUN_SHELLS,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_LESSLETHAL_SHELL,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_MP5_MAGAZINE,
        /*equipRank*/ 6
    },
    {
        /*equipItem*/ ITEM_ID_M4_MAGAZINE,
        /*equipRank*/ 6
    },
    {
        /*equipItem*/ ITEM_ID_RIFLE_BULLETS,
        /*equipRank*/ 6
    },
    {
        /*equipItem*/ ITEM_ID_TEARGAS,
        /*equipRank*/ 6
    },
    {
        /*equipItem*/ ITEM_ID_FLASHBANG,
        /*equipRank*/ 6
    },
    {
        /*equipItem*/ ITEM_ID_CASCO_SWAT1,
        /*equipRank*/ 6
    },
    {
        /*equipItem*/ ITEM_ID_MASCARA_GAS,
        /*equipRank*/ 6
    },
    {
        /*equipItem*/ ITEM_ID_BERETTA_PX4_STORM,
        /*equipRank*/ 3,

    },
    {
        /*equipItem*/ ITEM_ID_BALLESTER_MOLINA_45,
        /*equipRank*/ 3,
    },
    {
        /*equipItem*/ ITEM_ID_TASER,
        /*equipRank*/ 3,
    },
    {
        /*equipItem*/ ITEM_ID_MOSSBERG_500,
        /*equipRank*/ 3,
    },
    {
        /*equipItem*/ ITEM_ID_ESCOPETA_NO_LETAL,
        /*equipRank*/ 3,
    },
    {
        /*equipItem*/ ITEM_ID_MP5,
        /*equipRank*/ 3
    },
    {
        /*equipItem*/ ITEM_ID_M4,
        /*equipRank*/ 3
    },
    {
        /*equipItem*/ ITEM_ID_SNIPER,
        /*equipRank*/ 3
    }
};

static const Equipment_Fire[][e_EQUIP_DATA] = {
    {
        /*equipItem*/ ITEM_ID_RADIO,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_MEDIC_CASE,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_MASCARA_GAS,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_CASCO_BOMBERO1,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_CASCO_BOMBERO2,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_CHALECO_REFRACTARIO,
        /*equipRank*/ 9
    },
    {
        /*equipItem*/ ITEM_ID_SLEDGEHAMMER,
        /*equipRank*/ 3
    },
    {
        /*equipItem*/ ITEM_ID_CHAINSAW,
        /*equipRank*/ 3
    },
    {
        /*equipItem*/ ITEM_ID_FIREEXTINGUISHER,
        /*equipRank*/ 3
    }
};

forward Equipment_PoliceShow(playerid);
public Equipment_PoliceShow(playerid)
{
    if(!isPlayerCopOnDuty(playerid))
        return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en servicio para usar este comando.");
    
	new title[64] = "[Equipamiento policial]";
    new line[64], string[sizeof(Equipment_Police) * ITEM_MODEL_MAX_NAME_LEN] = "C�digo\tDescripci�n\tInsumos\n";
    new inputs, itemid;

    for(new i = 0; i < sizeof(Equipment_Police); i++) 
    {
        if(PlayerInfo[playerid][pRank] < Equipment_Police[i][equipRank])
        {
            itemid = Equipment_Police[i][equipItem];
            inputs = floatround(EQUIPMENT_PRICE_DISCOUNT * ItemModel_GetPrice(itemid) / ItemModel_GetPrice(ITEM_ID_MATERIALES), floatround_ceil);

            format(line, sizeof(line), "%i\t%s\t%i\n", itemid, ItemModel_GetName(itemid), inputs);
            strcat(string, line, sizeof(string));
        }
    }

    Dialog_Open(playerid, "DLG_EQUIPMENT", DIALOG_STYLE_TABLIST_HEADERS, title, string, "Seleccionar", "Cerrar");
    return 1;
}

forward Equipment_GNAShow(playerid);
public Equipment_GNAShow(playerid)
{
    if(!isPlayerSideOnDuty(playerid))
        return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en servicio para usar este comando.");
    
	new title[64] = "[Equipamiento Gendarmer�a]";
    new line[64], string[sizeof(Equipment_Police) * ITEM_MODEL_MAX_NAME_LEN] = "C�digo\tDescripci�n\tInsumos\n";
    new inputs, itemid;

    for(new i = 0; i < sizeof(Equipment_Police); i++) 
    {
        if(PlayerInfo[playerid][pRank] < Equipment_Police[i][equipRank])
        {
            itemid = Equipment_Police[i][equipItem];
            inputs = floatround(EQUIPMENT_PRICE_DISCOUNT * ItemModel_GetPrice(itemid) / ItemModel_GetPrice(ITEM_ID_MATERIALES), floatround_ceil);

            format(line, sizeof(line), "%i\t%s\t%i\n", itemid, ItemModel_GetName(itemid), inputs);
            strcat(string, line, sizeof(string));
        }
    }

    Dialog_Open(playerid, "DLG_EQUIPMENT", DIALOG_STYLE_TABLIST_HEADERS, title, string, "Seleccionar", "Cerrar");
    return 1;
}
forward Equipment_FireShow(playerid);
public Equipment_FireShow(playerid)
{
    if(!IsMedicOnDuty(playerid))
        return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en servicio para usar este comando.");
    
	new title[64] = "[Equipamiento m�dico y de bomberos]";
    new line[64], string[sizeof(Equipment_Fire) * ITEM_MODEL_MAX_NAME_LEN] = "C�digo\tDescripci�n\tInsumos\n";
    new inputs, itemid;

    for(new i = 0; i < sizeof(Equipment_Fire); i++) 
    {
        if(PlayerInfo[playerid][pRank] < Equipment_Fire[i][equipRank])
        {
            itemid = Equipment_Fire[i][equipItem];
            inputs = floatround(EQUIPMENT_PRICE_DISCOUNT * ItemModel_GetPrice(itemid) / ItemModel_GetPrice(ITEM_ID_MATERIALES), floatround_ceil);

            format(line, sizeof(line), "%i\t%s\t%i\n", itemid, ItemModel_GetName(itemid), inputs);
            strcat(string, line, sizeof(string));
        }
    }

    Dialog_Open(playerid, "DLG_EQUIPMENT", DIALOG_STYLE_TABLIST_HEADERS, title, string, "Seleccionar", "Cerrar");
    return 1;
}

Dialog:DLG_EQUIPMENT(playerid, response, listitem, inputtext[])
{
	if(!response)
		return 1;

    new itemid;

    if(sscanf(inputtext, "i", itemid) || !ItemModel_IsValidId(itemid))
		return 1;
    if(GetHandItem(playerid, HAND_RIGHT) != 0 )
        return SendClientMessage(playerid, COLOR_YELLOW2, "No debes tener nada en tu mano derecha.");
    
    new inputs = floatround(EQUIPMENT_PRICE_DISCOUNT * ItemModel_GetPrice(itemid) / ItemModel_GetPrice(ITEM_ID_MATERIALES), floatround_ceil);
    if(FactionInfo[PlayerInfo[playerid][pFaction]][fMaterials] < inputs)
        return SendClientMessage(playerid, COLOR_YELLOW2, "No hay insumos suficientes en el dep�sito.");

    SetHandItemAndParam(playerid, HAND_RIGHT, itemid, ItemModel_GetParamDefaultValue(itemid));
    Equipment_OnPlayerTake(playerid, inputs, ItemModel_GetName(itemid));
    PlayerActionMessage(playerid, 15.0, "toma su equipamiento de los casilleros.");
    return 1;
}
