#if defined _marp_police_included
	#endinput
#endif
#define _marp_police_included

#include <YSI_Coding\y_hooks>

#define POS_POLICE_ARREST_X		-2407.51
#define POS_POLICE_ARREST_Y		3224.98
#define POS_POLICE_ARREST_Z		2412.72 

#define POS_POLICE_ARREST2_X	1204.22 
#define POS_POLICE_ARREST2_Y	3214.12
#define POS_POLICE_ARREST2_Z	2412.72 

#define POS_POLICE_ARREST3_X	1199.8953
#define POS_POLICE_ARREST3_Y	2816.8643
#define POS_POLICE_ARREST3_Z	816.8492

#define POS_POLICE_FREEDOM_X	-2788.79
#define POS_POLICE_FREEDOM_Y	3220.57
#define POS_POLICE_FREEDOM_Z	2412.72

#define POS_POLICE_FREEDOM2_X	2337.62
#define POS_POLICE_FREEDOM2_Y	-1375.99
#define POS_POLICE_FREEDOM2_Z	24.00

#define POS_PRISON_FREEDOM_X	2690.64 
#define POS_PRISON_FREEDOM_Y	-2405.15
#define POS_PRISON_FREEDOM_Z	13.53

new //Licencia de armas
	wepLicOffer[MAX_PLAYERS],
	// Camaras
	bool:usingCamera[MAX_PLAYERS],
	// Multa
	TicketOffer[MAX_PLAYERS],
	TicketMoney[MAX_PLAYERS],
	// Revision
	ReviseOffer[MAX_PLAYERS],
	//Alcoholemia
	BlowingPipette[MAX_PLAYERS],
    OfferingPipette[MAX_PLAYERS],
    // Rastreo llamada
    lastPoliceCallNumber = 0,
	Float:lastPoliceCallPos[3],
	lastMedicCallNumber = 0,
	DOEM,
	Float:lastMedicCallPos[3];
	//Callsign
	new Text3D:PFA_Callsign_Text[MAX_VEHICLES];
	new PFA_Callsign[MAX_VEHICLES];

CMD:multar(playerid, params[])
{
    new reason[64], cost, targetid;

  	if(PlayerInfo[playerid][pFaction] != FAC_PMA || PlayerInfo[playerid][pRank] == 10)
	  	return 1;
	if(sscanf(params, "uds[64]", targetid, cost, reason))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /multar [ID/Jugador] [costo] [raz�n]");
	if(CopDuty[playerid] == 0)
	    return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en servicio como oficial de polic�a!");
	if(!IsPlayerInRangeOfPlayer(3.0, playerid, targetid) || targetid == playerid)
		return SendClientMessage(playerid, COLOR_YELLOW2, "Jugador inv�lido o se encuentra demasiado lejos.");
	if(cost > 50000 || cost < 1)
		return SendClientMessage(playerid, COLOR_YELLOW2, "�El costo no puede ser menor a $1 ni sobrepasar los $50.000!");

	SendFMessage(playerid, COLOR_YELLOW2, "Has multado a %s por $%d - raz�n: %s.", GetPlayerCleanName(targetid), cost, reason);
	SendFMessage(targetid, COLOR_YELLOW2, "%s te ha multado por $%d - raz�n: %s.", GetPlayerCleanName(playerid), cost, reason);
	SendClientMessage(targetid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" escribe /aceptar multa, para pagar.");
	TicketOffer[targetid] = playerid;
	TicketMoney[targetid] = cost;
  	return 1;
}

hook function OnPlayerCmdAccept(playerid, const subcmd[])
{
	if(!strcmp(subcmd, "multa", true))
	{
		if(TicketOffer[playerid] >= 999)
		    return SendClientMessage(playerid, COLOR_YELLOW2, " �No tienes ninguna multa!");
		if(!IsPlayerConnected(TicketOffer[playerid]))
			return SendClientMessage(playerid, COLOR_YELLOW2, "El oficial que te mult� se ha desconectado.");
		if(!IsPlayerInRangeOfPlayer(3.0, playerid, TicketOffer[playerid]))
			return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar cerca del oficial que te ha multado!");

		new string[128];
		format(string, sizeof(string), "%s ha aceptado tu multa. Espera a que elija una forma de pago.", GetPlayerCleanName(playerid));
		SendClientMessage(TicketOffer[playerid], COLOR_WHITE, string);
		format(string, sizeof(string), "Aceptaste la multa de %s por %d.\nSeleciona la forma de pago:", GetPlayerCleanName(TicketOffer[playerid]), TicketMoney[playerid]);
		Dialog_Show(playerid, DLG_POLICE_FINE, DIALOG_STYLE_MSGBOX, "Te han multado", string, "Efectivo", "Tarjeta");
		return 1;
	}

	if(!strcmp(subcmd, "revision", true))
	{
		new idToShow = ReviseOffer[playerid];
		
		if(idToShow == 999)
			return SendClientMessage(playerid, COLOR_YELLOW2, "Nadie te quiere revisar.");
		if(!IsPlayerConnected(idToShow))
		    return SendClientMessage(playerid, COLOR_YELLOW2, "El sujeto se ha desconectado.");
		if(!IsPlayerInRangeOfPlayer(2.0, idToShow, playerid))
			return SendClientMessage(playerid, COLOR_YELLOW2, "El sujeto no est� cerca tuyo.");
		
		PrintHandsForPlayer(playerid, idToShow);
		PrintInvForPlayer(playerid, idToShow);
		PrintToysForPlayer(playerid, idToShow);
		Back_PrintHandsForPlayer(playerid, idToShow);
		PlayerPlayerActionMessage(idToShow, playerid, 15.0, "ha revisado en busca de objetos a");
		ReviseOffer[playerid] = 999;
		return 1;
	}

	if(!strcmp(subcmd, "licencia", true))
	{
		new offer = wepLicOffer[playerid];
	
		if(offer == INVALID_PLAYER_ID)
			return SendClientMessage(playerid, COLOR_YELLOW2, "Nadie te ha ofrecido una licencia de armas.");
		if(!IsPlayerConnected(offer)) {
			wepLicOffer[playerid] = INVALID_PLAYER_ID;
			return SendClientMessage(playerid, COLOR_YELLOW2, "El jugador se ha desconectado.");
		}
		if(!IsPlayerInRangeOfPlayer(4.0, playerid, offer))
			return SendClientMessage(playerid, COLOR_YELLOW2, "Jugador inv�lido o se encuentra muy lejos.");
		if(GetPlayerCash(playerid) < PRICE_LIC_GUN) {
			SendFMessage(playerid, COLOR_YELLOW2, "No cuentas con el dinero suficiente ($%d).", PRICE_LIC_GUN);
			return 1;
		}

		GivePlayerCash(playerid, -PRICE_LIC_GUN);
		PlayerInfo[playerid][pWepLic] = 1;
		Faction_GiveMoney(FAC_GOB, PRICE_LIC_GUN);
		wepLicOffer[playerid] = INVALID_PLAYER_ID;
		SendClientMessage(playerid, COLOR_WHITE, " �Felicidades! has conseguido una licencia de armas. Ahora puedes comprar un arma en cualquier armer�a.");
		SendFMessage(offer, COLOR_WHITE, "Le has dado una licencia de armas a %s por %d. El dinero se recaudar� para el fondo de facci�n.", GetPlayerCleanName(playerid), PRICE_LIC_GUN);
		return 1;
	}

	if(!strcmp(subcmd, "pipeta", true))
	{
		if(BlowingPipette[playerid] == 0)
			return SendClientMessage(playerid,COLOR_YELLOW2, "ning�n oficial te est� ofreciendo una pipeta para soplar.");
		if(!IsPlayerInRangeOfPlayer(2.0, playerid, GetPVarInt(playerid, "OfertaPipeta")))
			return SendClientMessage(playerid,COLOR_YELLOW2, "est�s demasiado lejos del oficial que te ofreci� la pipeta.");
			
		BlowingPipette[playerid] = 0;
		PlayerCmeMessage(playerid, 15.0, 5000, "Toma la pipeta ofrecida por el oficial y comienza a soplarla");
		SetTimerEx("SoplandoPipeta", 6000, false, "i", playerid);
		TogglePlayerControllable(playerid, false);
		SetTimerEx("Unfreeze", 6000, false, "i", playerid);
		return 1;
	}

	return continue(playerid, subcmd);
}

Dialog:DLG_POLICE_FINE(playerid, response, listitem, inputtext[])
{
	new str[128];

    if(TicketOffer[playerid] >= 999)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �No tienes ninguna multa!");
	if(!IsPlayerConnected(TicketOffer[playerid]))
		return SendClientMessage(playerid, COLOR_YELLOW2, "El oficial que te mult� se ha desconectado.");

    if(response)
    {
		if(GetPlayerCash(playerid) < TicketMoney[playerid])
		{
			SendClientMessage(TicketOffer[playerid], COLOR_YELLOW2, "El sujeto decidi� pagar en efectivo, pero no tiene el dinero suficiente.");
			SendClientMessage(playerid, COLOR_YELLOW2, " �No tienes el dinero en efectivo suficiente!");
			return 1;
		}

		format(str, sizeof(str), "{878EE7}[INFO]{C8C8C8} %s ha pagado tu multa con efectivo - costo: $%d.", GetPlayerCleanName(playerid), TicketMoney[playerid]);
		GivePlayerCash(playerid, -TicketMoney[playerid]);
	}
	else
	{
		if(PlayerInfo[playerid][pBank] < TicketMoney[playerid])
		{
			SendClientMessage(TicketOffer[playerid], COLOR_YELLOW2, "El sujeto decidi� pagar por transferencia bancaria, pero no tiene dinero suficiente en su cuenta.");
			SendClientMessage(playerid, COLOR_YELLOW2, " �No tienes el dinero suficiente en tu cuenta bancaria!");
			return 1;
		}

		PlayerInfo[playerid][pBank] -= TicketMoney[playerid];
		format(str, sizeof(str), "{878EE7}[INFO]{C8C8C8} %s ha pagado tu multa por transferencia bancaria - costo: $%d.", GetPlayerCleanName(playerid), TicketMoney[playerid]);
	}

    SendClientMessage(TicketOffer[playerid], COLOR_LIGHTYELLOW2, str);
	format(str, sizeof(str), "{878EE7}[INFO]{C8C8C8} multa pagada - costo: $%d.", TicketMoney[playerid]);
	SendClientMessage(playerid, COLOR_WHITE, str);
	Faction_GiveMoney(FAC_GOB, TicketMoney[playerid]);
	TicketOffer[playerid] = 999;
	TicketMoney[playerid] = 0;
	return 1;
}

CMD:quitar(playerid, params[])
{
	new itemString[64],
		targetID;

  	if(PlayerInfo[playerid][pFaction] != FAC_SIDE && PlayerInfo[playerid][pFaction] != FAC_PMA)
	  	return 1;
	if(PlayerInfo[playerid][pRank] == 10 && PlayerInfo[playerid][pFaction] == FAC_PMA)
		return 1;
	if(sscanf(params, "us[64]", targetID, itemString))
	{
		SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /quitar [ID/Jugador] [�tem]");
		return SendClientMessage(playerid, COLOR_USAGE, "[�tems] "COLOR_EMB_GREY" licconducir, licvuelo, licarmas.");
  	}
	if(CopDuty[playerid] == 0 && SIDEDuty[playerid] == 0)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en servicio!");
	if(PlayerInfo[playerid][pFaction] == FAC_SIDE && PlayerInfo[playerid][pRank] > 7)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �No puedes revocar licencias con tu rango!");
	if(!IsPlayerInRangeOfPlayer(2.0, playerid, targetID))
		return SendClientMessage(playerid, COLOR_YELLOW2, " �El objetivo se encuentra demasiado lejos!");
	if(targetID == INVALID_PLAYER_ID)
 		return SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "{FF4600}[Error]{C8C8C8} ID de jugador incorrecta.");
	if(targetID == playerid)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �No te puedes revocar un �tem a t� mismo!");
		
	if(strcmp(itemString, "licconducir", true) == 0)
	{
		if(PlayerInfo[targetID][pCarLic] == 0)
  			return SendClientMessage(playerid, COLOR_YELLOW2, " �El sujeto no tiene una licencia de conducir!");
		PlayerPlayerActionMessage(playerid, targetID, 15.0, "le ha quitado la licencia de conducir a");
		PlayerInfo[targetID][pCarLic] = 0;
	}
	else if(strcmp(itemString, "licvuelo", true) == 0)
	{
 		if(PlayerInfo[targetID][pFlyLic] == 0)
   			return SendClientMessage(playerid, COLOR_YELLOW2, " �El sujeto no tiene una licencia de vuelo!");
		PlayerPlayerActionMessage(playerid, targetID, 10.0, "le ha quitado la licencia de vuelo a");
 		PlayerInfo[targetID][pFlyLic] = 0;
	}
	else if(strcmp(itemString, "licarmas", true) == 0)
	{
 		if(PlayerInfo[targetID][pWepLic] == 0)
    		return SendClientMessage(playerid, COLOR_YELLOW2, " �El sujeto no tiene una licencia de portaci�n de armas!");
		PlayerPlayerActionMessage(playerid, targetID, 10.0, "le ha quitado la licencia de armas a");
		PlayerInfo[targetID][pWepLic] = 0;
	}
	return 1;
}

CMD:revisar(playerid, params[])
{
	new targetID;
	
	if(PlayerInfo[playerid][pRank] == 10 && PlayerInfo[playerid][pFaction] == FAC_PMA)
		return 1;
	if(sscanf(params, "u", targetID))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /revisar [ID/Jugador]");
	if(!IsPlayerConnected(targetID) || targetID == INVALID_PLAYER_ID || targetID == playerid)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "Jugador inv�lido.");
	if(!IsPlayerInRangeOfPlayer(2.0, playerid, targetID))
		return SendClientMessage(playerid, COLOR_YELLOW2, " �El objetivo se encuentra demasiado lejos!");

	if(isPlayerCopOnDuty(playerid) || isPlayerSideOnDuty(playerid) ||
		PlayerInfo[targetID][pDisabled] == DISABLE_DYING ||
		PlayerInfo[targetID][pDisabled] == DISABLE_DEATHBED )
	{
		PrintHandsForPlayer(targetID, playerid);
		PrintInvForPlayer(targetID, playerid);
	  	PrintToysForPlayer(targetID, playerid);
	  	Back_PrintHandsForPlayer(targetID, playerid);
		SendFMessage(playerid, COLOR_WHITE, "Dinero en mano: $%i.", GetPlayerCash(targetID));
		PlayerPlayerActionMessage(playerid, targetID, 15.0, "ha revisado en busca de objetos a");
	}
	else
    {
        SendFMessage(playerid, COLOR_LIGHTBLUE, "Quieres revisar a %s en busca de objetos. Para evitar abusos, debes esperar su respuesta.", GetPlayerCleanName(targetID));
		SendFMessage(targetID, COLOR_LIGHTBLUE, "%s quiere revisarte en busca de objetos. Para evitar abusos, tienes que usar /aceptar revision.", GetPlayerCleanName(playerid));
		ReviseOffer[targetID] = playerid;
	}
	return 1;
}

CMD:buscados(playerid, params[])
{
    new count = 0;

	if(PlayerInfo[playerid][pFaction] != FAC_SIDE && PlayerInfo[playerid][pFaction] != FAC_PMA)
		return 1;
	if(PlayerInfo[playerid][pRank] == 10 && PlayerInfo[playerid][pFaction] == FAC_PMA)
		return 1;
	if(CopDuty[playerid] == 0 && SIDEDuty[playerid] == 0)
    	return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en servicio!");

	SendClientMessage(playerid, COLOR_LIGHTGREEN, "-----------[Sospechosos buscados]-----------");
    foreach(new i : Player)	{
	    if(PlayerInfo[i][pWantedLevel] >= 1) {
	        SendFMessage(playerid, COLOR_WHITE, "[BUSCADOS] %s (ID:%d) -  nivel de b�squeda: %d.", GetPlayerCleanName(i), i, PlayerInfo[i][pWantedLevel]);
			count++;
		}
	}
	if(count == 0)
		SendClientMessage(playerid,COLOR_WHITE,"{878EE7}[INFO]{C8C8C8} no hay criminales buscados on-line.");
	SendClientMessage(playerid, COLOR_LIGHTGREEN, "------------------------------------------------");
	return 1;
}

CMD:esposar(playerid, params[])
{
	new targetID;

 	if(PlayerInfo[playerid][pFaction] != FAC_SIDE && PlayerInfo[playerid][pFaction] != FAC_PMA)
 		return 1;
	if(PlayerInfo[playerid][pRank] == 10 && PlayerInfo[playerid][pFaction] == FAC_PMA)
		return 1;
	if(sscanf(params, "u", targetID))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /esposar [ID/Jugador]");
  	if(CopDuty[playerid] == 0 && SIDEDuty[playerid] == 0)
  		return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en servicio!");
	if(targetID == INVALID_PLAYER_ID)
 		return SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "{FF4600}[Error]{C8C8C8} ID de jugador incorrecta.");
 	if(PlayerCuffed[targetID] == 1)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �El objetivo ya se encuentra esposado!");
	if(!IsPlayerInRangeOfPlayer(1.0, playerid, targetID))
		return SendClientMessage(playerid, COLOR_YELLOW2, " �El objetivo se encuentra demasiado lejos!");
	if(targetID == playerid)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �No puedes esposarte a t� mismo!");

	SendFMessage(targetID, COLOR_LIGHTBLUE, " �Has sido esposado por %s!", GetPlayerCleanName(playerid));
	SendFMessage(playerid, COLOR_LIGHTBLUE, " �Has esposado a %s!", GetPlayerCleanName(targetID));
	PlayerPlayerActionMessage(playerid, targetID, 15.0, "ha esposado a");
	SetPlayerSpecialAction(targetID, SPECIAL_ACTION_CUFFED);
	SetPlayerAttachedObject(targetID, ATTACH_INDEX_ID_HANDCUFFS, 19418, 6, -0.011000, 0.028000, -0.022000, -15.600012, -33.699977, -81.700035, 0.891999, 1.000000, 1.168000);
	PlayerCuffed[targetID] = 1;
	return 1;
}

CMD:arrastrar(playerid, params[])
{
	new targetid, vehicleid, seat;

	if(PlayerInfo[playerid][pFaction] != FAC_SIDE && PlayerInfo[playerid][pFaction] != FAC_PMA)
		return 1;
	if(PlayerInfo[playerid][pRank] == 10 && PlayerInfo[playerid][pFaction] == FAC_PMA)
		return 1;
	if(sscanf(params, "ui", targetid, seat))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /arrastrar [ID/Jugador] [1/2/3]");
	if(CopDuty[playerid] == 0 && SIDEDuty[playerid] == 0)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en servicio!");
	if(seat < 1 || seat > 3)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "Solo son validos los asientos 1, 2 y 3.");

	if(IsPlayerInAnyVehicle(playerid))
		vehicleid = GetPlayerVehicleID(playerid);
	else
 		vehicleid = GetClosestVehicle(playerid, 4.0);

	if(vehicleid == INVALID_VEHICLE_ID || targetid == INVALID_PLAYER_ID)
		return SendClientMessage(playerid, COLOR_YELLOW2, "veh�culo o jugador incorrecto.");
	if(VehicleInfo[vehicleid][VehFaction] != FAC_PMA && VehicleInfo[vehicleid][VehFaction] != FAC_SIDE)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "El veh�culo no pertenece a tu facci�n.");
	if(!IsPlayerInRangeOfPlayer(2.5, playerid, targetid))
	    return SendClientMessage(playerid, COLOR_YELLOW2, "El jugador se encuentra demasiado lejos.");

 	PutPlayerInVehicle(targetid, vehicleid, seat);
	return 1;
}

CMD:quitaresposas(playerid, params[])
{
	new targetID;

	if(PlayerInfo[playerid][pFaction] != FAC_SIDE && PlayerInfo[playerid][pFaction] != FAC_PMA)
		return 1;
	if(PlayerInfo[playerid][pRank] == 10 && PlayerInfo[playerid][pFaction] == FAC_PMA)
		return 1;
	if(CopDuty[playerid] == 0 && SIDEDuty[playerid] == 0)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en servicio!");
	if(sscanf(params, "u", targetID))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /quitaresposas [ID/Jugador]");
	if(targetID == INVALID_PLAYER_ID)
 		return SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "{FF4600}[Error]{C8C8C8} ID de jugador incorrecta.");
	if(PlayerCuffed[targetID] == 0)
 		return SendClientMessage(playerid, COLOR_YELLOW2, " �El objetivo no est� esposado!");
	if(!IsPlayerInRangeOfPlayer(1.0, playerid, targetID))
		return SendClientMessage(playerid, COLOR_YELLOW2, " �El objetivo se encuentra demasiado lejos!");
	if(targetID == playerid)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �No puedes quitarte las esposas a t� mismo!");

	SendFMessage(targetID, COLOR_LIGHTBLUE, " �%s te ha quitado las esposas!", GetPlayerCleanName(playerid));
	SendFMessage(playerid, COLOR_LIGHTBLUE, " �Le has quitado las esposas a %s!", GetPlayerCleanName(targetID));
	PlayerPlayerActionMessage(playerid, targetID, 15.0, "le ha quitado las esposas a");
	SetPlayerSpecialAction(targetID, SPECIAL_ACTION_NONE);
	RemovePlayerAttachedObject(targetID, ATTACH_INDEX_ID_HANDCUFFS);
	PlayerCuffed[targetID] = 0;
	return 1;
}

CMD:arrestar(playerid, params[])
{
	new	targetID,
		time,
		string[256],
		reason[128];

	if(PlayerInfo[playerid][pFaction] != FAC_PMA && PlayerInfo[playerid][pFaction] != FAC_SIDE || PlayerInfo[playerid][pRank] == 10)
		return 1;
	if(sscanf(params, "uis[128]", targetID, time, reason))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /arrestar [ID/Jugador] [tiempo] [raz�n]");
	if(CopDuty[playerid] == 0 && SIDEDuty[playerid] == 0)
 		return SendClientMessage(playerid, COLOR_YELLOW2, "�Debes estar en servicio!");
	if(time < 1 || time > 700)
		return SendClientMessage(playerid, COLOR_YELLOW2, "�El tiempo no puede ser menor a 1 minuto ni mayor a 700!");
	if(!IsPlayerInRangeOfPlayer(5.0, playerid, targetID))
 		return SendClientMessage(playerid, COLOR_YELLOW2, "�El sujeto debe estar cerca tuyo!");

	if(IsPlayerInRangeOfPoint(playerid, 25.0, POS_POLICE_ARREST2_X, POS_POLICE_ARREST2_Y, POS_POLICE_ARREST2_Z)) {
		PlayerInfo[targetID][pJailed] = JAIL_IC_PRISON;
	} else if(IsPlayerInRangeOfPoint(playerid, 15.0, POS_POLICE_ARREST_X, POS_POLICE_ARREST_Y, POS_POLICE_ARREST_Z)) {
		PlayerInfo[targetID][pJailed] = JAIL_IC_PMA;
	} else if(IsPlayerInRangeOfPoint(playerid, 25.0, POS_POLICE_ARREST3_X, POS_POLICE_ARREST3_Y, POS_POLICE_ARREST3_Z)) {
		PlayerInfo[targetID][pJailed] = JAIL_IC_PMA_EAST;
	} else {
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar junto a las celdas!");
	}

	PlayerInfo[targetID][pJailTime] = time * 60;
	
	SendFMessage(playerid, COLOR_LIGHTBLUE, " �Has arrestado a %s por %i minutos!", GetPlayerCleanName(targetID), time);
	SendFMessage(targetID, COLOR_LIGHTBLUE, " �Arrestado! - Tiempo: %i minutos - raz�n: %s.", time, reason);
	SendClientMessage(targetID, COLOR_LIGHTYELLOW2, "Al estar arrestado no pasar� el tiempo para volver a trabajar hasta el proximo d�a de pago en libertad.");

	format(string, sizeof(string), "[CENTRAL] %s ha arrestado al criminal %s. raz�n: %s", GetPlayerCleanName(playerid), GetPlayerCleanName(targetID), reason);
	SendFactionMessage(PlayerInfo[playerid][pFaction], COLOR_PMA, string);

	ResetPlayerWantedLevelEx(targetID);
	Faction_GiveMoney(FAC_GOB, PlayerInfo[targetID][pJailTime]);
	AntecedentesLog(playerid, targetID, reason);
	SaveFaction(FAC_GOB);
	return 1;
}

CMD:central(playerid, params[])
{
	if(PlayerInfo[playerid][pFaction] != FAC_PMA || PlayerInfo[playerid][pRank] == 10)
		return 1;

	new inputtext[128];

	if(sscanf(params, "s[128]", inputtext))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /central [mensaje]");

	new outputtext[160];
	format(outputtext, sizeof(outputtext), "[%i] [CENTRAL]: %s ", playerid, inputtext);
	SendFactionRadioMessage(FAC_PMA, COLOR_RADIO, outputtext);
	return 1;
}

CMD:m(playerid, params[]) {
	return cmd_megafono(playerid, params);
}

CMD:megafono(playerid, params[])
{
	new text[256],
		factionID = PlayerInfo[playerid][pFaction];

	if(sscanf(params, "s[256]", text))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" (/m)eg�fono [mensaje]");
	if((factionID != FAC_PMA || CopDuty[playerid] != 1) && (factionID != FAC_SIDE || SIDEDuty[playerid] != 1))
	    return SendClientMessage(playerid, COLOR_YELLOW2, "No tienes un meg�fono o no te encuentras en servicio.");
	if(PlayerInfo[playerid][pRank] == 10 && PlayerInfo[playerid][pFaction] == FAC_PMA)
		return 1;
	if(!IsPlayerInAnyVehicle(playerid) || VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != factionID)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en un veh�culo con meg�fono!");
	if(IsPlayerMuted(playerid))
		return SendClientMessage(playerid, COLOR_ERROR, "[Error] "COLOR_EMB_GREY" no puedes usar el meg�fono, te encuentras silenciado.");

	format(text, sizeof(text), "[Meg�fono] [ID: %d]:  �%s!", playerid, text);
	SendPlayerMessageInRange(60.0, playerid, text, COLOR_PMA, COLOR_PMA, COLOR_PMA, COLOR_PMA, COLOR_PMA);
	return 1;
}

CMD:pservicio(playerid, params[])
{
	if(PlayerInfo[playerid][pFaction] != FAC_PMA || PlayerInfo[playerid][pRank] == 10)
		return 1;

	new string[128];

	if(CopDuty[playerid] == 0)
	{
		CopDuty[playerid] = 1;
		format(string, sizeof(string), "[CENTRAL] %s inicia su d�a de servicio como oficial de polic�a.", GetPlayerCleanName(playerid));
		SendFactionMessage(FAC_PMA, COLOR_PMA, string);
		SendClientMessage(playerid, COLOR_WHITE, "Ahora te encuentras en servicio.");
	}
	else
	{
		format(string, sizeof(string), "[CENTRAL] %s termina su d�a de servicio como oficial de polic�a.", GetPlayerCleanName(playerid));
		SendFactionMessage(FAC_PMA, COLOR_PMA, string);
		EndPlayerDuty(playerid);
	}
	return 1;
}

CMD:premolcar(playerid, params[])
{
	new vehicleid = GetPlayerVehicleID(playerid);
	new targetVehicleid = GetClosestVehicle(playerid, 7.0);
	if(PlayerInfo[playerid][pFaction] != FAC_PMA)
		return 1;
	if(targetVehicleid == INVALID_VEHICLE_ID)
	    return 1;
	if(GetVehicleModel(vehicleid) != 525 || GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
	    return SendClientMessage(playerid, COLOR_YELLOW2, " �Tienes que estar conduciendo una gr�a!");
	if(IsTrailerAttachedToVehicle(vehicleid)) 
	{
		return DetachTrailerFromVehicle(vehicleid);
	}
	AttachTrailerToVehicle(targetVehicleid, vehicleid);
	return 1;
}

CMD:sosp(playerid, params[]) {
	return cmd_sospechoso(playerid, params);
}

CMD:sospechoso(playerid, params[])
{
	new string[128], reason[64], targetID;

  	if(PlayerInfo[playerid][pFaction] != FAC_PMA && PlayerInfo[playerid][pFaction] != FAC_SIDE)
	  	return 1;
	if(PlayerInfo[playerid][pRank] == 10 && PlayerInfo[playerid][pFaction] == FAC_PMA && PlayerInfo[playerid][pFaction] != FAC_SIDE)
		return 1;
	if(sscanf(params, "us[64]", targetID, reason))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" (/sosp)echoso [ID/Jugador] [cr�men]");
	if(!IsPlayerLogged(targetID) || targetID == playerid)
   		return SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "{FF4600}[Error]{C8C8C8} ID inv�lida.");
	if(CopDuty[playerid] == 0 && SIDEDuty[playerid] == 0)
 		return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en servicio!");

	SetPlayerWantedLevelEx(targetID, PlayerInfo[targetID][pWantedLevel] + 1);
	SendFMessage(playerid, COLOR_YELLOW2, "Has marcado a %s como sospechoso por: %s.", GetPlayerCleanName(targetID), reason);
	format(string, sizeof(string), "[CENTRAL] %s ha marcado a %s como sospechoso por: %s.", GetPlayerCleanName(playerid), GetPlayerCleanName(targetID), reason);
	SendFactionMessage(PlayerInfo[playerid][pFaction], COLOR_PMA, string);
	PlayerInfo[targetID][pAccusedOf][0] = EOS;
	strcat(PlayerInfo[targetID][pAccusedOf], reason, 64);
	PlayerInfo[targetID][pAccusedBy] = GetPlayerCleanName(playerid);
	return 1;
}

CMD:localizar(playerid,params[])
{
	static CopTraceCooldown[MAX_PLAYERS];

    new vehid;

	if(PlayerInfo[playerid][pFaction] != FAC_SIDE && PlayerInfo[playerid][pFaction] != FAC_PMA)
		return 1;
	if(PlayerInfo[playerid][pRank] == 10 && PlayerInfo[playerid][pFaction] == FAC_PMA)
		return 1;
	if(sscanf(params, "i", vehid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /localizar [ID veh�culo]");
   	if(CopDuty[playerid] == 0 && SIDEDuty[playerid] == 0)
    	return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en servicio!");
	if(PlayerInfo[playerid][pFaction] == FAC_PMA)
	{
		if(VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_PMA && GetPlayerVirtualWorld(playerid) != BUILDING_VW_OFFSET + BLD_PMA)
	 	    return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en la comisar�a o dentro de una patrulla.");
	}
	if(gettime() < CopTraceCooldown[playerid])
	    return SendClientMessage(playerid, COLOR_WHITE, "Debes esperar 20 segundos antes de usar nuevamente el comando.");
	if(!Veh_IsValidId(vehid) || VehicleInfo[vehid][VehFaction] != PlayerInfo[playerid][pFaction])
	    return SendClientMessage(playerid, COLOR_WHITE, "ID de veh�culo inv�lida o no disponible para rastreo (debe ser de tu facci�n).");

	new Float:x, Float:y, Float:z, string[128], area[MAX_ZONE_NAME];

	GetVehiclePos(vehid, x, y, z);
	GetCoords2DZone(x, y, area, MAX_ZONE_NAME);

	new color = RandomRGBAColor();
	MapMarker_CreateForPlayer(playerid, x, y, z, .color = color, .time = 120000);
	Noti_Create(playerid, .time = 3000, .text = "Localizas la ubicaci�n precisa del m�vil mediante rastreo satelital. Se marcar� en tu GPS durante 2 minutos", .backgroundColor = color);

	format(string, sizeof(string),"[CENTRAL] %s ha rastreado el m�vil %i en la zona de %s.", GetPlayerCleanName(playerid), vehid, area);
	SendFactionMessage((PlayerInfo[playerid][pFaction] == FAC_SIDE) ? (FAC_SIDE) : (FAC_PMA), COLOR_PMA, string);

	CopTraceCooldown[playerid] = gettime() + 20;
	return 1;
}

CMD:pipeta(playerid,params[])
{
    new targetid;
	 
	if(PlayerInfo[playerid][pRank] == 10 && PlayerInfo[playerid][pFaction] == FAC_PMA)
		return 1;
	if(sscanf(params, "u", targetid))
   		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /pipeta [ID/jugador]");
    if(PlayerInfo[playerid][pFaction] != FAC_PMA)
		return 1;
	if(CopDuty[playerid] == 0)
    	return SendClientMessage(playerid, COLOR_YELLOW2, "�Debes estar en servicio!");
	if(OfferingPipette[playerid] == 1)
	    return SendClientMessage(playerid,COLOR_YELLOW2,"Ya has ofrecido una pipeta para que soplen.");
 	if(!IsPlayerInRangeOfPlayer(2.0, playerid, targetid))
 		return SendClientMessage(playerid, COLOR_YELLOW2, "�El sujeto debe estar cerca tuyo!");

	SendFMessage (playerid, COLOR_LIGHTYELLOW2, "Le diste una pipeta para que sople a %s, debes esperar que el sujeto responda.", GetPlayerCleanName(targetid));
    SendFMessage (targetid, COLOR_LIGHTYELLOW2, "%s te di� una pipeta de alcoholemia para que soples. (Utiliza /aceptar pipeta)", GetPlayerCleanName(playerid));
	BlowingPipette[targetid] = 1;
	OfferingPipette[playerid] = 1;
    SetPVarInt(targetid, "OfertaPipeta", playerid);
	SetTimerEx("AceptarPipeta", 20000, false, "i", playerid);
	return 1;
}

public AceptarPipeta(playerid)
{
	OfferingPipette[playerid] = 0;
	return 1;
}
	
public SoplandoPipeta(playerid)
{
	if(GetPlayerDrunkLevel(playerid) > 0)
		PlayerDoMessage(playerid, 15.0, "La pipeta marca que se super� el L�mite permitido de alcohol en la sangre.");
	else
		PlayerDoMessage(playerid, 15.0, "La pipeta indica que no hay alcohol en la sangre.");
	return 1;
}

CMD:refuerzos(playerid, params[]) {
	return cmd_ref(playerid, params);
}

CMD:ref(playerid, params[])
{
	if(PlayerInfo[playerid][pFaction] != FAC_SIDE && PlayerInfo[playerid][pFaction] != FAC_PMA && PlayerInfo[playerid][pFaction] != FAC_HOSP)
		return 1;
	if(PlayerInfo[playerid][pRank] == 10 && PlayerInfo[playerid][pFaction] == FAC_PMA)
		return 1;
	if(CopDuty[playerid] == 0 && SIDEDuty[playerid] == 0 && MedDuty[playerid] == 0)
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en servicio!");

	new type, requestingBackup = GetPVarInt(playerid, "requestingBackup");

	if(sscanf(params, "i", type))
	{
		if(!requestingBackup)
			return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" (/ref)uerzos [GENDARMER�A = 1 , PMA = 2, HMA = 3, TODOS = 4]");

		BackupClear(playerid);
	}
	else
	{
		if(type < 1 || type > 4)
			return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" (/ref)uerzos [GENDARMER�A = 1, PMA = 2, HMA = 3, TODOS = 4]");
		if(type == requestingBackup)
			return BackupClear(playerid);

		new Float:x, Float:y, Float:z, area[MAX_ZONE_NAME];
		GetPlayerPos(playerid, x, y ,z);
		GetCoords2DZone(x, y, area, MAX_ZONE_NAME);

	    SetPVarInt(playerid, "requestingBackup", type);

		switch(type)
		{
			case 1:
			{
				foreach(new i : Player)
				{
					if(PlayerInfo[i][pFaction] == FAC_SIDE && SIDEDuty[i])
					{
						SetPlayerMarkerForPlayer(i, playerid, COLOR_BACKUP);
						SendFMessage(i, COLOR_PMA, "[CENTRAL] %s requiere asistencia inmediata en la zona de %s, lo marcamos en azul en el mapa.", GetPlayerCleanName(playerid), area);
					}
				}
			}
			case 2:
			{
				foreach(new i : Player)
				{
					if(PlayerInfo[i][pFaction] == FAC_PMA && CopDuty[i])
					{
						SetPlayerMarkerForPlayer(i, playerid, COLOR_BACKUP);
						SendFMessage(i, COLOR_PMA, "[CENTRAL] %s requiere asistencia inmediata en la zona de %s, lo marcamos en azul en el mapa.", GetPlayerCleanName(playerid), area);
					}
				}
			}
			case 3:
			{
				foreach(new i : Player)
				{
					if(PlayerInfo[i][pFaction] == FAC_HOSP && MedDuty[i])
					{
						SetPlayerMarkerForPlayer(i, playerid, COLOR_BACKUP);
						SendFMessage(i, COLOR_PMA, "[CENTRAL] %s requiere asistencia inmediata en la zona de %s, lo marcamos en azul en el mapa.", GetPlayerCleanName(playerid), area);
					}
				}
			}
			case 4:
			{
				foreach(new i : Player)
				{
					if((PlayerInfo[i][pFaction] == FAC_PMA && CopDuty[i]) || (PlayerInfo[i][pFaction] == FAC_HOSP && MedDuty[i]) || (PlayerInfo[i][pFaction] == FAC_SIDE && SIDEDuty[i]))
					{
						SetPlayerMarkerForPlayer(i, playerid, COLOR_BACKUP);
						SendFMessage(i, COLOR_PMA, "[CENTRAL] %s requiere asistencia inmediata en la zona de %s, lo marcamos en azul en el mapa.", GetPlayerCleanName(playerid), area);
					}
				}
			}
		}

		SendFMessage(playerid, COLOR_WHITE, "Para cancelar la solicitud utiliza '/ref' o '/ref %i' nuevamente.", type);
	}
	return 1;
}

BackupClear(playerid)
{
	foreach(new i : Player)
	{
		if(PlayerInfo[i][pFaction] == FAC_PMA || PlayerInfo[i][pFaction] == FAC_HOSP || PlayerInfo[i][pFaction] == FAC_SIDE) {
			SetPlayerMarkerForPlayer(i, playerid, 0xFFFFFF00);
		}
	}

	DeletePVar(playerid, "requestingBackup");
	SendClientMessage(playerid, COLOR_YELLOW2, "Su solicitud de refuerzos ha sido eliminada.");
	return 1;
}

CMD:vercargos(playerid, params[])
{
	new targetid;
	
	if(PlayerInfo[playerid][pFaction] != FAC_SIDE && PlayerInfo[playerid][pFaction] != FAC_PMA)
		return 1;
	if(PlayerInfo[playerid][pRank] == 10 && PlayerInfo[playerid][pFaction] == FAC_PMA)
		return 1;
    if(sscanf(params, "u", targetid))
        return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /vercargos [ID/Jugador]");
	if(CopDuty[playerid] == 0 && SIDEDuty[playerid] == 0)
    	return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en servicio!");
    if(targetid == INVALID_PLAYER_ID)
        return SendClientMessage(playerid, COLOR_YELLOW2, "Jugador inv�lido.");
	if(PlayerInfo[playerid][pFaction] == FAC_PMA) {
	 	if(VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_PMA && GetPlayerVirtualWorld(playerid) != BUILDING_VW_OFFSET + BLD_PMA)
	 	    return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en la comisar�a o dentro de una patrulla.");
	} 
	else {
		if(PlayerInfo[playerid][pFaction] == FAC_SIDE) {
		 	if(VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_SIDE && Bld_GetPlayerLastId(playerid) != BLD_PMA)
		 	    return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en la central o dentro de algun m�vil.");
		}
	}

	SendFMessage(playerid, COLOR_LIGHTGREEN, "==================[Cargos de %s]==================", GetPlayerCleanName(targetid));
	SendFMessage(playerid, COLOR_WHITE, "- Nivel de b�squeda: %d", PlayerInfo[targetid][pWantedLevel]);
	SendFMessage(playerid, COLOR_WHITE, "- Acusado de: %s", PlayerInfo[targetid][pAccusedOf]);
	SendFMessage(playerid, COLOR_WHITE, "- Acusado por: %s", PlayerInfo[targetid][pAccusedBy]);
	return 1;
}

CMD:geof(playerid, params[])
{
	new string[128], toggle;

	if(PlayerInfo[playerid][pFaction] != FAC_PMA)
		return 1;
	if(PlayerInfo[playerid][pRank] > 4 || !CopDuty[playerid])
	    return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en servicio como polic�a y tener el rango suficiente.");
	if(sscanf(params, "i", toggle)) {
 		SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /geof [0-1]");
   		switch(DOEM) {
			case 0: SendClientMessage(playerid, COLOR_WHITE, "Estado G.E.O.F: {D40000}desautorizado");
		    case 1: SendClientMessage(playerid, COLOR_WHITE, "Estado G.E.O.F: {00D41C}autorizado");
   		}
   		return 1;
	}
	if(toggle < 0 || toggle > 1)
	    return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /doem [0-1]");

	if(toggle == 1 && DOEM != 1) {
 		format(string, sizeof(string), " �Atenci�n! a todas las unidades! el G.E.O.F ha sido autorizado por %s.", GetPlayerCleanName(playerid));
		SendFactionMessage(FAC_PMA, COLOR_PMA, string);
        DOEM = 1;
	} else
		if(toggle == 0 && DOEM != 0) {
  			format(string, sizeof(string), " �Atenci�n! a todas las unidades! el G.E.O.F ha sido desautorizado por %s.", GetPlayerCleanName(playerid));
			SendFactionMessage(FAC_PMA, COLOR_PMA, string);
	        DOEM = 0;
		}
	return 1;
}

CMD:ult(playerid, params[]) {
	cmd_ultimallamada(playerid, params);
	return 1;
}

CMD:ultimallamada(playerid, params[])
{
	new faction = PlayerInfo[playerid][pFaction];

	if(faction != FAC_PMA && faction != FAC_HOSP)
		return 1;
	if(PlayerInfo[playerid][pRank] == 10 && PlayerInfo[playerid][pFaction] == FAC_PMA)
		return 1;
	if(!CopDuty[playerid] && !MedDuty[playerid])
		return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en servicio como param�dico o polic�a.");

	if(faction == FAC_PMA)
	{
		if(lastPoliceCallNumber == 0)
			return SendClientMessage(playerid, COLOR_YELLOW2, "La �ltima llamada no ha podido ser registrada.");
		if(VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_PMA && Bld_GetPlayerLastId(playerid) != BLD_PMA)
			return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en la comisar�a o dentro de una patrulla.");

		MapMarker_CreateForPlayer(playerid, lastPoliceCallPos[0], lastPoliceCallPos[1], lastPoliceCallPos[2], .color = COLOR_RED, .time = 120000);
		Noti_Create(playerid, .time = 3000, .text = "Localizas la ubicaci�n de la llamada mediante rastreo satelital. Se marcar� en rojo en tu GPS durante 2 minutos");
		SendFMessage(playerid, COLOR_CENTRALRED, "[911] El �ltimo llamado al 911 (polic�a) ha sido con el n�mero %i.", lastPoliceCallNumber);
	}
	else if(faction == FAC_HOSP)
	{
		if(lastMedicCallNumber == 0)
			return SendClientMessage(playerid, COLOR_YELLOW2, "La �ltima llamada no ha podido ser registrada.");
		if(VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_HOSP && Bld_GetPlayerLastId(playerid) != BLD_HOSP && Bld_GetPlayerLastId(playerid) != BLD_HOSP2)
			return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en el hospital o dentro de un veh�culo de param�dico.");

		MapMarker_CreateForPlayer(playerid, lastMedicCallPos[0], lastMedicCallPos[1], lastMedicCallPos[2], .color = COLOR_RED, .time = 120000);
		Noti_Create(playerid, .time = 3000, .text = "Localizas la ubicaci�n de la llamada mediante rastreo satelital. Se marcar� en rojo en tu GPS durante 2 minutos");
		SendFMessage(playerid, COLOR_GREEN, "[911] El �ltimo llamado al 911 (param�dicos) ha sido con el n�mero %i.", lastMedicCallNumber);
	}

	return 1;
}

stock EndPlayerDuty(playerid)
{
	if(isPlayerCopOnDuty(playerid) || isPlayerSideOnDuty(playerid))
	{
		CopDuty[playerid] = 0;
		SIDEDuty[playerid] = 0;
		SendClientMessage(playerid, COLOR_WHITE, "Ya no te encuentras en servicio.");
	}
}

enum e_ProperoDlgTable {
	e_pdt_maxrank,
	e_pdt_skin,
	e_pdt_desc[32]
}

static const ProperoDlgTable[][e_ProperoDlgTable] = {
	{20, 3, "Civil\n"},
	{9, 282, "Cadete Masculino (1)\n"},
	{9, 302, "Cadete Masculino (2)\n"},
	{9, 309, "Cadete Femenino \n"},
	{8, 265, "Oficial Masculino (1)\n"},
	{8, 266, "Oficial Masculino (2)\n"},
	{8, 267, "Oficial Masculino (3)\n"},
	{8, 280, "Oficial Masculino (4)\n"},
	{8, 281, "Oficial Masculino (5)\n"},
	{8, 300, "Oficial Masculino (6)\n"},
	{8, 301, "Oficial Masculino (7)\n"},
	{8, 306, "Oficial Femenino (1)\n"},
	{8, 307, "Oficial Femenino (2)\n"},
	{8, 167, "Unidad Canina (1)\n"},
	{8, 284, "GOMF (Antidisturbios)\n"},
	{6, 285, "Operaciones Especiales (GEOF)\n"},
	{4, 286, "Divisi�n de Investigaciones\n"}
};

Dialog:DLG_PROPERO_CMD(playerid, response, listitem, inputtext[])
{
	if(!response)
		return 0;
	if(PlayerInfo[playerid][pRank] > ProperoDlgTable[listitem][e_pdt_maxrank] || (DOEM != 1 && listitem == 13))
		return SendClientMessage(playerid, COLOR_YELLOW2, "Tu rango no tiene acceso a esa vestimenta.");

	if(listitem == 0)
	{
		SetPlayerSkin(playerid, PlayerInfo[playerid][pSkin]);
		PlayerInfo[playerid][pJobSkin] = 0;
		PlayerCmeMessage(playerid, 15.0, 4000, "Deja su uniforme y toma su vestimenta de civil de los casilleros.");
	}
	else
	{
		PlayerInfo[playerid][pJobSkin] = ProperoDlgTable[listitem][e_pdt_skin];
		SetPlayerSkin(playerid, ProperoDlgTable[listitem][e_pdt_skin]);
		PlayerCmeMessage(playerid, 15.0, 4000, "Toma su placa y uniforme de los casilleros.");
	}

	return 1;
}

CMD:propero(playerid, params[])
{
	if(!isPlayerCopOnDuty(playerid))
	    return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en servicio para usar este comando.");
	
	new equipid = EquipmentPoint_GetId(playerid);   
	if(!EquipmentPoint_IsPlayerAt(playerid))
	    return SendClientMessage(playerid, COLOR_YELLOW2, "No puedes usar este comando en cualquier lado. Debes estar en los casilleros.");
	if(!EquipmentPoint_CanPlayerUse(playerid, equipid))
		return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en un casillero de tu faccion.");
	
	new propero_str[32 * sizeof(ProperoDlgTable)];

	for(new i = 0, size = sizeof(ProperoDlgTable); i < size; i++) {
		strcat(propero_str, ProperoDlgTable[i][e_pdt_desc]);
	}
	
	Dialog_Show(playerid, DLG_PROPERO_CMD, DIALOG_STYLE_LIST, "Selecciona la vestimenta a equipar:", propero_str, "Aceptar", "Cerrar");
	return 1;
}

stock AntecedentesLog(playerid, targetid, const antecedentes[])
{
	new targetName[MAX_PLAYER_NAME] = "Ninguno", targetSQLID = 0;
	
	if(targetid != INVALID_PLAYER_ID)
	{
		strcopy(targetName, PlayerInfo[targetid][pName], MAX_PLAYER_NAME);
		targetSQLID = PlayerInfo[targetid][pID];
	}

	new query[512];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), \
		"INSERT INTO `log_antecedentes` \
			(`pID`,`pName`,`pIP`,`date`,`tID`,`pAntecedentes`,`tName`) \
		VALUES \
			(%i,'%s','%s',CURRENT_TIMESTAMP,%i,'%e','%s');",
		PlayerInfo[playerid][pID],
		PlayerInfo[playerid][pName],
		PlayerInfo[playerid][pIP],
		targetSQLID,
		antecedentes,
		targetName
	);
	mysql_tquery(MYSQL_HANDLE, query);
	return 1;
}

CMD:verantecedentes(playerid, params[])
{
    new targetname[MAX_PLAYER_NAME];

    if(PlayerInfo[playerid][pFaction] != FAC_SIDE && PlayerInfo[playerid][pFaction] != FAC_PMA && PlayerInfo[playerid][pFaction] != FAC_GOB)
		return 1;
	if(PlayerInfo[playerid][pFaction] == FAC_PMA && PlayerInfo[playerid][pRank] == 10)
	    return 1;
    if(sscanf(params, "s[24]", targetname))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /verantecedentes [Nombre_Apellido]");
	
	mysql_f_tquery(MYSQL_HANDLE, 128, @Callback: "OnLogAntecedentesLoad", "is", playerid, targetname @Format: "SELECT * FROM `log_antecedentes` WHERE `tName`='%e' ORDER BY `date` DESC LIMIT 15;", targetname);
	return 1;
}

forward OnLogAntecedentesLoad(playerid, const targetname[]);
public OnLogAntecedentesLoad(playerid, const targetname[])
{
	new rows = cache_num_rows();

	if(!rows)
		return SendClientMessage(playerid, COLOR_YELLOW2, "El usuario no posee ning�n registro de antecedentes.");

	new result_text[256], result_date[24], result_name[24];

	SendFMessage(playerid, COLOR_LIGHTYELLOW2, "=========================[Registro de antecedentes de %s]=========================", targetname);

	for(new i; i < rows; i++)
	{
		cache_get_value_name(i, "pAntecedentes", result_text, sizeof(result_text));
		cache_get_value_name(i, "date", result_date, sizeof(result_date));
		cache_get_value_name(i, "pName", result_name, sizeof(result_name));

		SendFMessage(playerid, COLOR_WHITE, "[%s] %s, por: %s", result_date, result_text, result_name);
	}

	SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "===============================================================================");
	return 1;
}

CMD:darlicencia(playerid, params[])
{
	new targetid;
	
	if(PlayerInfo[playerid][pFaction] != FAC_PMA || PlayerInfo[playerid][pRank] != 1)
	    return 1;
	if(sscanf(params, "d", targetid))
  		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /darlicencia [ID/Jugador]");
	if(Bld_GetPlayerLastId(playerid) != BLD_PMA)
		return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en la comisar�a.");
	if(!IsPlayerInRangeOfPlayer(4.0, playerid, targetid))
	    return SendClientMessage(playerid, COLOR_YELLOW2, "Jugador inv�lido o se encuentra muy lejos.");
	if(PlayerInfo[targetid][pLevel] < 5)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "El jugador no posee el tiempo de residencia en el pais necesario ((NIVEL 5)).");
  	if(PlayerInfo[targetid][pWepLic] != 0)
  	    return SendClientMessage(playerid, COLOR_YELLOW2, "El sujeto ya cuenta con una licencia de armas.");
	if(GetPlayerCash(targetid) < PRICE_LIC_GUN)
	{
	    SendFMessage(playerid, COLOR_YELLOW2, "El sujeto no cuenta con el dinero suficiente ($%d).", PRICE_LIC_GUN);
	    return 1;
	}

	wepLicOffer[targetid] = playerid;
	SendFMessage(playerid, COLOR_LIGHTBLUE, "Le has ofrecido una licencia de armas a %s, espera su respuesta.", GetPlayerCleanName(targetid));
	SendFMessage(targetid, COLOR_LIGHTBLUE, "%s te ha ofrecido una licencia de armas por %d. Usa /aceptar licencia si la quieres.", GetPlayerCleanName(playerid), PRICE_LIC_GUN);
	return 1;
}

CMD:camaras(playerid, params[])
{
	if(IsPlayerInRangeOfPoint(playerid, 2.0, -2812.18, 3211.53, 2412.73)) {
		Dialog_Show(playerid, DLG_CAMARAS_POLICIA, DIALOG_STYLE_LIST, "Camaras disponibles", "24-7 Unity\nTaller Mercury\nHospital Central\nConsecionarios Grotti\nCentral ACEMA\n24-7 Norte\n24-7 Casa Rosada\n24-7 Este\nBanco de Malos Aires\nCasa Rosada", "Abrir", "Cerrar");
	}
	return 1;
}

Dialog:DLG_CAMARAS_POLICIA(playerid, response, listitem, inputtext[])
{
	if(!response)
		return 1;

	TogglePlayerControllable(playerid, false);
    usingCamera[playerid] = true;
    SendClientMessage(playerid, COLOR_WHITE, "Te encuentras mirando una c�mara de seguridad. Para salir utiliza /salircam.");
	switch(listitem)
	{
		case 0: {
            SetPlayerCameraPos(playerid,1810.6332,-1881.8149,19.5813);
            SetPlayerCameraLookAt(playerid,1826.7717,-1855.4510,13.5781);
            SetPlayerInterior(playerid, 0);
            SetPlayerVirtualWorld(playerid, 0);
			SetPlayerPos(playerid, 1808.0325, -1875.4358, 14.1098);
        }
        case 1: {
            SetPlayerCameraPos(playerid, 2484.62,-1492.18,33.92);
            SetPlayerCameraLookAt(playerid, 2521.91,-1543.21,29.62);
            SetPlayerInterior(playerid, 0);
            SetPlayerVirtualWorld(playerid, 0);
            SetPlayerPos(playerid, 2510.83, -1511.57, 22.00);
        }
        case 2: {
            SetPlayerCameraPos(playerid, 1176.9811,-1343.1915,19.4488);
            SetPlayerCameraLookAt(playerid, 1189.4771,-1324.0830,13.5669);
            SetPlayerInterior(playerid, 0);
            SetPlayerVirtualWorld(playerid, 0);
            SetPlayerPos(playerid, 1194.1521, -1325.6360, 9.3984);
        }
        case 3: {
            SetPlayerCameraPos(playerid,493.8351,-1271.0554,31.1417);
            SetPlayerCameraLookAt(playerid,536.0699,-1266.2397,16.5363);
            SetPlayerInterior(playerid, 0);
            SetPlayerVirtualWorld(playerid, 0);
			SetPlayerPos(playerid, 541.5012, -1257.4186, 10.5401);
        }
        case 4: {
            SetPlayerCameraPos(playerid,807.4939,-1307.5045,28.8984);
            SetPlayerCameraLookAt(playerid,783.412,-1327.025,13.254);
            SetPlayerInterior(playerid, 0);
            SetPlayerVirtualWorld(playerid, 0);
			SetPlayerPos(playerid, 778.0953, -1323.9830, 9.3906);
        }
        case 5: {
            SetPlayerCameraPos(playerid,1289.1920,-944.2938,59.1594);
            SetPlayerCameraLookAt(playerid,1316.086,-914.297,37.690);
            SetPlayerInterior(playerid, 0);
            SetPlayerVirtualWorld(playerid, 0);
			SetPlayerPos(playerid, 1315.8170, -915.3012, 32.0215);
        }
        case 6: {
            SetPlayerCameraPos(playerid, 1354.3875,-1725.0841,23.1490);
            SetPlayerCameraLookAt(playerid, 1352.600,-1740.055,13.171);
            SetPlayerInterior(playerid, 0);
            SetPlayerVirtualWorld(playerid, 0);
			SetPlayerPos(playerid, 1366.0573, -1754.3422, 14.0174);
        }
        case 7: {
            SetPlayerCameraPos(playerid,2352.8774,-1249.7654,36.8919);
            SetPlayerCameraLookAt(playerid, 2374.472,-1211.521,27.135);
            SetPlayerInterior(playerid, 0);
            SetPlayerVirtualWorld(playerid, 0);
			SetPlayerPos(playerid, 2348.5415, -1210.8560, 30.2480);
		}
		case 8: {
            SetPlayerCameraPos(playerid,1430.0485,-1151.9353,36.8923);
            SetPlayerCameraLookAt(playerid, 1465.9761,-1172.3713,23.8700);
            SetPlayerInterior(playerid, 0);
            SetPlayerVirtualWorld(playerid, 0);
			SetPlayerPos(playerid, 1466.8362, -1172.6542, 15.9016);
		}
		case 9: {
            SetPlayerCameraPos(playerid,1542.1896,-1714.8029,28.7414);
            SetPlayerCameraLookAt(playerid, 1507.4358,-1736.1678,13.3828);
            SetPlayerInterior(playerid, 0);
            SetPlayerVirtualWorld(playerid, 0);
			SetPlayerPos(playerid, 1512.8125, -1736.2164, 5.3828);
		}
	}
	return 1;
}

CMD:salircam(playerid, params[])
{
	if(usingCamera[playerid] == false)
		return SendClientMessage(playerid, COLOR_YELLOW2, "No te encuentras mirando ninguna c�mara.");

	TogglePlayerControllable(playerid, true);
	usingCamera[playerid] = false;

	TeleportPlayerTo(playerid, -2812.18, 3211.53, 2412.73, 0.0, 3, BUILDING_VW_OFFSET + BLD_PMA);
	SetCameraBehindPlayer(playerid);
	return 1;
}

CMD:carcelcomer(playerid, params[])
{
	new string[128];
	
	if(!IsPlayerInRangeOfPoint(playerid, 5.0, 1997.7754, 2011.4634, 1992.4028))
		return SendClientMessage(playerid, COLOR_YELLOW2, "�Debes estar en el comedor de la c�rcel!");
	if(PlayerInfo[playerid][pJailed] != JAIL_IC_PRISON && PlayerInfo[playerid][pJailed] != JAIL_IC_GOB)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "�No tienes una condena en la c�rcel!");
	if(PlayerInfo[playerid][pHunger] < 20 || PlayerInfo[playerid][pThirst] < 20)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "�S�lo puedes comer en los horarios asignados! (( Cuando tengas menos de 20 hambre o sed ))");

	format(string, sizeof(string), "El cocinero llena la bandeja de %s con la comida del d�a y unos cubiertos de pl�stico.", GetPlayerCleanName(playerid));
	PlayerDoMessage(playerid, 15.0, string);
	PlayerCmeMessage(playerid, 15.0, 4000, "Toma su bandeja con ambas manos.");
	BN_PlayerDrink(playerid, 75);
	BN_PlayerEat(playerid, 75);
	return 1;
}

CMD:verpatente(playerid, params[])
{
	new vehicleid;
	
	if(PlayerInfo[playerid][pRank] == 10 && PlayerInfo[playerid][pFaction] == FAC_PMA || PlayerInfo[playerid][pFaction] != FAC_PMA)
		return 1;
	if(CopDuty[playerid] == 0)
    	return SendClientMessage(playerid, COLOR_YELLOW2, "�Debes estar en servicio!");
    if(sscanf(params, "i", vehicleid))
        return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /verpatente [idvehiculo]");
    if(!Veh_IsValidId(vehicleid))
        return SendClientMessage(playerid, COLOR_YELLOW2, "Id de vehiculo invalida.");
	if(PlayerInfo[playerid][pFaction] == FAC_PMA) {
	 	if(VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_PMA && GetPlayerVirtualWorld(playerid) != BUILDING_VW_OFFSET + BLD_PMA)
	 	    return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en la comisar�a o dentro de una patrulla.");
	} 

	SendClientMessage(playerid, COLOR_PMA, "================[Registro Nacional de la Propiedad del Automotor]===============");
	SendFMessage(playerid, COLOR_WHITE, "veh�culo ID: %d", vehicleid);
	SendFMessage(playerid, COLOR_WHITE, "Patente: %s", VehicleInfo[vehicleid][VehPlate]);
	SendFMessage(playerid, COLOR_WHITE, "Modelo: %s", Veh_GetName(vehicleid));
	SendFMessage(playerid, COLOR_WHITE, "Titular: %s", VehicleInfo[vehicleid][VehOwnerName]);
	SendClientMessage(playerid, COLOR_PMA, "=======================================================================");
	return 1;
}

CMD:callsign(playerid, params[])
{
	new string[50], vehicleid;

	if(!Faction_HasTag(PlayerInfo[playerid][pFaction], FAC_TAG_GOB_TYPE))
		return SendClientMessage(playerid, COLOR_YELLOW2, "No eres miembro de una organizaci�n legal o no estas autorizado a utilizar este comando.");
	
	if(VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_PMA && VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_SIDE && VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_GOB && VehicleInfo[GetPlayerVehicleID(playerid)][VehFaction] != FAC_HOSP)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar dentro de un veh�culo faccionario para utilizar este comando.");
    
	if(sscanf(params, "s[128]", vehicleid))
	    return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY"/callsign [texto]");
	
	new veh = GetPlayerVehicleID(playerid);
    if(IsPlayerInAnyVehicle(playerid))
    {
        if(!PFA_Callsign[veh])
        {
        	format(string, sizeof(string), "%s", params);
           	PFA_Callsign_Text[veh] = Create3DTextLabel(string, 0xFFFFFFFF, 0.0, 0.0, 0.0, 30.0, 0, 1);
           	Attach3DTextLabelToVehicle(PFA_Callsign_Text[veh], veh, -0.7, -1.9, -0.3);
           	PFA_Callsign[veh] = true;
			SendClientMessage(playerid, COLOR_WHITE, "La descripci�n del veh�culo fue colocada correctamente, para eliminar la misma vuelve a colocar el comando.");
        }
        else
        {
        	Delete3DTextLabel(PFA_Callsign_Text[veh]);
        	PFA_Callsign[veh] = false;
			SendClientMessage(playerid, COLOR_WHITE, "La descripci�n del veh�culo fue eliminada correctamente.");
        	return 1;
        }
	}    
    return 1;
}

hook OnVehicleSpawn(vehicleid)
{
	Delete3DTextLabel(PFA_Callsign_Text[vehicleid]);
	PFA_Callsign[vehicleid] = false;
}