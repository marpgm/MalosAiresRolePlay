#if defined _marp_same_included
	#endinput
#endif
#define _marp_same_included

#include <YSI_Coding\y_hooks>

#define PRICE_HOSP_HEAL        	1000

#define POS_HOSP_HEAL_X_1		-264.11
#define POS_HOSP_HEAL_Y_1		1086.41
#define POS_HOSP_HEAL_Z_1		2812.72 

#define POS_HOSP_HEAL_X_2		-2337.34
#define POS_HOSP_HEAL_Y_2		190.63 
#define POS_HOSP_HEAL_Z_2		1546.99 

new HospHealing[MAX_PLAYERS];

CountMedicsOnDuty()
{
	new aux = 0;
 	foreach(new i : Player)
 	{
        if(IsMedicOnDuty(i))
    	    aux ++;
 	}
 	return aux;
}

IsMedicOnDuty(playerid) {
	return (PlayerInfo[playerid][pFaction] == FAC_HOSP && MedDuty[playerid]);
}

Same_CanEnterDuty(playerid)
{
	new vehicleid = GetPlayerVehicleID(playerid);
	new equipid = EquipmentPoint_GetId(playerid);   
	
	if(Veh_GetSystemType(vehicleid) == VEH_FACTION && VehicleInfo[vehicleid][VehFaction] == FAC_HOSP)
		return 1;
	else if(EquipmentPoint_IsPlayerAt(playerid))
		return EquipmentPoint_CanPlayerUse(playerid, equipid);
	
	return 0;
}

Same_IsInHealingZone(playerid)
{
	if(IsPlayerInRangeOfPoint(playerid, 3.0, POS_HOSP_HEAL_X_1, POS_HOSP_HEAL_Y_1, POS_HOSP_HEAL_Z_1) ||
		IsPlayerInRangeOfPoint(playerid, 3.0, POS_HOSP_HEAL_X_2, POS_HOSP_HEAL_Y_2, POS_HOSP_HEAL_Z_2))
		return 1;

	return 0;
}

CMD:mservicio(playerid, params[])
{
	//new string[128];

    if(PlayerInfo[playerid][pFaction] != FAC_HOSP)
		return 1;
	if(!Same_CanEnterDuty(playerid))
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en el vestuario o en un veh�culo de tu facci�n!");

	if(MedDuty[playerid] == 0)
	{
		SetPlayerHealthEx(playerid, 100.0);
		MedDuty[playerid] = 1;
		PlayerActionMessage(playerid, 15.0, "se coloca su uniforme y morral m�dico.");
	}
	else
	{
		PlayerActionMessage(playerid, 15.0, "se quita el uniforme de m�dico y guarda su morral en el armario.");
		SetPlayerHealthEx(playerid, 100.0);
		MedDuty[playerid] = 0;
		SetPlayerSkin(playerid, PlayerInfo[playerid][pSkin]);
	}
	return 1;
}

forward HospHeal(playerid);
public HospHeal(playerid)
{
	if(HospHealing[playerid])
	{
		TogglePlayerControllable(playerid, 1);
		SetPlayerHealthEx(playerid, 100.0);
		HospHealing[playerid] = 0;
        PlayerDoMessage(playerid, 15.0, "El m�dico ha finalizado el tratamiento del paciente.");
        PlayerInfo[playerid][pDisabled] = DISABLE_NONE;
	}
    return 1;
}

CMD:curarse(playerid, params[])
{
	new string[128];

    /*if(Bld_GetPlayerLastId(playerid) != BLD_HOSP && Bld_GetPlayerLastId(playerid) != BLD_HOSP2)
        return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en un hospital para usar este comando.");*/
	if(!Same_IsInHealingZone(playerid))
		return SendClientMessage(playerid, COLOR_YELLOW2, "Debes registrarte en la sala de recuperaci�n.");
	if(CountMedicsOnDuty() > 0)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "Actualmente hay un m�dico en servicio, ponte en contacto con �l para que te atienda.");
	if(HospHealing[playerid])
		return SendClientMessage(playerid, COLOR_YELLOW2, "Ya est�s siendo curado.");
	if(GetPlayerCash(playerid) < PRICE_HOSP_HEAL)
	{
	    SendFMessage(playerid, COLOR_YELLOW2, "No tienes el dinero necesario para el tratamiento ($%d).", PRICE_HOSP_HEAL);
		return 1;
	}

	GivePlayerCash(playerid, -PRICE_HOSP_HEAL);
	Faction_GiveMoney(FAC_HOSP, PRICE_HOSP_HEAL);
	PlayerDoMessage(playerid, 15.0, "Un m�dico examina al paciente y tras un diagn�stico inicial, comienza a curarlo.");
	TogglePlayerControllable(playerid, 0);
	SetTimerEx("HospHeal", 30000, false, "i", playerid);

	new str[BLD_MAX_TEXT_LENGTH];
	Bld_GetOusideText(Bld_GetPlayerLastId(playerid), str);

	format(string, sizeof(string), "[Hospital]: El paciente %s se ha registrado en el %s y est� siendo atendido.", GetPlayerCleanName(playerid), str);
	SendFactionMessage(FAC_HOSP, COLOR_WHITE, string);
	HospHealing[playerid] = 1;
	PlayerInfo[playerid][pDisabled] = DISABLE_HEALING;
	GameTextForPlayer(playerid, "Aguarda unos instantes mientras te atienden", 10000, 4);
	return 1;
}

CMD:curar(playerid,params[])
{
    new target, cost;

	if(PlayerInfo[playerid][pFaction] != FAC_HOSP)
	    return 1;
    if(!MedDuty[playerid])
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar en servicio!");
    if(sscanf(params, "ud", target, cost))
		return SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "{5CCAF1}[Sintaxis]:{C8C8C8} /curar [ID/Jugador] [precio]");
	if(cost < 0 || cost > 500)
	    return SendClientMessage(playerid, COLOR_YELLOW2, " �El costo no puede ser menor a 0 o mayor a 500!");
    if(GetPVarInt(playerid, "isHealing") != 0)
        return SendClientMessage(playerid, COLOR_YELLOW2, " �Ya est�s curando a una persona: debes esperar 15 segundos para usar nuevamente el comando!");
	if(target == INVALID_PLAYER_ID || target == playerid)
		return SendClientMessage(playerid, COLOR_YELLOW2, "Jugador inv�lido.");
	if(!IsPlayerInRangeOfPlayer(2.0, playerid, target))
		return SendClientMessage(playerid, COLOR_YELLOW2, " �Debes estar cerca del herido!");
	if(PlayerInfo[target][pDisabled] == DISABLE_DEATHBED)
  		return SendClientMessage(playerid, COLOR_YELLOW2, "El sujeto se encuentra en su lecho de muerte y no hay nada que puedas hacer por �l.");

	SendFMessage(target, COLOR_LIGHTBLUE, "El m�dico %s te ha ofrecido un tratamiento curativo por $%d. Escribe (/aceptar medico) para recibirlo.", GetPlayerCleanName(playerid), cost);
    SendClientMessage(target, COLOR_WHITE, "Si no tienes el dinero, se te cobrar� hasta lo que tengas, y el resto se descontar� de tu cuenta bancaria.");
 	SendFMessage(playerid, COLOR_LIGHTBLUE, "Le has ofrecido a %s un tratamiento curativo por $%d.", GetPlayerCleanName(target), cost);
	SetPVarInt(playerid, "healTarget", target);
	SetPVarInt(playerid, "isHealing", 1);
	SetPVarInt(target, "healIssuer", playerid);
	SetPVarInt(target, "healCost", cost);
	SetTimerEx("healTimer", 15000, false, "i", playerid);
	return 1;
}

hook function OnPlayerCmdAccept(playerid, const subcmd[])
{
	if(strcmp(subcmd, "medico", true))
		return continue(playerid, subcmd);

	if(GetPVarInt(GetPVarInt(playerid, "healIssuer"), "isHealing") == 1)
	{
		new medic = GetPVarInt(playerid, "healIssuer"),
			price = GetPVarInt(playerid, "healCost"),
			victimcash = GetPlayerCash(playerid);
			
		SetPlayerHealthEx(playerid, 100.00);

		TogglePlayerControllable(playerid, true);
		PlayerPlaySound(playerid, 1150, 0.0, 0.0, 0.0);
		PlayerPlaySound(medic, 1150, 0.0, 0.0, 0.0);
		// El pago del medico puede debitarse del banco del herido en caso de no tener efectivo
		if(victimcash > price)
		{
			GivePlayerCash(playerid, -price);
			GivePlayerCash(medic, price); // Al ser pago en efectivo, el dinero se le da al medico en el momento
			SendFMessage(playerid, COLOR_LIGHTBLUE, "Has aceptado el tratamiento por $%d en efectivo.", price);
	    	SendFMessage(medic, COLOR_LIGHTBLUE, "El herido ha aceptado el tratamiento y te ha pagado $%d en efectivo.", price);
		}
		else if(victimcash > 0)
		{
		    if(PlayerInfo[playerid][pBank] > price - victimcash)
		    {
				PlayerInfo[playerid][pBank] -= price - victimcash; // Se le debita lo que le falta del banco
				PlayerInfo[medic][pPayCheck] += price - victimcash; // Esa parte que se debita le entra al banco mediante payday
			}
			else if(PlayerInfo[playerid][pBank] > 0) // Esto es para que no saque plata del banco si no tiene
		    {
		        PlayerInfo[medic][pPayCheck] += PlayerInfo[playerid][pBank];
		        PlayerInfo[playerid][pBank] = 0;
			}
			GivePlayerCash(medic, victimcash); // La parte que se pag� en efectivo
		    ResetPlayerCash(playerid); // Todo lo que pudo pagar en efectivo
		    SendFMessage(playerid, COLOR_LIGHTBLUE, "Has aceptado el tratamiento por $%d. Una parte se descont� de tu cuenta bancaria al no tener todo en efectivo.", price);
			SendFMessage(medic, COLOR_LIGHTBLUE, "El herido acept� tu tratamiento por $%d. Una parte la pag� v�a banco, y la cobraras en el Pr�ximo Payday.", price);
		}
		else
	    {
	    	if(PlayerInfo[playerid][pBank] > price) // Esto es para que no saque plata del banco si no tiene
	    	{
				PlayerInfo[playerid][pBank] -= price; // Se le debita la totalidad del pago
	            PlayerInfo[medic][pPayCheck] += price; // Y el medico lo cobra via banco con el payday
			}
			else if(PlayerInfo[playerid][pBank] > 0) // Esto es para que no saque plata del banco si no tiene
		    {
				PlayerInfo[medic][pPayCheck] += PlayerInfo[playerid][pBank];
	    		PlayerInfo[playerid][pBank] = 0;
			}
	   		SendFMessage(playerid, COLOR_LIGHTBLUE, "Has aceptado el tratamiento por $%d. Como no ten�as efectivo, se descont� de tu cuenta bancaria.", price);
			SendFMessage(medic, COLOR_LIGHTBLUE, "El herido ha aceptado el tratamiento por $%d v�a banco, y lo cobraras en el Pr�ximo Payday.", price);
		}
		SetPVarInt(GetPVarInt(playerid, "healIssuer"), "isHealing", 0);
	}
	else {
		SendClientMessage(playerid, COLOR_YELLOW2, " Ning�n m�dico te ha ofrecido tratamiento!");
	}
	return 1;
}

static enum e_MEDIC_CLOTHES 
{
	mcSkin,
	mcDescription[32]
};

static const MedicClothes_Info[][e_MEDIC_CLOTHES] = {
	{-1, "Civil\n"},
	{276, "param�dico Masculino (1)\n"},
	{275, "param�dico Masculino (2)\n"},
	{274, "param�dico Masculino (3)\n"},
	{308, "param�dico Femenino (1)\n"},
	{277, "Bombero Masculino (1)\n"},
	{278, "Bombero Masculino (2)\n"},
	{279, "Bombero Masculino (3)\n"},
	{70, "m�dico\n"}
};

Dialog:dlg_medic_clothes(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		if(listitem == 0) {
			SetPlayerSkin(playerid, PlayerInfo[playerid][pSkin]);
			PlayerInfo[playerid][pJobSkin] = 0;
		} else {
			PlayerInfo[playerid][pJobSkin] = MedicClothes_Info[listitem][mcSkin]; 
			SetPlayerSkin(playerid, MedicClothes_Info[listitem][mcSkin]);
		}
		PlayerCmeMessage(playerid, 15.0, 4000, "Toma su vestimenta de los casilleros.");
	}
	return 1;
}

CMD:mropero(playerid, params[])
{
	if(!IsMedicOnDuty(playerid))
	    return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en servicio como m�dico.");
    if(!Same_CanEnterDuty(playerid))
    	return SendClientMessage(playerid, COLOR_YELLOW2, "Debes estar en el vestuario o en un veh�culo de tu facci�n.");

	new medic_clothes_str[256];

	for(new i = 0; i < sizeof(MedicClothes_Info); i++) {
		strcat(medic_clothes_str, MedicClothes_Info[i][mcDescription]);
	}

	Dialog_Show(playerid, dlg_medic_clothes, DIALOG_STYLE_LIST, "Selecciona la vestimenta a equipar:", medic_clothes_str, "Aceptar", "Cerrar");
	return 1;
}