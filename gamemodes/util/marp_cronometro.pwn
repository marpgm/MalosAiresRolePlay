#if defined _marp_cronometro_included
	#endinput
#endif
#define _marp_cronometro_included

////////////////////////////////////////////////////////////////////////////////
// 				Sencillo sistema de cronometro/cuenta atr�s.                  //
//                                                                            //
// Para que sirve: Inicia una cuenta atr�s (con textdraw mostrandola) desde   //
// el tiempo deseado en segundos y, al llegar a cero, llama a la funci�n      //
// PUBLICA que se indic� por par�metro.                                       //
//                                                                            //
// Al finalizar, se borra automaticamente el cronometro y su timer. Tambi�n,  //
// si se lo requiere, se pueden borrar ambas cosas con Cronometro_Borrar en   //
// cualquier momento de su ejecuci�n. Recuerde borrarlo en desconexi�n.		  //
//                                                                            //
// Por ahora solo tiene soporte para un cronometro a la vez por jugador.      //
//                                                                            //
// Si el cronometro se cre� con �xito, Cronometro_Crear devuelve 1. Caso      //
// contrario (par�metros inv�lidos o ese jugador ya tiene un cronometro       //
// funcionando en ese momento), devuelve 0.                                   //
////////////////////////////////////////////////////////////////////////////////

new pCronometroTime[MAX_PLAYERS],
	pCronometroTimer[MAX_PLAYERS],
	pCronometroFunction[MAX_PLAYERS][32],
	PlayerText:PTD_Cronometro[MAX_PLAYERS],
	PlayerText:PTD_CronometroIcon[MAX_PLAYERS],
	PlayerText:PTD_CronometroTime[MAX_PLAYERS],
	PlayerText:PTD_CronometroMin[MAX_PLAYERS],
	PlayerText:PTD_CronometroBackground[MAX_PLAYERS];

Cronometro_Crear(playerid, time, function[])
{
	if(pCronometroTime[playerid] == 0 && pCronometroTimer[playerid] == 0 && time > 0 && function[0])
	{	
		PTD_CronometroBackground[playerid] = CreatePlayerTextDraw(playerid, 693.000000, 440.000000, "Preview_Model");
		PlayerTextDrawFont(playerid, PTD_CronometroBackground[playerid], 5);
		PlayerTextDrawLetterSize(playerid, PTD_CronometroBackground[playerid], 0.600000, 2.000000);
		PlayerTextDrawTextSize(playerid, PTD_CronometroBackground[playerid], -257.500000, -131.500000);
		PlayerTextDrawSetOutline(playerid, PTD_CronometroBackground[playerid], 0);
		PlayerTextDrawSetShadow(playerid, PTD_CronometroBackground[playerid], 0);
		PlayerTextDrawAlignment(playerid, PTD_CronometroBackground[playerid], 1);
		PlayerTextDrawColor(playerid, PTD_CronometroBackground[playerid], 50);
		PlayerTextDrawBackgroundColor(playerid, PTD_CronometroBackground[playerid], 0);
		PlayerTextDrawBoxColor(playerid, PTD_CronometroBackground[playerid], 0);
		PlayerTextDrawUseBox(playerid, PTD_CronometroBackground[playerid], 0);
		PlayerTextDrawSetProportional(playerid, PTD_CronometroBackground[playerid], 1);
		PlayerTextDrawSetSelectable(playerid, PTD_CronometroBackground[playerid], 0);
		PlayerTextDrawSetPreviewModel(playerid, PTD_CronometroBackground[playerid], 1625);
		PlayerTextDrawSetPreviewRot(playerid, PTD_CronometroBackground[playerid], 0.000000, 90.000000, 0.000000, 2.000000);
		PlayerTextDrawSetPreviewVehCol(playerid, PTD_CronometroBackground[playerid], 1, 1);

		PTD_CronometroTime[playerid] = CreatePlayerTextDraw(playerid, 546.000000, 378.000000, "TIEMPO");
		PlayerTextDrawFont(playerid, PTD_CronometroTime[playerid], 2);
		PlayerTextDrawLetterSize(playerid, PTD_CronometroTime[playerid], 0.200000, 1.049999);
		PlayerTextDrawTextSize(playerid, PTD_CronometroTime[playerid], 400.000000, 17.000000);
		PlayerTextDrawSetOutline(playerid, PTD_CronometroTime[playerid], 0);
		PlayerTextDrawSetShadow(playerid, PTD_CronometroTime[playerid], 0);
		PlayerTextDrawAlignment(playerid, PTD_CronometroTime[playerid], 1);
		PlayerTextDrawColor(playerid, PTD_CronometroTime[playerid], -1);
		PlayerTextDrawBackgroundColor(playerid, PTD_CronometroTime[playerid], 255);
		PlayerTextDrawBoxColor(playerid, PTD_CronometroTime[playerid], 50);
		PlayerTextDrawUseBox(playerid, PTD_CronometroTime[playerid], 0);
		PlayerTextDrawSetProportional(playerid, PTD_CronometroTime[playerid], 1);
		PlayerTextDrawSetSelectable(playerid, PTD_CronometroTime[playerid], 0);

		PTD_Cronometro[playerid] = CreatePlayerTextDraw(playerid, 582.000000, 376.000000, "99:99");
		PlayerTextDrawFont(playerid, PTD_Cronometro[playerid], 2);
		PlayerTextDrawLetterSize(playerid, PTD_Cronometro[playerid], 0.254166, 1.450000);
		PlayerTextDrawTextSize(playerid, PTD_Cronometro[playerid], 400.000000, 17.000000);
		PlayerTextDrawSetOutline(playerid, PTD_Cronometro[playerid], 0);
		PlayerTextDrawSetShadow(playerid, PTD_Cronometro[playerid], 0);
		PlayerTextDrawAlignment(playerid, PTD_Cronometro[playerid], 1);
		PlayerTextDrawColor(playerid, PTD_Cronometro[playerid], -1);
		PlayerTextDrawBackgroundColor(playerid, PTD_Cronometro[playerid], 255);
		PlayerTextDrawBoxColor(playerid, PTD_Cronometro[playerid], 50);
		PlayerTextDrawUseBox(playerid, PTD_Cronometro[playerid], 0);
		PlayerTextDrawSetProportional(playerid, PTD_Cronometro[playerid], 1);
		PlayerTextDrawSetSelectable(playerid, PTD_Cronometro[playerid], 0);

		PTD_CronometroMin[playerid] = CreatePlayerTextDraw(playerid, 619.000000, 380.000000, "min.");
		PlayerTextDrawFont(playerid, PTD_CronometroMin[playerid], 2);
		PlayerTextDrawLetterSize(playerid, PTD_CronometroMin[playerid], 0.162499, 0.800000);
		PlayerTextDrawTextSize(playerid, PTD_CronometroMin[playerid], 400.000000, 17.000000);
		PlayerTextDrawSetOutline(playerid, PTD_CronometroMin[playerid], 0);
		PlayerTextDrawSetShadow(playerid, PTD_CronometroMin[playerid], 0);
		PlayerTextDrawAlignment(playerid, PTD_CronometroMin[playerid], 1);
		PlayerTextDrawColor(playerid, PTD_CronometroMin[playerid], -1);
		PlayerTextDrawBackgroundColor(playerid, PTD_CronometroMin[playerid], 255);
		PlayerTextDrawBoxColor(playerid, PTD_CronometroMin[playerid], 50);
		PlayerTextDrawUseBox(playerid, PTD_CronometroMin[playerid], 0);
		PlayerTextDrawSetProportional(playerid, PTD_CronometroMin[playerid], 1);
		PlayerTextDrawSetSelectable(playerid, PTD_CronometroMin[playerid], 0);

		PTD_CronometroIcon[playerid] = CreatePlayerTextDraw(playerid, 536.000000, 379.000000, "HUD:radar_mafiacasino");
		PlayerTextDrawFont(playerid, PTD_CronometroIcon[playerid], 4);
		PlayerTextDrawLetterSize(playerid, PTD_CronometroIcon[playerid], 0.600000, 2.000000);
		PlayerTextDrawTextSize(playerid, PTD_CronometroIcon[playerid], 9.500000, 10.000000);
		PlayerTextDrawSetOutline(playerid, PTD_CronometroIcon[playerid], 1);
		PlayerTextDrawSetShadow(playerid, PTD_CronometroIcon[playerid], 0);
		PlayerTextDrawAlignment(playerid, PTD_CronometroIcon[playerid], 1);
		PlayerTextDrawColor(playerid, PTD_CronometroIcon[playerid], -1);
		PlayerTextDrawBackgroundColor(playerid, PTD_CronometroIcon[playerid], 255);
		PlayerTextDrawBoxColor(playerid, PTD_CronometroIcon[playerid], 50);
		PlayerTextDrawUseBox(playerid, PTD_CronometroIcon[playerid], 1);
		PlayerTextDrawSetProportional(playerid, PTD_CronometroIcon[playerid], 1);
		PlayerTextDrawSetSelectable(playerid, PTD_CronometroIcon[playerid], 0);

		PlayerTextDrawShow(playerid, PTD_Cronometro[playerid]);
		PlayerTextDrawShow(playerid, PTD_CronometroIcon[playerid]);
		PlayerTextDrawShow(playerid, PTD_CronometroTime[playerid]);
		PlayerTextDrawShow(playerid, PTD_CronometroMin[playerid]);
		PlayerTextDrawShow(playerid, PTD_CronometroBackground[playerid]);
		pCronometroTime[playerid] = time;
		format(pCronometroFunction[playerid], 32, "%s", function);
		pCronometroTimer[playerid] = SetTimerEx("Cronometro", 1000, true, "i", playerid);
		return 1;
	}
	return 0;
}

Cronometro_Borrar(playerid)
{
	if(pCronometroTimer[playerid] > 0)
	{
		KillTimer(pCronometroTimer[playerid]);
    	PlayerTextDrawDestroy(playerid, PTD_Cronometro[playerid]);
		PlayerTextDrawDestroy(playerid, PTD_CronometroIcon[playerid]);
		PlayerTextDrawDestroy(playerid, PTD_CronometroTime[playerid]);
		PlayerTextDrawDestroy(playerid, PTD_CronometroMin[playerid]);
		PlayerTextDrawDestroy(playerid, PTD_CronometroBackground[playerid]);
		pCronometroTime[playerid] = 0;
		pCronometroTimer[playerid] = 0;
	}
}

forward Cronometro(playerid);
public Cronometro(playerid)
{
	if(pCronometroTime[playerid] > 0)
	{
		new minutes, segs, str[32];

		if(pCronometroTime[playerid] >= 60)
		{
		    minutes = floatround(pCronometroTime[playerid] / 60, floatround_floor);
		    segs = pCronometroTime[playerid] - minutes * 60;
		    format(str, sizeof(str), "%02d:%02d", minutes, segs);
		    PlayerTextDrawSetString(playerid, PTD_Cronometro[playerid], str);
		}
		else
	 	{
		 	format(str, sizeof(str), "%02d", pCronometroTime[playerid]);
		    PlayerTextDrawSetString(playerid, PTD_Cronometro[playerid], str);
		}
		pCronometroTime[playerid]--;
	}
	else
	{
	    Cronometro_Borrar(playerid);
 	    if(pCronometroFunction[playerid][0] && funcidx(pCronometroFunction[playerid]) != -1) // Chequear que no est� vacio y que exista la funcion
 	    {
	 		CallLocalFunction(pCronometroFunction[playerid], "i", playerid);
	 	}
	}
	return 1;
}
