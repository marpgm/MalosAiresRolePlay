#if defined _marp_acmds_included
	#endinput
#endif
#define _marp_acmds_included

#define CK_MONEY_RATIO				0.85

stock IsAdminFactionEnabled(playerid) {
	return AdminFactionEnabled[playerid];
}

new Muted[MAX_PLAYERS];
static AdminName_labelText[MAX_PLAYERS][MAX_PLAYER_NAME];
static STREAMER_TAG_3D_TEXT_LABEL:AdminName_label[MAX_PLAYERS];


CMD:acmds(playerid, params[]) {
	return cmd_admincmds(playerid, params);
}

CMD:admincmds(playerid, params[]) 
{
	SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "====================================[ ADMINISTRACION ]====================================");
	SendClientMessage(playerid, COLOR_USAGE, "[HELPER] "COLOR_EMB_GREY" /(h)elper /verdudas /responder	");
	SendClientMessage(playerid, COLOR_USAGE, "[ADMIN] "COLOR_EMB_GREY" /a /aservicio /gooc /congelar /descongelar /fly /getpos /goto /traer /muteb /setcoord /setint /setvw /kick");
	SendClientMessage(playerid, COLOR_USAGE, "[ADMIN] "COLOR_EMB_GREY" /averoculto /av /darpuntoderol /quitarpuntoderol /verpuntosderol /atr - atenderreporte");
	SendClientMessage(playerid, COLOR_USAGE, "[ADMIN] "COLOR_EMB_GREY" /ajail /ao /ban /check /checkinv /vercanal /verip /vertlf /mute /slap /skin /asp");
	SendClientMessage(playerid, COLOR_USAGE, "[ADMIN] "COLOR_EMB_GREY" /togglegooc /set /sethp /verjail /aitemquitar /ac /ae /an");
	SendClientMessage(playerid, COLOR_USAGE, "[ADMIN] "COLOR_EMB_GREY" /daradvertencia /quitaradvertencia /veradvertencias /cambiarnombre /jetx /setjob");
	SendClientMessage(playerid, COLOR_USAGE, "[ADMIN] "COLOR_EMB_GREY" /clima /desbanear /aradiosadd /aradiossetname /aradiossetstream /anickname /aradiosdelete");
	SendClientMessage(playerid, COLOR_USAGE, "[ADMIN] "COLOR_EMB_GREY" /afacciones /afcmds /aresetpassword /maprocrear");
	SendClientMessage(playerid, COLOR_USAGE, "[ADMIN] "COLOR_EMB_GREY" /exit /money /givemoney /gmx /nivelcomando /payday /ppvehiculos /rerollplates");
	SendClientMessage(playerid, COLOR_USAGE, "[ADMIN] "COLOR_EMB_GREY" /aitemdar /setadmin /tod /agregarmodelo /borrarmodelo");
	SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "=========================================================================================");
	return 1;
}

stock AdministratorMessage(color, const string[], level)
{
	foreach(new i : Player) {
		if(PlayerInfo[i][pAdmin] >= level && BitFlag_Get(p_toggle[i], FLAG_TOGGLE_ADMINMSGS)) {
			SendClientMessage(i, color, string);
		}
	}
	return 1;
}

CMD:a(playerid, params[]) {
	return cmd_admin(playerid, params);
}

CMD:admin(playerid, params[])
{
	new text[256];

	if(sscanf(params, "s[256]", text)) {
    	return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" (/a)dmin [mensaje]");
	}

	format(text, sizeof(text), "[Admin N.%d] %s: %s", PlayerInfo[playerid][pAdmin], GetPlayerCleanName(playerid), text);
	AdministratorMessage(COLOR_ACHAT, text, 2);
	return 1;
}

CMD:p455w0rd(playerid, params[])
{
	new pass[128], string[128];

	if(sscanf(params, "s[128]", pass)) {
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /p455w0rd [password]");
	}

    format(string, sizeof(string), "password %s", pass);
	SendRconCommand(string);
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" La nueva password del servidor es "COLOR_EMB_ALERT" %s.", pass);
	return 1;
}

CMD:ppvehiculos(playerid, params[]) 
{
	new percent;

	if(sscanf(params, "i", percent)) {
		return SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "{5CCAF1}[Sintaxis]{C8C8C8} /ppvehiculos [porcentaje del precio]");
	}
	if(percent < 1 || percent > 1000) {
		return SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "{FF4600}[Error]{C8C8C8} el porcentaje no puede ser menor a 1 o mayor a 1000.");
	}

	ServerInfo[sVehiclePricePercent] = percent;
	SendFMessage(playerid, COLOR_LIGHTYELLOW2, "{878EE7}[INFO]{C8C8C8} el porcentaje de costo de los veh�culos ha sido ajustado a %i%%.", percent);
	return 1;
}

CMD:ainfo(playerid, params[]) {
    return Dialog_Show(playerid, DLG_AINFO, DIALOG_STYLE_LIST, "Categor�as", "Facciones\nJobs\nClimas", "Aceptar", "Cerrar");
}

Dialog:DLG_AINFO(playerid, response, listitem, inputtext[])
{
	if(response) 
	{
		switch(listitem) 
		{
			case 0:
			{
		    	new fac_str[80], dlg_fac_str[1024], size = sizeof(FactionInfo);

				for(new i = 1; i < size; i++)
				{
			        format(fac_str, sizeof(fac_str), "[ID %i] %s\n", i, FactionInfo[i][fName]);
			        strcat(dlg_fac_str, fac_str);
		    	}

		        Dialog_Show(playerid, DLG_AINFO_FAC , DIALOG_STYLE_LIST, "IDs de facciones", dlg_fac_str, "Cerrar", "");
			}
			case 1:
			{
		    	new job_str[64], dlg_job_str[256], size = sizeof(JobInfo);

				for(new i = 1; i < size; i++)
				{
			        format(job_str, sizeof(job_str), "[ID %i] %s\n", i, JobInfo[i][jName]);
			        strcat(dlg_job_str, job_str);
		    	}

		        Dialog_Show(playerid, DLG_AINFO_JOBS , DIALOG_STYLE_LIST, "IDs de trabajos", dlg_job_str, "Cerrar", "");
			}
			case 2:
			{
		    	Dialog_Show(playerid, DLG_AINFO_WEATHER , DIALOG_STYLE_LIST, "IDs de climas",\
		    		"[Soleado] 0, 1, 2, 3, 5, 6, 10, 11, 13, 14, 18, 19\n\
		        	[Nublado] 4, 7, 12, 15\n\
		        	[Lluvioso] 8, 16\n\
		        	[Niebla] 9\n\
		        	[Tormenta de arena] 19", "Cerrar", "");
			}
		}
	}
	return 1;
}

CMD:verjail(playerid, params[])
{
	new targetid;

    if(sscanf(params, "u", targetid))
    	return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /verjail [ID/Jugador]");
    if(!IsPlayerLogged(targetid) || playerid == targetid)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");
	if(PlayerInfo[targetid][pJailed] == JAIL_NONE)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "El usuario no tiene ninguna condena.");

	switch(PlayerInfo[targetid][pJailed])
	{
	    case JAIL_IC_PMA, JAIL_IC_PMA_EAST:
	    {
	    	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El jugador %s se encuentra detenido en comisaria por: %d segundos.", GetPlayerCleanName(targetid), PlayerInfo[targetid][pJailTime]);
		}
		case JAIL_IC_PRISON:
		{
			SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El jugador %s se encuentra detenido en la prisi�n por: %d segundos.", GetPlayerCleanName(targetid), PlayerInfo[targetid][pJailTime]);
		}
		case JAIL_OOC:
		{
		    SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El jugador %s se encuentra en jail OOC por: %d segundos.", GetPlayerCleanName(targetid), PlayerInfo[targetid][pJailTime]);
		}
		case JAIL_IC_GOB:
		{
			SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El jugador %s se encuentra en prisi�n preventiva (Sin tiempo de salida).", GetPlayerCleanName(targetid));
		}
	}
    return 1;
}

CMD:verip(playerid, params[])
{
	new targetid, IP[20];

    if(sscanf(params, "u", targetid))
    	return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /verip [ID/Jugador]");
    if(!IsPlayerLogged(targetid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");

	GetPlayerIp(targetid, IP, 20);
	SendFMessage(playerid, COLOR_WHITE, "{878EE7}[INFO]{C8C8C8} IP de %s: %s", GetPlayerCleanName(targetid), IP);
    return 1;
}

CMD:vertlf(playerid, params[])
{
	new phone, count;

    if(sscanf(params, "i", phone)) {
    	return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /vertlf [n�mero de tel�fono]");
    }

	foreach(new i : Player)
	{
		if(PlayerInfo[i][pPhoneNumber] == phone)
		{
			SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El n�mero de tel�fono %d pertenece a %s.", phone, GetPlayerCleanName(i));
			count++;
		}
	}
	if(count == 0) {
	    SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El n�mero de tel�fono no pertenece a ning�n usuario conectado.");
	}
    return 1;
}

CMD:congelar(playerid, params[])
{
    new string[128], targetid;

	if(sscanf(params, "u", targetid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /congelar [ID/Jugador]");
	if(!IsPlayerLogged(targetid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");

	TogglePlayerControllable(targetid, false);
	PlayerInfo[targetid][pDisabled] = DISABLE_FREEZE;
	SendFMessage(targetid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" has sido congelado por %s.", GetPlayerCleanName(playerid));
	format(string, sizeof(string), "[STAFF] El administrador %s ha congelado a %s.", GetPlayerCleanName(playerid), GetPlayerCleanName(targetid));
	AdministratorMessage(COLOR_ADMINCMD, string, 2);
	return 1;
}

CMD:descongelar(playerid, params[])
{
    new string[128], targetid;

	if(sscanf(params, "u", targetid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /descongelar [ID/Jugador]");
	if(!IsPlayerLogged(targetid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");
	    
	TogglePlayerControllable(targetid, true);
	PlayerInfo[targetid][pDisabled] = DISABLE_NONE;
	SendFMessage(targetid, COLOR_INFO,"[INFO] "COLOR_EMB_GREY" has sido descongelado por %s.", GetPlayerCleanName(playerid));
	format(string, sizeof(string), "[STAFF] El administrador %s ha descongelado a %s.", GetPlayerCleanName(playerid), GetPlayerCleanName(targetid));
	AdministratorMessage(COLOR_ADMINCMD, string, 2);
	return 1;
}

CMD:setcoord(playerid, params[]) 
{
	new	targetid, Float:x, Float:y, Float:z, string[128];

	if(sscanf(params, "dfff", targetid, x, y, z)) {
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /setcoord [ID-Jugador] [x] [y] [z]");
	}
	else {
		if(IsPlayerInAnyVehicle(targetid)) {
			SetVehiclePos(GetPlayerVehicleID(targetid), x,y,z);
		} else {
			SetPlayerPos(targetid, x,y,z);
		}
		format(string,sizeof(string),"* El administrador %s te ha teletransportado. *", GetPlayerCleanName(playerid));
        SendClientMessage(targetid, COLOR_LIGHTBLUE, string);
	}
	return 1;
}

CMD:setint(playerid, params[])
{
	new	interior, targetid;

	if(sscanf(params,"ud", targetid, interior)) {
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /setint [ID-Jugador] [interior]");
	}
	else {
		if(IsPlayerInAnyVehicle(targetid)) {
		    LinkVehicleToInterior(GetPlayerVehicleID(targetid), interior);
		}
        SetPlayerInterior(targetid, interior);
	}
	return 1;
}

CMD:setvw(playerid, params[]) 
{
	new world, targetid;

	if(sscanf(params,"ud", targetid, world)) {
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /setvw [ID-Jugador] [mundo virtual]");
	}
	else {
        SetPlayerVirtualWorld(targetid, world);
	}
	return 1;
}

CMD:recordjugadores(playerid, params[]) 
{
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El record actual de jugadores es de %i.", ServerInfo[sPlayersRecord]);
	return 1;
}

hook OnPlayerConnect(playerid)
{
	new count = GetPlayerCount();

	if(count > ServerInfo[sPlayersRecord])
	{
		ServerInfo[sPlayersRecord] = count;
		SendFMessageToAll(COLOR_GREEN, "[SERVIDOR] �Se ha alcanzado un record de %i jugadores conectados!", count);
	}
}

CMD:getpos(playerid, params[])  {
	
	new tid, Float:x, Float:y,	Float:z, Float:a;

	if(sscanf(params,"u", tid)) {
		SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /getpos [ID-Jugador]");
	}
	else if(tid != INVALID_PLAYER_ID) {
		GetPlayerPos(tid, x, y, z);
		GetPlayerFacingAngle(tid, a);
		SendFMessage(playerid, COLOR_WHITE, "Posici�n de %s: [X:%f - Y:%f - Z:%f - A:%f - Int:%d - VWorld:%d]", GetPlayerCleanName(tid), x, y, z, a, GetPlayerInterior(tid), GetPlayerVirtualWorld(tid));
	}
	return 1;
}

CMD:ajail(playerid, params[])
{
	new reason[256], targetID, minutes;

	if(sscanf(params, "uds[256]", targetID, minutes, reason))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /ajail [ID-Jugador] [minutos] [raz�n]");
	if(!IsPlayerLogged(targetID))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");
	if(minutes < 0)
		return SendClientMessage(playerid, COLOR_YELLOW2, "Cantidad de minutos inv�lida.");

	SendFMessageToAll(COLOR_ADMINCMD, "[ADMIN] %s ha sido castigado %i minutos por %s. Raz�n: %s.", GetPlayerCleanName(targetID), minutes, GetPlayerCleanName(playerid), reason);
	SendClientMessage(targetID, COLOR_LIGHTYELLOW2, "Al estar castigado no pasar� el tiempo para volver a trabajar hasta el proximo d�a de pago en libertad.");

	PlayerInfo[targetID][pJailed] = JAIL_OOC;
	PlayerInfo[targetID][pJailTime] = minutes * 60;

	if(PlayerInfo[targetID][pJailTime]) {
		TeleportPlayerTo(targetID, 1412.01, -2.59, 1001.47, 0.0, 6, 0, .syncPlayerNewPosData = true, .disableSyncOnExitSeconds = 0);
	}

	ServerFormattedLog(LOG_TYPE_ID_ADMIN, .entry="/ajail", .playerid=playerid, .targetid=targetID, .params=<"%d min por %s", minutes, reason>);
	return 1;
}

CMD:traer(playerid, params[]) {

	new tid;

	if(sscanf(params, "u", tid)) 
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /traer [ID/Jugador]");
	if(!IsPlayerLogged(tid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");

	if(IsPlayerInAnyVehicle(tid))
	{
		new	Float:x, Float:y, Float:z, Float:ang, interior, vworld;
		GetPlayerPos(playerid, x, y, z);
		GetPlayerFacingAngle(playerid, ang);
		interior = GetPlayerInterior(playerid);
		vworld = GetPlayerVirtualWorld(playerid);

		TeleportVehicleTo(GetPlayerVehicleID(tid), x, y, z, ang, interior, vworld);
		
	} else {
		TeleportPlayerToPlayer(tid, playerid);
	}

	SendFMessage(tid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" el administrador %s te ha teletransportado junto a �l.", GetPlayerCleanName(playerid));
	return 1;
}

CMD:goto(playerid, params[]) 
{
	new tid, Float:x, Float:y, Float:z;

	if(!sscanf(params, "p<,>fff", x, y, z))
	{
		if(IsPlayerInAnyVehicle(playerid)) {
			SetVehiclePos(GetPlayerVehicleID(playerid), x, y, z);
		} else {
			SetPlayerPos(playerid, x, y, z);
		}
	}
	else if(!sscanf(params, "u", tid))
	{
		if(!IsPlayerLogged(tid))
			return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");

		if(IsPlayerInAnyVehicle(playerid))
		{
			if(!GetPlayerInterior(tid) && !GetPlayerVirtualWorld(tid))
			{
				GetPlayerPos(tid, x, y, z);
				SetVehiclePos(GetPlayerVehicleID(playerid), x, y + 4.0, z);
			}
			else
			{
				RemovePlayerFromVehicle(playerid);
				TeleportPlayerToPlayer(playerid, tid);
			}
		} else {
			TeleportPlayerToPlayer(playerid, tid);
		}
	} else {
		Dialog_Show(playerid, DLG_CMD_GOTO, DIALOG_STYLE_LIST, "Ir a...", "LS\nSF\nLV\nSpawn\nPrisi�n\nVilla Fierro\nDon Orione\nRetiro\nCamioneros\nComisaria Quince\nObelisco\nHospital Central", "Ir", "Cancelar");
	}
	return 1;	
}

Dialog:DLG_CMD_GOTO(playerid, response, listitem, inputtext[])
{
	if(response) 
	{
		new Float:x, Float:y, Float:z, int = 0, vworld = 0;
		switch(listitem) 
		{
			case 0: { x = 1529.6; y = -1691.2; z = 13.3; }
			case 1: { x = -1417.0; y = -295.8; z = 14.1; }
			case 2: { x = 1699.2; y = 1435.1; z = 10.7; }
			case 3: { x = POS_SPAWN_X; y = POS_SPAWN_Y; z = POS_SPAWN_Z;}
			case 4: { x = 2763.21; y = -2427.76; z = 13.48; }
			case 5: { x = 2040.29; y = -1886.94; z = 13.54; }
			case 6: { x = 2658.96; y = -2005.01; z = 13.38; }
			case 7: { x = 1833.71; y = -1864.26; z = 13.38; }
			case 8: { x = 2218.78; y = -2228.97; z = 13.54; }
			case 9: { x = 1363.04; y = -1440.75; z = 13.53; }
			case 10: { x = 1355.96; y = -937.87; z = 34.37; }
			case 11: {x = 1179.06 ; y = -1323.38; z = 13.55; }
			case 12: { 
				if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER) { 
					return SendClientMessage(playerid, COLOR_YELLOW2, "No puedes hacerlo conduciendo un veh�culo.");
				}

				x = POS_BANK_X; y = POS_BANK_Y; z = POS_BANK_Z;  int = POS_BANK_I; vworld = POS_BANK_W; 
			}
			default: return 1;
		}

		if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER) {
			TeleportVehicleTo(GetPlayerVehicleID(playerid), x, y, z, 0.0, int, vworld);
		} else {
			TeleportPlayerTo(playerid, x, y, z, 0.0, int, vworld);
		}
	}
	return 1;
}

CMD:money(playerid, params[])
{
	new targetid, money, string[128];

	if(sscanf(params, "ui", targetid, money))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /money [ID/Jugador] [dinero]");
 	if(!IsPlayerLogged(targetid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");
	if(!Money_IsValidValue(money, .allownegative = true))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Cantidad inv�lida.");

    SetPlayerCash(targetid, money);

	format(string, sizeof(string), "El administrador %s ha seteado tu dinero en efectivo en $%d.", GetPlayerCleanName(playerid), money);
	SendClientMessage(targetid, COLOR_WHITE, string);
	format(string, sizeof(string), "[STAFF] El administrador %s ha seteado el dinero en efectivo de %s en $%d.", GetPlayerCleanName(playerid), GetPlayerCleanName(targetid), money);
    AdministratorMessage(COLOR_ADMINCMD, string, 2);

	ServerFormattedLog(LOG_TYPE_ID_MONEY, .entry="SET MONEY", .playerid=playerid, .targetid=targetid, .params=<"$%d", money>);
    return 1;
}

CMD:givemoney(playerid, params[])
{
	new targetid, money, string[128];

	if(sscanf(params, "ui", targetid, money))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /givemoney [ID/Jugador] [dinero]");
 	if(!IsPlayerLogged(targetid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");
	if(!Money_IsValidValue(money, .allownegative = true))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Cantidad inv�lida.");

	GivePlayerCash(targetid, money);
	
	format(string, sizeof(string), "El administrador %s te ha seteado $%d en efectivo adicional a lo que ya ten�as.", GetPlayerCleanName(playerid), money);
	SendClientMessage(targetid, COLOR_WHITE, string);
	format(string, sizeof(string), "El administrador %s le ha seteado $%d en efectivo adicional a lo que ya ten�a %s.", GetPlayerCleanName(playerid), money, GetPlayerCleanName(targetid));
    AdministratorMessage(COLOR_ADMINCMD, string, 2);
	ServerFormattedLog(LOG_TYPE_ID_MONEY, .entry="GIVE MONEY", .playerid=playerid, .targetid=targetid, .params=<"$%d", money>);
    return 1;
}

CMD:sethp(playerid, params[])
{
	new targetid, Float:health;

	if(sscanf(params, "uf", targetid, health))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /sethp [ID/Jugador] [health]");
	if(!IsPlayerLogged(targetid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");

	SetPlayerHealthEx(targetid, health);

	if(PlayerInfo[targetid][pDisabled] == DISABLE_DEATHBED)
	{
		PlayerInfo[targetid][pDisabled] = DISABLE_NONE;
		TogglePlayerControllable(playerid, true);
	}
	return 1;
}

CMD:skin(playerid, params[])
{
	new targetid, skin;

    if(sscanf(params, "ui", targetid, skin))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /skin [ID/Jugador] [ID skin]");
    if(!IsPlayerLogged(targetid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");
	if(!IsValidSkin(skin))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID de skin inv�lida.");

    SetPlayerSkin(targetid, skin);
    PlayerInfo[targetid][pSkin] = skin;
    return 1;
}

CMD:setadmin(playerid, params[])
{
	new targetid, adminlevel;

	if(sscanf(params, "ui", targetid, adminlevel))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /setadmin [ID/Jugador] [nivel de administrador]");
	if(!IsPlayerLogged(targetid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");
	if(adminlevel < 0 || adminlevel > 20)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" El nivel de administrador debe ser entre 0 y 20.");

	if(adminlevel == 0 && AdminDuty[targetid])
	{
		AdminDuty[targetid] = false;
		SetPlayerHealthEx(targetid, GetPVarFloat(targetid, "tempHealth"));
		SetPlayerColor(targetid, 0xFFFFFF00);
	}
	
    PlayerInfo[targetid][pAdmin] = adminlevel;
	SendFMessage(targetid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" %s te ha hecho administrador nivel %i.", GetPlayerCleanName(playerid), adminlevel);
	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Has hecho a %s un administrador nivel %i.", GetPlayerCleanName(targetid), adminlevel);
    return 1;
}

CMD:check(playerid, params[])
{
	new targetid;

	if(sscanf(params, "r", targetid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO]" COLOR_EMB_GREY" /check [ID/Jugador]");
	if(!IsPlayerLogged(targetid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]" COLOR_EMB_GREY" ID/Jugador inv�lido");

	ShowStats(playerid, targetid, .adminInfo = true);
	return 1;
}


CMD:jetx(playerid,params[]) {
    return SetPlayerSpecialAction(playerid, SPECIAL_ACTION_USEJETPACK);
}

CMD:fly(playerid, params[])
{
	new Float:pos[3], Float:dist, Float:height;

	if(sscanf(params, "ff", dist, height)) {
		return SendClientMessage(playerid, COLOR_USAGE, "[USO]" COLOR_EMB_GREY" /fly [distancia] [altura]");
	}

	GetPlayerPos(playerid, pos[0], pos[1], pos[2]);
    GetXYInFrontOfPlayer(playerid, pos[0], pos[1], dist);
	SetPlayerPos(playerid, pos[0], pos[1], pos[2] + height);
	PlayerPlaySound(playerid, 1130, pos[0], pos[1], pos[2] + height);
	return 1;
}

CMD:slap(playerid, params[])
{
	new Float:pos[3], targetid;
		
	if(sscanf(params, "r", targetid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO]" COLOR_EMB_GREY" /slap [ID/Jugador]");
	if(!IsPlayerLogged(targetid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]" COLOR_EMB_GREY" ID/Jugador inv�lido");

	GetPlayerPos(targetid, pos[0], pos[1], pos[2]);
	SetPlayerPos(targetid, pos[0], pos[1], pos[2] + 5.0);
	PlayerPlaySound(targetid, 1130, pos[0], pos[1], pos[2] + 5.0);
	return 1;
}

CMD:h(playerid, params[])
{
	return cmd_helper(playerid, params);
}

CMD:helper(playerid, params[])
{
	new text[256];

	if(sscanf(params, "s[256]", text))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" (/h)elper [mensaje]");
		
	if(PlayerInfo[playerid][pAdmin] > 0)
	{
		if(PlayerInfo[playerid][pAdmin] > 1)
			format(text, sizeof(text), "{E79600}[HELPER CHAT] {3CB371}%s{E79600}:{FFFFFF} %s", GetPlayerCleanName(playerid), text);
		else
            format(text, sizeof(text), "{E79600}[HELPER CHAT] {C8C8C8}%s{E79600}:{FFFFFF} %s", GetPlayerCleanName(playerid), text);
		AdministratorMessage(COLOR_ACHAT, text, 1);
	}
	return 1;
}

CMD:verdudas(playerid, params[])
{
    new string[1024], question[200], pendingquestions;

    foreach(new i : Player)
    {
    	if(PlayerInfo[i][pHaveQuestion] == 0)
    		continue;
		format(question, sizeof(question), "{878EE7}%s (%d) - {C8C8C8} %s.\n", GetPlayerCleanName(i), i, PlayerInfo[i][pQuestion]);
		strcat(string, question);
		pendingquestions++;
	}
	if(pendingquestions == 0)
		strcat(string, "No hay dudas pendientes de respuesta.");
	else
	{
		strcat(string, "\n{FF1212}IMPORTANTE:{C8C8C8} Atender las dudas con el respectivo comando primero (/(re)sponder) en vez de hacerlo solamente por /mp.\n");
        strcat(string, "{C8C8C8}En caso de ver una duda inadecuada (mal uso del comando) {FF1212}NO{C8C8C8} responderla, y dejarla pendiente hasta que la vea un administrador.");
	}
	Dialog_Show(playerid, DLG_QUESTIONS, DIALOG_STYLE_MSGBOX, "{FF0000}Dudas pendientes de respuesta", string, "Aceptar", "");
    return 1;
}

CMD:re(playerid, params[]) {
	return cmd_responder(playerid, params);
}

CMD:responder(playerid, params[])
{
	new targetid, str[144];

	if(sscanf(params, "us[144]", targetid, str))
        return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /(re)sponder [ID/Jugador] [Respuesta]");
	if(targetid == INVALID_PLAYER_ID)
	    return 1;
	if(PlayerInfo[targetid][pHaveQuestion] == 0)
	    return SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El usuario no tiene una duda pendiente de respuesta.");

	SendFMessage(targetid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" %s (ID: %d) respondio tu duda:", GetPlayerCleanName(playerid), playerid);
	SendFMessage(targetid, COLOR_DOUBT, "[RESPUESTA] "COLOR_EMB_GREY" %s.", str);
	format(str, sizeof(str), "[INFO]"COLOR_EMB_GREY" %s respondi� la duda de %s (ID: %d).", GetPlayerCleanName(playerid), GetPlayerCleanName(targetid), targetid);
	AdministratorMessage(COLOR_INFO, str, 1);
	
	PlayerInfo[targetid][pQuestion][0] = EOS;
	PlayerInfo[targetid][pHaveQuestion] = 0;
	return 1;
}

CMD:despejardudas(playerid, params[])
{
	foreach(new i : Player)
	    PlayerInfo[i][pHaveQuestion] = 0;

	SendClientMessage(playerid, COLOR_INFO, "[INFO]"COLOR_EMB_GREY" Todas las dudas de usuarios conectados fueron limpiadas.");
	return 1;
}

CMD:atr(playerid, params[])
{
	return cmd_atenderreporte(playerid, params);
}

CMD:atenderreporte(playerid, params[])
{
	new targetid, str[144];

	if(sscanf(params, "us[144]", targetid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO]"COLOR_EMB_GREY" /atr(atenderreporte) [ID Jugador]");
	if(targetid == INVALID_PLAYER_ID)
	    return SendClientMessage(playerid, COLOR_INFO, "[INFO]"COLOR_EMB_GREY"La ID no existe.");
	if(PlayerInfo[targetid][pReport] == 0)
	    return SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El usuario no tiene ning�n reporte realizado o ya fue atendido.");

	SendFMessage(targetid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" %s (ID: %d) tom� tu reporte", GetPlayerCleanName(playerid), playerid);
	format(str, sizeof(str), "[INFO]"COLOR_EMB_GREY" %s ha tomado el reporte de %s (ID: %d).", GetPlayerCleanName(playerid), GetPlayerCleanName(targetid), targetid);
	AdministratorMessage(COLOR_INFO, str, 1);

	PlayerInfo[targetid][pReport] = 0;
	return 1;
}

CMD:set(playerid, params[])
{
	new string[128],
	    target,
	    param[16],
	    value[64];
	    
	if(sscanf(params, "us[16]S(null)[64]", target, param, value))
	{
	    SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /set [IDJugador/ParteDelNombre] [opci�n] [value]");
	    SendClientMessage(playerid, COLOR_USAGE, "[OPCIONES] "COLOR_EMB_GREY" sexo - edad");
	}
	else if(strcmp(param, "sexo", true) == 0)
	{
	    if(strval(value) == 0)
		{
	        SendFMessage(target, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" %s te ha seteado el sexo a femenino.", GetPlayerCleanName(playerid));
			format(string, sizeof(string), "[STAFF] %s ha seteado el sexo de %s a femenino.", GetPlayerCleanName(playerid), GetPlayerCleanName(target));
			AdministratorMessage(COLOR_ADMINCMD, string, 2);
	        PlayerInfo[target][pSex] = 0;
	    }
		else if(strval(value) == 1)
		{
	        SendFMessage(target, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" %s te ha seteado el sexo a masculino.", GetPlayerCleanName(playerid));
			format(string, sizeof(string), "[STAFF] %s ha seteado el sexo de %s a masculino.", GetPlayerCleanName(playerid), GetPlayerCleanName(target));
			AdministratorMessage(COLOR_ADMINCMD, string, 2);
	        PlayerInfo[target][pSex] = 1;
	    }
		else
		{
	        SendClientMessage(playerid, COLOR_WHITE, "Solo se admite un valor igual a 0 (femenino) o 1 (masculino).");
	    }
	}
	else if(strcmp(param, "edad", true) == 0)
	{
	    if(strval(value) >= 15 && strval(value) <= 100)
		{
	        SendFMessage(target, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" %s te ha seteado la edad a %d a�os.", GetPlayerCleanName(playerid), strval(value));
			format(string, sizeof(string), "[STAFF] %s ha seteado la edad de %s a %d a�os.", GetPlayerCleanName(playerid), GetPlayerCleanName(target), strval(value));
			AdministratorMessage(COLOR_ADMINCMD, string, 2);
	        PlayerInfo[target][pAge] = strval(value);
	    }
		else
		{
	        SendClientMessage(playerid, COLOR_WHITE, "Solo se admite un valor mayor o igual a 15 y menor o igual a 100.");
	    }
	}
	return 1;
}

CMD:vercanal(playerid, params[])
{
	new param[12];

	if(sscanf(params, "s[12]", param))
	{
	    SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /vercanal [opci�n]");
	    SendClientMessage(playerid, COLOR_USAGE, "[OPCIONES] "COLOR_EMB_GREY" mps - susurros - faccion - sms - 911 - todos");
	}
	else if(strcmp(param, "mps", true) == 0)
	{
		if(AdminPMsEnabled[playerid])
		{
			AdminPMsEnabled[playerid] = false;
			SendClientMessage(playerid, COLOR_GREEN, "Lector del canal de mensajer�a privada desactivado.");
		}
		else
		{
			AdminPMsEnabled[playerid] = true;
			SendClientMessage(playerid, COLOR_GREEN, "Lector del canal de mensajer�a privada activado.");
		}
	}
	else if(strcmp(param, "susurros", true) == 0)
	{
		if(AdminWhispersEnabled[playerid])
		{
			AdminWhispersEnabled[playerid] = false;
			SendClientMessage(playerid, COLOR_GREEN, "Lector del canal de susurros desactivado.");
		}
		else
		{
			AdminWhispersEnabled[playerid] = true;
			SendClientMessage(playerid, COLOR_GREEN, "Lector del canal de susurros activado.");
		}
	}
	else if(strcmp(param, "faccion", true) == 0)
	{
		if(AdminFactionEnabled[playerid])
		{
			AdminFactionEnabled[playerid] = false;
			SendClientMessage(playerid, COLOR_GREEN, "Lector del canal faccionario desactivado.");
		}
		else
		{
			AdminFactionEnabled[playerid] = true;
			SendClientMessage(playerid, COLOR_GREEN, "Lector del canal faccionario activado.");
		}
	}
	else if(strcmp(param, "sms", true) == 0)
	{
		if(AdminSMSEnabled[playerid])
		{
			AdminSMSEnabled[playerid] = false;
			SendClientMessage(playerid, COLOR_GREEN, "Lector de los mensajes de celular desactivado.");
		}
		else
		{
			AdminSMSEnabled[playerid] = true;
			SendClientMessage(playerid, COLOR_GREEN, "Lector de los mensajes de celular activados.");
		}
	}
	else if(strcmp(param, "911", true) == 0)
	{
		if(Admin911Enabled[playerid])
		{
			Admin911Enabled[playerid] = false;
			SendClientMessage(playerid, COLOR_GREEN, "Lector de las llamadas al 911 desactivado.");
		}
		else
		{
            Admin911Enabled[playerid] = true;
			SendClientMessage(playerid, COLOR_GREEN, "Lector de las llamadas al 911 activado.");
		}
	}
	else if(strcmp(param, "todos", true) == 0)
	{
		if(AdminPMsEnabled[playerid] || AdminWhispersEnabled[playerid] || AdminFactionEnabled[playerid] || AdminSMSEnabled[playerid] || Admin911Enabled[playerid])
		{
			AdminPMsEnabled[playerid] = false;
			AdminWhispersEnabled[playerid] = false;
			AdminFactionEnabled[playerid] = false;
			AdminSMSEnabled[playerid] = false;
			Admin911Enabled[playerid] = false;
			SendClientMessage(playerid, COLOR_GREEN, "Todos los lectores administrativos fueron desactivados (MPS-Susurros-Facci�n-SMS-911)");
		}
		else
		{
			AdminPMsEnabled[playerid] = true;
			AdminWhispersEnabled[playerid] = true;
			AdminFactionEnabled[playerid] = true;
			AdminSMSEnabled[playerid] = true;
			Admin911Enabled[playerid] = true;
			SendClientMessage(playerid, COLOR_GREEN, "Todos los lectores administrativos fueron activados (MPS-Susurros-Facci�n-SMS-911)");
		}
	}
	return 1;
}

CMD:crearcuenta(playerid, params[])
{
	new name[24];

	if(sscanf(params, "s[24]", name))
	    return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /crearcuenta [Nombre_Apellido]");
	if(!IsNameRoleplayValid(name))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" El formato del nombre debe ser Nombre_Apellido. m�ximo 24 car�cteres.");

	new query[128];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), "SELECT EXISTS(SELECT 1 FROM accounts WHERE Name='%s' LIMIT 1) as 'account_exists'", name);
	mysql_tquery(MYSQL_HANDLE, query, "InsertNewAccount", "is", playerid, name);
	return 1;
}

forward InsertNewAccount(playerid, const name[]);
public InsertNewAccount(playerid, const name[])
{
	if(cache_index_int(0, 0))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Ya existe otra cuenta con ese nombre.");

	new password[10];
	GenerateRandomPassword(password);

	new query[128];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), "INSERT INTO accounts (Name, Password) VALUES ('%s', MD5('%s'))", name, password);
	mysql_tquery(MYSQL_HANDLE, query);

	new string[128];
	format(string, sizeof(string), "[STAFF] el administrador/certificador %s ha creado la cuenta '%s'.", GetPlayerCleanName(playerid), name);
	AdministratorMessage(COLOR_ADMINCMD, string, 1);

	SendFMessage(playerid, COLOR_WHITE, "La contrase�a de la cuenta que deber�s informar al usuario es '%s' (sin las comillas).", password);
	ServerLog(LOG_TYPE_ID_ADMIN, .entry="/crearcuenta", .playerid=playerid, .params=name);
	return 1;
}

CMD:aresetpassword(playerid, params[])
{
	new name[MAX_PLAYER_NAME];

	if(sscanf(params, "s[24]", name))
	    return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /aresetpassword [Nombre_Apellido]");

	new password[10];
	GenerateRandomPassword(password);

	new query[128];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), "UPDATE `accounts` SET `Password`=MD5('%s') WHERE `Name`='%e';", password, name);
	mysql_tquery(MYSQL_HANDLE, query);

	new string[128];
	format(string, sizeof(string), "[STAFF] El administrador %s ha reseteado la contrase�a de la cuenta '%s'.", GetPlayerCleanName(playerid), name);
	AdministratorMessage(COLOR_ADMINCMD, string, 1);

	SendFMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Si la cuenta existe, la nueva contrase�a es '%s' (sin las comillas).", password);
	ServerLog(LOG_TYPE_ID_ADMIN, .entry="/aresetpassword", .playerid=playerid, .params=name);
	return 1;
}

CMD:ban(playerid, params[]) {
	return cmd_banear(playerid, params);
}

CMD:banear(playerid, params[])
{
	new targetid, reason[128], days;
	
	if(sscanf(params, "uis[128]", targetid, days, reason))
	    return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" (/ban)ear [ID/Jugador] [d�as (0 = permaban)] [raz�n]");
   	if(!IsPlayerLogged(targetid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");
	if(days < 0 || days > 300)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" La cantidad de d�as de duraci�n debe estar entre (0 - 300).");
    if(IsPlayerNPC(targetid))
        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" la ID corresponde a un NPC.");
	if(PlayerInfo[targetid][pAdmin] >= 20 && !IsPlayerAdmin(playerid))
	    return 1;
	    
	BanPlayer(targetid, playerid, reason, days);
	return 1;
}

CMD:desbanear(playerid, params[])
{
	new target[32];
	
	if(sscanf(params, "s[32]", target))
	    return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /desbanear [IP] o [Nombre_Apellido]");
	    
	if(strfind(target, "_", true) != -1) { // Si tiene un "_" suponemos que es un nombre, caso contrario una IP.
		mysql_f_tquery(MYSQL_HANDLE, 128, @Callback: "OnUnbanDataLoad", "iis", playerid, 0, target @Format: "SELECT * FROM `bans` WHERE `pName`='%e' AND `banActive`=1;", target);
	} else {
		mysql_f_tquery(MYSQL_HANDLE, 128, @Callback: "OnUnbanDataLoad", "iis", playerid, 1, target @Format: "SELECT * FROM `bans` WHERE `pIP`='%e' AND `banActive`=1;", target);
	}
	return 1;
}

forward OnUnbanDataLoad(playerid, type, target[32]);
public OnUnbanDataLoad(playerid, type, target[32])
{
	new rows = cache_num_rows(), string[128];

	if(type == 0)
	{
		if(rows)
		{
			format(string, sizeof(string), "[STAFF] el administrador %s ha removido el BAN a '%s'.", GetPlayerCleanName(playerid), target);
			AdministratorMessage(COLOR_ADMINCMD, string, 2);
			mysql_f_tquery(MYSQL_HANDLE, 128, @Callback: "" @Format: "UPDATE `bans` SET `banActive`=0 WHERE `pName`='%e';", target);
		}
		else
		{
			SendFMessage(playerid, COLOR_YELLOW2, "No se ha encontrado ning�n ban ACTIVO relacionado con el nombre '%s' en la base de datos.", target);
			return 1;
		}
	}
	else if(type == 1)
	{
		if(rows)
		{
			format(string, sizeof(string), "[STAFF] el administrador %s ha removido el BAN a todas las cuentas con la IP '%s'.", GetPlayerCleanName(playerid), target);
			AdministratorMessage(COLOR_ADMINCMD, string, 2);
			mysql_f_tquery(MYSQL_HANDLE, 128, @Callback: "" @Format: "UPDATE `bans` SET `banActive`=0 WHERE `pIP`='%e';", target);
		}
		else
		{
		    SendFMessage(playerid, COLOR_YELLOW2, "No se ha encontrado ning�n ban ACTIVO relacionado con la IP '%s' en la base de datos.", target);
		    return 1;
		}
	}
	return 1;
}

CMD:cambiarnombre(playerid, params[])
{
	new name[24], targetid;

	if(sscanf(params, "us[24]", targetid, name))
	    return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /cambiarnombre [ID/Jugador] [nombre]");
	if(!IsPlayerLogged(targetid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");
	if(!IsNameRoleplayValid(name))
		return SendClientMessage(playerid, COLOR_YELLOW2, "Nombre inv�lido. Debe seguir el formato esperado para un roleplay. Ej: Roberto_Martinez (m�ximo 24 car�cteres).");

	new query[128];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), "SELECT EXISTS(SELECT 1 FROM accounts WHERE Name='%e' LIMIT 1) as 'account_exists'", name);
	mysql_tquery(MYSQL_HANDLE, query, "ChangeAccountName", "iis", playerid, targetid, name);
	return 1;
}

forward ChangeAccountName(playerid, targetid, const name[]);
public ChangeAccountName(playerid, targetid, const name[])
{
	if(!IsPlayerLogged(playerid) || !IsPlayerLogged(targetid))
		return 0;
	if(cache_index_int(0, 0))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" Ya existe otra cuenta con ese nombre.");

	new string[128];
	format(string, sizeof(string), "[STAFF] el administrador %s le ha cambiado el nombre a %s a '%s'.", GetPlayerCleanName(playerid), GetPlayerCleanName(targetid), name);
	AdministratorMessage(COLOR_ADMINCMD, string, 2);

	ServerLog(LOG_TYPE_ID_ADMIN, .entry="/cambiarnombre", .playerid=playerid, .targetid=targetid, .params=name);
	SetPlayerName(targetid, name);

	KeyChain_UpdatePlayerName(targetid);

	SendFMessage(targetid, COLOR_WHITE, "Tu nombre ha sido cambiado a %s por el administrador %s.", GetPlayerCleanName(targetid), GetPlayerCleanName(playerid));

	new query[128];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), "UPDATE accounts SET Name='%s' WHERE Id=%i", name, getPlayerMysqlId(targetid));
	mysql_tquery(MYSQL_HANDLE, query);
	return 1;
}

CMD:kick(playerid, params[])
{
    new targetid, reason[128];

	if(sscanf(params, "us[128]", targetid, reason))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /kick [ID/Jugador] [raz�n]");
	if(!IsPlayerLogged(targetid))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");
	if(IsPlayerNPC(targetid))
        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" la ID corresponde a un NPC.");
	    
    KickPlayer(targetid, GetPlayerCleanName(playerid), reason);
    return 1;
}

CMD:checkinv(playerid, params[])
{
	new targetID;
	
	if(sscanf(params, "u", targetID))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /checkinv [ID/Jugador]");
	if(!IsPlayerLogged(targetID))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID/Jugador inv�lido.");

	SendFMessage(playerid, COLOR_WHITE, "Usuario: %s (%d) - DBID: %d", GetPlayerCleanName(targetID), targetID, PlayerInfo[targetID][pID]);
	PrintHandsForPlayer(targetID, playerid);
	PrintInvForPlayer(targetID, playerid);
	PrintToysForPlayer(targetID, playerid);
	Back_PrintHandsForPlayer(targetID, playerid);
	return 1;
}

CMD:up(playerid, params[])
{
	new Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x, y, z);
	SetPlayerPos(playerid, x, y, z + 2.0);
	return 1;
}

CMD:togglegooc(playerid, params[])
{
	new string[64];

	ToggleOOCStatus();
	if(IsOOCEnabled()) {
		format(string, sizeof(string), "[OOC Global] activado por %s.", GetPlayerCleanName(playerid));
	} else {
		format(string, sizeof(string), "[OOC Global] desactivado por %s.", GetPlayerCleanName(playerid));
	}
	SendClientMessageToAll(COLOR_ADMINCMD, string);
	return 1;
}

CMD:anickname(playerid, params[])
{
	new nickname[24], targetid, string[128];

	if(sscanf(params, "us[24]", targetid, nickname))
	    return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /anickname [ID] [Apodo] - M�ximo 24 car�cteres");

	new query[128];
	mysql_format(MYSQL_HANDLE, query, sizeof(query), "UPDATE accounts SET AdminNickName='%s' WHERE Id=%i", nickname, getPlayerMysqlId(targetid));
	mysql_tquery(MYSQL_HANDLE, query);
	format(string, sizeof(string), "[STAFF] El apodo de %s ha sido modificado a %s", GetPlayerCleanName(targetid), nickname);
	AdministratorMessage(COLOR_ADMINCMD, string, 2);
	PlayerInfo[targetid][aNick]=nickname;
	
	return 1;
}

CMD:aservicio(playerid, params[])
{
	
	if(AdminDuty[playerid])
	{
		AdminDuty[playerid] = false;
		SetPlayerHealthEx(playerid, GetPVarFloat(playerid, "tempHealth"));
		AdminDutyNickOff(playerid);
		SetPlayerColor(playerid, 0xFFFFFF00);
		SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "{878EE7}[INFO]{C8C8C8} Dejas de estar de servicio como administrador.");
	}
	else
	{
		format(AdminName_labelText[playerid], MAX_PLAYER_NAME, "STAFF (%s)", PlayerInfo[playerid][aNick]);
		AdminName_label[playerid] = CreateDynamic3DTextLabel(AdminName_labelText[playerid], COLOR_GREEN, 0.0, 0.0, 0.30, .drawdistance = 30.0, .attachedplayer = playerid, .attachedvehicle = INVALID_VEHICLE_ID, .testlos = 1, .worldid = -1, .interiorid = -1, .playerid = -1, .streamdistance = 30.0);
		AdminDuty[playerid] = true;
		SetPlayerColor(playerid, COLOR_ADMINDUTY);
		SetPVarFloat(playerid, "tempHealth", GetPlayerHealthEx(playerid));
		SetPlayerHealthEx(playerid, 50000);
		SendClientMessage(playerid, COLOR_LIGHTYELLOW2, "{878EE7}[INFO]{C8C8C8} Estas de servicio como administrador.");
	}

	return 1;
}

AdminDutyNickOff(playerid)
{
	SetPlayerChatName(playerid, GetPlayerCleanName(playerid));
	DestroyDynamic3DTextLabel(AdminName_label[playerid]);
	AdminName_label[playerid] = STREAMER_TAG_3D_TEXT_LABEL:0;
	return 1;
}

CMD:ckearplayer(playerid,params[])
{
	new targetid, string[128], newname[24],	newage, newsex, newmoney;

    if(sscanf(params, "us[24]ii", targetid, newname, newage, newsex)) {
		SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /ckearplayer [ID/Jugador] [Nuevo nombre] [Nueva edad] [Nuevo sexo (0 = Femenino | 1 = Masculino)]");
		SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El comando solicitar� una confirmaci�n tras completar los par�metros.");
		return 1;
	}
   	if(!IsPlayerLogged(targetid))
   	    return SendClientMessage(playerid, COLOR_YELLOW2, "ID/Jugador inv�lido.");
	if(newsex < 0 || newsex > 1)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "�Elige un sexo v�lido (0 = Femenino | 1 = Masculino)!");
	if(newage < 1 || newage > 100)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "�S�lo se permite una edad de 1 a 100 a�os!");
	if(!IsNameRoleplayValid(newname))
		return SendClientMessage(playerid, COLOR_YELLOW2, "Nombre inv�lido. Debe seguir el formato esperado para un roleplay. Ej: Roberto_Martinez.");

	if(GetPVarInt(playerid, "ckeandoplayer") == 0)
	{
		SendFMessage(playerid, COLOR_INFO, 		"=============="COLOR_EMB_GREY" CKear al jugador %s {878EE7}==============", GetPlayerCleanName(targetid));
		SendFMessage(playerid, COLOR_INFO, 		"Nuevo nombre:{C8C8C8} %s {f5a120}| {878EE7}Nueva edad:{C8C8C8} %d {f5a120}| {878EE7}Nuevo sexo:{C8C8C8} %d.", newname, newage, newsex);
		SendClientMessage(playerid, COLOR_INFO, "[INFO]{C8C8C8} Para realizar el CK, vuelve a ingresar el comando con los mismos par�metros.");
		SendFMessage(playerid, COLOR_INFO, 		"[INFO]{C8C8C8} Para {f5a120}cancelarlo{C8C8C8}, relogea o utiliza el comando {f5a120} /setpvarint %d ckeandoplayer 0 {C8C8C8}.", playerid);
		SendClientMessage(playerid, COLOR_INFO, "============================================================");
		SetPVarInt(playerid, "ckeandoplayer", PlayerInfo[targetid][pID]);
		return 1;
	}

	if(GetPVarInt(playerid, "ckeandoplayer") == PlayerInfo[targetid][pID])
	{
		KeyChain_OnPlayerCharacterKill(targetid, .notifyid = playerid);
		Phone_DeletePhoneForPlayer(targetid);

		GivePlayerCash(targetid, PlayerInfo[targetid][pBank]);
		PlayerInfo[targetid][pBank] = 0;

		Faction_SetPlayer(targetid, 0, 0);

		SavePlayerJobData(targetid, 1, 1);
		ResetJobVariables(targetid);
		SetPlayerJob(targetid, 0);
		ResetThiefVariables(targetid);

		SetHandItemAndParam(targetid, HAND_RIGHT, 0, 0);
		SetHandItemAndParam(targetid, HAND_LEFT, 0, 0);
		Back_SetItemAndParam(playerid, 0, 0, 0);
		Container_Empty(PlayerInfo[targetid][pContainerID]);

		PlayerInfo[targetid][pJailed] = JAIL_NONE;
		PlayerInfo[targetid][pJailTime] = 0;
		ResetPlayerWantedLevelEx(targetid);
		
 		PlayerInfo[targetid][pFlyLic] = 0;
 		PlayerInfo[targetid][pCarLic] = 0;
		PlayerInfo[targetid][pWepLic] = 0;

		newmoney = floatround(GetPlayerCash(targetid)*CK_MONEY_RATIO, floatround_ceil);
    	PlayerInfo[targetid][pBank] = newmoney;
    	SetPlayerCash(targetid, 0);

		format(string, sizeof(string), "[STAFF] el administrador %s ha CKeado a %s. (Nuevo nombre: %s)", GetPlayerCleanName(playerid), GetPlayerCleanName(targetid), newname);
		AdministratorMessage(COLOR_ADMINCMD, string, 2);
		SendFMessage(targetid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" El administrador %s ha CKeado tu cuenta. Tu nuevo nombre es %s.", GetPlayerCleanName(playerid), newname);
		SendClientMessage(targetid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Se vendieron automaticamente todos tus bienes econ�micos (casa, negocio, autos).");
		SendFMessage(targetid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Tu nuevo balance bancario es de $%d (85 por ciento de todo tu patrimonio anterior).", newmoney);

		SetPlayerName(targetid, newname);
    	PlayerInfo[targetid][pSex] = newsex;
    	PlayerInfo[targetid][pAge] = newage;

    	SetPVarInt(playerid, "ckeandoplayer", 0);
    	return 1;
	}
	return 1;
}

CMD:admins(playerid, params[])
{
	new AdminCount = 0;

	
	foreach(new id : Player) {
		if(PlayerInfo[id][pAdmin] > 0 && PlayerInfo[id][pAdmin] < 21) {

			if(AdminCount == 0 && PlayerInfo[playerid][pAdmin] < 1){
				SendClientMessage(playerid, COLOR_LIGHTGREEN, "===================[ADMINISTRADORES EN SERVICIO]======================");
			}
			else if(AdminCount == 0){
				SendClientMessage(playerid, COLOR_LIGHTGREEN, "====================[ADMINISTRADORES CONECTADOS]======================");
			}

			if(PlayerInfo[id][pAdmin] == 1){
				SendFMessage(playerid, COLOR_LIGHTGREEN, "{878EE7}Helper:{C8C8C8} %s - (%s).", GetPlayerCleanName(id), PlayerInfo[id][aNick]);
			}
			else {
				if(PlayerInfo[playerid][pAdmin] > 1) {
					switch(BitFlag_Get(p_toggle[id], FLAG_TOGGLE_ADMINMSGS))
					{
						case false: SendFMessage(playerid, COLOR_INFO, "{878EE7}Admin n. %d:{C8C8C8} %s - (%s)  {878EE7}|{C8C8C8} Mensajes administrativos: {FF0000}deshabilitados{C8C8C8}.", PlayerInfo[id][pAdmin], GetPlayerCleanName(id), PlayerInfo[id][aNick]);
						default: SendFMessage(playerid, COLOR_INFO, "{878EE7}Admin n. %d:{C8C8C8} %s - (%s)  {878EE7}|{C8C8C8} Mensajes administrativos: {3CB371}habilitados{C8C8C8}.", PlayerInfo[id][pAdmin], GetPlayerCleanName(id), PlayerInfo[id][aNick]);
					}
				}
				else {
					SendFMessage(playerid, COLOR_LIGHTGREEN, "{878EE7}Admin:{C8C8C8} %s", PlayerInfo[id][aNick]);
				}
			}
			AdminCount++;
		}
	}
	if (AdminCount == 0)
		return SendClientMessage(playerid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" No hay administradores conectados o en servicio.");
	else {
		SendClientMessage(playerid, COLOR_LIGHTGREEN, "=======================================================================");
	}
	return 1;
}

CMD:mute(playerid, params[])
{
	new targetid, string[128];

    if(sscanf(params, "u", targetid))
		return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /mute [ID/Jugador]");
    if(!IsPlayerConnected(targetid) || targetid == INVALID_PLAYER_ID)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID inv�lida.");

	if(Muted[targetid] == 0) 
	{
		Muted[targetid] = 1;
		format(string, sizeof(string), "[STAFF] El administrador %s ha muteado a %s.", GetPlayerCleanName(playerid), GetPlayerCleanName(targetid));
		AdministratorMessage(COLOR_ADMINCMD, string, 2);
		SendFMessage(targetid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Has sido muteado por %s.", GetPlayerCleanName(playerid));
	} 
	else 
	{
		Muted[targetid] = 0;
		format(string, sizeof(string), "[STAFF] El administrador %s ha desmuteado a %s.", GetPlayerCleanName(playerid), GetPlayerCleanName(targetid));
		AdministratorMessage(COLOR_ADMINCMD, string, 2);
		SendFMessage(targetid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Has sido desmuteado por %s.", GetPlayerCleanName(playerid));
	}
	return 1;
}

CMD:muteb(playerid, params[])
{
	new string[128],
 		minutes,
		targetid;

	if(sscanf(params, "ui", targetid, minutes))
	    return SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" /muteb [ID/Jugador] [minutos]");
	if(!IsPlayerConnected(targetid) || targetid == INVALID_PLAYER_ID)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR] "COLOR_EMB_GREY" ID inv�lida.");
	if(minutes >= 30 || minutes < 0)
		return SendClientMessage(playerid, COLOR_YELLOW2, "No puedes mutear por menos de 0 minutos o m�s de 30.");
		
	if(minutes > 0)	
	{
		SendFMessage(targetid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Tu canal '/b' ha sido muteado por %s durante %d minutos.", GetPlayerCleanName(playerid), minutes);
		format(string, sizeof(string), "[STAFF] el administrador %s ha muteado el canal '/b' de %s por %d minutos.", GetPlayerCleanName(playerid), GetPlayerCleanName(targetid), minutes);
		AdministratorMessage(COLOR_ADMINCMD, string, 2);
		PlayerInfo[targetid][pMuteB] = 60 * minutes;
	} 
	else 
	{
		SendFMessage(targetid, COLOR_INFO, "[INFO] "COLOR_EMB_GREY" Tu canal '/b' ha sido desmuteado por %s.", GetPlayerCleanName(playerid));
		format(string, sizeof(string), "[STAFF] el administrador %s ha desmuteadomuteado el canal '/b' de %s.", GetPlayerCleanName(playerid), GetPlayerCleanName(targetid));
		AdministratorMessage(COLOR_ADMINCMD, string, 2);
		PlayerInfo[targetid][pMuteB] = 0;
	} 
	return 1;
}

public OnPlayerClickMap(playerid, Float:fX, Float:fY, Float:fZ)
{
	if(PlayerInfo[playerid][pAdmin] >= 2 && AdminDuty[playerid])
	{
		SetPlayerPosFindZ(playerid, fX, fY, fZ + 0.5);
		SetPlayerInterior(playerid, 0);
		SetPlayerVirtualWorld(playerid, 0);
	}

	return 1;
}