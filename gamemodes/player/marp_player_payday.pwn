#if defined _marp_player_payday_inc
	#endinput
#endif
#define _marp_player_payday_inc

#include <YSI_Coding\y_hooks>

static String:gPaydayPlayerString[MAX_PLAYERS] = {STRING_NULL, ...};

hook OnPlayerDisconnect(playerid, reason)
{
	if(gPaydayPlayerString[playerid] != STRING_NULL)
	{
		str_release(gPaydayPlayerString[playerid]);
		gPaydayPlayerString[playerid] = STRING_NULL;
	}
	return 1;
}

Payday(playerid)
{
	if(!IsPlayerLogged(playerid))
		return 0;
	if(gPaydayPlayerString[playerid] != STRING_NULL) {
		str_release(gPaydayPlayerString[playerid]);
	}

	new line[144], paydayStatement[2048];

	//=============================EMPLEO===================================

	if(PlayerInfo[playerid][pPayCheck])
	{
		format(line, sizeof(line), " \n"COLOR_EMB_USAGE"[Empleo]\tIngresos: $%i.\n", PlayerInfo[playerid][pPayCheck]);
		strcat(paydayStatement, line, sizeof(paydayStatement));
	}

	/*_______________________________FACCION______________________________*/

	new factionPay;

	if(PlayerInfo[playerid][pFaction])
	{
		factionPay = Faction_GetRankSalary(PlayerInfo[playerid][pFaction], PlayerInfo[playerid][pRank]);
		PlayerInfo[playerid][pPayCheck] += factionPay;

		if(factionPay)
		{
			format(line, sizeof(line), " \n"COLOR_EMB_USAGE"[%s]\nSalario: $%i.\n", FactionInfo[PlayerInfo[playerid][pFaction]][fName], factionPay);
			strcat(paydayStatement, line, sizeof(paydayStatement));
		}
	}

	/*____________________________SALARIO FIJO____________________________*/

	if(!PlayerInfo[playerid][pCantWork] && !factionPay)
	{
		PlayerInfo[playerid][pPayCheck] += 500;
		strcat(paydayStatement, " \n"COLOR_EMB_USAGE"[Gobierno]\nAyuda social: $500.\n", sizeof(paydayStatement));
	}

	//_____________INGRESOS E IMPUESTOS DE BIENES PERSONALES______________*/

	new income, tax;
	KeyChain_OnPlayerPayday(playerid, income, tax);

	//========================CONTROL DE BARRIOS============================

	if(Faction_IsValidId(PlayerInfo[playerid][pFaction]))
	{
		if(Faction_HasTag(PlayerInfo[playerid][pFaction], FAC_TAG_ALLOW_GANGZONE) && PlayerInfo[playerid][pRank] == 1)
		{
			new gangProfits = GetGangZoneIncome(playerid);
			Faction_GiveMoney(PlayerInfo[playerid][pFaction], gangProfits);
			format(line, sizeof(line), " \n"COLOR_EMB_USAGE"[%s]\nIngresos faccionarios por control de barrios: $%i.\n", FactionInfo[PlayerInfo[playerid][pFaction]][fName], gangProfits);
			strcat(paydayStatement, line, sizeof(paydayStatement));
		}
	}

	//========================COSTOS BANCARIOS==============================

	new banktax = floatround(PlayerInfo[playerid][pBank] * 0.0003, floatround_ceil);

	if(banktax < 50) {
		banktax = 50; // m�nimo de 50 pesos por tener la cuenta abierta
	}

	//============================INGRESOS==================================

	new newbank = PlayerInfo[playerid][pBank] + PlayerInfo[playerid][pPayCheck] + income - tax - banktax;

	Faction_GiveMoney(FAC_GOB, tax);

	format(line, sizeof(line), " \n"COLOR_EMB_USAGE"[Gobierno]\nImpuestos por bienes personales: $-%i.\n", tax);
	strcat(paydayStatement, line, sizeof(paydayStatement));

	format(line, sizeof(line), " \n"COLOR_EMB_USAGE"[Propiedades]\nOtros ingresos/egresos: $%i.\n", income);
	strcat(paydayStatement, line, sizeof(paydayStatement));

	format(line, sizeof(line), " \n"COLOR_EMB_USAGE"[Banco]\nCosto de mantenimiento de cuenta: $%i.\nBalance anterior: $%i - Nuevo balance: $%i.\n", banktax, PlayerInfo[playerid][pBank], newbank);
	strcat(paydayStatement, line, sizeof(paydayStatement));

	//======================================================================

	PlayerInfo[playerid][pBank] = newbank;
	PlayerInfo[playerid][pPayCheck] = 0;
	PlayerInfo[playerid][pExp]++;

	if(PlayerInfo[playerid][pCantWork] > 0 && PlayerInfo[playerid][pJailed] == JAIL_NONE) {
		PlayerInfo[playerid][pCantWork] = 0;
	}

	if(PlayerInfo[playerid][pJobTime] > 0)	{
		PlayerInfo[playerid][pJobTime]--; // Reducimos la cantidad de tiempo que tiene que esperar para poder tomar otro empleo.
	}

	new expamount = (PlayerInfo[playerid][pLevel] + 1) * ServerInfo[svLevelExp], str[128];

	if(PlayerInfo[playerid][pExp] < expamount)
	{
		format(str, sizeof(str), " �D�a de pago! "COLOR_EMB_WHITE"m�s detalles con '/verpago'. (( Experiencia %i/%i ))", PlayerInfo[playerid][pExp], expamount);
		SendClientMessage(playerid, COLOR_LIGHTYELLOW2, str);

		format(str, sizeof(str), "~g~~h~ �D�a de pago!~w~~n~m�s detalles con '/verpago'.~n~(( Experiencia %i/%i ))", PlayerInfo[playerid][pExp], expamount);
		Noti_Create(playerid, .time = 6000, .text = str);
	}
	else
	{
		PlayerInfo[playerid][pExp] = 0;
		PlayerInfo[playerid][pLevel]++;
		SetPlayerScore(playerid, PlayerInfo[playerid][pLevel]);
		expamount = (PlayerInfo[playerid][pLevel] + 1) * ServerInfo[svLevelExp];

		format(str, sizeof(str), " �D�a de pago! "COLOR_EMB_WHITE"m�s detalles con '/verpago'. {E0EA64} �Tu cuenta subi� al nivel %i! "COLOR_EMB_WHITE"(( Experiencia %i/%i ))", PlayerInfo[playerid][pLevel], PlayerInfo[playerid][pExp], expamount);
		SendClientMessage(playerid, COLOR_LIGHTYELLOW2, str);

		format(str, sizeof(str), "~g~~h~ �D�a de pago!~w~~n~m�s detalles con '/verpago'", PlayerInfo[playerid][pLevel]);
		Noti_Create(playerid, .time = 5000, .text = str);

		format(str, sizeof(str), "~y~ �Tu cuenta subi� al nivel %i!~n~~w~(( Experiencia %i/%i ))", PlayerInfo[playerid][pLevel], PlayerInfo[playerid][pExp], expamount);
		Noti_Create(playerid, .time = 7000, .text = str);
	}

	gPaydayPlayerString[playerid] = str_new(paydayStatement);
	str_acquire(gPaydayPlayerString[playerid]);
	return 1;
}

Payday_AddStatementDeferedInfo(playerid, const info[])
{
	if(gPaydayPlayerString[playerid] == STRING_NULL)
		return 0;

	new String:str = str_new(info);
	str_append(gPaydayPlayerString[playerid], str);
	return 1;
}

CMD:payday(playerid, params[]) {
	return Payday(playerid);
}

CMD:verpago(playerid, params[])
{
	static paydayStatement[4096];

	if(gPaydayPlayerString[playerid] == STRING_NULL)
		return SendClientMessage(playerid, COLOR_YELLOW2, "No tienes ning�n resumen reciente del �ltimo pago (recuerda que el resumen se borra al desconectarte).");

	str_get(gPaydayPlayerString[playerid], paydayStatement, sizeof(paydayStatement));
	Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_LIST, ""COLOR_EMB_WHITE"Resumen del �ltimo pago", paydayStatement, "Cerrar", "");
	return 1;
}