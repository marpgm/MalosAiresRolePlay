#if defined _marp_player_jail_inc
	#endinput
#endif
#define _marp_player_jail_inc

hook OnPlayerGlobalUpdate(playerid)
{
	if(PlayerInfo[playerid][pJailed] == JAIL_NONE || PlayerInfo[playerid][pJailed] == JAIL_IC_GOB)
		return 0;

	if(PlayerInfo[playerid][pJailTime] != 0)
	{
		new string[128];
		PlayerInfo[playerid][pJailTime]--;
		/*if(!IsPlayerAFK(playerid))
		{
			PlayerInfo[playerid][pJailTime]--;
			new string[128];
		*/
		if(PlayerInfo[playerid][pJailTime] < 60) {
			format(string, sizeof(string), "~n~~n~~n~~n~~n~~n~~n~~w~Tiempo restante: ~g~%i segundos.", PlayerInfo[playerid][pJailTime]);
		} else {
			format(string, sizeof(string), "~n~~n~~n~~n~~n~~n~~n~~w~Tiempo restante: ~g~%i minutos.", PlayerInfo[playerid][pJailTime] / 60);
		}

		GameTextForPlayer(playerid, string, 2000, 3);
		
	}

	if(PlayerInfo[playerid][pJailTime] == 0)
	{
	    switch(PlayerInfo[playerid][pJailed])
		{
	        case JAIL_IC_PMA:
			{
				SendClientMessage(playerid, COLOR_LIGHTYELLOW2,"{878EE7}[Detenci�n]{C8C8C8} has finalizado tu condena y est�s en libertad, puedes retirarte.");
				TeleportPlayerTo(playerid, POS_POLICE_FREEDOM_X, POS_POLICE_FREEDOM_Y, POS_POLICE_FREEDOM_Z, 270.0, 3, 1034, .forceLoadingTime = true);
  			}
  			case JAIL_IC_PMA_EAST:
			{
				SendClientMessage(playerid, COLOR_LIGHTYELLOW2,"{878EE7}[Detenci�n]{C8C8C8} has finalizado tu condena y est�s en libertad, puedes retirarte.");
				TeleportPlayerTo(playerid, POS_POLICE_FREEDOM2_X, POS_POLICE_FREEDOM2_Y, POS_POLICE_FREEDOM2_Z, 270.0, 0, 0, .forceLoadingTime = true);
  			}
	        case JAIL_OOC:
			{
				SendClientMessage(playerid, COLOR_LIGHTYELLOW2,"{878EE7}[Castigo OOC]{C8C8C8} has finalizado tu castigo, puedes irte ahora.");
				SetPlayerHealthEx(playerid, 100.0);
				TeleportPlayerTo(playerid, POS_SPAWN_X, POS_SPAWN_Y, POS_SPAWN_Z, POS_SPAWN_A, POS_SPAWN_INTERIOR, POS_SPAWN_WORLD, .forceLoadingTime = true, .syncPlayerNewPosData = true, .disableSyncOnExitSeconds = 0);
	        }
	        case JAIL_IC_PRISON:
			{
				SendClientMessage(playerid, COLOR_LIGHTYELLOW2,"{878EE7}[Prisi�n]{C8C8C8} has finalizado tu condena y est�s en libertad, puedes retirarte.");
				TeleportPlayerTo(playerid, POS_PRISON_FREEDOM_X, POS_PRISON_FREEDOM_Y, POS_PRISON_FREEDOM_Z, 285.0, 0, 0, .forceLoadingTime = true);
  			}
		}

		PlayerInfo[playerid][pJailed] = JAIL_NONE;
	}

	return 1;
}