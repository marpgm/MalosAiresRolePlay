#if defined _marp_player_drugs_included
	#endinput
#endif
#define _marp_player_drugs_included

#include <YSI_Coding\y_hooks>

static Drugs_PlayerWeather[MAX_PLAYERS] = {-1, ...};
static bool:Drugs_PlayerEffects[MAX_PLAYERS][DRUG_IDS_AMOUNT] = {false, ...};
static Drugs_PlayerTimers[MAX_PLAYERS][DRUG_IDS_AMOUNT] = {0, ...};

static const Drugs_EffectTimerEnd[DRUG_IDS_AMOUNT][MAX_FUNC_NAME] = {
    {"NULL"},
    {"Drug_MarijuanaEffectEnd"},
    {"Drug_CocaineEffectEnd"},
    {"Drug_EcstasyEffectEnd"},
    {"Drug_LSDEffectEnd"}
};

hook OnPlayerSpawn(playerid)
{
    Drugs_ResetEffects(playerid);
    SyncPlayerWeather(playerid);
    return 1;
}

IsPlayerDrugged(playerid)
{
	return (Drugs_PlayerEffects[playerid][DRUG_ID_MARIJUANA] || Drugs_PlayerEffects[playerid][DRUG_ID_COCAINE] || 
		Drugs_PlayerEffects[playerid][DRUG_ID_ECSTASY] || Drugs_PlayerEffects[playerid][DRUG_ID_LSD]);
}

Drugs_GetPlayerWeather(playerid) {
    return Drugs_PlayerWeather[playerid];
}

Drugs_OnPlayerUsed(playerid, drugid, duration, weatherid) 
{
    if(drugid == DRUG_ID_NONE)
        return 0;

    Drugs_PlayerWeather[playerid] = weatherid;
    
    if(weatherid != -1 && drugid != DRUG_ID_LSD) {
        SetPlayerWeather(playerid, weatherid);
    }
    if(drugid == DRUG_ID_LSD) {
        SetTimerEx("Drug_LSDEffectInit", 10*1000, false, "iiii", playerid, PlayerInfo[playerid][pID], duration, weatherid);        
    }

    if(Drugs_PlayerTimers[playerid][drugid]) {
        KillTimer(Drugs_PlayerTimers[playerid][drugid]);
    }

    Drugs_PlayerEffects[playerid][drugid] = true;
    Drugs_PlayerTimers[playerid][drugid] = SetTimerEx(Drugs_EffectTimerEnd[drugid], duration*60*1000, false, "i", playerid);
    return 1;
}

forward Drug_MarijuanaEffectEnd(playerid);
public Drug_MarijuanaEffectEnd(playerid)
{
    BN_PlayerEat(playerid, -20);
    Drugs_PlayerEffects[playerid][DRUG_ID_MARIJUANA] = false;
    Drugs_PlayerTimers[playerid][DRUG_ID_MARIJUANA] = 0;
    SendClientMessage(playerid, COLOR_YELLOW2, "El efecto de la marihuana esta finalizando y comienzas a tener hambre.");
    SyncPlayerWeather(playerid);
    return 1;
}

forward Drug_CocaineEffectEnd(playerid);
public Drug_CocaineEffectEnd(playerid)
{
    Drugs_PlayerEffects[playerid][DRUG_ID_COCAINE] = false;
    Drugs_PlayerTimers[playerid][DRUG_ID_COCAINE] = 0;
    SyncPlayerWeather(playerid);
    return 1;
}

forward Drug_EcstasyEffectEnd(playerid);
public Drug_EcstasyEffectEnd(playerid)
{
    BN_PlayerDrink(playerid, -30);
    Drugs_PlayerEffects[playerid][DRUG_ID_ECSTASY] = false;
    Drugs_PlayerTimers[playerid][DRUG_ID_ECSTASY] = 0;
    SendClientMessage(playerid, COLOR_YELLOW2, "El efecto del extasis esta finalizando y comienzas a tener sed.");
    SyncPlayerWeather(playerid);
    return 1;
}

forward Drug_LSDEffectInit(playerid, playersqlid, duration, weatherid);
public Drug_LSDEffectInit(playerid, playersqlid, duration, weatherid)
{
	if(!IsPlayerLogged(playerid) || PlayerInfo[playerid][pID] != playersqlid)
		return 0;

    SetPlayerWeather(playerid, weatherid);
    SetPlayerDrunkLevel(playerid, duration*60*50);
    return 1;
}

forward Drug_LSDEffectEnd(playerid);
public Drug_LSDEffectEnd(playerid)
{
    SetPlayerDrunkLevel(playerid, 0);
    Drugs_PlayerEffects[playerid][DRUG_ID_LSD] = false;
    Drugs_PlayerTimers[playerid][DRUG_ID_LSD] = 0;
    SyncPlayerWeather(playerid);
    return 1;
}

Drugs_ResetEffects(playerid) 
{
	Drugs_PlayerWeather[playerid] = -1;
	Drugs_PlayerEffects[playerid] = bool:{false, false, false, false, false};

	for(new i = 1; i < DRUG_IDS_AMOUNT; i++)
	{
		if(Drugs_PlayerTimers[playerid][i])
		{
			KillTimer(Drugs_PlayerTimers[playerid][i]);
			Drugs_PlayerTimers[playerid][i] = 0;
		}		
	}
    return 1;
}

hook OnPlayerDisconnect(playerid, reason)
{
	Drugs_ResetEffects(playerid);
    return 1;
}