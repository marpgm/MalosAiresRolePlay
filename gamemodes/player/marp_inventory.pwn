#if defined _marp_inventory_included
	#endinput
#endif
#define _marp_inventory_included

#define CONTAINER_INV_SPACE   	25

PrintInvForPlayer(playerid, targetid)
{
	if(!Container_Show(playerid, CONTAINER_TYPE_INV, PlayerInfo[playerid][pContainerID], targetid, 0))
		SendClientMessage(targetid, COLOR_YELLOW2, "[SCRIPT ERROR]: Error al mostrar el contenedor del inventario. Reportar a un scripter.");
	return 1;
}

static const KEY_CUSTOM_INV_SAVE = KEY_SPRINT | KEY_YES;
static const KEY_CUSTOM_INV_SAVE_I = KEY_WALK | KEY_YES;

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if(KEY_PRESSED_MULTI(KEY_CUSTOM_INV_SAVE))
	{
		cmd_inventario(playerid, "guardar");
		return ~1;
	}

	if(KEY_PRESSED_MULTI(KEY_CUSTOM_INV_SAVE_I))
	{
		cmd_inventario(playerid, "guardari");
		return ~1;
	}

	if(KEY_PRESSED_SINGLE(KEY_YES))
	{
		cmd_inventario(playerid, "");
		return ~1;
	}

	return 1;
}

CMD:inv(playerid, params[]) {
	return cmd_inventario(playerid, params);
}

Inv_SaveItem(playerid, itemid, hand)
{
	if(!ItemModel_IsValidId(itemid))
		return 0;
	if(PlayerInfo[playerid][pDisabled] != DISABLE_NONE)
		return SendClientMessage(playerid, COLOR_YELLOW2, "No puedes hacerlo en este momento.");
	if(!ItemModel_HasTag(itemid, ITEM_TAG_INV))
		return SendClientMessage(playerid, COLOR_YELLOW2, "Ese item no se puede guardar en el inventario.");
	if(Item_IsHandlingCooldownOn(playerid))
		return SendClientMessage(playerid, COLOR_YELLOW2, "�Debes esperar un tiempo antes de volver a interactuar con otro item!");

	if(!Container_AddItemAndParam(PlayerInfo[playerid][pContainerID], itemid, GetHandParam(playerid, hand)))
		return SendClientMessage(playerid, COLOR_YELLOW2, "No hay suficiente espacio libre en tu inventario.");

	SendFMessage(playerid, COLOR_WHITE, "Has guardado [%s - %s: %i] en tu inventario.", ItemModel_GetName(itemid), ItemModel_GetParamName(itemid), GetHandParam(playerid, hand));	
	
	if(ItemModel_GetType(itemid) == ITEM_WEAPON) {
		ServerFormattedLog(LOG_TYPE_ID_WEAPONS, .entry="/inv", .playerid=playerid, .params=<"guardar %i %s", GetHandParam(playerid, hand), ItemModel_GetName(itemid)>);
	}
	
	SetHandItemAndParam(playerid, hand, 0, 0);
	Item_ApplyHandlingCooldown(playerid);
	return 1;
}

CMD:inventario(playerid, params[])
{
	new subcmd[24];

    if(sscanf(params, "s[24]", subcmd))
	{
		SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" (/inv)entario [comando] (guardar - guardari). Recuerda que puedes usar el atajo de tecla ~k~~CONVERSATION_YES~.");

		if(!Container_Show(playerid, CONTAINER_TYPE_INV, PlayerInfo[playerid][pContainerID], playerid))
			return SendClientMessage(playerid, COLOR_YELLOW2, "[SCRIPT ERROR]: Error al mostrar el contenedor del inventario. Reportar a un administador.");
    }
	else
	{
		if(!strcmp(subcmd, "guardar", true)) {
			Inv_SaveItem(playerid, GetHandItem(playerid, HAND_RIGHT), HAND_RIGHT);
		} else if(!strcmp(subcmd, "guardari", true)) {
			Inv_SaveItem(playerid, GetHandItem(playerid, HAND_LEFT), HAND_LEFT);
		} else {
			SendClientMessage(playerid, COLOR_USAGE, "[USO] "COLOR_EMB_GREY" (/inv)entario [comando] (guardar - guardari)");
		}
	}
	return 1;
}

DestroyPlayerInventory(playerid)
{
	if(PlayerInfo[playerid][pContainerSQLID] > 0 && PlayerInfo[playerid][pContainerID] > 0)
	{
		Container_Destroy(PlayerInfo[playerid][pContainerID]);
	}
	return 1;
}

Dialog:Dlg_Show_Inv_Container(playerid, response, listitem, inputtext[])
{
	new container_id = Container_Selection[playerid][csId];

	ResetContainerSelection(playerid);

	if(response)
	{
        new itemid,
            itemparam,
            free_hand = SearchFreeHand(playerid);

		if(free_hand == -1)
		    return SendClientMessage(playerid, COLOR_YELLOW2, "No puedes agarrar el item ya que tienes ambas manos ocupadas.");

		if(Container_TakeItem(container_id, listitem, itemid, itemparam))
		{
			SetHandItemAndParam(playerid, free_hand, itemid, itemparam); // Creaci�n l�gica y grafica en la mano.
            Item_ApplyHandlingCooldown(playerid);
            new str[128];
			format(str, sizeof(str), "Toma un/a %s de su inventario.", ItemModel_GetName(itemid));
			PlayerCmeMessage(playerid, 15.0, 5000, str);

			if(ItemModel_GetType(itemid) == ITEM_WEAPON)
				ServerFormattedLog(LOG_TYPE_ID_WEAPONS, .entry="/inv", .playerid=playerid, .params=<"sacar %i %s", itemparam, ItemModel_GetName(itemid)>);
				
		} else {
	        SendClientMessage(playerid, COLOR_YELLOW2, "Inventario vacio o el slot es inv�lido.");
		}
	}
	return 1;
}