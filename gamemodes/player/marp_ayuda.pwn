#if defined marp_ayuda_inc
	#endinput
#endif
#define marp_ayuda_inc

CMD:ayuda(playerid, params[]){
    return Help_Show(playerid);
}

CMD:help(playerid, params[]){
    return Help_Show(playerid);
}

CMD:comandos(playerid, params[]){
    return Help_Show(playerid);
}

CMD:cmds(playerid, params[]){
    return Help_Show(playerid);
}

Help_Show(playerid)
{
    Dialog_Show(playerid, DLG_HELP, DIALOG_STYLE_LIST,
        "Soporte sobre el servidor",
        "Cuenta\n\
        General\n\
        Empleos\n\
        Facciones\n\
        Veh�culos\n\
        Negocios\n\
        Casas",
        "Ver",
        "Salir"
    );
    return 1;
}

Dialog:DLG_HELP(playerid, response, listitem, inputtext[])
{
    if(!response)
        return 1;
    
    switch(listitem)
    {
        case 0: Help_ShowAccount(playerid);
        case 1: Help_ShowGeneral(playerid);
        case 2: Help_ShowJobs(playerid);
        case 3: Help_ShowFactions(playerid);
        case 4: Help_ShowVehicles(playerid);
        case 5: Help_ShowBusiness(playerid);
        case 6: Help_ShowHouses(playerid);
    }
    return 1;
}

Help_ShowAccount(playerid)
{
    Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
        "[Soporte] Comandos sobre la cuenta de usuario.",
        "En este apartado encontrar�s todo lo que necesitas saber sobre tu cuenta.\n\
        \n \n\
        {F0FF33}COMANDOS:\n\
        {FFFFFF}/changepass, /toggle, /stats, /verpago, /hora, /inv, /llavero.\n\
        \n \n\
        {E00C0C}USO:\n\
        {00D4FF}/changepass: {FFFFFF}Sirve para cambiar la contrase�a de tu cuenta.\n\
        {00D4FF}/toggle: {FFFFFF}Sirve para configurar ciertos apartados del servidor.\n\
        {00D4FF}/stats: {FFFFFF}Sirve para ver la informaci�n de tu cuenta.\n\
        {00D4FF}/hora: {FFFFFF}Este comando te muestra la hora del servidor.\n\
        {00D4FF}/inv: {FFFFFF}Este comando sirve para ver tu inventario y para tomar cosas del mismo.\n\
        {00D4FF}/llavero: {FFFFFF}Este comando sirve para ver todas las propiedades a las que tienes acceso.",
        "Cerrar", 
        ""
    );
    return 1;
}

Help_ShowGeneral(playerid)
{
    Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
        "[Soporte] Comandos generales del servidor.",
        "En este apartado encontrar�s todos los comandos de uso generico del servidor.\n\
        \n \n\
        {F0FF33}COMANDOS GENERALES:\n\
        {FFFFFF}/dar(i), /usar(i), /agarrar(i), /agarraruno, /mano, /mc, /comprar, /vender, /cla, /pagar,\n\
        {FFFFFF}/moneda, /dado, /toy, /mostrardoc, /mostrarlic, /mostrarced, /bidon, /inv, /pecho, /esp(alda),\n\
        {FFFFFF}/llenar, /yo, /donar, /desafiarpicada, /saludar, /examinar, /anim(aciones), /admins, /blackjack,\n\
        {FFFFFF}/garajepuerta, /edificiopuerta, /duda, /reportar.\n\
        \n \n\
        {F0FF33}COMANDOS DEL CHAT:\n\
        {FFFFFF}/mp, /vb, /g, /sus, /me, /do, /dop, /cme, /gooc, /b, /limpiarchat, /solidchat.\n\
        \n \n\
        {F0FF33}COMANDOS DEL TELEFONO:\n\
        {FFFFFF}/tel(efono), /t, /servicios.\n\
        \n \n\
        {F0FF33}COMANDOS DEL CAJERO/BANCO:\n\
        {FFFFFF}/verbalance, /depositar, /retirar, /transferir, /fverbalance, /fdepositar, /fretirar.",
        "Cerrar", 
        ""
    );
    return 1;
}

Help_ShowJobs(playerid)
{
    if(PlayerInfo[playerid][pJob] == 1)
    {   Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre el empleo.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu empleo.\n\
            \n \n\
            {FFFFFF}Bienvenido al trabajo de Moto Delivery, este trabajo consiste en ir desde la central de\n\
            {FFFFFF}repartidores hacia distintos puntos en el mapa entregando paquetes. Para comenzar a trabajar\n\
            {FFFFFF}deber�s subirte a una moto y colocar el comando /trabajar.\n\
            \n \n\
            {F0FF33}COMANDOS:\n\
            {FFFFFF}/trabajar, /terminar, /tomarempleo, /verempleo, /dejarempleo, /consultarempleo.\n\
            \n \n\
            {E00C0C}USO:\n\
            {00D4FF}/trabajar: {FFFFFF}con este comando podr�s empezar tu jornada de trabajo.\n\
            {00D4FF}/terminar: {FFFFFF}con este comando podr�s terminar tu jornada de trabajo.\n\
            {00D4FF}/tomarempleo: {FFFFFF}con este comando pod�s tomar el puesto de trabajo en caso de no tenerlo.\n\
            {00D4FF}/verempleo: {FFFFFF}observa las estadisticas de tu empleo.\n\
            {00D4FF}/dejarempleo: {FFFFFF}utiliza este comando para renunciar a tu trabajo.",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else if(PlayerInfo[playerid][pJob] == 2)
    {   Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre el empleo.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu empleo.\n\
            \n \n\
            {FFFFFF}Bienvenido al trabajo de Taxista, este trabajo consiste en recorrer la ciudad con tu taxi en\n\
            {FFFFFF}espera de los llamados que puedan llegar para posteriormente, transportar a las personas a su destino.\n\
            {FFFFFF}Para comenzar a trabajar deber�s subirte a un taxi y colocar el comando /trabajar.\n\
            \n \n\
            {F0FF33}COMANDOS:\n\
            {FFFFFF}/trabajar, /terminar, /tomarempleo, /verempleo, /dejarempleo, /consultarempleo, /taxiverllamadas, /taxicancelar,\n\
            {FFFFFF}/taximetro.\n\
            \n \n\
            {E00C0C}USO:\n\
            {00D4FF}/trabajar: {FFFFFF}con este comando podr�s empezar tu jornada de trabajo.\n\
            {00D4FF}/terminar: {FFFFFF}con este comando podr�s terminar tu jornada de trabajo.\n\
            {00D4FF}/tomarempleo: {FFFFFF}con este comando pod�s tomar el puesto de trabajo en caso de no tenerlo.\n\
            {00D4FF}/verempleo: {FFFFFF}observa las estadisticas de tu empleo.\n\
            {00D4FF}/dejarempleo: {FFFFFF}utiliza este comando para renunciar a tu trabajo.\n\
            {00D4FF}/taxiverllamadas: {FFFFFF}este comando sirve para poder ver y atender las llamadas recientes.\n\
            {00D4FF}/taxicancelar: {FFFFFF}con este comando podr�s cancelar una llamada que ya atendiste.\n\
            {00D4FF}/taximetro: {FFFFFF}con este comando podr�s cobrarle a un usuario por el viaje.",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else if(PlayerInfo[playerid][pJob] == 3)
    {   Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre el empleo.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu empleo.\n\
            \n \n\
            {FFFFFF}Bienvenido al trabajo de Granjero, este trabajo consiste en recorrer la granja con ayuda de\n\
            {FFFFFF}una cosechadora y al finalizar entregar�s el producto conseguido. Para comenzar a trabajar\n\
            {FFFFFF}deber�s subirte a una cosechadora y colocar el comando /trabajar.\n\
            \n \n\
            {F0FF33}COMANDOS:\n\
            {FFFFFF}/trabajar, /terminar, /tomarempleo, /verempleo, /dejarempleo, /consultarempleo.\n\
            \n \n\
            {E00C0C}USO:\n\
            {00D4FF}/trabajar: {FFFFFF}con este comando podr�s empezar tu jornada de trabajo.\n\
            {00D4FF}/terminar: {FFFFFF}con este comando podr�s terminar tu jornada de trabajo.\n\
            {00D4FF}/tomarempleo: {FFFFFF}con este comando pod�s tomar el puesto de trabajo en caso de no tenerlo.\n\
            {00D4FF}/verempleo: {FFFFFF}observa las estadisticas de tu empleo.\n\
            {00D4FF}/dejarempleo: {FFFFFF}utiliza este comando para renunciar a tu trabajo.",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else if(PlayerInfo[playerid][pJob] == 4)
    {   Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre el empleo.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu empleo.\n\
            \n \n\
            {FFFFFF}Bienvenido al trabajo de Transportista, tu trabajo consiste en rellenar el stock de los distintos\n\
            {FFFFFF}negocios de la ciudad cuando sus due�os lo soliciten. Para comenzar a trabajar\n\
            {FFFFFF}deber�s subirte a una cami�n y colocar el comando /trabajar.\n\
            \n \n\
            {F0FF33}COMANDOS:\n\
            {FFFFFF}/trabajar, /terminar, /tomarempleo, /verempleo, /dejarempleo, /consultarempleo.\n\
            \n \n\
            {E00C0C}USO:\n\
            {00D4FF}/trabajar: {FFFFFF}con este comando podr�s empezar tu jornada de trabajo.\n\
            {00D4FF}/terminar: {FFFFFF}con este comando podr�s terminar tu jornada de trabajo.\n\
            {00D4FF}/tomarempleo: {FFFFFF}con este comando pod�s tomar el puesto de trabajo en caso de no tenerlo.\n\
            {00D4FF}/verempleo: {FFFFFF}observa las estadisticas de tu empleo.\n\
            {00D4FF}/dejarempleo: {FFFFFF}utiliza este comando para renunciar a tu trabajo.",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else if(PlayerInfo[playerid][pJob] == 5)
    {   Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre el empleo.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu empleo.\n\
            \n \n\
            {FFFFFF}Bienvenido al trabajo de Basurero, tu trabajo consiste en recorrer la ciudad por la ruta marcada y as�\n\
            {FFFFFF}recoger la basura de los distintos contenedores para posteriormente, llevarla al basural. Para comenzar a trabajar\n\
            {FFFFFF}deber�s subirte a una cami�n y colocar el comando /basurerocomenzar.\n\
            \n \n\
            {F0FF33}COMANDOS:\n\
            {FFFFFF}/basurerocomenzar, /basureroinvitar, /basurerotomarempleo, /basurerocancelar, /basurerorenunciar, /basureroinfo.\n\
            \n \n\
            {E00C0C}USO:\n\
            {00D4FF}/basurerocomenzar: {FFFFFF}con este comando podr�s empezar tu jornada de trabajo.\n\
            {00D4FF}/basureroinvitar: {FFFFFF}con este comando podr�s invitar a un jugador a realizar una ruta de trabajo.\n\
            {00D4FF}/basurerotomarempleo: {FFFFFF}con este comando pod�s tomar el puesto de trabajo en caso de no tenerlo.\n\
            {00D4FF}/basurerocancelar: {FFFFFF}con este comando podr�s cancelar la jornada de trabajo.\n\
            {00D4FF}/basurerorenunciar: {FFFFFF}utiliza este comando para renunciar a tu trabajo.\n\
            {00D4FF}/basureroinfo: {FFFFFF}este comando sirve para ver la informaci�n de tu trabajo y su din�mica.",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else if(PlayerInfo[playerid][pJob] == 6)
    {   Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre el empleo.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu empleo.\n\
            \n \n\
            {FFFFFF}Bienvenido al trabajo de Delincuente, tu trabajo consiste en retirar las pertenencias de valor de otras personas.\n\
            {FFFFFF}Ten en cuenta que es un trabajo riesgoso y que cuando menos te lo esperes la polic�a podr�a atraparte, es por ello\n\
            {FFFFFF}que debes ser cuidadoso a la hora de ejecutar tus cr�menes. Cuanto m�s trabajes m�s experiencia recibir�s y por ende\n\
            {FFFFFF}aprender�s nuevas t�cnicas para cometer hechos de mayor complejidad.\n\
            \n \n\
            {F0FF33}COMANDOS:\n\
            {FFFFFF}/hurtartienda, /asaltartienda, /hurtarcasa, /asaltarcasa, /barreta, /puente, /desarmar, /verexp, /grupoayuda.\n\
            \n \n\
            {E00C0C}USO:\n\
            {00D4FF}/hurtartienda: {FFFFFF}con este comando podr�s hurtar un objeto del stock del negocio.\n\
            {00D4FF}/asaltartienda: {FFFFFF}con este comando podr�s robar el dinero de la caja del negocio.\n\
            {00D4FF}/hurtarcasa: {FFFFFF}con este comando sustraer�s objetos de valor de una casa abierta.\n\
            {00D4FF}/asaltarcasa: {FFFFFF}con este comando robar�s el dinero que haya dentro de una casa abierta.\n\
            {00D4FF}/barreta: {FFFFFF}este comando sirve para romper la cerradura de un auto, necesitar�s una barreta.\n\
            {00D4FF}/puente: {FFFFFF}este comando sirve para realizar un puente en un veh�culo y as� encenderlo, ten�s que estar como conductor.\n\
            {00D4FF}/desarmar: {FFFFFF}con este comando podr�s desarmar el veh�culo que lograste robar, ten�s que ir a un desarmadero.\n\
            {00D4FF}/grupoayuda: {FFFFFF}en este comando ver�s toda la informaci�n relacionada con el grupo de robo al banco.",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else if(PlayerInfo[playerid][pJob] == 7)
    {   Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre el empleo.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu empleo.\n\
            \n \n\
            {FFFFFF}Bienvenido al trabajo de Cosechador de Materia Prima, este trabajo consiste en recorrer la granja con ayuda de\n\
            {FFFFFF}una cosechadora y al finalizar entregar�s el producto conseguido. Para comenzar a trabajar\n\
            {FFFFFF}deber�s subirte a una cosechadora y colocar el comando /trabajar.\n\
            \n \n\
            {F0FF33}COMANDOS:\n\
            {FFFFFF}/trabajar, /terminar, /tomarempleo, /verempleo, /dejarempleo, /consultarempleo.\n\
            \n \n\
            {E00C0C}USO:\n\
            {00D4FF}/trabajar: {FFFFFF}con este comando podr�s empezar tu jornada de trabajo.\n\
            {00D4FF}/terminar: {FFFFFF}con este comando podr�s terminar tu jornada de trabajo.\n\
            {00D4FF}/tomarempleo: {FFFFFF}con este comando pod�s tomar el puesto de trabajo en caso de no tenerlo.\n\
            {00D4FF}/verempleo: {FFFFFF}observa las estadisticas de tu empleo.\n\
            {00D4FF}/dejarempleo: {FFFFFF}utiliza este comando para renunciar a tu trabajo.",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else if(PlayerInfo[playerid][pJob] == 8)
    {   Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre el empleo.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu empleo.\n\
            \n \n\
            {FFFFFF}Bienvenido al trabajo de Piloto, este trabajo consiste en viajar a distintos aeropuertos\n\
            {FFFFFF}transportando cargamento. Para comenzar a trabajar\n\
            {FFFFFF}deber�s subirte a un avi�n de tu rango y colocar el comando /trabajar.\n\
            \n \n\
            {F0FF33}COMANDOS:\n\
            {FFFFFF}/trabajar, /terminar, /tomarempleo, /verempleo, /dejarempleo, /consultarempleo, /ar.\n\
            \n \n\
            {E00C0C}USO:\n\
            {00D4FF}/trabajar: {FFFFFF}con este comando podr�s empezar tu jornada de trabajo.\n\
            {00D4FF}/terminar: {FFFFFF}con este comando podr�s terminar tu jornada de trabajo.\n\
            {00D4FF}/tomarempleo: {FFFFFF}con este comando pod�s tomar el puesto de trabajo en caso de no tenerlo.\n\
            {00D4FF}/verempleo: {FFFFFF}observa las estadisticas de tu empleo.\n\
            {00D4FF}/dejarempleo: {FFFFFF}utiliza este comando para renunciar a tu trabajo.\n\
            {00D4FF}/ar: {FFFFFF}radio para comunicarte con otras aeronaves.",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else
    {   Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre el empleo.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu empleo.\n\
            \n \n\
            {FFFFFF}No tienes ning�n empleo, para conseguir uno utiliza /gps y estando en el lugar coloca el comando /tomarempleo.",
            "Cerrar", 
            ""
        );
        return 1;
    }
}

Help_ShowFactions(playerid)
{
    if(PlayerInfo[playerid][pFaction] > 6)
    {   Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre la facci�n.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu facci�n.\n\
            \n \n\
            {F0FF33}COMANDOS GENERALES:\n\
            {FFFFFF}/fac invitar, /fac expulsar, /fac rango, /fac conectados, /fverbalance, /fretirar, /fdepositar.\n\
            {FFFFFF}/f, /r.\n\
            \n \n\
            {F0FF33}COMANDOS EXCLUSIVOS:\n\
            {FFFFFF}/tomarbarrio, /traficar",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else if(PlayerInfo[playerid][pFaction] == FAC_PMA)
    {
        Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre la facci�n.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu facci�n.\n\
            \n \n\
            {F0FF33}COMANDOS:\n\
            {FFFFFF}/fac invitar, /fac expulsar, /fac rango, /fac conectados, /fverbalance, /fretirar, /fdepositar.\n\
            {FFFFFF}/f, /r.\n\
            \n \n\
            {F0FF33}COMANDOS EXCLUSIVOS:\n\
            {FFFFFF}/equipar, /propero, /pservicio, /sosp, (/m)egafono, /arrestar, /esposar, /verpatente,\n\
            {FFFFFF}/quitaresposas, /revisar, /camaras, /quitar, /multar, /premolcar, /arrastrar, /central,\n\
            {FFFFFF}/refuerzos, /ult, /vercargos, /buscados, /localizar, /pipeta, /deposito, /verantecedentes,\n\
            {FFFFFF}/geof, /verregistros, /comprarinsumos, /guardarinsumos, /verinsumos, /pautorizar, /callsign.",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else if(PlayerInfo[playerid][pFaction] == FAC_SIDE)
    {
        Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre la facci�n.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu facci�n.\n\
            \n \n\
            {F0FF33}COMANDOS:\n\
            {FFFFFF}/fac invitar, /fac expulsar, /fac rango, /fac conectados, /fverbalance, /fretirar, /fdepositar.\n\
            {FFFFFF}/f, /r.\n\
            \n \n\
            {F0FF33}COMANDOS EXCLUSIVOS:\n\
            {FFFFFF}/equipar, /gropero, /gservicio, /sosp, (/m)egafono, /arrestar, /esposar, /verpatente,\n\
            {FFFFFF}/quitaresposas, /revisar, /quitar, /multar, /gremolcar, /arrastrar, /central,\n\
            {FFFFFF}/refuerzos, /ult, /vercargos, /buscados, /localizar, /pipeta, /verantecedentes,\n\
            {FFFFFF}/alacran, /verregistros, /comprarinsumos, /guardarinsumos, /verinsumos, /pautorizar, /callsign.",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else if(PlayerInfo[playerid][pFaction] == FAC_HOSP)
    {
        Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre la facci�n.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu facci�n.\n\
            \n \n\
            {F0FF33}COMANDOS:\n\
            {FFFFFF}/fac invitar, /fac expulsar, /fac rango, /fac conectados, /fverbalance, /fretirar, /fdepositar.\n\
            {FFFFFF}/f, /r.\n\
            \n \n\
            {F0FF33}COMANDOS EXCLUSIVOS:\n\
            {FFFFFF}/mservicio, /gobierno, (/d)epartamento, (/ult)imallamada, /curar, /callsign.",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else if(PlayerInfo[playerid][pFaction] == FAC_MECH)
    {
        Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre la facci�n.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu facci�n.\n\
            \n \n\
            {F0FF33}COMANDOS:\n\
            {FFFFFF}/fac invitar, /fac expulsar, /fac rango, /fac conectados, /fverbalance, /fretirar, /fdepositar.\n\
            {FFFFFF}/f, /r.\n\
            \n \n\
            {F0FF33}COMANDOS EXCLUSIVOS:\n\
            {FFFFFF}/mecremolcar, /mecreparar, /mectunear, /mecdestunear, /meccerradura.",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else if(PlayerInfo[playerid][pFaction] == FAC_MAN)
    {
        Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre la facci�n.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu facci�n.\n\
            \n \n\
            {F0FF33}COMANDOS:\n\
            {FFFFFF}/fac invitar, /fac expulsar, /fac rango, /fac conectados, /fverbalance, /fretirar, /fdepositar.\n\
            {FFFFFF}/f, /r.\n\
            \n \n\
            {F0FF33}COMANDOS EXCLUSIVOS:\n\
            {FFFFFF}(/n)oticia, /entrevistar, /pronostico.",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else if(PlayerInfo[playerid][pFaction] == FAC_GOB)
    {
        Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre la facci�n.",
            "En este apartado encontrar�s todo lo que necesitas saber sobre tu facci�n.\n\
            \n \n\
            {F0FF33}COMANDOS:\n\
            {FFFFFF}/fac invitar, /fac expulsar, /fac rango, /fac conectados, /fverbalance, /fretirar, /fdepositar.\n\
            {FFFFFF}/f, /r.\n\
            \n \n\
            {F0FF33}COMANDOS EXCLUSIVOS:\n\
            {FFFFFF}/verconectados, /verpresos, /verantecedentes, /departamento, /liberar, /ppreventiva, /gobierno, /callsign.",
            "Cerrar", 
            ""
        );
        return 1;
    }
    else
    {
        Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
            "[Soporte] Comandos sobre la facci�n.",
            "No formas parte de ninguna facci�n, necesitas estar dentro de una para ver este apartado.",
            "Cerrar", 
            ""
        );
        return 1;
    }
}

Help_ShowVehicles(playerid)
{
    Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
        "[Soporte] Comandos de veh�culos.",
        "En este apartado encontrar�s todo lo que necesitas saber sobre veh�culos.\n\
        \n \n\
        {F0FF33}COMANDOS:\n\
        {FFFFFF}/motor, /vehpuertas, /vehestacionar, /vehluces, /vehvender, /vehvendera,\n\
        {FFFFFF}/vehmal, /mal(etero), /vehcapot, /vent(anilla), /cint(uron), /vercint(uron),\n\
        {FFFFFF}/vehradio, /llavero, /sacar, /carreraayuda.",
        "Cerrar", 
        ""
    );
    return 1;
}

Help_ShowBusiness(playerid)
{
    Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
        "[Soporte] Comandos de negocios.",
        "En este apartado encontrar�s todo lo que necesitas saber sobre negocios.\n\
        \n \n\
        {F0FF33}COMANDOS:\n\
        {FFFFFF}/negociocomprar, /negociovender, /negociovendera, /negociogestionar, /negocionombre,\n\
        {FFFFFF}/negociocaja, /negociollave, /negocioradio, /negociocontratar, /negociodespedir,\n\
        {FFFFFF}/negociorenunciar, /negociotrabajo.",
        "Cerrar", 
        ""
    );
    return 1;
}

Help_ShowHouses(playerid)
{
    Dialog_Open(playerid, "DLG_NO_RESPONSE", DIALOG_STYLE_MSGBOX,
        "[Soporte] Comandos de casas.",
        "En este apartado encontrar�s todo lo que necesitas saber sobre casas.\n\
        \n \n\
        {F0FF33}COMANDOS:\n\
        {FFFFFF}/casacomprar, /casavender, /casavendera, /casaalquilar, /casadesalquilar, /casaradio,\n\
        {FFFFFF}/casallave, /casaenalquiler, /casasinalquiler, /casacontrato, /puerta, /armario,\n\
        {FFFFFF}/casavisitar, /casadomicilio.",
        "Cerrar", 
        ""
    );
    return 1;
}