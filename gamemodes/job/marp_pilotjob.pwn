#if defined _marp_pilotob_included
	#endinput
#endif
#define _marp_pilotjob_included

#include <YSI_Coding\y_hooks>

#define JOB_PILOT_HANGAR_X       1983.21
#define JOB_PILOT_HANGAR_Y       -2399.08
#define JOB_PILOT_HANGAR_Z       13.54

#define MA_LZ_X	1986.86 
#define MA_LZ_Y -2593.56 
#define MA_LZ_Z 13.54 

#define JOB_PILOT_MAX_FLIGHTS (2)
#define JOB_PILOT_PAY_PER_FLIGHT (1200)
#define JOB_PILOT_BONUS_PER_FLIGHTS (1 / JOB_PILOT_MAX_FLIGHTS)
#define JOB_PILOT_BONUS_PER_CHARGE (120 / JOB_PILOT_MAX_FLIGHTS)

enum PilotJob_RouteInfo {
	Float:pjX,
	Float:pjY,
	Float:pjZ,
	pjTime,
	pjAirport[64],
	Float:lzX,
	Float:lzY,
	Float:lzZ
};



static const PilotJob_Route[][PilotJob_RouteInfo] = {
	{
		-1383.19, -212.84, 14.14, 420, "Aeropuerto Internacional de Cordopa",
		-1461.31, 31.41, 14.14  
	},
	{
		1586.89, 1627.56, 10.82, 420, "Aeropuerto Internacional de Mala F�",
		1477.40, 1501.43, 10.82
	},
	{
		-23.26, 2506.47, 16.48, 480, "Aerodromo de San Lucho",
		373.68, 2537.48, 16.57
	},
	{
		299.24, 2016.74, 17.64, 420, "Base A�rea Militar de San Lucho",
		305.73, 1887.36, 17.64
	}
};

stock PilotJob_GetVehicleCharge(vehicleid)
{
    new modelid = GetVehicleModel(vehicleid);
    
 	switch(modelid)
 	{
 	    case 593: return 1; //Dodo
		case 511: return 2; //Beagle
		case 553: return 3; //Nevada
	}
	return 1;
}

PilotJob_StartWork(playerid)
{
	new vehicleid = GetPlayerVehicleID(playerid),
	    destination = random(sizeof(PilotJob_Route)),
	    area[MAX_ZONE_NAME];

    if(Veh_GetJob(vehicleid) != JOB_PILOT)
        return SendClientMessage(playerid, COLOR_YELLOW2, "�Debes estar en un avi�n de la empresa!");
	if(PilotJob_GetVehicleCharge(vehicleid) > PlayerJobInfo[playerid][pCharge])
	    return SendClientMessage(playerid, COLOR_YELLOW2, "�Tu cargo en la empresa no tiene acceso a este avi�n!");
	if(PlayerInfo[playerid][pCantWork] >= JOB_PILOT_MAX_FLIGHTS)
	    return SendClientMessage(playerid, COLOR_YELLOW2, "Ya has alcanzado el m�ximo de vuelos posibles por este d�a de pago.");
    if(Job_IsVehicleWorking(vehicleid))
        return SendClientMessage(playerid, COLOR_YELLOW2, "Ese veh�culo actualmente esta siendo usado para trabajar por otro piloto.");
	if(!Cronometro_Crear(playerid, PilotJob_Route[destination][pjTime] + 7, "PilotJob_TravelTimeLimit"))
	    return SendClientMessage(playerid, COLOR_YELLOW2, "[SCRIPT ERROR]: No se pudo crear el cronometro asociado al trabajo. Reportar a un administrador.");
	    
	GetCoords2DZone(PilotJob_Route[destination][pjX], PilotJob_Route[destination][pjY], area, MAX_ZONE_NAME);
	SetPlayerCheckpoint(playerid, JOB_PILOT_HANGAR_X, JOB_PILOT_HANGAR_Y, JOB_PILOT_HANGAR_Z, 2.0);
	PlayerActionMessage(playerid, 15.0, "ha encendido el motor del veh�culo.");
	SendClientMessage(playerid, COLOR_WHITE, "�Apurate! Rod� al hangar y carg� el avi�n. Vas a tener que volar r�pido y manteniendo una altura mayor a 100 metros para evitar problemas.");
    SendFMessage(playerid, COLOR_WHITE, "Ten�s %d minutos para ir, entregar la carga, y volver al aeropuerto. La entrega es en el %s", (PilotJob_Route[destination][pjTime] + 60)/60, PilotJob_Route[destination][pjAirport]);
    SendClientMessage(playerid, COLOR_WHITE, "Record� que si desgast�s mucho el avi�n o si llegas tarde, tu imagen laboral disminuir�.");
	SendClientMessage(playerid, COLOR_WHITE, "Para comunicarte con otras aeronaves podes usar el comando "COLOR_EMB_ALERT" /ar ");
	jobDuty[playerid] = true;
	SetEngine(vehicleid, 1);
	jobVehicle[playerid] = vehicleid;
	jobCheckpoint[playerid] = 0;
	jobRoute[playerid] = destination;
    GetVehicleHealth(vehicleid, jobVehicleHealth[playerid]);
    UpdatePlayerJobLastWorked(playerid);
    PlayerInfo[playerid][pCantWork]++;
	return 1;
}

J_Pilot_OnPlayerDisconnect(playerid)
{
	if(jobDuty[playerid]==true)
	{
		jobDuty[playerid] = false;
		SetVehicleToRespawn(GetPlayerVehicleID(playerid));
	}
	return 1;
}

forward PilotJob_NextCheckpoint(playerid, cp);
public PilotJob_NextCheckpoint(playerid, cp)
{
    TogglePlayerControllable(playerid, true);
    jobCheckpoint[playerid]++;
	if(cp == 1)
    {
        SendClientMessage(playerid, COLOR_WHITE, "Vol� a la direcci�n indicada y aterriz�.");
        SetPlayerCheckpoint(playerid, PilotJob_Route[jobRoute[playerid]][lzX], PilotJob_Route[jobRoute[playerid]][lzY], PilotJob_Route[jobRoute[playerid]][lzZ], 2.0);
    }
    else if(cp == 2)
    {
        SendClientMessage(playerid, COLOR_WHITE, "Rod� hasta la zona de descarga.");
        SetPlayerCheckpoint(playerid, PilotJob_Route[jobRoute[playerid]][pjX], PilotJob_Route[jobRoute[playerid]][pjY], PilotJob_Route[jobRoute[playerid]][pjZ], 2.0);
    }
	else if(cp == 3)
    {
        SendClientMessage(playerid, COLOR_WHITE, "Volv� al aeropuerto de Malos Aires");
		SetPlayerCheckpoint(playerid, MA_LZ_X, MA_LZ_Y, MA_LZ_Z, 2.0);
    }
    else if(cp == 4)
    {
        SendClientMessage(playerid, COLOR_GREEN, "Estaciona el avion");
		SetPlayerCheckpoint(playerid, VehicleInfo[jobVehicle[playerid]][VehPosX], VehicleInfo[jobVehicle[playerid]][VehPosY], VehicleInfo[jobVehicle[playerid]][VehPosZ], 2.0);
    }
	return 1;
}

PilotJob_IsPlayerWorking(playerid, vehicleid)
{
	if(PlayerInfo[playerid][pJob] == JOB_PILOT && jobDuty[playerid] && VehicleInfo[vehicleid][VehJob] == JOB_PILOT)
	    return 1;
	return 0;
}

/*PilotJob_PlayerDeath(playerid)
{
		jobDuty[playerid] = false;
		SetVehicleToRespawn(GetPlayerVehicleID(playerid));
		return 1;
}*/
	


hook function OnPlayerEnterCPId(playerid, checkpointid)
{
	if(!IsPlayerInAnyVehicle(playerid))
		return continue(playerid, checkpointid);
	if(!PilotJob_IsPlayerWorking(playerid, GetPlayerVehicleID(playerid)))
		return continue(playerid, checkpointid);

	new actualCP = jobCheckpoint[playerid],
	    vehicleid = GetPlayerVehicleID(playerid);
	
	    
	if(actualCP == 0) //Carga
	{
		GameTextForPlayer(playerid, "Cargando el avion...", 4000, 4);
		TogglePlayerControllable(playerid, false);
		SetTimerEx("PilotJob_NextCheckpoint", 4000, false, "ii", playerid, actualCP + 1);
	}
	else if(actualCP == 1) // Aterrizando
	{
	    GameTextForPlayer(playerid, "Rueda hasta la zona de descarga", 4000, 4);
		PilotJob_NextCheckpoint(playerid, actualCP + 1);
	    
	}
	else if(actualCP == 2) // Yendo a descargar
	{
	    GameTextForPlayer(playerid, "Descargando el avion...", 4000, 4);
	    TogglePlayerControllable(playerid, false);
	    SetTimerEx("PilotJob_NextCheckpoint", 4000, false, "ii", playerid, actualCP + 1);
	}
	else if(actualCP == 3) // Aterrizando
	{
	    GameTextForPlayer(playerid, "Estaciona el avion", 4000, 4);
		PilotJob_NextCheckpoint(playerid, actualCP + 1);
	}
	
	else if(actualCP == 4) // Volviendo
	{
 		PlayerJobInfo[playerid][pWorkingHours]++;
		PilotJob_Payment(playerid, vehicleid);
		RemovePlayerFromVehicle(playerid);
  		SetEngine(vehicleid, 0);
		SetVehicleToRespawn(vehicleid);
	    jobDuty[playerid] = false;
	    jobVehicle[playerid] = 0;
	    SetPlayerSkin(playerid, PlayerInfo[playerid][pSkin]);
		PlayerActionMessage(playerid, 15.0, "apaga el motor del avi�n y pone el freno.");
		Cronometro_Borrar(playerid);
	}
	else if(actualCP == 5) 
	{
		jobDuty[playerid] = false;
		SetEngine(jobVehicle[playerid], 0);
		SetVehicleToRespawn(jobVehicle[playerid]);
	 	jobVehicle[playerid] = 0;
	 	SetPlayerSkin(playerid, PlayerInfo[playerid][pSkin]);
	 	Cronometro_Borrar(playerid);
	 	PlayerActionMessage(playerid, 15.0, "apaga el motor del avi�n y pone el freno.");
	}
	else if(actualCP == 6) 
	{
	    PlayerJobInfo[playerid][pWorkingHours]++;
		jobDuty[playerid] = false;
		SetEngine(jobVehicle[playerid], 0);
		SetVehicleToRespawn(jobVehicle[playerid]);
	 	jobVehicle[playerid] = 0;
	 	SetPlayerSkin(playerid, PlayerInfo[playerid][pSkin]);
	 	Cronometro_Borrar(playerid);
		PlayerInfo[playerid][pPayCheck] += JOB_PILOT_PAY_PER_FLIGHT / 2;
		SendFMessage(playerid, COLOR_WHITE, "[INFO]: Has devuelto el avi�n al aeropuerto, y recibes la mitad del pago base (%d) a fin del d�a, sin ning�n tipo de bonus.", JOB_PILOT_PAY_PER_FLIGHT / 2);
	 	PlayerActionMessage(playerid, 15.0, "apaga el motor del avi�n y pone el freno.");
	}
	return 1;
}

forward PilotJob_TravelTimeLimit(playerid);
public PilotJob_TravelTimeLimit(playerid)
{
	Job_GiveReputation(playerid, -6);
	
	//5 minutos para que devuelva el avi�n al aeropuerto
	if(!Cronometro_Crear(playerid, 300, "PilotJob_ReturnTimeLimit")) // Si fall� la creaci�n del cronometro
	{
	    SendClientMessage(playerid, COLOR_YELLOW2, "[SCRIPT ERROR]: No se pudo crear el cron�metro asociado al trabajo. Reportar a un administrador.");
        jobDuty[playerid] = false;
		SetEngine(jobVehicle[playerid], 0);
		SetVehicleToRespawn(jobVehicle[playerid]);
	 	jobVehicle[playerid] = 0;
	 	SetPlayerSkin(playerid, PlayerInfo[playerid][pSkin]);
	}
	else // Se pudo crear el cronometro
	{
		if(jobCheckpoint[playerid] == 0 || jobCheckpoint[playerid] == 1 || jobCheckpoint[playerid] == 2) // No lleg� ni a entregarlo a destino
		{
			SendClientMessage(playerid, COLOR_WHITE, "Te has pasado del tiempo l�mite: tu jefe se ha enojado y adem�s no recibir�s ninguna paga por fallar la entrega.");
            SendClientMessage(playerid, COLOR_WHITE, "Ten�s cinco minutos para devolver el avi�n al aeropuerto. Si no lo haces, tu reputaci�n empeorar� a�n m�s.");
			jobCheckpoint[playerid] = 3;
		}
		else if(jobCheckpoint[playerid] == 3) // Ya estaba volviendo para la central
		{
			SendClientMessage(playerid, COLOR_WHITE, "Te has pasado del tiempo l�mite: tu jefe se ha enojado y solo recibir�s la mitad de la paga.");
	    	SendClientMessage(playerid, COLOR_WHITE, "Ten�s un minuto para devolver el avi�n al aeropuerto. Si no lo haces, no tendr�s paga alguna.");
            jobCheckpoint[playerid] = 4;
		}
	    DisablePlayerCheckpoint(playerid);
	    SetPlayerCheckpoint(playerid, VehicleInfo[jobVehicle[playerid]][VehPosX], VehicleInfo[jobVehicle[playerid]][VehPosY], VehicleInfo[jobVehicle[playerid]][VehPosZ], 2.0);
	}
	return 1;
}

forward PilotJob_ReturnTimeLimit(playerid);
public PilotJob_ReturnTimeLimit(playerid)
{
    jobDuty[playerid] = false;
	SetEngine(jobVehicle[playerid], 0);
	SetVehicleToRespawn(jobVehicle[playerid]);
 	jobVehicle[playerid] = 0;
 	SetPlayerSkin(playerid, PlayerInfo[playerid][pSkin]);
 	Job_GiveReputation(playerid, -4);
 	SendClientMessage(playerid, COLOR_WHITE, "[INFO]: No has logrado devolver el avi�n al aeropuerto en el tiempo estipulado. Tu reputaci�n baja unos puntos");
 	return 1;
}
 	
PilotJob_Payment(playerid, vehicleid)
{
	new paycheck, Float:vHealth, injuriesCost, seniorityBonus, chargeBonus;

    GetVehicleHealth(vehicleid, vHealth);
    if(vHealth + 30.0 < jobVehicleHealth[playerid])
    {
        Job_GiveReputation(playerid, -(floatround(jobVehicleHealth[playerid] - vHealth, floatround_ceil) / 10)); // La reputaci�n que baja es proporcional al da�o del veh�culo (un d�cimo)
        injuriesCost = floatround(jobVehicleHealth[playerid] - vHealth, floatround_ceil) / 2; // Se le paga x$$$ menos correspondientes a la diferencia de vida en el veh�culo / 2.
		SendClientMessage(playerid, COLOR_WHITE, "Tu jefe se enoj� con vos ya que desgastaste mucho al avi�n.");
	}
	else
	    Job_GiveReputation(playerid, 1);

	seniorityBonus = PlayerJobInfo[playerid][pWorkingHours] * JOB_PILOT_BONUS_PER_FLIGHTS; // Extra por antiguedad
	chargeBonus = (PlayerJobInfo[playerid][pCharge] - 1) * JOB_PILOT_BONUS_PER_CHARGE; // Extra por cargo

	paycheck = JOB_PILOT_PAY_PER_FLIGHT + seniorityBonus + chargeBonus - injuriesCost;
	PlayerInfo[playerid][pPayCheck] += paycheck;
	PlayerJobInfo[playerid][pTotalEarnings] += paycheck;

	SendFMessage(playerid, COLOR_WHITE, "�Genio! finalizaste la entrega a tiempo y recibir�s $%d adicionales en el pr�ximo payday. Puedes hacer %d vuelo/s m�s el d�a de hoy.", paycheck, JOB_PILOT_MAX_FLIGHTS - PlayerInfo[playerid][pCantWork]);
	SendFMessage(playerid, COLOR_WHITE, "[Detalles]: Pago base por entrega: $%d - Bonus por antig�edad: $%d - Bonus por cargo: $%d - Descuento por roturas: ${FF0000}%d", JOB_PILOT_PAY_PER_FLIGHT, seniorityBonus, chargeBonus, injuriesCost);

	PilotJob_CheckPromotePlayer(playerid);
	Job_CheckReputationLimit(playerid);
}

PilotJob_CheckPromotePlayer(playerid)
{
	if(400 * PlayerJobInfo[playerid][pCharge] <= PlayerJobInfo[playerid][pWorkingHours] && PlayerJobInfo[playerid][pReputation] >= 0 && PlayerJobInfo[playerid][pCharge] < 5) // Minimo de X horas trabajadas (proporcional al siguiente puesto) y de una im�gen laboral neutral.
	{
	    if(PlayerJobInfo[playerid][pWorkingHours] + PlayerJobInfo[playerid][pReputation] / 5 >= 500 * PlayerJobInfo[playerid][pCharge]) // Para ascender hay que llegar a una cantidad de puntos. Nuestro puntaje se suma entre la reputacion y la antiguedad.
	    {
	        PlayerJobInfo[playerid][pCharge]++;
	        SendFMessage(playerid, COLOR_WHITE, "�Felicitaciones! fuiste promovido a %s, lo que te garantiza una mejora en tu sueldo. �Buen trabajo! Segu� as�.", GetJobChargeName(JOB_PILOT, PlayerJobInfo[playerid][pCharge]));
		}
	}
}
