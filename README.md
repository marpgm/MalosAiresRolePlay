# Malos Aires RolePlay
 
GameMode de tipo RolePlay para San Andreas Multiplayer inspirado en la ciudad de Buenos Aires.
 
## Instalacion y ejecucion del GM para desarrollo
 
Para ejecutar la GM se requiere de:
 
* Un servidor MYSQL.
* [sampctl](https://github.com/Southclaws/sampctl/wiki).
 
Dentro de la carpeta `resources/database` se encuentra una copia de la base de datos de pruebas. Además en la carpeta `resources/scriptfiles/database` se encuentra el archivo `db.cfg` el cual debe colocarse en `scriptfiles/database`. 
 
Como extra, dentro de la carpeta `resources/plugins` se encuentran los plugins que deben copiarse a la carpeta `plugins`.
 
Para poder ejecutar el gamemode se debe utilizar los siguientes comandos en el siguiente orden:

* `sampctl p ensure` - este descargara todas las dependencias necesarias de la gamemode.
* `sampctl p build`- compilara la gamemode.
* `sampctl p run test` - correra el servidor en modo test server en el puerto 7779.

